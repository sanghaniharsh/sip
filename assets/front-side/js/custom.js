$(document).ready(function() {
    
    // $('#delivery_form .submit_btn').on('click', function(e){
    //     e.preventDefault();
    //     $('#delivery_form .ajax_loader').removeClass('hide');
    //     $('body').addClass('page-loader');
    // });

//    setTimeout(function(){
//        if ($('#welcomeModal').length > 0) {
//            var current_storage = localStorage.getItem('welcome_popup');
//            var current         = new Date();
//            if ((current_storage == null) || (current.getTime() > current_storage)) {
//                var date    = new Date();
//                date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
//                localStorage.setItem('welcome_popup', date.getTime());
//                $('#welcomeModal').modal('show');
//            }            
//        }
//    },500);

    if ($('.reload_captcha').length > 0) {
        $('.reload_captcha').on('click', function(){
            var randomString = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
            $(this).parents('form').find("#captcha_code").attr('src', base_url+'secureimage/securimage?'+randomString);
        });
    }

    if ($('.do_login_class').length > 0) {
        $('.do_login_class').on('click', function(e){
            e.preventDefault();
            $('#loginModal').modal('show');
        });
    }

    if ($('.forgot_password').length > 0) {
        $('.forgot_password').on('click', function(e){
            e.preventDefault();
            $('#loginModal').modal('hide');
            $('#forgotPasswordModal').modal('show');
        });
    }
    
    if ($('.register_class').length > 0) {
        $('.register_class').on('click', function(e){
            e.preventDefault();
            $('#registerModal').modal('show');
        });
    }

    if ($('#loginForm').length > 0) {
        $('#loginForm').validate({
            rules: {
                email: {
                    required: true,
                    email : true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email : {
                    required : "Please enter email address",
                    email : "Please enter valid email address"
                },
                password: {
                    required: "Please enter password"
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });
    }

    if ($('#registerForm').length > 0) {
        $('#registerForm').validate({
            rules: {
                firstname: {
                    required    : true
                },
                lastname: {
                    required    : true
                },
                email: {
                    required    : true,
                    email       : true,
                    remote      : {
                                    type    : "POST",
                                    url     : base_url+'auth/check_user_exists'
                                }
                },
                phone: {
                    required    : true,
                    number      : true,
                    minlength   : 10,
                    maxlength   : 10
                },
                password: {
                    required    : true
                },
                cpassword: {
                    required    : true,
                    equalTo     : "#password",
                },
                register_captcha: {
                    required    : true,
                    remote      : {
                                    url: base_url+'secureimage/check_captcha',
                                    type: "POST",
                                    data: {
                                        captcha: function()
                                        {
                                            return $('#registerForm :input[name="register_captcha"]').val();
                                        }
                                    }
                                }
                },
                terms: {
                    required    : true
                }
            },
            messages: {
                firstname: {
                    required    : "Please enter first name"
                },
                lastname: {
                    required    : "Please enter first name"
                },
                email : {
                    required    : "Please enter email address",
                    email       : "Please enter valid email address",
                    remote      : "Email already register"
                },
                phone: {
                    required    : "Please enter phone number",
                    number      : "Please enter valid phone number",
                    minlength   : "Please enter valid phone number",
                    maxlength   : "Please enter valid phone number"
                },
                password: {
                    required    : "Please enter password"
                },
                cpassword: {
                    required    : "Please confirm password",
                    equalTo     : "Password not match"
                },
                register_captcha: {
                    required    : "Please enter captcha",
                    remote      : "Wrong captcha code"
                },
                terms: {
                    required    : "Please accept terms and conditions"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "terms" ) {
                    $('.checkbox-inline').after(error);  
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }
    if ($('#forgot_password_form').length > 0) {
        $('#forgot_password_form').validate({
            rules: {
                email: {
                    required    : true,
                    email       : true,
                    remote      : {
                                    type    : "POST",
                                    url     : base_url+'auth/check_user_register'
                                }
                }
            },
            messages: {
                email : {
                    required    : "Please enter email address",
                    email       : "Please enter valid email address",
                    remote      : "Account is not exist with given email address"
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });
    }

     $(document).on('hide.bs.modal','#registerModal', function () {
        $('#registerModal input[name="firstname"]').val('');
        $('#registerModal input[name="lastname"]').val('');
        $('#registerModal input[name="email"]').val('');
        $('#registerModal input[name="phone"]').val('');
        $('#registerModal input[name="password"]').val('');
        $('#registerModal input[name="cpassword"]').val('');
        $('#registerModal input[name="register_captcha"]').val('');
        $('#registerModal [name="terms"]').removeAttr('checked');
        $('.reload_captcha').trigger("click");
    });

    $('#forgot_btn').on('click', function(e){
        e.preventDefault();
        if ($('#forgot_password_form').valid()) {
            $.ajax({
                type    : "POST",
                url     : base_url+'/auth/forgot_password',
                data    : $('#forgot_password_form').serialize(),
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('#forgotPasswordModal input[name="email"]').val('');
                        $('#forgotPasswordModal').modal('hide');
                        showMessage('success', response.message);
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        }
        return false;
    });
    $('#registerForm').on('submit', function(e){
        e.preventDefault();
        if ($('#registerForm').valid()) {
            $.ajax({
                type    : "POST",
                url     : base_url+'/auth/register',
                data    : $('#registerForm').serialize(),
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('#registerModal').modal('hide');
                        showMessage('success', response.message);
                        
                        setTimeout(function(){
                            window.location.reload();
                        }, 2000);
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        }
        return false;
    });

    $('.search_product').on('keyup', function(e) {
        var p_name = $(this).val();
        if ( p_name != "" && p_name.length > 2 ) {
            if (e.which == 13) {
                window.location.href = base_url + "search?q="+p_name;
            } else {
                $.ajax({
                    type    : "POST",
                    url     : base_url + "home/search_product",
                    data    : { search: p_name },
                    success : function(response){
                        var response = $.parseJSON(response);
                        if (response.status == true) {
                            $('.product_search_result').html(response.html);
                        } else {
                            $('.product_search_result').html('');
                        }
                    }
                });
            }
        } else {
            $('.search_product').html('');
            $('.product_search_result').html('');
        }
    });

    $(document).on('click', function(e) {
        if ($(e.target).closest('.search_product').length == 0) {
            $('.product_search_result').html('');  
            $('.product_search_result').html('');  
        }

        if ($(e.target).closest('.search-zipcode').length == 0) {
            $('.location_result').html('');  
            $('.location_result').html('');  
        }

        if ($(e.target).closest('.shipping-search-zipcode').length == 0) {
            $('.shipping_location_result').html('');  
            $('.shipping_location_result').html('');  
        }
    });

    $(document).on('keyup', '.search-zipcode', function(e) {
        e.preventDefault();
        var $this   = $(this);
        setTimeout(function() {
            var b_name  = $this.val();
            if( b_name != "" ) {
                $.ajax({
                    type    : "POST",
                    url     : base_url + "home/search_location",
                    data    : {
                        search: b_name
                    },
                    success : function(response){
                        var response = $.parseJSON(response);
                        if (response.status == true) {
                            $('.location_result').html(response.html); 
                        } else {
                            $('.location_result').html('');
                        }
                    }
                });
            } else {
                $('.location_result').html('');
            }
        }, 800);
    });

    $('.shipping-search-zipcode').on('keyup', function(e) {
        e.preventDefault();
        var $this   = $(this);
        setTimeout(function() {
            var b_name  = $this.val();
            if( b_name != "" ) {
                $.ajax({
                    type    : "POST",
                    url     : base_url + "home/search_location",
                    data    : {
                        search: b_name
                    },
                    success : function(response){
                        var response = $.parseJSON(response);
                        if (response.status == true) {
                            $('.shipping_location_result').html(response.html); 
                        } else {
                            $('.shipping_location_result').html('');
                        }
                    }
                });
            } else {
                $('.shipping_location_result').html('');
            }
        }, 800);
    });

    $(document).on('click', '.location_result .listing-li', function(e) {
        e.preventDefault();
        var city_state  = $(this).attr('data-rel');
        var zipcode     = $(this).attr('data-slug');
        
        $('.search-zipcode').val(zipcode);
        $('.city_state').val(city_state);
        $('.search-zipcode').focus();
        $('.location_result').html('');
    });

    $(document).on('click', '.shipping_location_result .listing-li', function(e) {
        e.preventDefault();
        var city_state  = $(this).attr('data-rel');
        var zipcode     = $(this).attr('data-slug');
        
        $('.shipping-search-zipcode').val(zipcode);
        $('.shipping-search-zipcode').trigger("change");
        $('.shipping_city_state').val(city_state);
        $('.shipping-search-zipcode').focus();
        $('.shipping_location_result').html('');
    });

});

function showMessage(type, message) {
    toastr.options = {
        closeButton: true,
        debug: true,
        progressBar: false,
        positionClass: 'toast-top-center',
        onclick: null
    };
    
    if (type == 'success') {
        toastr.success(message, 'Success', {timeOut: 5000})
    }
    if (type == 'danger') {
        toastr.error(message, 'Error', {timeOut: 5000})
    }
}
