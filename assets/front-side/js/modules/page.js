$(document).ready(function() {
    $('#contactForm').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email : true
                },
                phone: {
                    required    : true,
                    number      : true,
                    minlength   : 10,
                    maxlength   : 10
                },
                message: {
                    required: true
                },
                register_captcha: {
                    required    : true,
                    remote      : {
                                    url: base_url+'secureimage/check_captcha',
                                    type: "POST",
                                    data: {
                                        captcha: function()
                                        {
                                            return $('#contactForm :input[name="register_captcha"]').val();
                                        }
                                    }
                                }
                }
            },
            messages: {
                name : {
                    required : "Please enter name"
                },
                email : {
                    required : "Please enter email address",
                    email : "Please enter valid email address"
                },
                phone: {
                    required    : "Please enter phone number",
                    number      : "Please enter valid phone number",
                    minlength   : "Please enter valid phone number",
                    maxlength   : "Please enter valid phone number"
                },
                message : {
                    required : "Please enter message"
                },
                register_captcha: {
                    required    : "Please enter captcha",
                    remote      : "Wrong captcha code"
                }
            },
            errorPlacement: function(error, element) {
                console.log('sdsdsd');
                console.log(element);
                error.insertAfter(element);
            }
        });
    $('#delivery_form .submit_btn').on('click', function(e){
        e.preventDefault();
        $(".custom_image_validation2").addClass("hide");
        
        $('#delivery_form .custom_images_content').html("");
        $( "#ssi-previewBox .ssi-imgToUpload" ).each(function( index ) {
            $('#delivery_form .custom_images_content').append('<input type="hidden" name="images[]" value="'+ $( this ).attr("src") +'" id="custom_images" >');
        });
        $("#delivery_form").submit();
        
        
        
        // $("#delivery_form").validate();
        //     if($('#delivery_form').valid()){
        //         console.log('heres');
        //         $('#delivery_form .ajax_loader').removeClass('hide');
        //         $('body').addClass('page-loader');
        //         //return true;
        //     }
        
    });
    $('#delivery_form').validate({
        rules: {
            "infile[]"      : {
                maximage    :true
            }, 
            register_captcha: {
                required    : true,
                remote      : {
                                url: base_url+'secureimage/check_captcha',
                                type: "POST",
                                data: {
                                    captcha: function()
                                    {
                                        return $('#delivery_form :input[name="register_captcha"]').val();
                                    }
                                }
                            }
            }
        },
        messages: {
            register_captcha: {
                required    : "Please enter captcha",
                remote      : "Wrong captcha code"
            }
        },
        submitHandler: function() { 
             $('#delivery_form .ajax_loader').removeClass('hide');
             $('body').addClass('page-loader');
             return true;
            
        }
    });
    

    if ($('#infile').length > 0) {
        $('#infile').ssi_uploader({
            inForm:true,
            dropZone: false,
            maxFileSize: 5,
            maxNumberOfFiles: 4,
            // The method that will be used to display the messages.
            errorHandler: {
                method: function (msg) {
                    $(".custom_image_validation").addClass("hide");
                    if(msg != "") {
                        $(".custom_image_validation").removeClass("hide");
                    }
                },
                success: 'success',
                error: 'error'
            }
        });
    }

    $.validator.addMethod("maximage", function(value, element) {
        var img_size = 0;
        
        if(element.files.length > 4) {           
            return false;
        } else {
            $.each(element.files, function( key, value ) {
                console.log(this.size);
                img_size += this.size;
            });
            if(img_size >= 8000000) {
                return false;
            } else {
                return true;
            }
       }
    }, 'Please select only 4 image &  Total image size 8MB');
});