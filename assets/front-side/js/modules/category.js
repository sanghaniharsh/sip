$(document).ready(function() {
    setTimeout(function() {
        loadCategories();
    }, 1000);

    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        pageURL = $(this).attr('href');
        loadCategories();
    });
});

function loadCategories() {
    if ($('.category_block').length > 0) {
        $.ajax({
           type :"POST",
           url  : pageURL,
           success: function(data) {   
              var responsedata  = $.parseJSON(data);
              $('.category_block').html(responsedata.category_html);
           }
        });        
    }    
}