var pageURL = base_url+'home/index/';
$(document).ready(function() {
    
    $('#FoodTruckModal').modal('show');
    
    setTimeout(function() {
        loadCategory();
    }, 1000);
    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        pageURL = $(this).attr('href');
        loadCategory();
    });

    $(".my_order_tab").on('click', function(e){
        e.preventDefault();
        loadOrders();
    });

    if ($('#user_profile_form').length > 0) {
        $('#user_profile_form').validate({
            rules: {
                firstname: {
                    required    : true
                },
                lastname: {
                    required    : true
                },
                email: {
                    required    : true,
                    email       : true,
                    remote      : {
                                    type    : "POST",
                                    url     : base_url+'home/check_email_exists',
                                    data    : {
                                        user_id: function()
                                        {
                                            return $('#user_profile_form :input[name="user_id"]').val();
                                        }
                                    }
                                }
                },
                phone: {
                    required    : true,
                    number      : true
                },
                cpassword: {
                    equalTo     : "#password",
                }
            },
            messages: {
                firstname: {
                    required    : "Please enter first name"
                },
                lastname: {
                    required    : "Please enter first name"
                },
                email : {
                    required    : "Please enter email address",
                    email       : "Please enter valid email address",
                    remote      : "Email already register"
                },
                phone: {
                    required    : "Please enter phone number",
                    number      : "Please enter valid phone number"
                },
                cpassword: {
                    equalTo     : "Password not match"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "terms" ) {
                    $('.checkbox-inline').after(error);  
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#user_profile_form button[type="submit"]').on('click', function(e){
            e.preventDefault();
            if ($('#user_profile_form').valid()) {
                $.ajax({
                    type    : "POST",
                    url     : base_url+'/home/myaccount',
                    data    : $('#user_profile_form').serialize(),
                    async   : true,
                    success : function(response){
                        response    = $.parseJSON(response);
                        if (response.status == true) {
                            showMessage('success', response.message);
                        } else {
                            showMessage('danger', response.message);
                        }
                    }
                });
            }
            return false;
        });
    }
    
    jQueryTabs();

    $(".tabs li a").each(function() {
        var t = $(this).attr("href"),
            i = $(this).html();
        $(t + " .tab-title").prepend("<p><strong>" + i + "</strong></p>")

    })
});

$(window).resize(function() {
    jQueryTabs()
});


function loadOrders() {
    if ($('#user_order_table').length > 0) {
        $('#user_order_table').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": base_url + '/home/get_user_orders',
                "type": "POST",
                "data": function() {
                    
                }
            },
            "order": [[1, 'desc']],
            "columns": [
                { "data": "no", "bSortable": false },
                { "data": "date" },
                { "data": "total_item" },
                { "data": "total_amount" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}

function loadCategory() {
    $.ajax({
       type :"POST",
       url  : pageURL,
       success: function(data) {   
          var responsedata  = $.parseJSON(data);
          $('.category_block').html(responsedata.category_html);
       }
    });
}
function jQueryTabs() {
    console.log('here')
    $(".tabs").each(function() {
        $(".tabs-panel").not(":first").hide(), $("li", this).removeClass("active"), $("li:first-child", this).addClass("active"), $(".tabs-panel:first-child").show(), $("li", this).on('click', function(t) {
            var i = $("a", this).attr("href");
            $(this).siblings().removeClass("active"), $(this).addClass("active"), $(i).siblings().hide(), $(i).fadeIn(400), t.preventDefault()
        }), $(window).width() < 100 && $(".tabs-panel").show()
    })
}