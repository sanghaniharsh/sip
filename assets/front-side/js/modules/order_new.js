$(document).ready(function() {

    $(document).on('click', '.delete_cart_item', function(e){
        var result = confirm("Are you sure you want to delete this item ? ");

        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var id          = $this.data('id');
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/cart_delete_item',
                data    : { id : id },
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);

                    if (response.status == true) {
                        showMessage('success', response.message);
                        cart_detail();
                        cart_count();
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('change', '.quantity_change', function(e){
        alert("i am in new file");

        e.preventDefault();
        var $this       = $(this);
        var id          = $this.data('id');
        var qty          = $this.val();

        $.ajax({
            type    : "POST",
            url     : base_url+'/order/cart_update_qty',
            data    : { id : id , quantity : qty},
            async   : true,
            success : function(response){
                response    = $.parseJSON(response);

                if (response.status == true) {
                    cart_detail();
                } else {
                    showMessage('danger', response.message);
                }
            }
        });
    });
    
    $(document).on('click', '.promotional_code_btn', function(e){
        e.preventDefault();
        var $this   = $(this);
        var p_code    = $('.discount_code_txt').val();

        $.ajax({
            type    : "POST",
            url     : base_url+'/order/check_promotional_code',
            data    : { promotional_code : p_code },
            async   : true,
            success : function(response){
                response    = $.parseJSON(response);
            }
        });  
    });

    $('.checkoutForm').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                remote : {
                    url: base_url+'order_new/validate',
                    type: "POST",
                    async:false,
                    data: {
                            email: function()
                            {
                                return $('.checkoutForm :input[name="email"]').val();
                            }
                        }
                    }
            },
            phone: {
                required: true,
                number: true
            },
            address: {
                required: true
            },
            zipcode: {
                required: true
            },
            terms: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter your first name"
            },
            last_name: {
                required: "Please enter your last name"
            },
            email: {
                required: "Please enter your email",
                remote: "This email already exitst"
            },
            phone: {
                required: "Please enter your first name",
                number: "Please enter only numerical value"
            },
            address: {
                required: "Please enter your address"
            },
            zipcode: {
                required: "Please enter your zipcode"
            },
            terms: {
                required: "Please accept terms and condition to complete your payment"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "terms" ) {
                $('.checkbox-inline').after(error);  
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $('.search-zipcode').on('keyup', function(e) {
        e.preventDefault();
        var $this   = $(this);
        setTimeout(function() {
            var b_name  = $this.val();
            if( b_name != "" ) {
                $.ajax({
                    type    : "POST",
                    url     : base_url + "home/search_location",
                    data    : {
                        search: b_name
                    },
                    success : function(response){
                        var response = $.parseJSON(response);
                        if (response.status == true) {
                            $('.location_result').html(response.html);
                        } else {
                            $('.location_result').html('');
                        }
                    }
                });
            } else {
                $('.location_result').html('');
            }
        }, 800);
    });

    $(document).on('click', '.location_result .listing-li', function(e) {
        e.preventDefault();
        var city_state  = $(this).attr('data-rel');
        var zipcode     = $(this).attr('data-slug');

        $('.search-zipcode').val(zipcode);
        $('.city_state').val(city_state);
        $('.location_result').html('');
    });
});

function cart_detail() {
    if($('.cart_block').length > 0) {

        $.ajax({
            type : "POST",   
            url  : base_url+'order/cart',
            success: function(data) {   
                response    = $.parseJSON(data);
                $('.cart_block').html(response.html);
            }
        });
    }
}
