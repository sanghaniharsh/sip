$(document).ready(function() {

    $('#same_address').on('change',function() {
        if($(this).is(':checked')) {
            $('.checkoutForm input[name=address]').val($('.checkoutForm input[name=shipping_address]').val());
            $('.checkoutForm input[name=zipcode]').val($('.checkoutForm input[name=shipping_zipcode]').val());
            $('.checkoutForm input[name=city]').val($('.checkoutForm input[name=shipping_city]').val());
        }
    });

    $(document).on('click', '.delete_cart_item', function(e){
        var result = confirm("Are you sure you want to delete this item ? ");

        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var id          = $this.data('id');
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/cart_delete_item',
                data    : { id : id },
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);

                    if (response.status == true) {
                        showMessage('success', response.message);
                        if (response.cart_count != "") {
                            $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                            $('.total_items').html(response.cart_count);
                        } else {
                            $('.cart_total_item').html("");
                        }
                        cart_detail();
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('change', '.quantity_change', function(e){
        e.preventDefault();
        var $this           = $(this);
        var id              = $this.data('id');
        var qty             = $this.val();
        
        if(qty > 0) {
            var product_slug    = $this.data('rel');
            var ground          = $this.data('price');
            //alert(id + "  ----- " + qty + " ------ " + product_slug);
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/cart_update_qty',
                data    : { id : id , quantity : qty,ground:ground},
                success : function(response){

                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('.promotional_appliedbox').addClass('hide');
                        $('.promotionalbox').removeClass('hide');
                        $('.discountAmount').addClass('hide');
                        $('.discount_code_txt').val('');
                        $('.total_amount').text(response.full_total);
                        $('.total_tax').text("$"+response.tax);
                        $('.sub_total_text').text("$"+response.sub_total);
                        $(".total_price_"+product_slug).html("$"+response.total_price);
                        $('.total_shipping_amount').html(response.total_shipping_price);
                        // $('.total_shipping_amount').attr('data-amount', response.shipping);
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            $this.val(1);
            showMessage('danger', 'Quantity must be more than 0');
            return false;
        }
        
    });

    $(document).on('click', '.promotional_code_btn', function(e){
        e.preventDefault();
        var $this       = $(this);
        var p_code      = $this.parents('.discount_block').find('.discount_code_txt').val();
        var device_Type = $this.parents('.discount_block').attr('data-device'); 

        $.ajax({
            type    : "POST",
            url     : base_url+'/order/check_promotional_code',
            data    : { promotional_code : p_code, device : device_Type},
            async   : true,
            success : function(response){
                response    = $.parseJSON(response);
                if (response.status == true) {
                    $('.discount_block').html(response.html);
                } else {
                    showMessage('danger', response.message);
                }   
            }
        });  
    });
    $(document).on('click', '.calculate_shipping_btn', function(e){

	
	
        e.preventDefault();
        var $this   = $(this);
        var qty     = $('.quantity_change').val();
        var zipcode = $this.parents('.shipping_calculator').find('input[name="shipping_zipcoe"]').val();
        var city = $this.parents('.shipping_calculator').find('input[name="shipping_city"]').val();
        var state = $this.parents('.shipping_calculator').find('input[name="shipping_state"]').val();
	var isResidential = $( "#isResidential" ).val();;
 	
	//alert(isResidential);
        //alert(zipcode + " -- " + city + " -- " + state);

        if (zipcode != '') {
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/shipping_price',
                data    : { zipcode : zipcode, city:city, state:state,isResidential:isResidential},
                async   : true,
                success : function(response){
                    //console.log(response);
                    response    = $.parseJSON(response);
                    // alert(" ------------- ?> " + response.subtotal);
                    if (response.status == true) {
                        var total_shipping = 0;
                        $('.shipping.price').each(function(){
                            if ($(this).find('.in_shipping_total').length > 0) {
                                total_shipping += parseFloat($(this).find('.in_shipping_total').attr('data-amount'));
                            }
                        });

                        total_shipping += parseFloat(response.subtotal);

                        $('.total_shipping_amount').html((parseFloat(total_shipping)*parseFloat(qty)).toFixed(2));
                        $('.total_shipping_amount').attr('data-amount', total_shipping.toFixed(2));
                        $('.total_tax').html('$'+response.tax);
                        $('.total_tax').attr('data-amount', response.tax);

                        calculate_total();
                    } else {
                        showMessage('danger', response.message);
                    } 
                    // if (response.status == true) {
                    //     var total_shipping = 0;
                    //     $('.shipping.price').each(function(){
                    //         if ($(this).find('.in_shipping_total').length > 0) {
                    //             total_shipping += parseFloat($(this).find('.in_shipping_total').attr('data-amount'));
                    //         }
                    //     });

                    //     total_shipping += parseFloat(response.subtotal);

                    //     $('.total_shipping_amount').html((parseFloat(total_shipping)*parseFloat(qty)).toFixed(2));
                    //     $('.total_shipping_amount').attr('data-amount', total_shipping.toFixed(2));
                    //     $('.total_tax').html('$'+response.tax);
                    //     $('.total_tax').attr('data-amount', response.tax);

                    //     calculate_total();
                    //     $('#shipping-data').html(response.shipping[0]);
                    //     // alert(convert(response.shipping))
                    //     $('#shipping-data').html('<tr><td>'+response.shipping[0]+'</td><td>'+convert(response.shipping[2])+'</td><td><input type="button" class="btn btn-warning" id="shipping_value" value='+response.shipping[1]+'></td></tr>')
                    // } else {
                    //     showMessage('danger', response.message);
                    // }   
                }
            });  
        } else {
            showMessage('danger', "Enter shipping zipcode");
        }
    });

    function convert(d){
        // date = new Date(datetime);
        // year = date.getFullYear();
        // month = date.getMonth()+1;
        // dt = date.getDate();

        // if (dt < 10) {
        //   dt = '0' + dt;
        // }
        // if (month < 10) {
        //   month = '0' + month;
        // }

        return d.toString().substr(0, 10);
    }

    $(document).on('click', '#shipping_value', function(e){
        e.preventDefault();
        var shipping_price   = $(this).val();
        var qty     = $('.quantity_change').val();
        // var city = $this.parents('.shipping_calculator').find('input[name="shipping_city"]').val();
        // var state = $this.parents('.shipping_calculator').find('input[name="shipping_state"]').val();
        // alert(shipping_price);
        if (shipping_price != '') {
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/check_shipping_price',
                data    : { shipping_price : shipping_price},
                async   : true,
                success : function(response){
                    console.log(response);
                    response    = $.parseJSON(response);
                    // alert(" ------------- ?> " + response.subtotal);
                    if (response.status == true) {
                        var total_shipping = 0;
                        $('.shipping.price').each(function(){
                            if ($(this).find('.in_shipping_total').length > 0) {
                                total_shipping += parseFloat($(this).find('.in_shipping_total').attr('data-amount'));
                            }
                        });

                        total_shipping += parseFloat(response.subtotal);

                        $('.total_shipping_amount').html((parseFloat(total_shipping)*parseFloat(qty)).toFixed(2));
                        $('.total_shipping_amount').attr('data-amount', total_shipping.toFixed(2));
                        $('.total_tax').html('$'+response.tax);
                        $('.total_tax').attr('data-amount', response.tax);

                        calculate_total();
                    } else {
                        showMessage('danger', response.message);
                    }   
                }
            });  
        } else {
            showMessage('danger', "Enter shipping zipcode");
        }
    });

    $(document).on('click', '.remove_promotional_code', function(e){
        e.preventDefault();
        cart_detail();
    });

    $('.checkoutForm').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            password: {
                required: true
            },
            cpassword: {
                equalTo: "#password"
            },
            email: {
                required: true,
                email   : true,
                remote  : {
                            type    : "POST",
                            url     : base_url+'order/check_email_exists'
                        }
            },
            phone: {
                required: true,
                number: true,
                minlength:10,
                maxlength:10,
            },
            address: {
                required: true
            },
            zipcode: {
                required: true,
                number: true,
                remote : {
                    url     : base_url+'order/check_zipcode',
                    type    : "POST",
                    async   : false,
                    data    : {
                        zipcode: function()
                        {
                            return $('.checkoutForm :input[name="zipcode"]').val();
                        }
                    }    
                }
            },
            shipping_address: {
                required: true
            },
            shipping_zipcode: {
                required: true,
                number: true,
                remote : {
                    url     : base_url+'order/check_zipcode',
                    type    : "POST",
                    async   : false,
                    data    : {
                        zipcode: function()
                        {
                            return $('.checkoutForm :input[name="shipping_zipcode"]').val();
                        }
                    }    
                }
            },
            checkout_captcha: {
                required: true,
                remote : {
                    url: base_url+'secureimage/check_captcha',
                    type: "POST",
                    data: {
                        captcha: function()
                        {
                            return $('.checkoutForm :input[name="checkout_captcha"]').val();
                        }
                    }
                }
            },
            terms: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter your first name"
            },
            last_name: {
                required: "Please enter your last name"
            },
            email: {
                required: "Please enter your email",
                remote  : "Email already registered"
            },
            password: {
                required: "Please enter your password",
            },
            cpassword: {
                equalTo: "Password is not match",
            },
            phone: {
                required: "Please enter your first name",
                number: "Please enter only numerical value",
                minlength:"Please put 10  digit mobile number",
                maxlength:"Please put 10  digit mobile number"
            },
            address: {
                required: "Please enter your address"
            },
            zipcode: {
                required: "Please enter your zipcode",
                remote  : "Please enter valid zipcode"
            },
            shipping_address: {
                required: "Please enter your shipping address"
            },
            shipping_zipcode: {
                required: "Please enter your shipping zipcode",
                remote  : "Please enter valid zipcode"
            },
            terms: {
                required: "Please accept terms and condition to complete your payment"
            },
            checkout_captcha: {
                required : "Please enter captcha",
                remote : "Wrong captcha code"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "terms" ) {
                $('.checkbox_error').html(error);  
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#checkoutForm').on('submit', function(e){
        if ($('#checkoutForm').valid()) {
            return true;
        } else {
            return false;
        }
    });

    $(document).on('change', '.shipping-search-zipcode, select[name="address_type"]', function(e){
        var $this               = $(this);
        var zipcode             = $('.shipping-search-zipcode').val();
        var address_type        = $('select[name="address_type"]').val();
        var promotional_code    = $('.shipping-search-zipcode').val();
        if(zipcode != "") {
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/check_state_tax',
                data    : { zipcode : zipcode, address_type : address_type, promotional_code : promotional_code},
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    console.log(response);
                    if (response.status == true) {
                        $('.order_summary_content').html(response.order_summary_view);
                    }  
                }
            }); 
        }
    });

});

function cart_detail() {
    if($('.cart_block').length > 0) {
        $.ajax({
            type : "POST",   
            url  : base_url+'order/cart',
            success: function(data) {   
                response    = $.parseJSON(data);
                $('.cart_block').html(response.html);
            }
        });
    }
}

function calculate_total() {
    setTimeout(function(){
        var final_total = 0;
        $('.in_final_total').each(function() {
            final_total += parseFloat($(this).attr('data-amount'));
        });

        $('.total_amount').html(final_total.toFixed(2));
    }, 200);
}