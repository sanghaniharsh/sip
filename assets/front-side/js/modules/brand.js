$(document).ready(function() {
    pageURL     = base_url+'brand/';
    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        pageURL     = $(this).attr('href');
        loadBrand();
    });

    $(document).on('click', '#brand_pagination  a', function(e){
        e.preventDefault();
        BrandUrl     = $(this).attr('href');
        loadBrandProducts();
      
    });
});
function loadBrand() {

    $.ajax({
        type        : "POST",
        url         : pageURL,
        success     : function(data) {  
            var responsedata  = $.parseJSON(data);
            if (responsedata.status == true) {
                $('.brand_list').html(responsedata.html);
            }
       }
    });
}

function loadBrandProducts() {
    $.ajax({
        type    : "POST",
        url     : BrandUrl,
        success : function(data) {   
            var responsedata  = $.parseJSON(data);
            if (responsedata.status == true) {
                $('.brand_product_list').html(responsedata.html);
            }
       }
    });
}