$(document).ready(function() {
    $(document).on('click', '.auction_video_btn', function(e){
        e.preventDefault();
        var id   = $(this).attr('data-id');
        $.ajax({
            type    : "POST",
            data    : {id : id},
            url     : base_url+'/auction/auctionvideo',
            success : function(data) {   
                var responsedata  = $.parseJSON(data);
                $('#auction_video_modal .modal-dialog').html(responsedata.html);
                $('#auction_video_modal').modal('show');
           }
       });
    }); 
});