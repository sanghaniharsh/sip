$(document).ready(function() {
    setTimeout(function() {
        loadProducts();
    }, 1000);

    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        pageURL = $(this).attr('href');
        loadProducts();
    });
});

function loadProducts() {
    $.ajax({
       type :"POST",
       url  : pageURL,
       success: function(data) {   
          var responsedata  = $.parseJSON(data);
          $('.product-block').html(responsedata.html);
       }
    });
}