$(document).ready(function() {
    if ($('.select2').length > 0) {
        $(".select2").select2();
    }

    setTimeout(function() {
        related_Product();
        load_addon_product();
    }, 2000);

    $(document).on('submit', '.add-cart-frm', function(e){
        e.preventDefault();
        var formData    = new FormData(this);
        $.ajax({
            type    : "POST",
            url     : base_url+'order/add_cart_item',
            data    : formData,
            processData : false,
            contentType : false,
            cache       : false,
            success: function(data){
                response    = $.parseJSON(data);
                if (response.status == true) {
                    add_cart_block();
                    load_addon_product();
                    if (response.cart_count != "") {
                        $('.cart_total_item').html("<span class='cart_badge'>"+response.cart_count+"</span>");
                    }
                    showMessage('success', response.message);
                } else {
                    showMessage('danger', response.message);
                }
            }
        });
    });

    $(document).on('click', '.contact_us_btn', function(e){
        e.preventDefault();
        window.open(base_url+'contact-us', '_blank');
        
    });

    $(document).on('click', '.delete_cart_item', function(e){
        var result = confirm("Are you sure you want to delete this item ? ");

        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var id          = $this.data('id');
            $.ajax({
                type    : "POST",
                url     : base_url+'/order/cart_delete_item',
                data    : { id : id },
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        add_cart_block();
                        if (response.cart_count != "") {
                            $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                        } else {
                           $('.cart_total_item').html("");  
                        }
                        showMessage('success', response.message);
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.product_video_btn', function(e){
        e.preventDefault();
        $('#product_video_model').modal('show');
    }); 

    if ($('.display_popup').length > 0) {
        $('.display_popup').on('click', function(e){
            e.preventDefault();
            $('#shippingDetailsModal').modal('show');
        });
    }
    $(document).on('change','.addon_product_checkbox', function(e) {
//        var cart_id         =   e.params.data.element.attributes.cart_id.value;
        var cart_id     =   $(this).attr('cart_id');
        if (cart_id != "") {
            $.ajax({
              type : "POST",
              data : { id : cart_id},    
              url  : base_url+'order/cart_delete_item',
              success: function(data) {
                  response    = $.parseJSON(data);
                  if (response.status == true) {
                      if (response.cart_count != "") {
                          $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                      } else {
                         $('.cart_total_item').html("");  
                      }
                      showMessage('success', response.message);
                  } else {
                      showMessage('danger', response.message);
                  } 
                  load_addon_product();
              }
          });
        }
    }).trigger('change');
});

function related_Product() {
    if ($('.relatedproduct').length > 0) {

        var category_id = $('.relatedproduct').attr('data-id');
        var product_id  = $('.relatedproduct').attr('product-id');

        $.ajax({
            type : "POST",
            data : { category_id : category_id , product_id : product_id},    
            url  : base_url+'product/related_product',
            success: function(data) {   
                response    = $.parseJSON(data);
                if (response.status == true) {
                    $('.relatedproduct').html(response.html);
                } else {
                    
                }   
            }
        });
    }
}

function add_cart_block() {

    if($('.add_to_cart_block').length > 0) {
        var product_id  = $('.add_to_cart_block').attr('data-id');

        $.ajax({
            type : "POST",
            data : { product_id : product_id },    
            url  : base_url+'product/add_cart_block',
            success: function(data) {   
                response    = $.parseJSON(data);
                if (response.status == true) {
                    $('.add_to_cart_block').html(response.html);
                }
            }
        });
    }
}
function load_addon_product(product_id) {

    var product_id = $('.product_id').val();
    $.ajax({
        type : "get",    
        url  : base_url+'product/get_addon_product/'+product_id,
        success: function(data) {   
            response    = $.parseJSON(data);
            if (response.status == true) {
                $('.addon_block').show();
                $('.addon_products').html(response.html);
            } else {
                $('.addon_block').hide();
            }
        }
    });

}
