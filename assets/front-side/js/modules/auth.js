
$(document).ready(function() {
    
    $('#reset_password_form').validate({
        rules: {
            password: {
                required    : true
            },
            cpassword: {
                required    : true,
                equalTo     : "#password",
            }
        },
        messages: {
            password: {
                required    : "Please enter password"
            },
            cpassword: {
                required    : "Please confirm password",
                equalTo     : "Password not match"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });
    $('#reset_password_form').on('submit', function(e){
        if($('#reset_password_form').valid()) {
            return true;
        } else {
            return false;
        }
        
        
    });
});
