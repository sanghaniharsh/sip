$(document).ready(function() {
    
    loadOrders();
    init_validation();
    $(".my_order_tab").on('click', function(e){
        e.preventDefault();
        loadOrders();
    });
    
    jQueryTabs();

    $(".tabs li a").each(function() {
        var t = $(this).attr("href"),
            i = $(this).html();
        $(t + " .tab-title").prepend("<p><strong>" + i + "</strong></p>")

    })

    $(document).on('change','[name="delivery"]',function(e){
        e.preventDefault();
        if ($(this).is(':checked')) {
            $('.cart_info').addClass('d-none');
        } else {
            $('.cart_info').removeClass('d-none');
        }
    });

});

$(window).resize(function() {
    jQueryTabs()
});

function loadOrders() {
    if ($('#user_order_table').length > 0) {
        $('#user_order_table').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": enterprise_url + 'get-user-orders',
                "type": "POST",
                "data": function() {
                    
                }
            },
            "order": [[1, 'desc']],
            "columns": [
                { "data": "no", "bSortable": false },
                { "data": "invoice_number"},
                { "data": "date" },
                { "data": "total_item" },
                { "data": "total_amount" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}
function init_validation() {
    if ($('#re_order_form').length > 0) {
        $('#re_order_form').validate({
            rules: {
                terms: {
                    required: true,
                },
            },
            messages: {
                terms : {
                    required : "Please agree to the user Agreement/privacy policy",
                },
            },
            errorPlacement: function(error, element) {
                error.insertAfter($(element).parents('div').find($('.checkbox_error')))
            }
        });
    }
}
function jQueryTabs() {
    $(".tabs").each(function() {
        $(".tabs-panel").not(":first").hide(), $("li", this).removeClass("active"), $("li:first-child", this).addClass("active"), $(".tabs-panel:first-child").show(), $("li", this).on('click', function(t) {
            var i = $("a", this).attr("href");
            $(this).siblings().removeClass("active"), $(this).addClass("active"), $(i).siblings().hide(), $(i).fadeIn(400), t.preventDefault()
        }), $(window).width() < 100 && $(".tabs-panel").show()
    })
}