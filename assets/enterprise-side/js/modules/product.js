var pageURL;

$(document).ready(function() {
    setTimeout(function() {
        loadProducts();
    }, 1000);

    pageURL = enterprise_url+'product-list';
    
    $(document).on('click','.page-link',function(e){
        e.preventDefault();
        pageURL  = $(this).attr('href');

        loadProducts();
    });

    $(document).on('keyup','.product_search',function(){
        var searchval   = $(this).val();
        if(searchval != "") {

            if(searchval.length > 2 ) {
                loadProducts();
            } 
        } else {
            loadProducts();
        }

    });
    
    $(document).on('change','.sort',function(){
        loadProducts();
    });

    $(document).on('click','.product-add',function(e){
        e.preventDefault();
        var e = $(this).next(".product-action");
        $(this).hide(),
        e.css("display", "flex");
        var product_id  = $(this).parents(".pr-img-area").find('.product_id').val();
        var quantity    = $(this).parents(".pr-img-area").find('.quantity').val();
        if(quantity == 0) {
            $(this).parents(".pr-img-area").find('.quantity').val(1);
            var quantity    = $(this).parents(".pr-img-area").find('.quantity').val();
        }
        $(this).parents(".pr-img-area").find('.action-minus').removeAttr("disabled")
        add_cart_item(product_id,quantity);
        add_deduct_qauntity();
    });
    $(document).on('click','.action-plus',function(e){
        var e = $(this).closest(".product-action").children(".action-input").get(0).value++
          , c = $(this).closest(".product-action").children(".action-minus");
        e > 0 && c.removeAttr("disabled");
        var product_id  = $(this).parents(".pr-img-area").find('.product_id').val();
        var quantity    = $(this).parents(".pr-img-area").find('.quantity').val();
        add_cart_item(product_id,quantity);
        add_deduct_qauntity();
    });

    $(document).on('click','.action-minus',function(e){
        var e =     $(this).closest(".product-action").children(".action-input").get(0).value-- ;
        if ($(this).closest(".product-action").children(".action-input").get(0).value == 0) {
            $(this).attr("disabled", "disabled");
        }
        var product_id  = $(this).parents(".pr-img-area").find('.product_id').val();
        var quantity    = $(this).parents(".pr-img-area").find('.quantity').val();
        add_cart_item(product_id,quantity);
        check_product_quantity($(this));
        add_deduct_qauntity();
    });

    // $(document).on('mouseover', '.product', function(e){
    //     e.preventDefault();
    //     var $this       = $(this);
    //     var product_id  = $this.find(".product_id").val();

    //     $.ajax({
    //         type : "POST",
    //         data : { product_id : product_id },    
    //         url  : enterprise_url+'product-details',
    //         success: function(data) {   
    //             response    = $.parseJSON(data);
    //             if (response.status == true) {
    //                 $('.modal-content').html(response.html);
    //                 $('#productModal').modal('show');
    //             }
    //         }
    //     });
    // });
    $(document).on('click', '.delete_cart_item', function(e){
        var result = confirm("Are you sure you want to delete this item ? ");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var id          = $this.data('id');

            $.ajax({
                type    : "POST",
                url     : enterprise_url+'cart-remove-item',
                data    : { id : id },
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        showMessage('success', response.message);
                        if (response.cart_count != "") {
                            add_cart_block();
                            $('.add_to_cart_block').html(response.html);
                        } else {
                            $('.add_to_cart_block').html("");
                        }
                        if (response.cart_item_count != "") {
                            $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                            $('.total_items').html(response.cart_count);
                        } else {
                            $('.cart_total_item').html("");
                        }
                        cart_detail();
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            return false;
        }
    });
    
    $(document).on('submit', '.add-cart-frm', function(e){
        e.preventDefault();
        var formData    = new FormData(this);
        $.ajax({
            type    : "POST",
            url     : enterprise_url+'add-cart-item',
            data    : formData,
            processData : false,
            contentType : false,
            cache       : false,
            success: function(data){
                response    = $.parseJSON(data);
                if (response.status == true) {
                    add_cart_block();
                    $('.add_to_cart_block').html(response.html);
                    showMessage('success', response.message);
                    if (response.cart_count != "") {
                        $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                        $('.total_items').html(response.cart_count);
                    } else {
                        $('.cart_total_item').html("");
                    }
                } else {
                    showMessage('danger', response.message);
                }
            }
        });
    });

});

function loadProducts() {
    
    var searchval   = $('.product_search').val();
    var sort        = $('.sort').val();
    
    $.ajax({
       type :"POST",
       data : { searchval : searchval , sort : sort},
       url  : pageURL,
       success: function(data) {   
          var responsedata  = $.parseJSON(data);
          $('.product-block').html(responsedata.html);
       }
    });
}

function add_cart_block() {
    if($('.add_to_cart_block').length > 0) {
        var product_id  = $('.add_to_cart_block').attr('data-id');
        $.ajax({
            type : "POST",
            data : { product_id : product_id },    
            url  : enterprise_url+'add-cart-block',
            success: function(data) {   
                response    = $.parseJSON(data);
                if (response.status == true) {
                    $('.add_to_cart_block').html(response.html);
                }
            }
        });
    }
}

function add_cart_item(product_id,quantity) {
    if ($('.product-action').length > 0) {
        $.ajax({
            type : "POST",
            data : { product_id : product_id ,quantity: quantity},    
            url  : enterprise_url+'add-to-cart-item',
            success: function(data) {   
                response    = $.parseJSON(data);
                if (response.status == true) {
                    add_cart_block();
                    add_deduct_qauntity();
                } else {
                    // showMessage('danger', response.message);
                }
            }
        });
    }
}
function add_deduct_qauntity() {
    var array       = [];
    $(".quantity").each(function() {

        var qty         = $(this).val();
        var product_id  = $(this).attr('data-id');
        if(qty > 0) {
            array.push(product_id);
        }
    });
    cart_count_header = array.length;
    $('.cart_total_item').html("<span class='cart_badge'>"+ cart_count_header +"</span>");
    $('.total_items').html(cart_count_header);
    if(cart_count_header == 0) {
        $('.cart_total_item').html("");
    }
}
function check_product_quantity(e) {
    if ($('.product-action').length > 0) {
        var product_quantity =  e.closest(".product-action").children(".action-input").get(0).value;
        if (product_quantity == 0) {
            e.parents(".pr-img-area").find('.product-add').show();
            e.closest(".product-action").hide();
            // e.closest(".product-action").children(".action-input").val(1);
        }
    }
}