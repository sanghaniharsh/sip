$(document).ready(function() {
    init_validation();
    $(document).on('click', '.delete_cart_item', function(e){
        var result = confirm("Are you sure you want to delete this item ? ");

        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var id          = $this.data('id');
            $.ajax({
                type    : "POST",
                url     : enterprise_url+'cart-delete-item',
                data    : { id : id },
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);

                    if (response.status == true) {
                        showMessage('success', response.message);
                        if (response.cart_count != "") {
                            $('.cart_total_item').html("<span class='cart_badge'>"+ response.cart_count +"</span>");
                            $('.total_items').html(response.cart_count);
                        } else {
                            $('.cart_total_item').html("");
                        }
                        cart_detail();
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            return false;
        }
    });
    
    $(document).on('change', '.quantity_change', function(e){
        e.preventDefault();
        var $this           = $(this);
        var id              = $this.data('id');
        var qty             = $this.val();
        
        if(qty > 0) {
            var product_slug    = $this.data('rel');
            var ground          = $this.data('price');

            $.ajax({
                type    : "POST",
                url     : enterprise_url+'cart-update-qty',
                data    : { id : id , quantity : qty,ground:ground},
                success : function(response){

                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        cart_detail();
                    } else {
                        showMessage('danger', response.message);
                    }
                }
            });
        } else {
            $this.val(1);
            showMessage('danger', 'Quantity must be more than 0');
            return false;
        }
    });

    $(document).on('change','[name="delivery"]',function(e){
        e.preventDefault();
        if ($(this).is(':checked')) {
            $('.cart_info').addClass('d-none');
        } else {
            $('.cart_info').removeClass('d-none');
        }
    });
    
});

function cart_detail() {
    if($('.cart_block').length > 0) {
        $.ajax({
            type : "POST",   
            url  : enterprise_url+'cart',
            success: function(data) {   
                response    = $.parseJSON(data);
                $('.cart_block').html(response.html);
                qty_slug();
            }
        });
    }
}

function init_validation() {
    if ($('#cart_details_form').length > 0) {
        $('#cart_details_form').validate({
            rules: {
                terms: {
                    required: true,
                },
            },
            messages: {
                terms : {
                    required : "Please agree to the User Agreement & Privacy Policy",
                },
            },
            errorPlacement: function(error, element) {
                error.insertAfter($(element).parents('div').find($('.checkbox_error')))
            }
        });
    }
}

function  qty_slug() {
    $("input[id='qty_slug']").each(function(){

        var slug = $(this).val();
        
        $('.quantity_'+slug).on('change', function(){

                var type    = $('.ship_type_'+slug).val();
                var qty     = $(this).val();

                var manual  = $('.manual_rate_'+slug).val();
                var total_shipping   = $('.total_shipping_amount').text();
                var man  = 0;
                // alert(total_shipping);
                
                if(type == 'manual'){

                    man = (parseFloat(qty)*parseFloat(manual)).toFixed(2);
                    $('.ship_'+slug).html('$'+man);
                    // $('.total_shipping_amount').text(parseFloat(total_shipping) + parseFloat(manual));
                }
        });
            
    })
}

