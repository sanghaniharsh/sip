$(document).ready(function() {
    initDatePicker();

    $(document).on('keyup', '.search_container .serch_zipcode', function(e) {
        var $this   = $(this);
        setTimeout(function() {
            var b_name  = $this.val();
            if( b_name != "" ) {
                $.ajax({
                    type    : "POST",
                    url     : base_url + "home/search_location",
                    data    : {
                        search: b_name
                    },
                    success : function(response){
                        var response = $.parseJSON(response);
                        if (response.status) {
                            $this.parents('.search_container').find('.business_search_result').html(response.html);
                        } else {
                            $this.parents('.search_container').find('.business_search_result').html('');
                        }
                    }
                });
            } else {
                $this.parents('.search_container').find('.business_search_result').html('');
            }
        }, 800);
    });
    
    $(document).on('click', '.search_container .business_search_result .listing-li', function(e) {
        e.preventDefault();
        var city_state  = $(this).attr('data-rel');
        var zipcode     = $(this).attr('data-slug');

        $(this).parents('.search_container').find('.fill_city_state').val(city_state);
        $(this).parents('.search_container').find('.serch_zipcode').val(zipcode);
        $(this).parents('.search_container').find('.business_search_result').html('');
    });
    
    $('.numeric').formatter({
        'pattern': '{{9999999999}}'
    });

    $(".input-44").fileinput({
       //uploadUrl: ADMIN_URL+"/product/updateProductImage", // you must set a valid URL here else you will get an error
       maxFilePreviewSize: 10240,
       maxFileSize: allowed_filesize_mb * 1024 * 1024,
    });

    $('#profileForm').validate({
        rules: {
            username: {
                required: true,
                remote : {
                        url: ADMIN_URL+'/admin/validate',
                        type: "POST",
                        async:false
                    }
            },
            email: {
                required: true,
                email : true
            },
            password: {
                minlength: 5
            },
            cpassword: {
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            username : {
                required : "Please enter your username",
                remote : "Username already exists"
            },
            email : {
                required : "Please enter email address",
                email : "Please enter valid email address"
            },
            password: {
                minlength: "Your password must be at least 5 characters long"
            },
            cpassword: {
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

//    if ($('.ckeditor').length > 0) {
//        $('.ckeditor').ckeditor();
//    }
});
$(document).ready(function() {
    if ($('.example-filter-placeholder').length > 0) {
        $('.example-filter-placeholder').multiselect({
            enableFiltering: true,
            filterPlaceholder: 'Search for something...'
        });
    }
});
function toaster(heading, text, icon) {
   $.toast({
    heading: heading,
    text: text,
    position: 'top-right',
    icon: icon,
    stack: false
  });  
}
function initDatePicker() {
    if ($('.date-picker').length > 0) {
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd'
        });
    }
}
function showMessage(type, message) {
    toastr.options = {
        closeButton     : true,
        debug           : true,
        progressBar     : false,
        positionClass   : 'toast-top-right',
        onclick         : null
    };
    
    if (type == 'success') {
        toastr.success(message, 'Success', {timeOut: 5000})
    }
    if (type == 'danger') {
        toastr.error(message, 'Error', {timeOut: 5000})
    }
}