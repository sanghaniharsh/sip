$(document).ready(function() {

    loadAdminUsers();
    
    
    initValidation();
    
    
    $(document).on('click', '#delete', function(e){
        
        var enterprise_id = $('#users_table').attr('enterprise_id');
        
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this = $(this);
            var myId  = $this.data('id');

            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/enterprise-user/delete/'+enterprise_id,
                data:{
                     id:myId
                },
                success:function(response){
                    response    = $.parseJSON(response);
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });
    
});


function initValidation() {
    if ($('#userForm').length > 0) {
        
         var enterprise_id = $('#userForm').attr('enterprise_id');
        
        $("#userForm").validate({
            rules: {
                firstname: {
                    required    : true
                },
                lastname: {
                    required    : true
                },
                email: {
                    required    : true,
                    email       : true,
                    remote      : {
                                    type    : "POST",
                                    url     : ADMIN_URL+'/enterprise-user/validate/'+enterprise_id,
                                    data    : {
                                        user_id: function()
                                        {
                                            return $('#userForm :input[name="userId"]').val();
                                        }
                                    }
                                }
                },
                sales_tax: {
                    required    : true,
                    number      : true
                },
                cpassword: {
                    equalTo     : "#password",
                }
            },
            messages: {
                firstname: {
                    required    : "Please enter first name"
                },
                lastname: {
                    required    : "Please enter first name"
                },
                email : {
                    required    : "Please enter email address",
                    email       : "Please enter valid email address",
                    remote      : "Email already register"
                },
                sales_tax: {
                    required    : "Please enter sales tax",
                    number      : "Please enter sales tax in number"
                },
                cpassword: {
                    equalTo     : "Password not match"
                }
            },
        });
    
        $.validator.addMethod("checkPassword", function(value, element) {

            var userId      =  $("#userId").val();
            var password    =  $("#password").val();
            var cpassword   =  $("#cpassword").val();
            var ret         = true;
            if (userId != '' || password != '' && cpassword != '') {
                return ret;
            } else if (userId == '' && password != '' && cpassword != '') {
                return ret;
            } else {
                ret = false;
                return ret;
            }

        }, "Please enter password");
    
    }
}

function loadAdminUsers() {
    if ($('#users_table').length > 0) {
        
        var enterprise_id = $('#users_table').attr('enterprise_id');
        
        $('#users_table').DataTable({
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL + '/enterprise-user/'+enterprise_id,
                "type": "POST"
            },
            "order": [[0, 'desc']],
            "columns": [
                { "data": "firstname" },
                { "data": "lastname" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}