$(document).ready(function() {
    setTimeout(function() {
        loadBrand();
    }, 2500);

    pageURL = base_url+'admin/brand/index';
    $(".brand-frm").validate({
        rules: {
            brand_name: {
              required: true
            },
            avatar: {
               requiredImage:true
            }
        },
        messages: {
            brand_name: {
                required : "Please enter brand name"
            },
            avatar: {
                requiredImage: ""
            }
        }
    }); 
    
    $.validator.addMethod("requiredImage", function(value, element) {
        var returnValue = true;

        if ($('.brand_id').val() == '' && $('.brandimage').val() == '') {
            $('.avatar-error').html("PLEASE SELECT IMAGE");
            returnValue = false;
        } else if (this.optional(element) || ! element.files || ! element.files[0]) {
            returnValue = true;
        } else {
            returnValue = element.files[0].size <= 1024 * 1024 * allowed_filesize_mb;
            if(!returnValue) {
                $('.avatar-error').html("The file size can not exceed "+allowed_filesize_mb+"MB.");
            }
        }
        return returnValue;
    });

    $(document).on('submit', '.brand-frm', function(e){
        e.preventDefault();
        var formAction  = $(this).attr('action');
        var formData    = new FormData(this);
        var avatar      = $('.brand-frm [type="file"]')[0].files;
        formData.append('avatar', avatar);

        if ($(this).valid()) {
            $.ajax({
                type    : "POST",
                url     : formAction,
                data    : formData,
                processData : false,
                contentType : false,
                cache       : false,
                success: function(data){
                    var obj = $.parseJSON(data);

                    if (obj.status == true) {
                        $('.brand-modal').modal('hide');
                        loadBrand();
                        $('.error-message-success').html(obj.message).show();
                    } else {
                        $('.error-msg').html(obj.message).show();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.brand_button', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');
        $.ajax({
            type    : "POST",
            data    : {id : id},
            url     : ADMIN_URL+'/brand/setup/',
            success : function(data) {   
                var responsedata  = $.parseJSON(data);
                $('.brand-frm').html(responsedata.html);
                $('.brand-modal').modal('show');
           }
       });
    });

    $(document).on('click', '.brand_remove_btn', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');

        if (confirm("Are you sure you want to delete this?")) {
            $.ajax({
                type    : "POST",
                data    : {id : id},
                url     : ADMIN_URL+'/brand/delete/',
                success : function(data) {   
                    var responsedata  = $.parseJSON(data);
	
                    if (responsedata.status == true) {
			pageURL = base_url+'admin/brand/index';
                        $('.error-message-success').html(responsedata.message).show();
                        loadBrand();
                    } else {
                        $('.error-message-danger').html(responsedata.message).show(); 
                    }
                }
           });
       } else {
           return false;
       }
    });

    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        pageURL     = $(this).attr('href');
        var search  = $('.brand_search').val();

        if(search != '') {
            loadBrand(search);
        } else {
            loadBrand();
        }
    });

    $('.brand_search').keyup(function(){
        var search = $(this).val();

        if (search != '') {

            if (search.length > 2 ) {
                loadBrand(search);
            }
        } else {
            loadBrand();
        }
    });
});

function initSortable() {
    if($( "#sortable" ).length > 0) {
        $( function() {
            $("#sortable").sortable({
                delay: 150,
                update:  function (event, ui) {
                    var selectedData = new Array();
                    $('.row_position>div').each(function() {
                        selectedData.push($(this).attr("id"));
                    });
                    updateOrder(selectedData);
                    
                }
            });
            $( "#sortable" ).disableSelection();
        });
    }
}
function updateOrder(data) {
    $.ajax({
        url: base_url+'admin/brand/sorting',
        type: 'post',
        data: {position: data},
        success: function () {
            showMessage('success', "brand shorting updated");
        }
    });
}
function loadBrand(search) {
    
    $.ajax({
        type    : "POST",
        data    : { search : search },
        url     : pageURL,
        success : function(data) {   
            var responsedata  = $.parseJSON(data);
            $('.brandlist').html(responsedata.html);
            initSortable();
       }
    });
}