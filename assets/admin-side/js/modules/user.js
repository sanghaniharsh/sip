$(document).ready(function() {

    loadAdminUsers();
    
    $('.dataTable').DataTable();

    $('#forgot_password').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "email") {
                $(".mail-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $('#reset_password').validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            cpassword: {
                required: "Please provide a Confirm password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "password") {
                $(".password-error").html(error);
            } else if (element.attr("name") == "cpassword") {
                $(".cpassword-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $(document).on('submit', '#forgot_password', function(e) {
        e.preventDefault();
        var $this   = $(this);
        if ($this.valid()) {
            $.ajax({
                url : ADMIN_URL+"/admin/forgot_password",
                type: 'POST',
                data: $this.serialize(),
                success:function(response){
                    response = $.parseJSON(response);
                    if (response.status == true && response.message != '') {
                        toaster('success', response.message, 'success');
                    } else {
                        toaster('error', response.message, 'error');
                    }
                }
            });
        }
    });
    
    $.validator.addMethod("checkPassword", function(value, element) {
        
        var userId      =  $("#userId").val();
        var password    =  $("#password").val();
        var cpassword   =  $("#cpassword").val();
        var ret         = true;
        if (userId != '' || password != '' && cpassword != '') {
            return ret;
        } else if (userId == '' && password != '' && cpassword != '') {
            return ret;
        } else {
            ret = false;
            return ret;
        }

    }, "Please enter password");

    $("#userForm").validate({
        rules: {
            firstname: {
                required    : true
            },
            lastname: {
                required    : true
            },
            email: {
                required    : true,
                email       : true,
                remote      : {
                                type    : "POST",
                                url     : ADMIN_URL+'/user/validate',
                                data    : {
                                    user_id: function()
                                    {
                                        return $('#userForm :input[name="userId"]').val();
                                    }
                                }
                            }
            },
            phone: {
                required    : true,
                number      : true
            },
            cpassword: {
                equalTo     : "#password",
            }
        },
        messages: {
            firstname: {
                required    : "Please enter first name"
            },
            lastname: {
                required    : "Please enter first name"
            },
            email : {
                required    : "Please enter email address",
                email       : "Please enter valid email address",
                remote      : "Email already register"
            },
            phone: {
                required    : "Please enter phone number",
                number      : "Please enter valid phone number"
            },
            cpassword: {
                equalTo     : "Password not match"
            }
        },
    });
    $(document).on('click', '#delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this = $(this);
            var myId  = $this.data('id');

            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/user/delete',
                data:{
                     id:myId
                },
                success:function(response){
                    response    = $.parseJSON(response);
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });
    
    $(document).on('click','.status_checks',function(e) {
        e.preventDefault();
        var current_element = $(this);
        $.ajax({
          type:"POST",
          url: ADMIN_URL+'/user/checkStatus',
          data: {id:$(current_element).attr('data')},
          success: function(data) {   
            location.reload();
          }
        });
      return false;
    });
    
    //orderlist daywise filter 
    $(document).on('change','.order_day_filter',function(e) {
        e.preventDefault();
        var order_day     = $(this).val();
        $.ajax({
            type:"POST",
            url: ADMIN_URL+'/admin/order_filter_daywise',
            data: { order_day : order_day },
            success: function(data) {   
            response = $.parseJSON(data);
                if (response.status == true) {
                    $('.order_block').html(response.order_html);
                } else {  
                }
            }
        });
    });

});

function loadAdminUsers() {
    if ($('#users_table').length > 0) {
        $('#users_table').DataTable({
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL + '/user',
                "type": "POST"
            },
            "order": [[0, 'desc']],
            "columns": [
                { "data": "firstname" },
                { "data": "lastname" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}