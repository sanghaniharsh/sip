var pageURL;
$(document).ready(function() {
    setTimeout(function() {
        loadBlogs();
    }, 1000);
    
    pageURL = base_url+'admin/blog/index';

    $(document).on('click', '.addBlogCategory', function(e){
        e.preventDefault();
        $.ajax({
            type :"post",
            url  : ADMIN_URL+'/blog/category_setup/',
            success: function(data) {
                var response = $.parseJSON(data);

                if (response.status == true) {
                    $('#blogCategoryForm').html(response.html);
                    $('#blogCategoryModal').modal('show');
                }
           }
       });
    });

    $(document).on('click', '.close-modal', function(e){
        e.preventDefault();
        $('#blogCategoryModal').modal('hide');
        $('#blogCategoryForm')[0].reset();
    });

    $(document).on('submit', '#blogCategoryForm', function(e){
        e.preventDefault();
        var formData    = new FormData(this);

        if ($(this).valid()) {
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/blog/category_commit',
                data: formData,
                cache:false,
                processData: false,
                contentType: false,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('#blogCategoryModal').modal('hide');
                        loadBlogCategory();
                    } else {
                        $('.error-msg').text(response.message);
                        $('.error-msg').css("display", "block");
                    }
                }
            });
        }
    });

    $(document).on('keyup', '.blog_category_slug', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        $('#slug').val(ans);
        $('#slugText').val(ans);
    });
    
    $.validator.addMethod("requiredImage", function(value, element) {
        var returnValue = true;

        if ($('.blog_id').val() == '' && $('.blog_images').val() == '') {
            returnValue = false;
        }

        return returnValue;
    }, "Please upload property images");

    $(document).on('click', '.delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/blog/delete_category',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        loadBlogCategory();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.edit', function(e){
        e.preventDefault();
        var $this       = $(this);
        var myId        = $this.data('id');
        $.ajax({
            type: "POST",
            url : ADMIN_URL+'/blog/category_setup/'+myId,
//            data:{ id : myId},
            async : true,
            success:function(response){
                response    = $.parseJSON(response);
                if (response.status == true) {
                    $('#blogCategoryForm').html(response.html);
                    $('#blogCategoryModal').modal('show');
                }
            }
        });
    });

    $('#blogCategoryForm').validate({
        rules: {
            name: {
                required: true,
                remote : {
                    url: base_url+'admin/blog/category_validate',
                    type: "POST",
                    async:false,
                    data: {
                            name: function()
                            {
                                return $('#blogCategoryForm :input[name="name"]').val();
                            },
                            id: function()
                            {
                                return $('#blogCategoryForm :input[name="blogCategory"]').val();
                            }
                        }
                    }
            },
            slug: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please enter a category name",
                remote: "This category already exists"
            },
            slug: {
                required: "Please enter a URL"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

    $('#blogForm').validate({
        rules: {
            title: {
                required: true
            },
            slug: {
                required: true
            },
            category: {
                required: true
            },
            "blog_images[]": {
                requiredImage: true
            },
            description: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please enter blog title"
            },
            slug: {
                required: "Please enter blog slug"
            },
            category: {
                required: "Please choose blog category"
            },
            "blog_images[]": {
                requiredImage: "Please choose blog image"
            },
            description: {
                required: "Please enter blog description"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "blog_images[]") {
                $(".img-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('.delete-blog-image').on('click', function(e){
        e.preventDefault();
        var $this       = $(this);
        var image_id    = $this.attr('rel');
        var itemId      = $this.attr('data-itemid');

        if (! confirm("Are you sure you want to delete?")) {
           return false;
        } else {
            $.ajax({
                url:base_url+"admin/blog/deleteBlogImage",
                type:'POST',
                data: {"image_id" : image_id,'blogId' : itemId},
                success:function(response){
                    var data = $.parseJSON(response);
                    if ((data.status == true)) {
                        $this.parent().fadeOut('slow');
                        $('.alert-success').find('.success_text').text(data.message);
                    } else {
                        $('.alert-danger').find('.success_text').text(data.message);                        
                    }
                 }
            });     
        }
    });
    
    $(document).on('click', '.delete-blog', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/blog/delete',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        pageURL = base_url+'admin/blog/index';
                        loadBlogs();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.pagination-block .pagination li a',function(e){
        e.preventDefault();
        pageURL     = $(this).attr('href');
        var search  = $('.blogpost_search').val();

        if(search != '') {
            loadBlogs(search);
        } else {
            loadBlogs();
        }
    });

    $('.blogpost_search').keyup(function(){
        var search = $(this).val();

        if(search != '') {

            if(search.length > 2 ) {
                loadBlogs(search);
            }
        } else {
            loadBlogs();
        }
    });
});

function loadBlogCategory() {
    if ($('#blogCategoryDataTable').length > 0) {
        $('#blogCategoryDataTable').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL+'/blog/category',
                "type": "POST"  
            },
            "order": [[2, 'desc']],
            "columns": [
                { "data": "name" },
                { "data": "slug" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}

function loadBlogs(search) {
    $.ajax({
        type    : 'POST',
        data    : { search : search },
        url     : pageURL,
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".blog_post_block").html(obj.html);
            }
        }
    });
}