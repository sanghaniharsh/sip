$(document).ready(function() {

    loadAdminUsers();
    
    $('.dataTable').DataTable();

    $('#forgot_password').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "email") {
                $(".mail-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $('#reset_password').validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            cpassword: {
                required: "Please provide a Confirm password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "password") {
                $(".password-error").html(error);
            } else if (element.attr("name") == "cpassword") {
                $(".cpassword-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $(document).on('submit', '#forgot_password', function(e) {
        e.preventDefault();
        var $this   = $(this);
        if ($this.valid()) {
            $.ajax({
                url : ADMIN_URL+"/admin/forgot_password",
                type: 'POST',
                data: $this.serialize(),
                success:function(response){
                    response = $.parseJSON(response);
                    if (response.status == true && response.message != '') {
                        toaster('success', response.message, 'success');
                    } else {
                        toaster('error', response.message, 'error');
                    }
                }
            });
        }
    });
    
    $.validator.addMethod("checkPassword", function(value, element) {
        
        var userId      =  $("#userId").val();
        var password    =  $("#password").val();
        var cpassword   =  $("#cpassword").val();
        var ret         = true;
        if (userId != '' || password != '' && cpassword != '') {
            return ret;
        } else if (userId == '' && password != '' && cpassword != '') {
            return ret;
        } else {
            ret = false;
            return ret;
        }

    }, "Please enter password");

    $("#userForm").validate({
        rules: {
            username : {
                required : true,
                remote : {
                    url: ADMIN_URL+'/admin/validate',
                    type: "POST",
                    async:false,
                    data: {
                            id: function()
                            {
                                return $('#userForm :input[name="userId"]').val();
                            }
                        }
                    }
            },
            email : {
                required : true,
                email : true,
                remote : {
                    url: ADMIN_URL+'/admin/validate',
                    type: "POST",
                    async:false,
                    data: {
                            id: function()
                            {
                                return $('#userForm :input[name="userId"]').val();
                            }
                        }
                    }
            },
            password : {
                checkPassword : true
            },
            cpassword : {
                checkPassword : true,
                equalTo: "#password"
            }
        },
        messages: {
            username : {
                required : "Please enter username",
                remote : "Username already exists"
            },
            email : {
                required : "Please enter email address",
                email : "Please enter valid email address",
                remote: "Email address already exists"
            },
            password : {
                checkPassword : "Please enter password"
            },
            cpassword : {
                checkPassword : "Please enter confirm password",
                equalTo : "Password and confirm password does not match"
            }
        }
    });
    $(document).on('click', '#delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this = $(this);
            var myId  = $this.data('id');

            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/admin/delete',
                data:{
                     id:myId
                },
                success:function(response){
                    response    = $.parseJSON(response);
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });
    
    $(document).on('click','.status_checks',function(e) {
        e.preventDefault();
        var current_element = $(this);
        $.ajax({
          type:"POST",
          url: ADMIN_URL+'/admin/checkStatus',
          data: {id:$(current_element).attr('data')},
          success: function(data) {   
            location.reload();
          }
        });
      return false;
    });
    
    //orderlist daywise filter 
    $(document).on('change','.order_day_filter',function(e) {
        e.preventDefault();
        var order_day     = $(this).val();
        $.ajax({
            type:"POST",
            url: ADMIN_URL+'/admin/order_filter_daywise',
            data: { order_day : order_day },
            success: function(data) {   
            response = $.parseJSON(data);
                if (response.status == true) {
                    $('.order_block').html(response.order_html);
                } else {  
                }
            }
        });
    });
    if($('#siteSettingsForm').length > 0) {

        $('#siteSettingsForm').validate({
            rules: {
                from_name: {
                    required: true
                },
                from_email: {
                    required: true,
                    email: true
                },
                paypal_username: {
                    required: true
                },
                paypal_signature: {
                    required: true,
                },
                paypal_password: {
                    required: true,
                }
            },
            messages: {
                from_name: {
                    required: "Please enter from name",
                },
                from_email: {
                    required: "Please enter from email",
                    email: "Please enter valid email address"
                },
                paypal_username: {
                    required: "Please enter paypal username",
                },
                paypal_signature: {
                    required: "Please enter paypal signature",
                },
                paypal_password: {
                    required: "Please enter paypal password",
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });
    }
    
    if ($('.reload_captcha').length > 0) {
        $('.reload_captcha').on('click', function(){
            var randomString = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

            $(this).parents('form').find("#captcha_code").attr('src', base_url+'secureimage/securimage?'+randomString);
        });
    }
    
    $('.form-signin').validate({
        rules: { 
            register_captcha: {
                required    : true,
                remote      : {
                                url: base_url+'secureimage/check_captcha',
                                type: "POST",
                                data: {
                                    captcha: function()
                                    {
                                        return $('.form-signin :input[name="register_captcha"]').val();
                                    }
                                }
                            }
            }
        },
        messages: {
            register_captcha: {
                required    : "Please enter captcha",
                remote      : "Wrong captcha code"
            }
        }
    });
    

});

function loadAdminUsers() {
    if ($('#adminUsers').length > 0) {
        $('#adminUsers').DataTable({
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL+'/admin/users',
                "type": "POST"
            },
            "order": [[0, 'desc']],
            "columns": [
                { "data": "username" },
                { "data": "email" },
                { "data": "password" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}