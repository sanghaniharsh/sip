var pageURL;
$(document).ready(function() {
    setTimeout(function() {
        loadAuction();
    }, 1000);

    pageURL = base_url+'admin/auction/index';

    $(document).on('keyup', '.auction_slug', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        $('#slug').val(ans);
        $('#slugText').val(ans);
    });
    
    $('.auction_search').keyup(function(){
        var search = $(this).val();

        if (search != '') {

            if (search.length > 2 ) {
                loadAuction(search);
            }
        } else {
            loadAuction();
        }
    });

    $(document).on('click', '.delete-auction', function(e){
        var result = confirm("Are you sure you want to delete?");
        if (result) {
            e.preventDefault();
            var $this   = $(this);
            var myId    = $this.data('id');
            $.ajax({
                type    : "POST",
                url     : ADMIN_URL+'/auction/delete',
                data    : { id : myId},
                async   : true,
                success : function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        pageURL = base_url+'admin/auction/index';
                        loadAuction();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $('.delete-auction-image').on('click', function(e){
        e.preventDefault();
        var $this       = $(this);
        var image_id    = $this.attr('rel');
        var itemId      = $this.attr('data-itemid');

        if (! confirm("Are you sure you want to delete?")) {
           return false;
        } else {
            $.ajax({
                url     : ADMIN_URL+"/auction/deleteAuctionImage",
                type    : 'POST',
                data    : {"image_id" : image_id,'auction_id' : itemId},
                success : function(response){
                    var data = $.parseJSON(response);
                    if ((data.status == true)) {
                        $this.parent().fadeOut('slow');
                        $('.alert-success').find('.success_text').text(data.message);
                    } else {
                        $('.alert-danger').find('.success_text').text(data.message);                        
                    }
                }
            });     
        }
    });

    $(document).on('click', '.pagination-block .pagination li a',function(e){
        e.preventDefault();
        pageURL     = $(this).attr('href');
        var search  = $('.auction_search').val();

        if(search != '') {
            loadAuction(search);
        } else {
           loadAuction(); 
        }
    });

    $( ".auctiondate" ).datepicker();

    $('.auction_timepicker').timepicker({
        timeFormat  : 'hh:mm p',
        interval    : 30,
        dynamic     : false,
        dropdown    : true,
        scrollbar   : true
    });

    $('.from_timepicker').timepicker({
        timeFormat  : 'hh:mm p',
        interval    : 30,
        dynamic     : false,
        dropdown    : true,
        scrollbar   : true,
        change      : tmTotalHrsOnSite
    });
    
    $('.to_timepicker').timepicker({
        timeFormat  : 'hh:mm p',
        interval    : 30,
        dynamic     : false,
        dropdown    : true,
        scrollbar   : true,
        change      : tmTotalHrsOnSite
    });
    
    
    $(document).on('change','.auction_type', function(e){
        e.preventDefault();
        
        if ($(this).prop('checked')==true){
            $('.online_label').removeClass('hide');
            $('.online_label').removeAttr('disabled');
            $('.offline_label').addClass('hide');
            $('.offline_label').attr('disabled','disabled');
        } else {
            $('.offline_label').removeClass('hide');
            $('.offline_label').removeAttr('disabled');
            $('.online_label').addClass('hide');
            $('.online_label').attr('disabled','disabled');
        }
    });
    
});

function loadAuction(search) {

    $.ajax({
        type    : 'POST',
        data    : { search : search },
        url     :  pageURL,
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".auction_block").html(obj.html);
                initAuctionSortable();
            }
        }
    });
}

function GetHours(d) {
    var h = parseInt(d.split(':')[0]);
    if (d.split(':')[1].split(' ')[1] == "PM") {
        h = h + 12;
    }
    return h;
}

function GetMinutes(d) {
    return parseInt(d.split(':')[1].split(' ')[0]);
}


function tmTotalHrsOnSite () {

    $('.to_timepicker').timepicker("destroy");
    $(".to_timepicker").val('');
    var startTime   = $(".from_timepicker").val();
    var from_time   = startTime.replace(/\s/g, '');

    setTimeout(function(){
        $('.to_timepicker').timepicker({
            timeFormat  : 'h:mm p',
            interval    : 30,
            minTime     : from_time,
            dynamic     : false,
            dropdown    : true,
            scrollbar   : true
        });
    }, 500);
}
function initAuctionSortable() {
    if($( "#auction_sort" ).length > 0) {
        $( function() {
            $("#auction_sort").sortable({
                delay: 150,
                update:  function (event, ui) {
                    var selectedData = new Array();
                    $('.row_position>div').each(function() {
                        selectedData.push($(this).attr("id"));
                    });
                    updateAuctionOrder(selectedData);
                    
                }
            });
            $( "#auction_sort" ).disableSelection();
        });
    }
}
function updateAuctionOrder(data) {
    $.ajax({
        url: base_url+'admin/auction/sorting',
        type: 'post',
        data: {position: data},
        success: function () {
            showMessage('success', "Auction position has been updated");
        }
    });
}
