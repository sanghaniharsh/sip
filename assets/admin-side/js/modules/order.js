
$(document).ready(function() {
    setTimeout(function() {
        loadOrders();
    }, 1000);
    
    $(document).on('click', '#delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this = $(this);
            var myId  = $this.data('id');

            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/order/delete',
                data:{
                     id:myId
                },
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        loadOrders();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(".order_status_frm").validate({
        rules: {
            order_status: {
                required: true
            },
            order_captcha: {
                required: true,
                remote : {
                    url: base_url+'secureimage/check_captcha',
                    type: "POST",
                    data: {
                        captcha: function()
                        {
                            return $('.order_status_frm :input[name="order_captcha"]').val();
                        }
                    }
                }
            }
        },
        messages: {
            order_status: {
                required : "Please select status"
            },
            order_captcha: {
                required : "Please enter captcha",
                remote : "Wrong captcha code"
            }
        }
    }); 

    if ($('.reload_captcha').length > 0) {
        $('.reload_captcha').on('click', function(){
            var randomString = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

            $(this).parents('form').find("#captcha_code").attr('src', base_url+'secureimage/securimage?'+randomString);
        });
    }

    $(document).on('submit', '.order_status_frm', function(e){
        e.preventDefault();
        var formData    = new FormData(this);

        if($(this).valid()) {
            $.ajax({
                type        : "POST",
                url         : ADMIN_URL+'/order/update_order_status',
                data        : formData,
                processData : false,
                contentType : false,
                cache       : false,
                success: function(data){
                    var obj = $.parseJSON(data);

                    if (obj.status == true) {
                        if (obj.order_status != '') {
                            if (obj.order_status == '0') {
                                $('.order_status_div').removeClass('btn-outline-info btn-outline-success').addClass('btn-outline-default').text("In Progress");
                            }
                            if (obj.order_status == '1') {
                                $('.order_status_div').removeClass('btn-outline-default btn-outline-success').addClass('btn-outline-info').text("Shipped");
                            }
                            if (obj.order_status == '2') {
                                $('.order_status_div').removeClass('btn-outline-default btn-outline-info').addClass('btn-outline-success').text("Delivered");
                            }
                        }
                        $('.order_captcha_txt').val('');
                        $('.order_status').val('');
                        $('.order-status-modal').modal('hide');
                        $('.error-message-success').html(obj.message).show();
                    } else {
                        $('.error-msg').html(obj.message).show();
                    }
                }
            });
        } else {
            return false;
        }
    });

});

function loadOrders() {
    if ($('#orderDataTable').length > 0) {
        $('#orderDataTable').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL+'/order',
                "type": "POST"  
            },
            "order": [[4, 'desc']],
            "columns": [
                { "data": "order_id" },
                { "data": "user" },
                { "data": "email" },
                { "data": "amount" },
                { "data": "order_status" },
                { "data": "created_date" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}
