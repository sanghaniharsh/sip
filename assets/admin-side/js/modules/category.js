$(document).ready(function() {
    setTimeout(function() {
        loadCategory();
    }, 1000);

    $(".category-frm").validate({
        rules: {
            cat_name: {
              required: true
            },
            avatar: {
               requiredImage:true
            }
        },
        messages: {
            cat_name: {
                required : "Please enter category"
            },
            avatar: {
                requiredImage: ""
            }
        }
    }); 

    $.validator.addMethod("requiredImage", function(value, element) {
        var returnValue = true;

        if ($('.category_id').val() == '' && $('.categoryImage').val() == '') {
            $('.avatar-error').html("PLEASE SELECT IMAGE");
            returnValue = false;
        } else if (this.optional(element) || ! element.files || ! element.files[0]) {
            returnValue = true;
        } else {
            returnValue = element.files[0].size <= 1024 * 1024 * allowed_filesize_mb;
            if(!returnValue) {
                $('.avatar-error').html("The file size can not exceed "+allowed_filesize_mb+"MB.");
            }
        }
        return returnValue;
    });

    $(document).on('submit', '.category-frm', function(e){
        e.preventDefault();
        var formAction  = $(this).attr('action');
        var formData    = new FormData(this);
        var avatar      = $('.category-frm [type="file"]')[0].files;
        formData.append('avatar', avatar);

        if($(this).valid()) {
            $.ajax({
                type    : "POST",
                url     : formAction,
                data    : formData,
                processData : false,
                contentType : false,
                cache       : false,
                success: function(data){
                    var obj = $.parseJSON(data);

                    if (obj.status == true) {
                        $('.category-modal').modal('hide');
                        loadCategory();
                        $('.error-message-success').html(obj.message).show();
                    } else {
                        $('.error-msg').html(obj.message).show();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.categorybutton', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');
        $.ajax({
            type    : "POST",
            data    : {id : id},
            url     : ADMIN_URL+'/category/setup/',
            success : function(data) {   
                var responsedata  = $.parseJSON(data);
                $('.category-frm').html(responsedata.category_html);
                $('.category-modal').modal('show');
           }
       });
    });

    $(document).on('click', '.category-remove-btn', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');

        if(confirm("Are you sure you want to delete this?")) {
            $.ajax({
                type    : "POST",
                data    : {id : id},
                url     : ADMIN_URL+'/category/delete/',
                success : function(data) {   
                    var responsedata  = $.parseJSON(data);

                    if(responsedata.status == true) {
                        pageURL = mainURL;
                        $('.error-message-success').html(responsedata.message).show();
                        loadCategory();
                    } else {
                        $('.error-message-danger').html(responsedata.message).show(); 
                    }
                }
           });
       } else {
           return false;
       }
    });

    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        pageURL     = $(this).attr('href');
        var search  = $('.category_search').val();

        if(search != '') {
            loadCategory(search);
        } else {
            loadCategory();
        }
    });
    
    $('.category_search').keyup(function(){
        var search = $(this).val();

        if(search != '') {

            if(search.length > 2 ) {
                loadCategory(search);
            }
        } else {
            loadCategory();
        }
    });
});

function loadCategory(search) {

    $.ajax({
        type    : "POST",
        data    : { search : search },
        url     : pageURL,
        success : function(data) {   
            var responsedata  = $.parseJSON(data);
            $('.categorylist').html(responsedata.category_html);
            initSortable();
       }
    });
}
function initSortable() {
    if($( "#category_sort" ).length > 0) {
        $( function() {
            $("#category_sort").sortable({
                delay: 150,
                update:  function (event, ui) {
                    var selectedData = new Array();
                    $('.row_position>div').each(function() {
                        selectedData.push($(this).attr("id"));
                    });
                    updateOrder(selectedData);
                    
                }
            });
            $( "#category_sort" ).disableSelection();
        });
    }
}
function updateOrder(data) {
    $.ajax({
        url: base_url+'admin/category/sorting',
        type: 'post',
        data: {position: data},
        success: function () {
            showMessage('success', "Category position has been updated");
        }
    });
}