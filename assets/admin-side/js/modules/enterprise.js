$(document).ready(function() {

    load_enterprise();
    
    
    $(document).on('change','.enterpriseImage',function () {
        read_url(this,'category_img');
        $('.category_img_parent').removeClass('d-none');
    });

    $(document).on('click', '.delete-restaurant-image', function(e){
        e.preventDefault();
        var $this           = $(this);

        var enterprise_id   = $this.data('id');

        if (! confirm("Are you sure you want to delete?")) {
           return false;
        } else {
            if(enterprise_id != '') {
                $.ajax({
                    url     : ADMIN_URL+'/enterprise/remove_image/',
                    type    : 'POST',
                    data    : {"enterprise_id" : enterprise_id},
                    success:function(response){
                        var data = $.parseJSON(response);
                        if ((data.status == true)) {
                            $('input.enterpriseImage').val("");
                            $('.category_img_parent').addClass('d-none');
                        } else {
                            $('input.enterpriseImage').val("");
                            $('.category_img_parent').addClass('d-none');
                        }
                     }
                });
            } else {
                $('input.enterpriseImage').val("");
                $('.category_img_parent').addClass('d-none');
            }
        }
    });

    $(document).on('keyup', '.enterprise-name', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var e_slug = ans;
        
        var ans = base_url+'enterprise/'+ans
        
        $('.slug').val(e_slug);
        $('.url_name').val(ans);
    });
    
    
    $(document).on('click', '.enterprise-button', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');
        $.ajax({
            type    : "POST",
            data    : {id : id},
            url     : ADMIN_URL+'/enterprise/setup/',
            success : function(data) {   
                var responsedata  = $.parseJSON(data);
                $('.enterprise-modal').html(responsedata.html);
                $('.enterprise-modal').modal('show');
                init_validation();
           }
       });
    });
    
    $(document).on('submit','.enterprise-frm', function(e){
        e.preventDefault();
        var formAction  = $(this).attr('action');
        var formData    = new FormData(this);
        
        if($(this).valid()) {

            $.ajax({
                type    : "POST",
                url     : formAction,
                data    : formData,
                processData : false,
                contentType : false,
                cache       : false,
                success: function(data){
                    var obj = $.parseJSON(data);

                    if (obj.status == true) {
                        $('.enterprise-modal').modal('hide');
                        load_enterprise();
                        $('.error-message-success').html(obj.message).show();
                    } else {
                        $('.error-message-danger').html(obj.message).show();
                    }
                }
            });
        }
    });
    
    $(document).on('click', '.enterprise-remove', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');

        if(confirm("Are you sure you want to delete this?")) {
            $.ajax({
                type    : "POST",
                data    : {id : id},
                url     : ADMIN_URL+'/enterprise/delete/',
                success : function(data) {   
                    var responsedata  = $.parseJSON(data);

                    if(responsedata.status == true) {
                        load_enterprise();
                         $('.error-message-success').html(responsedata.message).show();
                    } else {
                         $('.error-message-danger').html(responsedata.message).show();
                    } 
                }
           });
       } else {
           return false;
       }
    });
    
    
});

function init_validation() {
    $(".enterprise-frm").validate({
        rules: {
            name: {
              required: true
            },
            url_name: {
              required: true
            },
            // avatar: {
            //    requiredImage:true
            // }
        },
        messages: {
            name: {
                required : "Please enter name"
            },
            url_name: {
                required : "Please enter URL name"
            },
            // avatar: {
            //     requiredImage: ""
            // }
            
        }
    }); 

    // $.validator.addMethod("requiredImage", function(value, element) {
    //     var returnValue = true;

    //     if ($('.enterprise_id').val() == '' && $('.enterpriseImage').val() == '') {
    //         $('.avatar-error').html("PLEASE SELECT IMAGE");
    //         returnValue = false;
    //     }
    //     return returnValue;
    // });    
}


function load_enterprise() {
    if ($('#enterprise_table').length > 0) {
        $('#enterprise_table').DataTable({
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "destroy"    : true,
            "ajax": {
                "url": ADMIN_URL + '/enterprise',
                "type": "POST"
            },
            "order": [[0, 'desc']],
            "columns": [
                { "data": "name" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}

function read_url(input, imgclass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.'+imgclass).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}