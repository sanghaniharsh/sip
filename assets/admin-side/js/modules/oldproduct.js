var pageURL;
$(document).ready(function() {
    if ($('.select2').length > 0) {
        $(".select2").select2();
    }
    setTimeout(function() {
        loadProducts();
        loadQuestion();
        loadAddonOptions();
        loadAddonProducts();
    }, 1000);

    pageURL = base_url+'admin/oldproduct/index';

    $('.add_question_btn').on('click', function(e){
        e.preventDefault();
        openQuestionModal('');
    });

    $(document).on('click', '.edit-question', function(e){
        e.preventDefault();
        openQuestionModal($(this).data('id'));
    });

    $(document).on('click', '.add_more_option', function(e){
        e.preventDefault();
        var option_html = '\n\
            <div class="form-group row">\n\
                <label class="col-md-2 col-form-label" for="options[]">Option</label>\n\
                <div class="col-md-8">\n\
                    <input class="form-control blog_category_slug" name="options[]" placeholder="Enter option" type="text" value="" autocomplete="off">\n\
                </div>\n\
                <div class="col-md-2">\n\
                    <button class="btn btn-danger ripple remove_option">Remove</button>\n\
                </div>\n\
            </div>\n\
        ';

        $('.option_box').append(option_html);
    });
    
    $(document).on('click', '.remove_option', function(e) {
        e.preventDefault();
        $(this).parents('.form-group').remove();
    });

    $('.question_form').on('submit', function(e) {
        e.preventDefault();
        if ($(this).valid()) {
            $.ajax({
                type: "POST",
                url : $(this).attr('action'),
                data: $(this).serialize(),
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('.question_modal').modal('hide');
                        loadQuestion();
                    } else {
                        $('.q-error-msg').show().html(response.message);
                    }
                }
            });
        }
    });

    $(document).on('click', '.remove-question', function(e) {
        e.preventDefault();

        if (confirm('Are you sure, You wants to delete question?')) {
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/oldproduct/removeQuestion',
                data: {question_id : $(this).data('id')},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        loadQuestion();
                    }
                }
            });
        }

    });

    $(document).on('click', '.remove_specification_pdf', function(e) {
        e.preventDefault();
        var $this = $(this);

        if (confirm('Are you sure, You wants to delete specification PDF?')) {
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/product/removeSpecificationPDF',
                data: {product_id : $(this).data('id')},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $this.parents('.form-group').remove();
                    }
                }
            });
        }
    });

    $('input[name="shipping_option"]').on('change', function(e) {
        e.preventDefault();

        var shipping_value = $(this).val();

        if (shipping_value == 'free' || shipping_value == 'contact_us') {
            $('.shipping_price_box').addClass('hide');
            $('.ups_box').addClass('hide');
        } else if (shipping_value == 'manual') {
            $('.ups_box').addClass('hide');
            $('.shipping_price_box').removeClass('hide');
        } else if (shipping_value == 'ups_shipping') {
            $('.ups_box').removeClass('hide');
            $('.shipping_price_box').addClass('hide');
        }
    });

    $('.question_form2').validate({
        rules: {
            "options[]": {
                required: true
            },
            question: {
                required: true
            }
        },
        messages: {
            "options[]": {
                required: 'Please enter option value'
            },
            question: {
                required: "Pleae enter question"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

//    $('.productForm').validate({
//        rules: {
//            "category[]": {
//                required: true
//            },
//            slug: {
//                required: true
//            },
//            pname: {
//                required: true
//            },
//            pModalNumber: {
//                required: true
//            },
//            pprice: {
//                required: true,
//                number: true
//            },
//            lprice: {
//                required: true,
//                number: true
//            },
//            video_url: {
//                url: true
//            }
//        },
//        messages: {
//            "category[]": {
//                required: "Please choose category"
//            },
//            slug: {
//                required: "Please enter url"
//            },
//            pname: {
//                required: "Please enter product name"
//            },
//            pModalNumber: {
//                required: "Please enter Product Modal Number"
//            },
//            pprice: {
//                required: "Please enter product price",
//                number: "Please enter numerical price"
//            },
//            lprice: {
//                required: "Please enter list price",
//                number: "Please enter numerical price"
//            },
//            video_url: {
//                url: "Please enter valid url"
//            }
//        },
//        errorPlacement: function(error, element) {
//            error.insertAfter(element);
//        }
//    });

//    $('.productDetailForm').validate({
////        ignore: [],
////        debug: false,
//        rules: {
//            description: {
//                required: true
//            },
//            specification: {
//                required: true
//            },
//            warranty: {
//                required: true
//            }
//        },
//        messages: {
//            description: {
//                required: "Please enter product description"
//            },
//            specification: {
//                required: "Please enter product specification"
//            },
//            warranty: {
//                required: "Please enter product warranty"
//            }
//        },
//        errorPlacement: function(error, element) {
//            error.insertAfter(element);
//        }
//    });


    $('.productDetailForm').on('submit', function(e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        if ($(this).valid()) {
            var formData    = new FormData($(this)[0]);
//            var formData    = $(this).serialize();
            var url         = $(this).attr('action');
            $.ajax({
                type: "POST",
                url : url,
                data: formData,
                processData: false,
                contentType: false,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('.error-msg').hide();
                        $('.success-msg').show().text(response.message);
                    } else {
                        $('.success-msg').hide();
                        $('.error-msg').show().text(response.message);
                    }

                    $('html, body').animate({
                        scrollTop: $("body").offset().top
                    }, 2000);
                }
            });
        }
    });
    
    $('.tabs-block').on('click', function(e) {
        $('.error-msg').hide();
        $('.success-msg').hide();
    });
    
    $(".product_images").change(function(){
        $('.productImageForm').trigger("submit");
         
    });
    $('.photoUpload').on('drop',function(){
        $('.productImageForm').trigger("submit");
    }
    );

    $('.productImageForm').on('submit', function(e) {
        e.preventDefault();
        var product_id = $('.product_id').val();
        if (product_id != '') {
            var formData    = new FormData(this);
            var files_data  = $('.productImageForm .product_images');
            $.each($(files_data), function(i, obj) {
                $.each(obj.files,function(j, file){
                    formData.append('product_images[' + j + ']', file);
                })
            });

            $.ajax({
                url:ADMIN_URL+"/oldproduct/updateProductImage",
                type:'POST',
                data : formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success:function(response){
                    response = $.parseJSON(response);
                    if (response.status == true) {
                        setTimeout(function(){  
                            $('.fileinput-remove:not([disabled])').trigger("click");
                        }, 500);
                         $('.file-preview-status').hide();
                        $('.product_image_block').html(response.html);
                        $('.productImageForm')[0].reset();

                        $('.error-msg').hide();
                        $('.success-msg').show().text(response.message);
                    } else {
                        $('.success-msg').hide();
                        $('.error-msg').show().text(response.message);
                    }
                }
            });
        }
    });

    $(document).on('click', '.delete-product-image', function(e){
        e.preventDefault();
        var $this       = $(this);
        var image_id    = $this.attr('rel');

        if (! confirm("Are you sure you want to delete?")) {
           return false;
        } else {
            $.ajax({
                url:base_url+"admin/oldproduct/deleteProductImage",
                type:'POST',
                data: {"image_id" : image_id},
                success:function(response){
                    var data = $.parseJSON(response);
                    if ((data.status == true)) {
                        $this.parent().fadeOut('slow');
                        $('.alert-success').find('.success_text').text(data.message);
                    } else {
                        $('.alert-danger').find('.success_text').text(data.message);                        
                    }
                 }
            });     
        }
    });

    $(document).on('keyup', '.blog_category_slug', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        $('#slug').val(ans);
        $('#slugText').val(ans);
    });

    $.validator.addMethod("requiredImage", function(value, element) {
        var returnValue = true;

        if ($('.blog_id').val() == '' && $('.blog_images').val() == '') {
            returnValue = false;
        }

        return returnValue;
    }, "Please upload property images");

    $.validator.addMethod("checkSubCategory", function(value, element) {
        var returnValue = true;

        if ($('.blog_id').val() == '' && $('.blog_images').val() == '') {
            returnValue = false;
        }

        return returnValue;
    }, "Please upload property images");

    $(document).on('click', '.deleteProduct', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/product/delete',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        loadProducts();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.pagination-block .pagination li a',function(e){
        e.preventDefault();
        pageURL  = $(this).attr('href');

        var searchval           = $('.product_search').val();
        var search_category     = $('.search-category-list').val();

        if(searchval  != '' || search_category != '') {
            loadProducts(searchval,search_category);
        } else {
            loadProducts();
        }
    });

    $(document).on('change', '.search-category-list',function(e){
        e.preventDefault();

        var search_category     = $('.search-category-list').val();
        var searchval          = $('.product_search').val('');

        if(search_category != '') {
            loadProducts('',search_category);
        }
    });

    $(document).on('click', '.product-reset-btn',function(e){
        $('.product_search').val('');
        $('.search-category-list').val('');
        loadProducts();
    });

    $('.product_search').keyup(function(){
        var searchval           = $(this).val();
        var search_category     = $('.search-category-list').val();

        if(searchval != "") {

            if(searchval.length > 2 ) {
                loadProducts(searchval,search_category);
            } 
        } else {
            loadProducts('',search_category);
        }

    });

    $('.addon_product_form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url : $(this).attr('action'),
            data: $(this).serialize(),
            async : true,
            success:function(response){
                var data = $.parseJSON(response);
                if (data.status == true) {
                    $('.addon_product').val("0").trigger('change');
                    loadAddonProducts();
                    loadAddonOptions();
                    $('.error-msg').hide();
                    $('.success-msg').show().text(data.message);
                } else {  
                    $('.success-msg').hide().text(data.message);                    
                    $('.error-msg').show().text(data.message); 
                }
            }
        });
    });
    $(document).on('click', '.delete_addon_product', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/oldproduct/addon_product_delete',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        loadAddonProducts();
                        loadAddonOptions();
                        $('.error-msg').hide();
                        $('.success-msg').show().text(response.message);
                    } else {  
                        $('.success-msg').hide().text(response.message);                    
                        $('.error-msg').show().text(response.message); 
                    }
                }
            });
        } else {
            return false;
        }
    });
});

function loadProducts(searchval,search_category) {
    if ($(".product-block").length > 0) {
        $.ajax({
            type    : 'POST',
            data    : { searchval : searchval ,search_category : search_category},
            url     :  pageURL,
            success : function(data) {
                var obj = $.parseJSON(data);
                if (obj.status == true && obj.html != '') {
                    $(".product-block").html(obj.html);
                }
            }
        });
    }
}
function loadQuestion() {
    $.ajax({
        type    : 'POST',
        url     :  ADMIN_URL+'/product/loadQuestion/'+product_id,
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".question_box").html(obj.html);
            }
        }
    });
}
function openQuestionModal(question_id) {
    $.ajax({
        type    : 'POST',
        data    : {question_id : question_id},
        url     :  ADMIN_URL+'/product/openQuestion/'+product_id,
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".question_modal .modal-body").html(obj.html);
                $('.question_modal').modal('show');
            }
        }
    });
}
function loadAddonProducts() {
    var product_id  = $('.addon_product_id').val();
    $.ajax({
        type    : 'POST',
        data    : {product_id: product_id},
        url     :  base_url+'admin/oldproduct/addon_product_list',
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".addon-product-block").html(obj.html);
            }
        }
    });
}
function loadAddonOptions() {
    var product_id  = $('.addon_product_id').val();
    $.ajax({
        type    : 'POST',
        data    : {product_id: product_id},
        url     :  ADMIN_URL+'/oldproduct/addon_product_options/',
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html !="") {
                $(".addon_product").html(obj.html);
                $(".addon_product").trigger('change');
            } else {
               $(".addon_product").html('');
               $(".addon_product").trigger('change'); 
            }
        }
    });
}