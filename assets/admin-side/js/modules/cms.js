$(document).ready(function() {
    setTimeout(function() {
        loadCmsPages();
    }, 1000);

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000);
            
        })
    }
    
    $('#title').on('keyup', function(e){
        e.preventDefault();
        var url = $(this).val();
        url = url.replace(/\s+/g, '-').toLowerCase();
        url = url.replace("'","");
        url = url.replace('"',"");
        $('#url').val(url);
        $('#urlText').val(base_url+'page/'+url);
    });
    
    $('#CmsForm').validate({
        rules: {
            title: {
                required: true,
                remote : {
                    url: base_url+'admin/cms/validate',
                    type: "POST",
                    async:false,
                    data: {
                            title: function()
                            {
                                return $('#CmsForm :input[name="title"]').val();
                            },
                            id: function()
                            {
                                return $('#CmsForm :input[name="cmsId"]').val();
                            }
                        }
                    }
            },
            url: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please enter a Title",
                remote: "Page already exists."
            },
            url: {
                required: "Please enter a URL"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "terms" ) {
                $(element).parent('div').after(error);  
            } else {
                error.insertAfter(element);
            }
        }
    });

    $(document).on('click','.status_checks',function(e) {

        e.preventDefault();
        var current_element = $(this);
        var id              = $(current_element).data('id');
        $.ajax({
          type:"POST",
          url: ADMIN_URL+'/cms/checkStatus',
          data: { id :  id },
          success: function(response) {   
            response    = $.parseJSON(response);
            if (response.status) {
                loadCmsPages();
            }
          }
        });
      return false;
    });

    $(document).on('click', '#delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this = $(this);
            var myId  = $this.data('id');

            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/cms/delete',
                data:{
                     id:myId
                },
                success:function(response){
                    response    = $.parseJSON(response);
                    if ((response.status == true)) {
                        loadCmsPages();
                    }
                }
            });
        } else {
            return false;
        }
    });
    
});

function loadCmsPages() {
    if ($('#cmsDataTable').length > 0) {
        $('#cmsDataTable').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL+'/cms',
                "type": "POST"
            },
            "order": [[4, 'desc']],
            "columns": [
                { "data": "title", "width": "23%" },
                { "data": "url", "width": "20%" },
                { "data": "created_by", "width": "12%" },
                { "data": "created_date", "width": "12%" },
                { "data": "modify_date", "width": "12%" },
                { "data": "status", "width": "10%" },
                { "data": "action", "bSortable": false, "width": "20%" }
            ]
        });
    }
}