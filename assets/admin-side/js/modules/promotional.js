
$(document).ready(function() {
    setTimeout(function() {
        loadPromotional();
    }, 1000);

    $('#promotionalForm').validate({
        rules: {
            title: {
                required: true
            },
            promo_code: {
                required: true,
                remote : {
                        url: ADMIN_URL+'/promotional/validate',
                        type: "POST",
                        async:false,
                        data: {
                                promo_code: function()
                                {
                                    return $('#promotionalForm :input[name="promo_code"]').val();
                                },
                                id: function()
                                {
                                    return $('#promotionalForm :input[name="promo_id"]').val();
                                }
                            }
                        }
            },
            use_number: {
                required: true,
                number: true,
                min: 1
            },
            type: {
                required: true
            },
            discount: {
                required: true,
                number: true
            }
        },
        messages: {
            title: {
                required: "Please enter promotional title"
            },
            promo_code: {
                required: "Please enter promo code",
                remote  : "Promo code already exist!"
            },
            type: {
                required: "Please select type of discount"
            },
            discount: {
                required: "Please enter discount",
                number:"Please enter only numeric value in descount"
            },
            use_number: {
                required: "Please enter usage number of code",
                number: "Please enter only numeric value in descount",
                min: "should be grater then Zero"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

    $(document).on('click','.status_checks',function(e) {

        e.preventDefault();
        var current_element = $(this);
        $.ajax({
          type:"POST",
          url: ADMIN_URL+'/promotional/checkStatus',
          data: { id : $(current_element).attr('data') },
          success: function(response) {   
            response    = $.parseJSON(response);
            if (response.status) {
                if(response.value == '0') {
                    $(current_element).text("Deactive");
                    $(current_element).addClass('badge-danger').removeClass('badge-success');
                } else if(response.value == '1') {
                    $(current_element).text("Active");
                    $(current_element).addClass('badge-success').removeClass('badge-danger');
                }
            }
          }
        });
      return false;
    });

   $(document).on('click', '#delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/promotional/delete',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    loadPromotional();
                }
            });
        } else {
            return false;
        }
    });

    $('#type').on('change', function(){
       var $this = $(this);
       var type  = $this.val();
       if (type == '0') {
           $('.discount-type-text').text("In (%)");
       } else if (type == '1') {
           $('.discount-type-text').text("In ($)");
       }
    });
});

function loadPromotional() {
    if ($('#promotionalDataTable').length > 0) {
        $('#promotionalDataTable').DataTable({
            "destroy"    : true,
            "processing" : true,
            "serverSide" : true,
            'pageLength' : 10,
            "ajax": {
                "url": ADMIN_URL+'/promotional',
                "type": "POST"  
            },
            "order": [[4, 'desc']],
            "columns": [
                { "data": "title" },
                { "data": "promo_code" },
                { "data": "type" },
                { "data": "discount" },
                { "data": "created_date" },
                { "data": "status" },
                { "data": "action", "bSortable": false }
            ]
        });
    }
}
