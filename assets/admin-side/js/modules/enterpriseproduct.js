var pageURL;

$(document).ready(function() {

    init_validation();
    loadProducts();
    init_select();


    var tab_name  = localStorage.getItem('tab_name');
    if (typeof tab_name != 'undefined' && tab_name == 'product_tab' ) {
        $('.products-tab').trigger("click");
        localStorage.removeItem('tab_name');
    }
    pageURL         = base_url+'admin/enterprise-product';
    
    $(document).on('click', '.pagination-block .pagination li a',function(e){
        e.preventDefault();
        pageURL  = $(this).attr('href');

        var searchval           = $('.product_search').val();
        var search_category     = $('.search-enterprise-list').val();

        if(searchval  != '' || search_category != '') {
            loadProducts(searchval,search_category);
        } else {
            loadProducts();
        }
    });

    $(document).on('change', '.search-enterprise-list',function(e){
        e.preventDefault();

        var search_category     = $(this).val();

        if(search_category != '') {
            loadProducts('',search_category);
        }
    });

    $(document).on('click', '.product-reset-btn',function(e){
        $('.product_search').val('');
        $('.search-category-list').val('');
        loadProducts();
    });

    $(".nav-link").click(function(){
        var product_id = $('.enterprise_product_id').val();
        if(product_id != ''){
                $('.tab-content .tab-pane').removeClass('active');
                $('.tab-content .tab-pane').attr("aria-expanded", "false");
                var id = $(this).attr("href").replace("#", "") ;
                $("#" + id).addClass("active");
                $('#'+id).attr("aria-expanded", "true");
        }
    });

    $(document).on('click', '.enterpriseProduct',function(e){
        e.preventDefault();
        var check_tab_name = localStorage.getItem('tab_name');
        localStorage.setItem('tab_name', 'product_tab');
        if (typeof check_tab_name != 'undefined' && check_tab_name == 'product_tab') {
            localStorage.removeItem('tab_name');
        } else {
            if ($(".enterpriseproductForm").length > 0) {
                $('.enterpriseproductForm').submit();
                tab_redirect();
            }
        }
    });

    $(document).on('keyup','.product_search',function(){
        var searchval           = $(this).val();
        var search_category     = $('.search-enterprise-list').val();
        if(searchval != "") {
            if(searchval.length > 2 ) {
                loadProducts(searchval,search_category);
            } 
        } else {
            loadProducts('',search_category);
        }

    });
    
    
    $(document).on('click', '.deleteProduct', function(e) {
        e.preventDefault();
        var id   = $(this).attr('data-id');

        if(confirm("Are you sure you want to delete this?")) {
            $.ajax({
                type    : "POST",
                data    : {id : id},
                url     : ADMIN_URL+'/enterprise-product/delete/',
                success : function(data) {   
                    var responsedata  = $.parseJSON(data);
                    if(responsedata.status == true) {
                        loadProducts();
                         $('.error-message-success').html(responsedata.message).show();
                    } else {
                         $('.error-message-danger').html(responsedata.message).show();
                    } 
                }
           });
       } else {
           return false;
       }
    });
    
    $(document).on('keyup', '.enterprise-product', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        $('#slug').val(ans);
        $('#slugText').val(ans);
    });
    
    
    $('.productDetailForm').on('submit', function(e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        if ($(this).valid()) {
            var formData    = new FormData($(this)[0]);
//            console.log(formData); return false;
//            var formData    = $(this).serialize();
            var url         = $(this).attr('action');
            $.ajax({
                type: "POST",
                url : url,
                data: formData,
                processData: false,
                contentType: false,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        $('.error-msg').hide();
                        $('.success-msg').show().text(response.message);
                        $('.images-tab').trigger("click");
                    } else {
                        $('.success-msg').hide();
                        $('.error-msg').show().text(response.message);
                    }
                    $('html, body').animate({
                        scrollTop: $("body").offset().top
                    }, 2000);
                }
            });
        }
    });

    $('.productDetailForm').validate({
//        ignore: [],
//        debug: false,
        rules: {
            description: {
                required: true
            },
            specification: {
                required: true
            },
            warranty: {
                required: true
            }
        },
        messages: {
            description: {
                required: "Please enter product description"
            },
            specification: {
                required: "Please enter product specification"
            },
            warranty: {
                required: "Please enter product warranty"
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });
    
    $('.productImageForm').on('submit', function(e) {
        e.preventDefault();
        var product_id = $('.product_id').val();
        if (product_id != '') {
            var formData    = new FormData(this);
            var files_data  = $('.productImageForm .product_images');
            $.each($(files_data), function(i, obj) {
                $.each(obj.files,function(j, file){
                    formData.append('product_images[' + j + ']', file);
                })
            });

            $.ajax({
                url:ADMIN_URL+"/enterprise-product/updateProductImage",
                type:'POST',
                data : formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success:function(response){
                    response = $.parseJSON(response);
                    if (response.status == true) {
                        setTimeout(function(){  
                            $('.fileinput-remove:not([disabled])').trigger("click");
                        }, 500);
                        $('.file-preview-status').hide();
                        $('.product_image_block').html(response.html);
                        $('.productImageForm')[0].reset();

                        $('.error-msg').hide();
                        $('.success-msg').show().text(response.message);
                    } else {
                        $('.success-msg').hide();
                        $('.error-msg').show().text(response.message);
                    }
                }
            });
        }
    });
    
    $(document).on('click', '.delete-product-image', function(e){
        e.preventDefault();
        var $this       = $(this);
        var image_id    = $this.attr('rel');

        if (! confirm("Are you sure you want to delete?")) {
           return false;
        } else {
            $.ajax({
                url:base_url+"admin/enterprise-product/deleteProductImage",
                type:'POST',
                data: {"image_id" : image_id},
                success:function(response){
                    var data = $.parseJSON(response);
                    if ((data.status == true)) {
                        $this.parent().fadeOut('slow');
                        $('.alert-success').find('.success_text').text(data.message);
                    } else {
                        $('.alert-danger').find('.success_text').text(data.message);                        
                    }
                 }
            });     
        }
    });
    
    $(document).on('change','.awarded_price',function(e) {
        e.preventDefault();
        if ($(this).is(":checked")) {
            $('.ep-price').hide();
        } else {
            $('.ep-price').show();
        }

    });


    
});

function init_validation() {
    
    if($(".enterpriseproductForm").length > 0) {
        
        $(".enterpriseproductForm").validate({
            rules: {
                name: {
                  required: true
                },
                price : {
                  required: true
                },
                model_number : {
                  required: true
                },
            },
            messages: {
                name: {
                    required : "Please enter name"
                },
                price: {
                    required : "Please enter name"
                },
                model_number: {
                    required : "Please Select model number"
                },
            }
        }); 
    }
}

function loadProducts(searchval,search_category) {
    if($(".product-block").length > 0) {
        $.ajax({
            type    : 'POST',
            data    : { searchval : searchval ,search_enterprise : search_category},
            url     :  pageURL,
            success : function(data) {
                var obj = $.parseJSON(data);
                if (obj.status == true && obj.html != '') {
                    $(".product-block").html(obj.html);
                }
            }
        });
    }
}
// function tab_active_deactive() {
//     if($(".enterprise_product_id").length > 0) {
//         var id = $('.enterprise_product_id').val();
//         $(".products-tab").addClass("disabled");
//         $(".images-tab").addClass("disabled");

//         if(id != '') {
//            $(".products-tab").removeClass("disabled");
//         //    $(".images-tab'").removeClass("disabled");
//         }
//     }
// }
function init_select() {
    $('.enterprise_product').multiselect({
        // includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'Search for something...',
    });
}