var pageURL;
$(document).ready(function() {
    setTimeout(function() {
        loadNews();
    }, 1000);
    
    pageURL = base_url+'admin/news/index';

    $.validator.addMethod("requiredImage", function(value, element) {
        $('.avatar-error').html("");
        var returnValue = true;

        if ($('.news_id').val() == '' && $('.news_image').val() == '') {
            returnValue = false;
            $('.avatar-error').html("Please upload property images");
        } else if (this.optional(element) || ! element.files || ! element.files[0]) {
            returnValue = true;
        } else {
            returnValue = element.files[0].size <= (1024 * 1024 * allowed_filesize_mb);
            if (! returnValue) {
                $('.avatar-error').html("The file size can not exceed "+allowed_filesize_mb+"MB.");
            }
        }
        return returnValue;
    });
    
    $(".news_image").on('change', function(){
        var news_id = $('.news_id').val();
        if (news_id != '' && ($("#newsForm").valid() == true)) {
            var formData    = new FormData($("#newsForm")[0]);
            $.ajax({
                url:ADMIN_URL+"/news/updateNewsImage",
                type:'POST',
                data : formData,
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success:function(response){
                    response = $.parseJSON(response);
                    if (response.status == true) {
                        $('.news-image').attr('src', response.image);

                        $('.error-msg').css("display", "none");
                        $('.success-msg').text(response.message);
                        $('.success-msg').css("display", "block");
                    } else {
                        $('.success-msg').css("display", "none");
                        $('.error-msg').text(response.message);
                        $('.error-msg').css("display", "block");
                    }
                }
            });
        }
    });

    $('.blog_category_slug').on('keyup', function(e){
        e.preventDefault();
        var slug    = $(this).val();
        var str     = slug.toLowerCase();
        var ans     = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        $('#slug').val(ans);
        $('#slugText').val(ans);
    });

    $('#newsForm').validate({
        rules: {
            title: {
                required: true
            },
            slug: {
                required: true
            },
            news_image: {
                requiredImage: true
            },
            description: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please enter news title"
            },
            slug: {
                required: "Please enter news slug"
            },
            news_image: {
                requiredImage: ""
            },
            description: {
                required: "Please enter news description"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "news_image") {
                $(".img-error").html(error);
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    $(document).on('click', '.delete', function(e){
        var result = confirm("Want to delete?");
        if (result) {
            e.preventDefault();
            var $this       = $(this);
            var myId        = $this.data('id');
            $.ajax({
                type: "POST",
                url : ADMIN_URL+'/news/delete',
                data:{ id : myId},
                async : true,
                success:function(response){
                    response    = $.parseJSON(response);
                    if (response.status == true) {
                        pageURL = base_url+'admin/news/index';
                        loadNews();
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.pagination-block .pagination li a',function(e){
        e.preventDefault();
        pageURL  = $(this).attr('href');
        var search  = $('.search_news').val();

        if(search != '') {
            loadNews(search);
        } else {
            loadNews();
        }
    });

    $('.search_news').keyup(function(){
        var search = $(this).val();

        if(search != '') {

            if(search.length > 2 ) {
                loadNews(search);
            }
        } else {
            loadNews();
        }
    });
});

function loadNews(search) {

    $.ajax({
        type    : 'POST',
        data    : {search : search},
        url     :  pageURL,
        success : function(data) {
            var obj = $.parseJSON(data);
            if (obj.status == true && obj.html != '') {
                $(".news_block").html(obj.html);
            }
        }
    });
}