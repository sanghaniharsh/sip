-- 27-03-2023 --
ALTER TABLE `angel_enterprise_users` ADD `sales_tax` DOUBLE(9,2) NOT NULL DEFAULT '0.00' AFTER `visible_password`;
-- 23-12-2022 --

-- create angel_enterprise, angel_enterprise_product, angel_enterprise_product_images


-- 14-12-2022--

-- Executed
ALTER TABLE `angel_auction` ADD `auction_type` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0 - offline, 1 - online' AFTER `equipment_pdf`;

ALTER TABLE `angel_auction` CHANGE `a_date` `a_date` VARCHAR(255) NOT NULL, CHANGE `a_time` `a_time` VARCHAR(255) NOT NULL;


