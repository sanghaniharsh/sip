<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['admin/brand/(:num)']                = "admin/brand/$1";
$route['admin/blog/(:num)']                 = "admin/admin/blog/$1";
$route['admin/category/(:num)']             = "admin/admin/category/$1";
$route['admin/auction/(:num)']              = "admin/admin/auction/$1";

$route['admin/profile']                     = "admin/admin/profile";
$route['admin/dashboard']                   = "admin/admin/dashboard";
$route['admin/admin-users']                 = "admin/admin/users";
$route['admin/admin-users/setup']           = "admin/admin/setup";
$route['admin/admin-users/setup/(:any)']    = "admin/admin/setup/$1";
$route['admin/admin-users/commit/(:any)']   = "admin/admin/commit/$1";
$route['admin/admin-users/commit']          = "admin/admin/commit";
$route['admin/forgot-password']             = "admin/admin/forgot_password";
$route['admin/site-settings']               = "admin/admin/site_settings";
$route['admin/reset-password/(:any)']       = "admin/admin/reset_password/$1";
$route['admin/logout']                      = "admin/admin/logout";
$route['admin/login']                       = "admin/admin/check_login";
$route['admin']                             = "admin/admin";

$route['admin/enterprise-product/setup']                = "admin/enterpriseProduct/setup";
$route['admin/enterprise-product/setup/(:any)']         = "admin/enterpriseProduct/setup/$1";
$route['admin/enterprise-product/commit/(:any)']        = "admin/enterpriseProduct/commit/$1";
$route['admin/enterprise-product/commit']               = "admin/enterpriseProduct/commit";
$route['admin/enterprise-product/commit-specification/(:any)'] = "admin/enterpriseProduct/commit_specification/$1";
$route['admin/enterprise-product/updateProductImage']   = "admin/enterpriseProduct/updateProductImage";
$route['admin/enterprise-product/deleteProductImage']   = "admin/enterpriseProduct/deleteProductImage";
$route['admin/enterprise-product/delete']               = "admin/enterpriseProduct/delete";
$route['admin/enterprise-product']                      = "admin/enterpriseProduct/index";
$route['admin/enterprise-product/(:any)']               = "admin/enterpriseProduct/index/$1";

$route['admin/enterprise-user/(:any)']                  = "admin/enterpriseUser/index/$1";
$route['admin/enterprise-user/setup/(:any)']            = "admin/enterpriseUser/setup/$1";
$route['admin/enterprise-user/setup/(:any)/(:any)']     = "admin/enterpriseUser/setup/$1/$2";
$route['admin/enterprise-user/commit/(:any)']           = "admin/enterpriseUser/commit/$1";
$route['admin/enterprise-user/commit/(:any)/(:any)']    = "admin/enterpriseUser/commit/$1/$2";
$route['admin/enterprise-user/validate/(:any)']         = "admin/enterpriseUser/validate/$1";
$route['admin/enterprise-user/delete/(:any)']           = "admin/enterpriseUser/delete/$1";

$route['admin/enterprise-order']                        = "admin/enterpriseOrder/index";
$route['admin/enterprise-order/order-detail/(:any)']    = "admin/enterpriseOrder/order_detail/$1";
$route['admin/enterprise-order/update-order-status']    = "admin/enterpriseOrder/update_order_status";
$route['admin/enterprise-order/delete']                 = "admin/enterpriseOrder/delete";

$route['enterprise/check_login']                        = "enterprise/auth/check_login";
//$route['enterprise/(:any)']                             = "enterprise/home/index/$1";
$route['enterprise']                                    = "enterprise/home/index/$1";
$route['enterprise/(:any)']                             = "enterprise/product/index/$1";
$route['enterprise/(:any)/check_login']                 = "enterprise/auth/check_login";
$route['enterprise/(:any)/logout']                      = "enterprise/auth/logout";

$route['enterprise/(:any)/myaccount']                   = "enterprise/home/myaccount";
$route['enterprise/(:any)/get-user-orders']             = "enterprise/home/get_user_orders";

$route['enterprise/(:any)/order-email/(:any)']                = "enterprise/order/order_email/$2";


$route['enterprise/(:any)/order-details/(:any)']        = "enterprise/home/order_details/$2";

$route['enterprise/(:any)/reorder/(:any)']                     = "enterprise/order/reorder/$2";

$route['enterprise/(:any)/product']                     = "enterprise/product/index";
$route['enterprise/(:any)/product-list']                = "enterprise/product/product_list";

$route['enterprise/(:any)/product-list/(:any)']         = "enterprise/product/product_list/$2";
$route['enterprise/(:any)/cart-remove-item']            = "enterprise/product/cart_delete_item";



$route['enterprise/(:any)/product/(:any)']              = "enterprise/product/detail/$2";
$route['enterprise/(:any)/add-cart-item']               = "enterprise/order/add_cart_item";
$route['enterprise/(:any)/add-to-cart-item']            = "enterprise/order/add_to_cart_item";

$route['enterprise/(:any)/add-cart-block']              = "enterprise/product/add_cart_block";
$route['enterprise/(:any)/product-details']             = "enterprise/product/product_details";
$route['enterprise/(:any)/product-details/product-info-modal']       = "enterprise/product/product_modal_popup";
$route['enterprise/(:any)/cart']                        = "enterprise/order/cart";
$route['enterprise/(:any)/cart-delete-item']            = "enterprise/order/cart_delete_item";

$route['enterprise/(:any)/cart-update-qty']             = "enterprise/order/cart_update_qty";

$route['enterprise/(:any)/checkout']                    = "enterprise/order/checkout";
$route['enterprise/(:any)/thankyou']                    = "enterprise/order/thankyou";

$route['brand/(:num)']                          = "brand/index/$1";
$route['brand/(:any)']                          = "brand/brand_products/$1";
$route['brand/get_brand_produts/(:any)']        = "brand/get_brand_products/$1";
$route['brand/get_brand_produts/(:any)/(:num)'] = "brand/get_brand_products/$1/$2";

$route['news/(:num)']                   = "news/index/$1";

$route['auction/(:num)']                = "auction/index/$1";

$route['blog/most_viewed']              = "blog/most_viewed";
$route['blog/(:num)']                   = "blog/index/$1";
$route['blog/(:any)']                   = 'blog/view/$1';

$route['category']                      = "category/index";
$route['category/view']                 = "category/view";
$route['category/view/(:num)']          = "category/view/$1";
$route['category/(:any)']               = 'category/index/$1';

$route['product/add_cart_block']        = "product/add_cart_block";
$route['product/related_product']       = "product/related_product";
$route['product/(:any)']                = "product/detail/$1";

$route['search/getProducts']            = "search/getProducts";
$route['search/getProducts/(:num)']     = "search/getProducts/$1";

$route['search/(:any)']                 = 'search/index/$1';
$route['search_new/(:any)']             = 'search_new/index/$1';

$route['big-dipper-parts/(:num)']       = "dipper_product/index/$1";
$route['big-dipper-parts']              = "dipper_product";

$route['small-wares']                   = "page/smallwares";
$route['grease-traps']                  = "page/greasetraps";
$route['restaurant-design']             = "page/restaurant_design";
$route['delivery']                      = "page/delivery";
$route['delivery-thank-you']            = "page/thank_you";
$route['delivery-pdf']                  = "page/delivery_pdf";
$route['used-equipment/(:num)']         = "usedproduct/index/$1";
$route['used-equipment']                = "usedproduct";
$route['campaign']                      = "page/campaign";
$route['our-work']                      = "page/our_work";
$route['about-us']                      = "page/about_us";
$route['myaccount']                     = "home/myaccount";
$route['contact-us']                    = "page/contact_us";
$route['login']                         = "auth/login";
$route['logout']                        = "auth/logout";
$route['register']                      = "auth/register";
$route['forgot-password']               = "auth/forgot_password";
$route['reset-password/(:any)']         = "auth/reset_password/$1";
$route['order-details/(:any)']          = "home/order_details/$1";

$route['page/(:any)']                   = "page/index/$1";
$route['default_controller']            = 'home';
$route['404_override']                  = 'home/notFound';
$route['translate_uri_dashes']          = FALSE;
