<?php

class Ups {

    public function check_shipping($postal_code= "", $type = "", $params = array()) {
        $return = array();
        
        if ($postal_code != '' && ! empty($params)) {
            $xml_data = $this->generate_xml($postal_code, $type, $params);
            $ch = curl_init("https://www.ups.com/ups.app/xml/Rate");
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            $result     = curl_exec($ch);

            if ($result != '') {
                $return = $this->parse_response($result);
            } else {
                $return['status']   = false;
                $result['message']  = curl_error($ch);
            }
        } else {
            $return['status']   = false;
            $return['message']  = "Parameter missing";
        }

        curl_close($ch);

        return $return;
    }

    public function parse_response($result) {
        $return = array();

        if ($result != '') {
            error_reporting(0);

            $data       = strstr($result, '<?');
            $xml_parser = xml_parser_create();
            xml_parse_into_struct($xml_parser, $data, $vals, $index);
            xml_parser_free($xml_parser);
            $params = array();
            $level  = array();
            foreach ($vals as $xml_elem) {
                if ($xml_elem['type'] == 'open') {
                    if (array_key_exists('attributes', $xml_elem)) {
                        list($level[$xml_elem['level']], $extra) = array_values($xml_elem['attributes']);
                    } else {
                        $level[$xml_elem['level']] = $xml_elem['tag'];
                    }
                }
                if ($xml_elem['type'] == 'complete') {
                    $start_level = 1;
                    $php_stmt = '$params';
                    while ($start_level < $xml_elem['level']) {
                        $php_stmt .= '[$level[' . $start_level . ']]';
                        $start_level++;
                    }
                    $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';

                    eval($php_stmt);
                }
            }
            
            if (isset($params['RATINGSERVICESELECTIONRESPONSE']['RESPONSE']['ERROR'])) {
                $return['status']   = false;
                $return['message']  = $params['RATINGSERVICESELECTIONRESPONSE']['RESPONSE']['ERROR']['ERRORDESCRIPTION'];
            } else if (isset($params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'])) {
                $return['status']       = true;
                $return['shipping_rate']= $params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'];
            } else {
                $return['status']   = false;
                $return['message']  = "Something went wrong";
            }
        } else {
            $return['status'] = false;
            $return['message']= "No result found";
        }

        return $return;
    }
    
    public function generate_xml($postal_code= "", $type = "", $params = array()) {
        $xml = "";

        if ($postal_code != '' && ! empty($params)) {
            $AccessLicenseNumber = '9D71D265204E7932'; // Your license number
            $UserId         = 'rep465'; // Username
            $Password       = 'Paradise99'; // Password
            $from_postcode  = '06108'; // Zipcode you are shipping FROM
            $ShipperNumber  = '98765'; // Your UPS shipper number
            $service        = '03';
            $address_type   = "";

            if ($type == 1) {
                $address_type = "<ResidentialAddressIndicator/>";
            }

            $package = "";
            foreach($params as $row) {
                $package .= "
                    <Package>
                        <PackagingType>
                            <Code>02</Code>
                        </PackagingType>
                        <Dimensions>
                            <UnitOfMeasurement>
                            <Code>IN</Code>
                            </UnitOfMeasurement>
                            <Length>".$row['length']."</Length>
                            <Width>".$row['width']."</Width>
                            <Height>".$row['height']."</Height>
                        </Dimensions>
                        <PackageWeight>
                            <UnitOfMeasurement>
                            <Code>LBS</Code>
                            </UnitOfMeasurement>
                            <Weight>".$row['weight']."</Weight>
                        </PackageWeight>
                    </Package>
                ";
            }
            
            if ($package != '') {
                $xml = "<?xml version=\"1.0\"?>
                            <AccessRequest xml:lang=\"en-US\">
                                <AccessLicenseNumber>$AccessLicenseNumber</AccessLicenseNumber>
                                <UserId>$UserId</UserId>
                                <Password>$Password</Password>
                            </AccessRequest>
                        <?xml version=\"1.0\"?>
                            <RatingServiceSelectionRequest xml:lang=\"en-US\">
                                    <Request>
                                            <TransactionReference>
                                            <CustomerContext>Product Shipping Rate Request</CustomerContext>
                                            <XpciVersion>1.0001</XpciVersion>
                                            </TransactionReference>
                                            <RequestAction>Rate</RequestAction>
                                            <RequestOption>Rate</RequestOption>
                                    </Request>
                                <PickupType>
                                        <Code>01</Code>
                                </PickupType>
                                <Shipment>
                                    <Shipper>
                                        <Address>
                                            <PostalCode>$from_postcode</PostalCode>
                                            <CountryCode>US</CountryCode>
                                        </Address>
                                        <ShipperNumber>$ShipperNumber</ShipperNumber>
                                    </Shipper>
                                    <ShipTo>
                                        <Address>
                                            <PostalCode>$postal_code</PostalCode>
                                            <CountryCode>US</CountryCode>
                                            ".$address_type."
                                        </Address>
                                    </ShipTo>
                                    <ShipFrom>
                                        <Address>
                                            <PostalCode>$from_postcode</PostalCode>
                                            <CountryCode>US</CountryCode>
                                        </Address>
                                    </ShipFrom>
                                    <Service>
                                            <Code>$service</Code>
                                    </Service>
                                    ".$package."
                                </Shipment>
                            </RatingServiceSelectionRequest>";
            }    
        }

        return $xml;
    }
}