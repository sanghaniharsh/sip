<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Code Igniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		Rick Ellis
 * @copyright	Copyright (c) 2006, pMachine, Inc.
 * @license		http://www.codeignitor.com/user_guide/license.html
 * @link		http://www.codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * PayPal_Lib Controller Class (Paypal IPN Class)
 *
 * This CI library is based on the Paypal PHP class by Micah Carrick
 * See www.micahcarrick.com for the most recent version of this class
 * along with any applicable sample files and other documentaion.
 *
 * This file provides a neat and simple method to interface with paypal and
 * The paypal Instant Payment Notification (IPN) interface.  This file is
 * NOT intended to make the paypal integration "plug 'n' play". It still
 * requires the developer (that should be you) to understand the paypal
 * process and know the variables you want/need to pass to paypal to
 * achieve what you want.  
 *
 * This class handles the submission of an order to paypal as well as the
 * processing an Instant Payment Notification.
 * This class enables you to mark points and calculate the time difference
 * between them.  Memory consumption can also be displayed.
 *
 * The class requires the use of the PayPal_Lib config file.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Commerce
 * @author      Ran Aroussi <ran@aroussi.com>
 * @copyright   Copyright (c) 2006, http://aroussi.com/ci/
 *
 */

// ------------------------------------------------------------------------

class paypal_lib {
	
	var $CI;
        protected $paypal_username; //PayPal API username
        protected $paypal_password; //PayPal API password
        protected $paypal_signature; //PayPal API signature
        protected $apiVersion = "64"; //Set PayPal API version
    //    If you are using live environment use the following URL: https://api-3t.paypal.com/nvp 
    //    If you are using sandbox environment then use the following URL: https://api-3t.sandbox.paypal.com/nvp
        protected $paypal_nvp_url;
        protected $errorReportingEnabled    = true;
        protected $errors                   = array(); //Here you can find errors for your last API call 
        protected $lastServerResponse; //Here you can find PayPal response for your last successfull API call
        protected $curl;

	function __construct()
	{
            $this->CI =& get_instance();
            $this->CI->load->helper('url');
            $this->CI->load->helper('form');
            $this->CI->load->config('paypallib_config');

            $paypal_type            = $this->CI->config->item('PAYPALTYPE');
            $this->paypal_url       = ($paypal_type == '0')? 'https://www.sandbox.paypal.com/cgi-bin/webscr':'https://www.paypal.com/cgi-bin/webscr';

            $this->paypal_nvp_url   = ($paypal_type == '0') ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp';

            $this->paypal_username  = $this->CI->config->item('SANDBOXUSERNAME');
            $this->paypal_password  = $this->CI->config->item('SANDBOXPASSWORD');
            $this->paypal_signature = $this->CI->config->item('SANDBOXSIGNATURE');
            if ($paypal_type == '1') {
                $this->paypal_username  = $this->CI->config->item('PAYPALUSERNAME');
                $this->paypal_password  = $this->CI->config->item('PAYPALPASSWORD');
                $this->paypal_signature = $this->CI->config->item('PAYPALSIGNATURE');
            }

	}
/**
     * The SetExpressCheckout API operation initiates an Express Checkout transaction. Look here to see method parameters: https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/
     * @param array $request Array should contain key value pairs defined by PayPal
     * @return array Response from the PayPal saved in the array and returned from the function
     */
    public function SetExpressCheckout($request) {
        
        $requestData = $this->setRequestFields($request);
        return $this->sendRequest($requestData, "SetExpressCheckout");
    }
    /**
     * The DoExpressCheckoutPayment API operation completes an Express Checkout transaction. If you set up a billing agreement in your SetExpressCheckout API call, the billing agreement is created when you call the DoExpressCheckoutPayment API operation.The DoExpressCheckoutPayment API operation completes an Express Checkout transaction. If you set up a billing agreement in your SetExpressCheckout API call, the billing agreement is created when you call the DoExpressCheckoutPayment API operation. Look here to see method parameters: https://developer.paypal.com/docs/classic/api/merchant/DoExpressCheckoutPayment_API_Operation_NVP/
     * @param array $request Array should contain key value pairs defined by PayPal
     * @return array Response from the PayPal saved in the array and returned from the function
     */
    public function DoExpressCheckoutPayment($request) {
        return $this->sendRequest($request, "DoExpressCheckoutPayment");
    }
    /**
     * Calles GetExpressCheckoutDetails PayPal API method. This method gets buyer and transaction data. This method WILL NOT- make transaction itself. Look here for method parameters: https://developer.paypal.com/docs/classic/api/merchant/GetExpressCheckoutDetails_API_Operation_NVP/
     * @param array $request Array should contain key value pairs defined by PayPal
     * @return array Response from the PayPal saved in the array and returned from the function
     */
    public function GetExpressCheckoutDetails($request) {
        return $this->sendRequest($request, "GetExpressCheckoutDetails");
    }
    
    
     public function DoAuthorization($request){
        return $this->sendRequest($request, "DoAuthorization");
    }
    
    /* AUTORIZATION AND CAPTURE METHODS*/
    /**
     * Captures an authorized payment. You can read more about authorization and payment: https://developer.paypal.com/docs/classic/admin/auth-capture/
     * @param array $request
     * @return array
     */
    public function DoCapture($request){
        return $this->sendRequest($request, "DoCapture");
    }
    /**
     * The DoReauthorization API operation reauthorizes an existing authorization transaction. The resulting reauthorization is a new transaction with a new AUTHORIZATIONID. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/DoReauthorization_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function DoReauthorization($request){
        return $this->sendRequest($request, "DoReauthorization");
    }
    /**
     * Void an order or an authorization. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/DoVoid_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function DoVoid($request){
        return $this->sendRequest($request, "DoVoid");
    }
    
    /**
     * To see method arguments visit this link: 
     * @param array $request
     * @return array
     */
    public function UpdateAuthorization($request){
        return $this->sendRequest($request, "UpdateAuthorization");
    }
    
    
    /*Recurring Payments / Reference Transactions*/
    
    /**
     * The BAUpdate API operation updates or deletes a billing agreement. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/BAUpdate_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function BAUpdate($request){
        return $this->sendRequest($request, "BAUpdate");
    }
    /**
     * The BillOutstandingAmount API operation bills the buyer for the outstanding balance associated with a recurring payments profile. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/BillOutstandingAmount_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function BillOutstandingAmount($request){
        return $this->sendRequest($request, "BillOutstandingAmount");
    }
    /**
     * The CreateBillingAgreement API operation creates a billing agreement with a PayPal account holder. CreateBillingAgreement is only valid for reference transactions. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/CreateBillingAgreement_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function CreateBillingAgreement($request){
        return $this->sendRequest($request, "CreateBillingAgreement");
    }
    /**
     * The DoReferenceTransaction API operation processes a payment from a buyer's account, which is identified by a previous transaction. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/DoReferenceTransaction_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function CreateRecurringPaymentsProfile($request){
        return $this->sendRequest($request, "CreateRecurringPaymentsProfile");
    }
    /**
     * The DoReferenceTransaction API operation processes a payment from a buyer's account, which is identified by a previous transaction. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/DoReferenceTransaction_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function DoReferenceTransaction($request){
        return $this->sendRequest($request, "DoReferenceTransaction");
    }
    
    /**
     * Obtain information about a recurring payments profile. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/GetRecurringPaymentsProfileDetails_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function GetRecurringPaymentsProfileDetails($request){
        return $this->sendRequest($request, "GetRecurringPaymentsProfileDetails");
    }
    /**
     * The ManageRecurringPaymentsProfileStatus API operation cancels, suspends, or reactivates a recurring payments profile. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/ManageRecurringPaymentsProfileStatus_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function ManageRecurringPaymentsProfileStatus($request){
        return $this->sendRequest($request, "ManageRecurringPaymentsProfileStatus");
    }
    /**
     * The UpdateRecurringPaymentsProfile API operation updates a recurring payments profile. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/UpdateRecurringPaymentsProfile_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function UpdateRecurringPaymentsProfile($request){
        return $this->sendRequest($request, "UpdateRecurringPaymentsProfile");
    }
    /**
     * The RefundTransaction API operation issues a refund to the PayPal account holder associated with a transaction. This API operation can be used to issue a full or partial refund for any transaction within a default period of 60 days from when the payment is received. To see method arguments visit this link: https://developer.paypal.com/docs/classic/api/merchant/RefundTransaction_API_Operation_NVP/
     * @param array $request
     * @return array
     */
    public function RefundTransaction($request){
        return $this->sendRequest($request, "RefundTransaction");
    }

    private function setRequestFields($request = array()){

        $items  = array();
        if (isset($request['item']) && !empty($request['item'])  ) {
            foreach ($request['item'] as $key => $item) {
                $items['L_PAYMENTREQUEST_'.$key.'_NAME'.$key] = $item['name'];
                $items['L_PAYMENTREQUEST_'.$key.'_DESC'.$key] = $item['desc'];
                $items['L_PAYMENTREQUEST_'.$key.'_QTY'.$key] = $item['qty'];
                $items['L_PAYMENTREQUEST_'.$key.'_AMT'.$key] = $item['amount'];
            }
        }

        $profile_config = array(
            
            'BRANDNAME' => $request['brand_name'],
            'LOGOIMG'   => $request['brand_logo'], 

            'PAYMENTREQUEST_0_AMT'          => $request['amount'],
            'PAYMENTREQUEST_0_CURRENCYCODE' => $request['currencyCode'],
            'PAYMENTREQUEST_0_PAYMENTACTION'=> 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT'      => $request['amount'],
            'PAYMENTREQUEST_0_INVNUM'       => $request['inv_number'],
            'PAYMENTREQUEST_0_CUSTOM'       => isset($request['custom']) ? $request['custom'] : "-",

            'CANCELURL'                     => $request['cancel_url'],
            'RETURNURL'                     => $request['return_url'],
            'BUTTONSOURCE'                  => 'BR_EC_EMPRESA'
        );

        $profile_config = array_merge($profile_config, $items);

        return $profile_config;
    }

    /**
     * This method makes calls PayPal method provided as argument.
     * @param array $requestData
     * @param string $method
     * @return array 
     */
    public function sendRequest($requestData, $method) {
        
        if (!isset($method)) {
            array_push($this->errors, "Method name can not be empty");
        }
        if (!isset($requestData)) {
            array_push($this->errors, "Request data is can not be empty");
        }
        if ($this->checkForErrors()) {//If there are errors, STOP
            if ($this->errorReportingEnabled())//If error reporting is enabled, show errors
                $this->showErrors();

            $this->lastServerResponse = null;
            return false; //Do not send a request
        }
        $requestParameters = array(
            "USER"      => $this->paypal_username,
            "PWD"       => $this->paypal_password,
            "SIGNATURE" => $this->paypal_signature,
            "METHOD"    => $method,
            "VERSION"   => $this->apiVersion,
        );
        $requestParameters+=$requestData;
//        echo "<pre>"; print_r($requestParameters); die;
        $finalRequest = http_build_query($requestParameters);
        $ch         = curl_init();
        $this->curl = $ch;

        $curlOptions = $this->getcURLOptions();
        $curlOptions[CURLOPT_POSTFIELDS]=$finalRequest;
        //var_dump($curlOptions);exit;
        
        curl_setopt_array($ch, $curlOptions);
        $serverResponse = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->errors = curl_error($ch);
            curl_close($ch);
            if ($this->errorReportingEnabled) {
                $this->showErrors();
            }
            $this->lastServerResponse = null;
            return false;
        } else {
            curl_close($ch);
            $result = array();
            parse_str($serverResponse, $result);
            $this->lastServerResponse = $result;
            return $this->lastServerResponse;
        }
    }

    public function procced_paypal($nvp = array()) {
        if (!empty($nvp)) {
            if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
                $query = array(
                    'cmd'    => '_express-checkout',
                    'token'  => $nvp['TOKEN']
                );

                $redirectURL = sprintf($this->paypal_url.'?%s', http_build_query($query));

                header('Location: ' . $redirectURL);
            } else {
                return $nvp;
                //Opz, alguma coisa deu errada.
                //Verifique os logs de erro para depuração.
            }
        } else {
            return $nvp;
        }
    }

    /**
     * Returns latest result from the PayPal servers
     * @return array
     */
    public function getLastServerResponse() {
        return $this->lastServerResponse;
    }
    /** 
     * Call this function if you want to retreave errors occured during last API call
     * @return void Prints all errors during last API call.
     */
    public function showErrors() {
        var_dump($this->errors);
    }
    /**
     * 
     * @param string $username Set your PayPal API username
     */
    public function setUsername($username) {
        $this->username = $username;
    }
    /**
     * 
     * @param string $password Set your PayPal API password
     */
    public function setPassword($password) {
        $this->password = $password;
    }
    /**
     * 
     * @param string $apiSignature Set your PayPal API signature
     */
    public function setApiSignature($apiSignature) {
        $this->apiSignature = $apiSignature;
    }
    /**
     * Call this function if you want to disable error reporting
     */
    public function disableErrorReporting(){
        $this->errorReportingEnabled=false;
    }
    /**
     * Call this function if you want to enable error reporting
     */
    public function enableErrorReporting(){
        $this->errorReportingEnabled=true;
    }

    /**
     * 
     * @return boolean Checks if there are errors and returns the true/false
     */
    private function checkForErrors() {
        
        if(!is_array($this->errors) && $this->errors!="") return TRUE;
        
        if (count($this->errors) > 0) {
            return true;
        }
        return false;
    }
    /**
     * Returns an array of options to initialize cURL
     * @return array
     */
    private function getcURLOptions() {
        return array(
            CURLOPT_URL     => $this->paypal_nvp_url,
            CURLOPT_VERBOSE => 1,
            //Have a look at this: http://stackoverflow.com/questions/14951802/paypal-ipn-unable-to-get-local-issuer-certificate
            //You can download a fresh cURL pem file from here http://curl.haxx.se/ca/cacert.pem
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2,
//            CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', //CA cert file
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
        );
    }
    
    /**
     * If you want to set cURL with additional parameters, use this function. NOTE: Call this function prior sendRequest method
     * @param int $option
     * @param mixed $value
     */
    public function setCURLOption($option, $value){
        curl_setopt($this->curl, $option, $value);
    }
}

?>
