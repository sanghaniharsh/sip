<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usedproduct extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }
    
     public function index($start = 0) {
        $search_category = array();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $searchval          = $this->input->get('search');
            $search_category    = $this->input->get('search_category');
            if($searchval != "") {
               $sfilter['search']      = $searchval;
            }
            if($search_category != "") {
                $sfilter['category_ids']= explode(",", $search_category);
                
                $c_filter['where_in']   = array(
                                            array('field' => 'id', 'value' => $sfilter['category_ids'])
                                        );
                $c_filter['select']     = array('id', 'name', 'slug');
                $search_category        = $this->used_category_model->get_rows($c_filter);
            }
        }
        $perpage            = 20;
        $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
        $sfilter['active']  = '1';
        $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);
        $sfilter['type']    = 1;
        $result             = $this->product_model->getProducts($sfilter);
        $data['product_list']  = $result['product'];
        $data['total']      = $result['total'];

        //pagination
        $config                     = initPagination();
        $config["base_url"]         = base_url() . "used-equipment/";
        $config["total_rows"]       = $data['total'];
        $config["per_page"]         = $perpage;
        $config['uri_segment']      = 2;
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $data['total'];
        $this->pagination->initialize($config);

        $str_links              = $this->pagination->create_links();
        $data["links"]          = $str_links;

        $cFilter['result']      = 1;
        $cFilter['orderby']     = array('field' => 'name', 'order' => 'ASC');
        $data['categoryData']   = $this->used_category_model->get_rows($cFilter);
        $data['search_category']= $search_category;
        $this->template->view("front/used_equipment/index", $data);
         
    }
}
?>