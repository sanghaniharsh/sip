<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_new extends MY_Controller {

    public function __construct() {
        parent::__construct();
        echo phpinfo();die;
        // $this->load->library('dpaypal_lib');
    }

    public function checkout() {
        $data           = array();
        $ip_address     = $this->input->ip_address();
        $promocode      = 'FULL100';

        $cFilter['ip_address']  = $ip_address;
        $result                 = $this->cart_model->getItems($cFilter);

        if (!empty($result)) {
            $data['cart_items']         = $result;

            if ($promocode != '') {
                $pFilter['where']       = array('promo_code' => $promocode);
                $pFilter['row']         = 1;
                $promo_code             = $this->promotional_model->get_rows($pFilter);

                if (!empty($promo_code)) {
                    $data['promo_code'] = $promo_code;
                }
            }
            $this->template->view('front/order/checkout', $data);
        } else {
            not_found();
        }
    }

    public function commit() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
            $this->form_validation->set_rules('address', 'Phone', 'trim|required');
            $this->form_validation->set_rules('zipcode', 'Phone', 'trim|required');

            if ($this->form_validation->run() == TRUE) {
                $product                    = array();
                $promocode                  = $this->input->post('promocode');
                $ip_address                 = $this->input->ip_address();
                $cFilter['ip_address']      = $ip_address;
                $cart_details               = $this->cart_model->getItems($cFilter);

                if (!empty($cart_details)) {
                    $amount     = 0;
                    $discount   = 0;
                    foreach ($cart_details as $value) {
                        $amount     += $value->quantity * $value->price;
                        $product[]  = $value->product_id;
                    }

                    if ($promocode != '') {
                        $pData['promo_code']    = $promocode; 
                        $pData['total_amount']  = $amount; 
                        $promo_data             = $this->promotional_model->check_promocode($pData);

                        if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                            $amount     = $promo_data['total_amount'];
                            $discount   = $promo_data['total_discount'];
                        }
                    }

                    $email                      = $this->input->post('email');
                    $user_id                    = 0;

                    $uFilter['select']          = array('id');
                    $uFilter['where']           = array('email' => $email);
                    $uFilter['row']             = 1;
                    $result                     = $this->user_model->get_rows($uFilter);

                    if (!empty($result) && isset($result->id) && $result->id > 0) {
                        $user_id = $result->id;
                    } else {
                        $data['email']              = $email;
                        $data['firstname']          = $this->input->post('first_name');
                        $data['lastname']           = $this->input->post('last_name');
                        $data['phone']              = $this->input->post('phone');
                        $password                   = generateRandomString(10);
                        $data['password']           = encreptIt($password);
                        $data['visible_password']   = $password;
                        $data['is_active']          = '1';

                        $user_id                    = $this->user_model->insert($data);
                    }

                    $zipcode                = $this->input->post('zipcode');
                    $lFilter['select']      = array('city', 'state');
                    $lFilter['where']       = array('zipcode' => $zipcode);
                    $lFilter['row']         = 1;
                    $location_result        = $this->location_model->get_rows($lFilter);

                    if (!empty($location_result)) {
                        $user_data['user_id']           = $user_id;
                        $user_data['city']              = $location_result->city;
                        $user_data['state']             = $location_result->state;
                        $user_data['street_address']    = $this->input->post('address');
                        $user_data['zipcode']           = $this->input->post('zipcode');

                        $aFilter['where']       = array('user_id' => $user_id);
                        $aFilter['row']         = 1;
                        $userDetails            = $this->user_address_model->get_rows($aFilter);

                        if (!empty($userDetails)) {
                            $this->user_address_model->update_table($user_data, array('id' => $userDetails->id));
                        } else {
                            $this->user_address_model->insert($user_data);
                        }
                        
                        if ($user_id > 0 && $amount > 0 && !empty($product)) {
                            $data['user_id']    = $user_id;
                            $data['product_id'] = $product;
                            $data['amount']     = $amount;
                            $data['discount']   = $discount;
                            $data['promo_code'] = $promocode;
                            $this->payment($data);
                            die;
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Please fill user details');
                        redirect(base_url().'order_new/checkout');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please try again latter');
                    redirect(base_url().'order_new/checkout');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url().'order_new/checkout');
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong');
            redirect(base_url().'order_new/checkout');
        }
    }
    
    public function validate() {
        $email      = $this->input->post('email');
        $return     = FALSE;

        if ($email != '') {
            $filter['where']    = array('email' => $email);
            $result             = $this->user_model->get_rows($filter);

            if (!empty($result)) {
                $return = FALSE;
            } else {
                $return = TRUE;
            }
        }
        echo json_encode($return);die;
    }

    private function payment($param = array()){
//        if ($this->is_logined) {
        if (!empty($param)) {
        
            $returnURL          = base_url() . 'order/payment_success'; //payment success url
            $cancelURL          = base_url() . 'order/payment_cancel';

            $this->removeSession(array('products'));
            $data['products'] = array(
                                    'product_id'        => $param['product_id'],
                                    'amount'            => $param['amount'],
                                    'discount'          => $param['discount'],
                                    'user_id'           => $param['user_id'],
                                    'promo_code'        => $param['promo_code']
                                );
            $this->setSession($data);

            $finalAmount        = $param['amount'];
            $paypalemail        = $this->config->item('PAYPALEMAIL');
            $paypalcharge       = $this->config->item('PAYPALCHARGE');
            $paypalpayment      = $finalAmount + ($finalAmount * ($paypalcharge / 100));

            $paypal_url         = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
            $paypalType         = $this->config->item('PAYPALTYPE');

            if ($paypalType == '0') {
                $paypalemail    = $this->config->item('SANDBOXEMAIL');
                $paypal_url     = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
            }

            $requestParams = array(
                'RETURNURL'         => $returnURL, //Enter your webiste URL here
                'CANCELURL'         => $cancelURL, //Enter your website URL here
                'LOGOIMG'           => front_asset_url().'images/logo-main.png', //You can paste here your logo image URL
//                "MAXAMT"            => "100", //Set max transaction amount
                "NOSHIPPING"        => "1", //I do not want shipping
                "ALLOWNOTE"         => "0", //I do not want to allow notes
                "BRANDNAME"         => 'Resturant Equipment',
                "GIFTRECEIPTENABLE" => "0",
                "GIFTMESSAGEENABLE" => "0",
                'PAYMENTREQUEST_0_AMT'          => $paypalpayment,
                'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
                'PAYMENTREQUEST_0_ITEMAMT'      => $paypalpayment,
                'L_PAYMENTREQUEST_0_NAME0'      => 'Spotlight Rental',
                'L_PAYMENTREQUEST_0_DESC0'      => 'Spotlight Rental',
                'L_PAYMENTREQUEST_0_AMT0'       => $paypalpayment,
                'L_PAYMENTREQUEST_0_NUMBER0'    => '1',
                'L_PAYMENTREQUEST_0_QTY0'       => '1',
                    //"PAYMENTREQUEST_0_INVNUM" => $transaction->id - This field is useful if you want to send your internal transaction ID
            );
//            $response = $this->dpaypal_lib->SetExpressCheckout($requestParams);
            $response = true;
            if ($response) {
//            if (is_array($response) && $response['ACK'] == 'Success') { //Request successful

                //Now we have to redirect user to the PayPal
//                $token = $response['TOKEN'];
//                header("Location: $paypal_url" . urlencode($token));
                $url = base_url().'order_new/success';
                header("Location: $url");
                
            } else if (is_array($response) && $response['ACK'] == 'Failure') {
                echo 'else';die;
                $this->session->set_flashdata('error', 'Payment not success');
                redirect("/home");
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong, payment failed');
            redirect(base_url());
        }
//        } else {
//            redirect(base_url());
//        }
    }
    
    public function success() {
        $products   = $this->getSession('products');
        $promocode  = $products['promo_code'];
        
        $data['user_id']        = $products['user_id'];
        $data['amount']         = $products['amount'];
        $data['discount']       = $products['discount'];
        $data['created_date']   = date('Y-m-d H:i:s');
        $data['promo_code']     = $products['promo_code'];
        
        $order_id = $this->order_model->insert($data);
        
        $order_items = array();
        foreach ($products['product_id'] as $row) {
            $order_items[] = array('order_id' => $order_id, 'product_id' => $row);
        }

        $this->order_items_model->insert_betch($order_items);
        
        if ($promocode != '') {
            $pFilter['where']   = array('promo_code' => $products);
            $pFilter['row']     = 1;
            $result             = $this->promotional_model($pFilter);
            
            if (!empty($result)) {
                $promoData['promo_count']   = $result->promo_count + 1;
                $this->promotional_model->update_table($promoData, array('id' => $result->id));
            }
        }

        $where = array('ip_address' => $this->input->ip_address());
        $this->cart_model->delete($where);

        $this->removeSession(array('products'));
        $this->session->set_flashdata('success', 'Payment successfull.');
        redirect(base_url());
    }
    
    public function payment_success() {
        if (!empty($_GET) && $_GET['PayerID'] != '' && $_GET['token'] != '') {
            ini_set('max_execution_time', '0');
            $payer_id               = $_GET['PayerID'];
            $token                  = $_GET['token'];
            $requestParams          = array('TOKEN' => $_GET['token']); 
            $response               = $this->dpaypal_lib->GetExpressCheckoutDetails($requestParams);
            if (!empty($response) && ($response['ACK'] == 'Success')) {
                $payerId        = $response["PAYERID"];//Payer id returned by paypal
                //Create request for DoExpressCheckoutPayment
                $requestParams1 = array(
                        "TOKEN"                         => $response['TOKEN'],
                        "PAYERID"                       => $payerId,
                        "PAYMENTREQUEST_0_AMT"          => $response['AMT'],//Payment amount. This value should be sum of of item values, if there are more items in order
                        "PAYMENTREQUEST_0_CURRENCYCODE" => "USD",//Payment currency
                        "PAYMENTREQUEST_0_ITEMAMT"      => $response['AMT']//Item amount
                );

                $transactionResponse        = $this->dpaypal_lib->DoExpressCheckoutPayment($requestParams1);//Execute transaction
                if (!empty($transactionResponse)) {
p($transactionResponse);
                    $spotlight                  = $this->getSession('spotlight');
                    $data['item_id']            = $spotlight['item_id'];
                    $data['transaction_id']     = $transactionResponse['PAYMENTINFO_0_TRANSACTIONID'];
                    $data['transaction_amount'] = $transactionResponse['PAYMENTINFO_0_AMT'];
                    $data['transaction_status'] = $transactionResponse['PAYMENTINFO_0_PAYMENTSTATUS'];

                    $type                       = $spotlight['transaction_type'];
                    if ($type == 'urgent') {
                        $data['transaction_type'] = '0';
                    } else if ($type == 'featured') {
                        $data['transaction_type'] = '1';
                    } else if ($type == 'both') {
                        $data['transaction_type'] = '2';
                    }
                    $transaction_type           = $data['transaction_type'];
                    $data['createdDate']        = date("Y-m-d H:i:s");

                    $this->item_payments_model->insert($data);

                    $where              = array('id' => $spotlight['item_id']);
                    $itemdata           = array();
                    if ($transaction_type == '0') {
                        $itemdata['isUrgent']   = '1';
                    } else if ($transaction_type == '1') {
                        $itemdata['isFeatured'] = '1';
                    } else if ($transaction_type == '2') {
                        $itemdata['isUrgent']   = '1';
                        $itemdata['isFeatured'] = '1';
                    }

                    $this->item_model->update_table($itemdata, $where);

                    newItemInAreaEmail($spotlight['item_id']);
                    newItemSaveSearchUserEmail($spotlight['item_id']);

                    $this->removeSession(array('spotlight'));
                    $this->session->set_flashdata('success', 'Payment successfull.');
                    redirect(base_url() . 'item/my-ads');
                } else {
                    $this->session->set_flashdata('error', 'Payment Not completed. Something went wrong.');
                    redirect(base_url() . 'item/my-ads');
                }
            } else {
                $this->session->set_flashdata('error', 'Property not found Try Again later!');
                redirect(base_url() . 'item/my-ads');
            }
        } else {
            $this->session->set_flashdata('error', 'Payment not completed, Please try again later');
            redirect(base_url() . 'item/my-ads');
        }
        
    }

    public function payment_cancel() {
        $this->session->set_flashdata('error', 'Payment cancelled');
        redirect("/home");
    }
}

?>