<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index() {
        if (!$this->is_logined) {
            $this->template->view('admin/front_page');
        } else {
            redirect(admin_url().'dashboard');
        }
    }
    public function login() {
        if (!$this->is_logined) {
            $this->template->view('admin/login');
        } else {
            redirect(admin_url().'dashboard');
        }
    }
    public function check_login() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return         = $filter = array();
            $filter['where']= array('username' => trim($this->input->post('username')), 'password' => trim(md5($this->input->post('password'))));
            $filter['row']  = 2;
            $row            = $this->admin_model->get_rows($filter);
            if (empty($row)) {
                $filter['where']= array('email' => trim($this->input->post('username')), 'password' => trim(md5($this->input->post('password'))));
                $filter['row']  = 2;
                $row            = $this->admin_model->get_rows($filter);
            }
            if (!empty($row)) {
                if ($row['is_active'] == 1) {
                    $this->set_admin_user($row);
                    if ($row['user_type'] == '1') {
                        redirect(admin_url().'business');
                    } else {
                        redirect(admin_url().'dashboard');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Your account is not active.');
                    redirect(admin_url());
                }
            } else {
                $this->session->set_flashdata('error', 'Wrong Username or Password.');
                redirect(admin_url().'login');
            }
        } else {
            $this->template->view('admin/login');
        }
    }

    public function dashboard() {
        if ($this->is_logined) {
            $data               = array();
 
            $data['total_users']        = $this->user_model->get_rows(array(),true);

            $Ifilter['select']          = array('SUM(angel_order.amount) AS total_income');
            $Ifilter['row']             = 1;
            $Ifilter['where']           = array('angel_order.status' => 'Success');   
            $result_income              = $this->order_model->get_rows($Ifilter);
            $data['total_income']       = number_format($result_income->total_income, 2);

            $data['total_products']     = $this->product_model->get_rows(array(),true);
            
            $cofilter['where']          = array('angel_order.status' => 'Success');
            $data['total_orders']       = $this->order_model->get_rows($cofilter,true);
            $Ofilter['select']          = array('angel_order.*','u.email');
            $Ofilter['join']            = array(
                                            0 => array('table' => 'angel_users as u', 'condition' => 'u.id  = angel_order.user_id', 'type' => 'left')
            );
            $Ofilter['where']           = array('angel_order.status' => 'Success');
            $Ofilter['orderby']         = array('field' => 'angel_order.id', 'order' => 'DESC');
            $Ofilter['groupby']         = array('field' => 'angel_order.id');
            $Vdata['order']             = $this->order_model->get_rows($Ofilter);
            $data['order_html']         = $this->load->view('admin/admin/ajax_dashboard_orders_list', $Vdata, true);

            //state order count list 
            $Ufilter['select']          = array('angel_state.code', 'angel_state.name','COUNT(o.id) as orderCount');
            $Ufilter['join']            = array(
                                            0 => array('table' => 'angel_user_address  as ua', 'condition' => 'ua.state = angel_state.code', 'type' => 'left'),
                                            1 => array('table' => 'angel_order as o', 'condition' => 'o.user_id = ua.user_id', 'type' => 'left')
            );
            $Ufilter['groupby']         = array('field' => 'angel_state.code');
            $data['state_orders']       = $this->state_model->get_rows($Ufilter);

            $this->template->view("admin/dashboard",$data);
        } else {
            redirect(admin_url());
        }
    }

    public function profile() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $this->form_validation->set_rules('username', 'Username', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                
                if ($this->form_validation->run() == TRUE) {
                    $data['username']   = $this->input->post('username');
                    $data['email']      = $this->input->post('email');
                    $password           = $this->input->post('password');
                    $cpassword          = $this->input->post('cpassword');
                    if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
                        $id                 = $this->loginUser['id'];

                        if ($id > 0) {
                            $filter['where']= array('id' => $id);
                            $filter['row']  = 1;
                            $query          = $this->admin_model->get_rows($filter);

                            if (!empty($query)) {
                                if ($password != '' && $cpassword != '' && $password == $cpassword) {
                                    $data['password']           = md5($password);
                                    $data['visiblePassword']   = $password;
                                }
                                $this->admin_model->update_table($data, array('id' => $query->id));
                                
                                $filter['where']= array('id' => $id);
                                $filter['row']  = 2;
                                $row            = $this->admin_model->get_rows($filter);

                                if (!empty($row)) {
                                    $this->set_admin_user($row);
                                }
                                $this->session->set_flashdata('success', 'Profile updated successfully');
                            } else {
                                $this->session->set_flashdata('error', 'no record found');
                            }
                        } else {
                            $this->session->set_flashdata('error', 'somthing went wrong');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Please enter valid email address');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please fill required fields');
                }
                redirect(admin_url().'profile');
            }
            $loginUser      = $this->getLoginUser();
            $filter['where']= array('id' => $loginUser['id']);
            $filter['row']  = 1;
            $user           = $this->admin_model->get_rows($filter);
            if (!empty($user)) {
                $data['user'] = $user;
                $this->template->view("admin/admin/profile", $data);
            } else {
                redirect(admin_url());
            }
        } else {
            redirect(admin_url());
        }
    }

    public function logout() {
        $this->removeAdminSession();
        redirect(admin_url());
    }
    
    public function forgot_password() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $email  = $this->input->post('email');
            $return = array();
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

                $filter['where']= array('email' => trim($email));
                $filter['row']  = 1;
                $row            = $this->admin_model->get_rows($filter);

                if ((count($row) > 0)) {

                    $updateData                     = array();
                    $updateData['activation_code']  = md5($row->id);

                    $this->admin_model->update_table($updateData, array('email' => trim($email)));

                    $link = base_url().'admin/reset-password/'.md5($row->id);
                    
                    $viewData['email']          = $row->email;
                    $viewData['username']       = $row->username;
                    $viewData['activation_code']= $link;

                    $sent_data['subject']       = "Password Reset | Admin SIP";
                    $sent_data['email']         = $row->email;
                    $sent_data['message']       = $this->load->view('admin/admin/forgotPass', $viewData, true);
                    $this->sentMail($sent_data);

                    $return['status']   = true;
                    $return['message']  = 'You will get password reset link through your email';
                } else {
                    $return['status']   = false;
                    $return['message']  = 'Somthing went wrong';
                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Please enter valid email address';
            }
            echo json_encode($return);die;
        }
        $this->template->view("admin/forgot_password");
    }
    
    public function reset_password($token = '') {
        if ($token != '') {
            $filters['where']   = array('activation_code' => $token);
            $filters['row']     = 1;
            $admin_user         = $this->admin_model->get_rows($filters);

            if (! empty($admin_user)) {
                if ($this->input->server('REQUEST_METHOD') == 'POST') {
                    $password   = $this->input->post('password');
                    $cpassword  = $this->input->post('cpassword');
                    
                    if ($password == $cpassword) {
                        
                        $update_passsword = array(
                            'password'          => md5($password),
                            'visiblePassword'   => $password,
                            'activation_code'   => ''
                        );

                        $this->admin_model->update_table($update_passsword, array('id' => $admin_user->id));

                        $this->session->set_flashdata('success', 'Password changed successfully.');
                        redirect(admin_url());
                    } else {
                        $this->session->set_flashdata('error', 'Password and Confirm password does not match.');
                    }
                }

                $data               = array();
                $this->template->view('admin/reset_password', $data);
            } else {
                $this->session->set_flashdata('error', 'Invalid token.');
                redirect(base_url());
            }
        } else {
            redirect(admin_url());
        }
    }
    
    public function users() {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'username',
                                    '1' => 'email',
                                    '2' => 'password',
                                    '3' => 'created_date',
                                    '4' => 'is_active'
                                    );
                $filterCount    = $totalCount = $this->admin_model->get_rows(array(), true);
                
                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_admin.username', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_admin.email', 'value' => $searchString),
                        '1' => array('field' => 'angel_admin.visiblePassword', 'value' => $searchString),
                        '2' => array('field' => 'angel_admin.created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->admin_model->get_rows($filter, true);
                }
                $filter['where']    = array('id !=' => $this->loginUser['id'], 'user_type' => 0);
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->admin_model->get_rows($filter);
                $admin              = array();

                foreach ($query as $row) {
                    $row_data               = array();
                    $row_data['id']         = $row->id;
                    $row_data['username']   = $row->username;
                    $row_data['password']   = $row->visiblePassword;
                    $row_data['email']      = $row->email;
                    $row_data['created_date']= date('Y/m/d H:i:s', strtotime($row->created_date));
                    $row_data['is_active']  = $row->is_active;
                    $row_data['status']     = ($row->is_active == '0') ? '<a class="badge badge-danger status_checks" data="'.$row->id.'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.$row->id.'"  title="click to Deactive">Active</a>';
                    $row_data['action']     = ' <a class="color-content" href="'.base_url().'admin/admin-users/setup/'.md5($row->id).'"><i class="material-icons">edit</i></a>
                                                        <a class="color-content" data-Id="'.md5($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $admin[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $admin;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/admin/index');
            }
        } else {
            redirect('admin');
        }
    }

    public function setup($id = '') {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            $data = array();
            if ($id != '') {
                $filter['where']= array('md5(id)' => $id);
                $filter['row']  = 1;
                $user           = $this->admin_model->get_rows($filter);
                if (!empty($user)) {
                    $data['user']   = $user;
                }
            }

            $this->template->view("admin/admin/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {

            $data['username']   = $this->input->post('username');
            $data['email']      = $this->input->post('email');
            $data['is_active']  = $this->input->post('is_active') == 'on' ? '1' : '0';
            $data['user_type']  = 0;
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
                if ($id != '') {
                    $password = $this->input->post('password');
                    if ($password != "") {
                        $data['password']       = md5($password);
                        $data['visiblePassword']= $password;
                    }

                    $this->admin_model->update_table($data, array('md5(id)' => $id));

                    $this->session->set_flashdata('success', 'User saved successfully.');
                } else {
                    $password               = $this->input->post('password');
                    $data['password']       = md5($password);
                    $data['visiblePassword']= $password;
                    $data['created_date']   = date('Y-m-d H:i:s');

                    $id = $this->admin_model->insert($data);
                    $id = md5($id);
                    if ($id != '') {
                        $this->session->set_flashdata('success', 'User saved successfully.');
                    } else {
                        $this->session->set_flashdata('success', 'Something went worng, Please try again later.');
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Please enter valid email address');
            }
            redirect(admin_url().'admin-users');
        } else {
            redirect(admin_url());
        }
    }
    
    public function validate() {
        $username   = $this->input->post('username');
        $email      = $this->input->post('email');
        $id         = $this->input->post('id');
        $filter     = array();

        if (isset($username) && ($username != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('md5(id) !=' => $id, 'username' => $username);
            } else {
                $filter['where'] = array('username' => $username);
            }
        }

        if (isset($email) && ($email != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('md5(id) !=' => $id, 'email' => $email);
            } else {
                $filter['where'] = array('email' => $email);
            }
        }
        $filter['row']  = 1;
        $result         = $this->admin_model->get_rows($filter);
        $return         = TRUE;

        if ($result != '') {
            if ($result->id != $this->loginUser['id']) {
                $return = FALSE;
            }
        }

        echo json_encode($return);
    }

    public function delete() {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('md5(id)' => $id);
            $uFilter['row']     = 1;
            $user               = $this->admin_model->get_rows($uFilter);

            if (!empty($user)) {
                $where = array('md5(id)' => $id);
                $this->admin_model->delete($where);                
                
                $return['status']   = true;
                $return['message']  = 'Admin User deleted successfully';
            } else {
                $return['status']   = false;
                $return['message']  = 'Somthing went wrong';
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }
    
    public function checkStatus() {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter['where']        = array('md5(id)' => md5($id),);
                $filter['row']          = 1;
                $item                   = $this->admin_model->get_rows($filter);

                if (!empty($item)) {
                    if ($item->is_active == '0') {
                        $data['is_active']   = '1';
                    } else {    
                        $data['is_active']   = '0';
                    }

                    $this->admin_model->update_table($data, array('md5(id)' => md5($item->id)));
                    $data['status']   = TRUE;
                    $data['message']  = 'Admin User deleted successfully';
                } else {
                    $data['status']   = FALSE;
                    $data['message']  = 'Somthing went wrong';
                }
                echo json_encode($data);die;
            } else {
                redirect(admin_url());
            }
        }
    }
    
    public function site_settings() {
        if ($this->is_logined) {
            $data = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                
                $this->form_validation->set_rules('from_name', 'From name', 'trim|required');
                $this->form_validation->set_rules('from_email', 'From email', 'trim|required');
                $this->form_validation->set_rules('paypal_username', 'Paypal Username', 'trim|required');
                $this->form_validation->set_rules('paypal_signature', 'Paypal Signature', 'trim|required');
                $this->form_validation->set_rules('paypal_password', 'Paypal Password', 'trim|required');

                if ($this->form_validation->run() == TRUE) {

                    $data['from_name']                  = $this->input->post('from_name');
                    $data['from_email']                 = $this->input->post('from_email');
                    $data['paypalUsername']             = $this->input->post('paypal_username');
                    $data['paypalSignature']            = $this->input->post('paypal_signature');
                    $data['paypalPassword']             = $this->input->post('paypal_password');

                    $filter['where']        = array('id' => 1);
                    $getExistRow            = $this->sitesettings_model->get_rows($filter);
                    if (empty($getExistRow)) {
                        $data['id']     = 1;
                        $insertedId     = $this->sitesettings_model->insert($data);
                    } else {
                        $this->sitesettings_model->update_table($data, array('id' => 1));
                    }

                    $this->session->set_flashdata('success', 'Website settings updated successfully.');
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
                redirect('/admin/site-settings');
            }
            $filter2['where']   = array('id' => 1);
            $filter2['row']     = 1;
            $data['settings']   = $this->sitesettings_model->get_rows($filter2);
            $this->template->view("admin/site-settings", $data);
        } else {
            redirect('/admin');
        }
    }

    public function search_user() {
        $return = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $seach      = $this->input->post('search');

            $filter['like']     = array('field' => 'email', 'value' => $seach, 'type' => 'after');
            $filter['or_like']  = array(
                                    array('field' => 'first_name', 'value' => $seach, 'type' => 'after'),
                                    array('field' => 'last_name', 'value' => $seach, 'type' => 'after')
                                );

            $users           = $this->user_model->get_rows($filter);

            if (! empty($users)) {
                $data['users']      = $users;
                $return['html']     = $this->load->view('admin/admin/search_user', $data, true);
                $return['status']   = true;
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }

        echo json_encode($return);
    }
    
    public function block_user() {
        $return = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $block_id      = $this->input->post('block_id');

            if ($block_id != '') {
                $filter['where']= array('md5(id)' => $block_id);
                $filter['row']  = 1;
                $users          = $this->user_model->get_rows($filter);

                if (! empty($users)) {
                    $i_data['user_id']  = $users->id;
                    $insert_id          = $this->blocked_user_model->insert($i_data);
                    if ($insert_id > 0) {
                        $return['status']   = true;
                    } else {
                        $return['status'] = false;
                    }
                } else {
                    $return['status'] = false;
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }

        echo json_encode($return);
    }

    public function ublock_user() {
        $return = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $block_id      = $this->input->post('block_id');

            if ($block_id != '') {
                $filter['where']= array('md5(id)' => $block_id);
                $filter['row']  = 1;
                $users          = $this->user_model->get_rows($filter);

                if (! empty($users)) {
                    $i_data['user_id']  = $users->id;
                    $this->blocked_user_model->delete($i_data);
                    $return['status']   = true;
                } else {
                    $return['status'] = false;
                }
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }

        echo json_encode($return);
    }
    
    public function order_filter_daywise() {
        $return = array();

        if ($this->is_logined  && $this->input->server('REQUEST_METHOD') == 'POST') {

            $order_day      = $this->input->post('order_day');
            $Ofilter['select']          = array('angel_order.*','u.email');
            $Ofilter['join']            = array(
                                            0 => array('table' => 'angel_users as u', 'condition' => 'u.id  = angel_order.user_id', 'type' => 'left')
            );

            if ($order_day == 'today') { 
                $Ofilter['where']   = array('angel_order.status' => 'Success' , "DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') = "   => date('Y-m-d'));
            } else if ($order_day == 'yesterday') { 
                $Ofilter['where']   = array('angel_order.status' => 'Success' , "DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') = "   => date('Y-m-d',strtotime("-1 days")));
            } else if ($order_day == 'oneWeekago') { 
                $end_date   =   date('Y-m-d');
                $start_date =   date('Y-m-d',strtotime("-7 days"));
                $Ofilter['where']   = array('angel_order.status' => 'Success' ," DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') >= " =>  $start_date , "DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') <= "  =>   $end_date);
            } else if ($order_day == 'oneMonthago') { 
                $end_date   =   date('Y-m-d');
                $start_date =   date('Y-m-d',strtotime("-30 days"));
                $Ofilter['where']   = array('angel_order.status' => 'Success' ," DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') >= " =>  $start_date , "DATE_FORMAT(angel_order.created_date, '%Y-%m-%d') <= "  =>   $end_date);
            } else {
                $Ofilter['where']   = array('angel_order.status' => 'Success');
            }

            $Ofilter['orderby']         = array('field' => 'angel_order.id', 'order' => 'DESC');
            $Ofilter['groupby']         = array('field' => 'angel_order.id');
            $Vdata['order']             = $this->order_model->get_rows($Ofilter);
            $return['order_html']       = $this->load->view('admin/admin/ajax_dashboard_orders_list', $Vdata, true);
            $return['status']           = true;

            echo json_encode($return);
        } else {
            redirect('/admin');
        }
    }
}
?>