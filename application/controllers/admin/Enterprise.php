<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enterprise extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'name',
                                    '1' => 'created_date',
                                    '2' => 'status'
                                    );
                $filterCount    = $totalCount = $this->enterprise_model->get_rows(array(), true);
                
                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'name', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->enterprise_model->get_rows($filter, true);
                }
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->enterprise_model->get_rows($filter);
                $admin              = array();

                foreach ($query as $row) {
                    $row_data               = array();
                    $row_data['id']         = $row->id;
                    $row_data['name']       = $row->name;
                    $row_data['created_date']= date('Y/m/d H:i:s', strtotime($row->created_date));
                    $row_data['status']     = ($row->status == '0') ? '<a class="badge badge-danger status_checks" data="'.encreptIt($row->id).'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.encreptIt($row->id).'"  title="click to Deactive">Active</a>';
                    $row_data['action']     = ' <a href="javascript:void(0)" class="color-content enterprise-button" data-id="'.encreptIt($row->id).'"><i class="material-icons">edit</i></a>
                                                        <a class="color-content enterprise-remove" data-id="'.encreptIt($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>
                                          <a href="'.  admin_url().'enterprise-user/'.encreptIt($row->id).'" class="color-content"><i class="material-icons">person</i></a>';
                    $admin[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $admin;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/enterprise/index');
            }
        } else {
            redirect(admin_url());
        }
    }
    
    public function setup() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id = $this->input->post('id');
                $editdata = array();
                
                $enterprise_id = $this->input->post('id');

                if (!empty($enterprise_id)) {
                    $filter['select']   = array('angel_enterprise.*');
                    $filter['where']    = array('angel_enterprise.id' => decreptIt($enterprise_id));
                    $filter['row']      = 1;
                    
                    $editdata['enterprise']   = $this->enterprise_model->get_rows($filter);
                    
                }
                
                $editdata['enterprise_id'] = $enterprise_id;
                
                $data['html']          = $this->load->view('admin/enterprise/form_modal', $editdata, true);
                echo json_encode($data);
                die;
            } else {
                $this->template->view('admin/dipper_category/index');
            }
        } else {
            redirect(admin_url());
        }
    }
    
    public function commit() {
        if ($this->is_logined && ($this->input->server('REQUEST_METHOD') == 'POST')) {
            $return     = array();

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('url_name', 'URL Name', 'required');
            
            $validate   = $this->form_validation->run();

            if ($validate == true) {
                
                $id                         = $this->input->post('enterprise_id');
                
                $data['name']               = $this->input->post('name');
                $data['slug']               = $this->input->post('slug');
                $data['url_name']           = $this->input->post('url_name');
                $data['status']             = ($this->input->post('status') == "on" ? '1' : '0');

                if ($id != '') {
                    $cFilter['where']   = array('id' => decreptIt($id));
                    $cFilter['row']     = 1;
                    $cdata              = $this->enterprise_model->get_rows($cFilter);
                    
                    if (!empty($cdata)) {

                        $e_id = $cdata->id;
                        $this->enterprise_model->update_table($data, array('id' => decreptIt($id)));
                        $return['status']   = TRUE;
                        $return['message']  = "Enterprise updated successfully";
                    }
                } else {
                    $insert_id              = $this->enterprise_model->insert($data);

                    if ($insert_id > 0) {
                        $e_id = $insert_id;
                    }

                    $return['status']   = TRUE;
                    $return['message']  = "Enterprise inserted successfully";
                }

                if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {
                    
                    $path = getcwd() . '/uploads/enterprise';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }

                    $config['upload_path']  = $path . '/' . md5($e_id);
                    $upload_path            = '/uploads/enterprise/' . md5($e_id);

                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }
                    $this->load->library('image_lib');
                    // $config['allowed_types']    = 'gif|jpg|jpeg|png|PNG|JPG|JPEG';
                    $config['allowed_types']    = '*';
                    // $config['image_library']    = 'gd2';
                    $config['file_name']        = $_FILES["avatar"]['name'];
                    $config['max_size']         = $this->allowed_file_size;
                    // $config['maintain_ratio']   = true;
                    // $config['width']            = 500;
                    // $config['height']           = 500;
                    // $config['quality']          = '100%';
                    // $config['new_image']        = $_FILES["avatar"]['name'];

                    
                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('avatar')) {

                        if ($id != "") {
                            $cFilter['where']   = array('id' => decreptIt($id));
                            $cFilter['row']     = 1;
                            $cData = $this->enterprise_model->get_rows($cFilter);
                            if (!empty($cData) && !empty($cData->image)) {
                                $this->attachment_model->removeAttachment(array($cData->image));
                            }
                        } 

                        $upload_data    = $this->upload->data();
                        $aData['path']  = $upload_path.'/'.$upload_data['file_name'];
                        $aData['type']  = 'image';
                        $attachment_id   = $this->attachment_model->insert($aData);

                        $temp_path      = getcwd() . '/uploads/enterprise/'.md5($e_id);
                        if (!is_dir($temp_path)) {
                            mkdir($temp_path, 0777, true);
                        }
                        $ext            = pathinfo($upload_data['file_name'], PATHINFO_EXTENSION);
                        $r_path         = getcwd().$upload_path.'/'.$upload_data['file_name'];
                        $filename       = $temp_path.'/'.$upload_data['file_name'];

                        copy($r_path, $filename);
                        /// resize image
                        $this->load->library('image_lib');
                        $s_config['image_library']    = 'gd2';
                        $s_config['source_image']       = $filename;
                        $config['create_thumb']         = FALSE;
                        $config['maintain_ratio']       = FALSE;
                        $s_config['width']              = 500;
                        $s_config['height']             = 500;
                        
                        $this->image_lib->clear();
                        $this->image_lib->initialize($s_config);
                        $this->image_lib->resize();


                        if (!empty($attachment_id)) {

                            $update_attachment = array('image' => $attachment_id);
                            $this->enterprise_model->update_table($update_attachment, array('id' => $e_id));
                        } else {
                            $return['status']   = FALSE;
                            $return['message']  = "Somthing went wrong";
                        }
                    } else {
                        $return['status']   = FALSE;
                        $return['message']  = $this->upload->display_errors();
                    }
                }
            } else {
                $return['status']   = FALSE;
                $return['message']  = validation_errors();
            }
            echo json_encode($return);
            die;
        } else {
            redirect(admin_url());
        }
    }

    public function resizeImage($filename)
    {
       $source_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $filename;
       $target_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
       $config_manip = array(
           'image_library' => 'gd2',
           'source_image' => $source_path,
           'new_image' => $target_path,
           'maintain_ratio' => TRUE,
           'width' => 500,
       );
    
       $this->load->library('image_lib', $config_manip);
       if (!$this->image_lib->resize()) {
           echo $this->image_lib->display_errors();
       }
    
       $this->image_lib->clear();
    }

    public function  remove_image() {
        if ($this->is_logined && ($this->input->server('REQUEST_METHOD') == 'POST')) {
            $id                         = $this->input->post('enterprise_id');
            if ($id != '') {
                $cFilter['where']   = array('id' => decreptIt($id));
                $cFilter['row']     = 1;
                $cdata              = $this->enterprise_model->get_rows($cFilter);
                if (isset($cdata) && $cdata->image != "") {
                    $this->attachment_model->removeAttachment(array($cdata->image));
                    $folder_path = getcwd().'/uploads/enterprise/'.md5($cdata->id);
                    rrmdir($folder_path);
                    $update_attachment = array('image' => '');
                    $this->enterprise_model->update_table($update_attachment, array('id' =>$cdata->id));
                    $return['status']   = TRUE;
                    $return['message']  = 'Enterprise image not found';
                } else { 
                    $return['status']   = FALSE;
                    $return['message']  = 'Enterprise deleted successfully';
                }
            } else {
                $return['status']   = FALSE;
                $return['message']  = 'Enterprise Not found';
            }
            echo json_encode($return);die;
        }
        else {
            redirect(admin_url());
        }
    }
    public function delete($id = 0) {

        
        $id       = $this->input->post('id');
        
        if ($id != '') {

            $cFilter['where']   = array('id' => decreptIt($id));
            $cFilter['row']     = 1;
            $cdata              = $this->enterprise_model->get_rows($cFilter);

            if (!empty($cdata)) {
                $this->attachment_model->removeAttachment(array($cdata->image));
                $folder_path = getcwd().'/uploads/enterprise/'.md5($cdata->id);
                rrmdir($folder_path);
            }
            $delete = $this->enterprise_model->delete(array('id' => decreptIt($id)));
            
            if($delete) {
                $return['status']   = TRUE;
                $return['message']   = 'Enterprise deleted successfully';
            } else {
                $return['status']   = FALSE;
                $return['message']   = 'Something went wrong';
            }
        } else {
            $return['status']   = FALSE;
            $return['message']   = 'Something went wrong';
        }
        echo json_encode($return);
        die;
    }
    
}
?>