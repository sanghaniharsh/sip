<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined = $this->get_admin_user();
        $this->loginUser = $this->getLoginUser();
    }

    public function index($start = 0) {

        if ($this->is_logined) {
            $return = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $sfilter= array();
                $search = $this->input->post('search');
                if ($search != "") {
                    $sfilter['search']  = $search;
                }
                $result                 = $this->brand_model->getBrand($sfilter);

                $data['brand_list']     = $result['brand'];
                $data['total']          = $result['total'];

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/brand/ajax_brand_view', $data, true);
                } else {
                    $return['status']   = false;
                }
                echo json_encode($return);die;
            }
            $v_data["js_files"]   = array("jquery-ui.js");
            $this->template->view('admin/brand/index', $v_data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit() {
        if ($this->is_logined && ($this->input->server('REQUEST_METHOD') == 'POST')) {
            $return = array();
            $this->form_validation->set_rules('brand_name', 'Brand Name', 'required');
            $validate = $this->form_validation->run();

            if ($validate == true) {

                $data['name']       = $this->input->post('brand_name');
                $id                 = $this->input->post('brand_id');
                $id                 = decreptIt($id);
                $brand_id           = 0;
                $data['slug']       = $this->brand_model->create_slug($id , $data['name']);

                if ( $id  >  0) {
                    $bFilter['where']   = array('id' => $id);
                    $bFilter['row']     = 1;
                    $bdata              = $this->brand_model->get_rows($bFilter);

                    if (!empty($bdata)) {

                        $brand_id           = $bdata->id;
                        $this->brand_model->update_table($data, array('id' => $id));
                        $return['status']   = TRUE;
                        $return['message']  = "Brand updated successfully";
                    }
                } else {
                    $c_Filter['select'] = array('max(sort) as sort_order');
                    $c_Filter['row']    = 1;
                    $c_bdata            = $this->brand_model->get_rows($c_Filter);

                    if (! empty($c_bdata)) {
                        $data['sort']           = $c_bdata->sort_order+1;
                    }
                    $data['created_date']   = date('Y-m-d H:i:s', time());
                    $insert_id              = $this->brand_model->insert($data);

                    if ($insert_id > 0) {
                        $brand_id = $insert_id;
                    }

                    $return['status']   = TRUE;
                    $return['message']  = "Brand inserted successfully";
                }

                if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {

                    $path = getcwd() . '/uploads/brand';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }

                    $config['upload_path']  = $path . '/' . md5($brand_id);
                    $upload_path            = '/uploads/brand/' . md5($brand_id);

                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }

                    $config['allowed_types']    = 'gif|jpg|jpeg|png|PNG|JPG|JPEG';
                    $config['file_name']        = $_FILES["avatar"]['name'];
                    $config['max_size']         = $this->allowed_file_size;// 2MB=> 2097152 , 5MB => 5242880
                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('avatar')) {

                        if ($id > 0) {
                            $bFilter['where']   = array('id' => $id);
                            $bFilter['row']     = 1;
                            $bData = $this->brand_model->get_rows($bFilter);
                            if (!empty($bData) && !empty($bData->image)) {
                                $this->attachment_model->removeAttachment(array($bData->image));
                            }
                        } 

                        $upload_data    = $this->upload->data();
                        $aData['path']  = $upload_path.'/'.$upload_data['file_name'];
                        $aData['type']  = 'image';
                        $attachment_id   = $this->attachment_model->insert($aData);

                        if (!empty($attachment_id)) {

                            $update_attachment = array('image' => $attachment_id);
                            $this->brand_model->update_table($update_attachment, array('id' => $brand_id));
                        } else {
                            $return['status']   = FALSE;
                            $return['message']  = "Somthing went wrong";
                        }
                    } else {
                        $return['status']   = FALSE;
                        $return['message']  = $this->upload->display_errors();
                    }
                }
            } else {
                $return['status']   = FALSE;
                $return['message']  = validation_errors();
            }
            echo json_encode($return);
            die;
        } else {
            redirect(admin_url());
        }
    }

    public function setup() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id = $this->input->post('id');
                $id = decreptIt($id);
                $editdata = array();

                if ($id > 0) {
                    $filter['select']   = array('angel_brand.*');
                    $filter['where']    = array('angel_brand.id' => $id);
                    $filter['row']      = 1;
                    $editdata['branddata']   = $this->brand_model->get_rows($filter);
                }
                $data['html']          = $this->load->view('admin/brand/form', $editdata, true);
                echo json_encode($data);
                die;
            } else {
                $this->template->view('admin/brand/index');
            }
        } else {
            redirect(admin_url());
        }
    }

    public function delete() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id         = $this->input->post('id');
                $id         = decreptIt($id);

                if ($id > 0) {
                    $bFilter['where']   = array('id' => $id);
                    $bFilter['row']     = 1;
                    $brand              = $this->brand_model->get_rows($bFilter);

                    if (!empty($brand)) {
                        if ($brand->image > 0) {
                            $this->attachment_model->removeAttachment($brand->image);
                            //$folder_path    = getcwd().'/uploads/brand/'.md5($id);
                            //rrmdir($folder_path);
                        }
                        $this->brand_model->delete(array('id' => $id));
                        $return['status']   = true;
                        $return['message']  = 'Brand deleted successfully.';
                    } else {
                        $return['status']   = false;
                        $return['message']  = 'Somthing went wrong';
                    }

                    echo json_encode($return);die;
                }
            } else {
                $this->template->view('admin/brand/index');
            }
        } else {
            redirect(admin_url());
        }
    }
    public function sorting() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $position       = $this->input->post('position');
                $i              = 1;
                $updateArray    = array();
                if (!empty($position)){
                    foreach($position as $val){
                        $updateArray[] = array( 'id'   => $val,
                                                'sort' => $i
                            );
                        $i++;
                    }
                }
                $this->brand_model->update_batch($updateArray, 'id');
                $data['status'] = true;
                echo json_encode($data);
                die;
            } else {
                redirect(admin_url());
            }
        } else {
            redirect(admin_url());
        }
    }
}

?>