<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {
        if ($this->is_logined) {
            $return = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search = $this->input->post('search');

                if($search != "") {
                    $sfilter['search']  = $search;
                }

                $perpage                = 8;
                $start                  = ($start != 0) ? ($start - 1) * $perpage : 0;
                $sfilter['limit']       = array('limit' => $perpage, 'from' => $start);
                $result                 = $this->blog_model->blogPost($sfilter);
                $data['blog_list']      = $result['blog'];
                $data['total']          = $result['total'];

                //pagination
                $config                     = initPagination();
                $config["base_url"]         = base_url() . "admin/blog/index/";
                $config["total_rows"]       = $data['total'];
                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = $data['total'];
                $config['attributes']       = array('class' => 'page-link');
                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();
                $data["links"]          = $str_links;

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/blog/ajax_post_view', $data, true);
                } else {
                    $return['status'] = false;
                }

                echo json_encode($return);die;
            }

            $this->template->view('admin/blog/index');
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $data = array();

            if ($id != '') {
                $filter['where']= array('md5(id)' => $id);
                $filter['row']  = 1;
                $blogs          = $this->blog_model->get_rows($filter);

                if (!empty($blogs)) {

                    $iFilter['where']   = array('blogId' => $blogs->id);
                    $data['blog_image'] = $this->blog_images_model->get_rows($iFilter);
                    $data['blog']       = $blogs;
                }
            }
            $cFilter['where']       = array('isActive' => '1');
            $data['blog_category']  = $this->blog_category_model->get_rows($cFilter);
            $this->template->view("admin/blog/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {
        if ($this->is_logined) {

            $this->form_validation->set_rules('title', 'Blog Title', 'trim|required');
            $this->form_validation->set_rules('category', 'Blog Category', 'trim|required');

            if ($this->form_validation->run() == TRUE) {
                $data['title']          = $this->input->post('title');
                $data['category_id']    = $this->input->post('category');
                $data['description']    = $this->input->post('description');
                $data['slug']           = slug($data['title']);
                $data['is_active']      = $this->input->post('is_active') == 'on' ? '1' : '0';
                $insert_id              = 0;

                if ($id != '') {
                    
                    $filter['select']   = array('id');
                    $filter['where']    = array('md5(id)' => $id);
                    $filter['row']      = 1;
                    $result             = $this->blog_model->get_rows($filter);

                    if (!empty($result)) {
                        $insert_id = $result->id;
                        $this->blog_model->update_table($data, array('id' => $result->id));
                        $this->session->set_flashdata('success', 'Category saved successfully.');
                    } else {
                        $this->session->set_flashdata('error', 'somthing went wrong.');
                    }
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $insert_id              = $this->blog_model->insert($data);
                    if ($insert_id > 0) {
                        $this->session->set_flashdata('success', 'Blog saved successfully.');
                    } else {
                        $this->session->set_flashdata('error', 'Something went worng, Please try again later.');
                    }
                }

                if ($_FILES['blog_images']['name'][0] != '') {
                    $files = $_FILES['blog_images'];
                    foreach ($files['name'] as $i => $file) {
                        $_FILES['blog_images']['name']     = $files['name'][$i];
                        $_FILES['blog_images']['type']     = $files['type'][$i];
                        $_FILES['blog_images']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['blog_images']['error']    = $files['error'][$i];
                        $_FILES['blog_images']['size']     = $files['size'][$i];

                        $blogPath = getcwd().'/uploads/blog';
                        if (!is_dir($blogPath)) {
                            mkdir($blogPath, 0777, true);
                        }

                        $uploadPath = $blogPath.'/'.md5($insert_id).'/';

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }

                        $config['upload_path']          = $uploadPath;
                        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPG|JPEG';
                        $config['max_size']             = $this->allowed_file_size;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('blog_images')){
                            $fileData                   = $this->upload->data();

                            $attachmentData['path']     = '/uploads/blog/'.md5($insert_id).'/'.$fileData['file_name'];
                            $attachmentData['type']     = 'image';
                            $attachment_id              = $this->attachment_model->insert($attachmentData);

                            $uploadData['blogId']       = $insert_id;
                            $uploadData['image']        = $attachment_id;
                            $this->blog_images_model->insert($uploadData);
                        } else {
//                            echo '<pre>';print_r($this->upload->display_errors());die;
                        }
                    }
                 }

                 redirect(admin_url() . 'blog');
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(admin_url().'blog/');
            }
        } else {
            redirect('/admin');
        }
    }

    public function upload_image() {
        if ($_FILES['upload']['name'] != '') {

            $blogPath = getcwd().'/uploads/blog/comman';
            if (!is_dir($blogPath)) {
                mkdir($blogPath, 0777, true);
            }

            $uploadPath = $blogPath.'/';

            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, 0777, true);
            }

            $config['upload_path']          = $uploadPath;
            $config['allowed_types']        = 'gif|jpg|png|jpeg|JPG|JPEG';
            $config['max_size']             = $this->allowed_file_size;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            $re = '';
            if($this->upload->do_upload('upload')){
                $fileData       = $this->upload->data();
                $allowed_array  = explode('|', $config['allowed_types']);
                $sepext         = explode('.', strtolower($_FILES['upload']['name']));
                $type           = end($sepext);
                $CKEditorFuncNum= $_GET['CKEditorFuncNum'];
                $url            = base_url(). '/uploads/blog/comman/'.$fileData['file_name'];
                if (in_array($type, $allowed_array)) {
                    $re = "window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '')";
                } else {
                    $re = 'var cke_ob = window.parent.CKEDITOR; for(var ckid in cke_ob.instances) { if(cke_ob.instances[ckid].focusManager.hasFocus) break;} cke_ob.instances[ckid].insertHtml(\'\', \'unfiltered_html\'); var dialog = cke_ob.dialog.getCurrent();  dialog.hide();';
                }
            } else {
                $re = 'alert("Unable to upload the file")';
            }
            @header('Content-type: text/html; charset=utf-8');

            $response = "<script type='text/javascript'>".$re."</script>";
            
            echo $response;die;
         }
    }
    
    public function delete() {
        if ($this->is_logined) {
            $data = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter_add['where']    = array('md5(blogId)' => $id);
                $item                   = $this->blog_images_model->get_rows($filter_add);

                if (!empty($item)) {
                    $attachmentId = array();
                    foreach ($item as $list) {
                        $attachmentId[] = md5($list->image);
                    }
                    $this->attachment_model->removeAttachment($attachmentId);

                    $folder_path = getcwd().'/uploads/blog/'.$id;
                    rrmdir($folder_path);

                    $this->blog_view_model->delete(array('md5(blog_id)' => $id));

                    $this->blog_images_model->delete(array('md5(blogId)' => $id));
                }
                $this->blog_model->delete(array('md5(id)' => $id));

                $data['status']     = true;
                $data['message']    = "Blog deleted successfully.";
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }

    public function category() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'name',
                                    '1' => 'slug',
                                    '2' => 'created_date',
                                    '3' => 'isActive'
                                    );
                $filterCount    = $totalCount = $this->blog_category_model->get_rows(array(), true);

                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_blog_category.name', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_blog_category.slug', 'value' => $searchString),
                        '1' => array('field' => 'angel_blog_category.created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->blog_category_model->get_rows($filter, true);
                }
                
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->blog_category_model->get_rows($filter);
                $blog_category      = array();

                foreach ($query as $row) {
                    $row_data                   = array();
                    $row_data['id']             = $row->id;
                    $row_data['name']           = $row->name;
                    $row_data['slug']           = $row->slug;
                    $row_data['created_date']   = date('m/d/Y', strtotime($row->created_date));
                    $row_data['status']         = ($row->isActive == '0') ? '<a class="badge badge-danger status_checks" data-id="'.md5($row->id).'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.$row->id.'"  title="click to Deactive">Active</a>';
                    $row_data['action']         = ' <a class="color-content edit" data-id="'.md5($row->id).'" href="javascript:void(0)"><i class="material-icons">edit</i></a>
                                                    <a class="color-content delete" data-id="'.md5($row->id).'" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $blog_category[]            = $row_data;
                }
                $data['recordsTotal']           = $totalCount;
                $data['recordsFiltered']        = $filterCount;
                $data['data']                   = $blog_category;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/blog/category');
            }
        } else {
            redirect(admin_url());
        }
    }

    public function category_setup($id = '') {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $view_data = array();

                if ($id != '') {
                    $filter['where']= array('md5(id)' => $id);
                    $filter['row']  = 1;
                    $blog_category  = $this->blog_category_model->get_rows($filter);

                    if (!empty($blog_category)) {
                        $view_data['blog_category']  = $blog_category;
                    }
                }

                $data['status'] = true;
                $data['html']   = $this->load->view("admin/blog/category_form", $view_data, true);
                echo json_encode($data);die;
            } else {
                redirect(admin_url());
            }
        } else {
            redirect(admin_url());
        }
    }

    public function category_commit() {
        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $return = array();
                $this->form_validation->set_rules('name', 'Name', 'trim|required');

                if ($this->form_validation->run() == TRUE) {
                    $id                 = $this->input->post('blogCategory');
                    $data['name']       = $this->input->post('name');
                    $data['slug']       = slug($data['name']);
                    $insert_id          = 0;

                    if ($id != '') {
                        $this->blog_category_model->update_table($data, array('md5(id)' => $id));

                        $return['status']   = true;
                        $return['message']  = 'Category saved successfully.';
                    } else {
                        $data['isActive']       = '1';
                        $data['created_date']   = date('Y-m-d H:i:s', time());
                        $insert_id              = $this->blog_category_model->insert($data);

                        if ($insert_id == 0) {
                            $return['status']   = false;
                            $return['message']  = 'Something went worng, Please try again later.';
                        } else {
                            $return['status']   = true;
                            $return['message']  = 'Category saved successfully.';
                        }
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = 'Please fill required fileds';
                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Something went wrong, please try again latter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }

    public function delete_category() {
        if ($this->is_logined) {
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('md5(id)' => $id);
            $uFilter['row']     = 1;
            $category           = $this->blog_category_model->get_rows($uFilter);

            if (!empty($category)) {
                
                $bFilter['where']   = array('category_id' => $category->id);
                $blog               = $this->blog_model->get_rows($bFilter);

                if (!empty($blog)) {
                    $bData = array();
                    foreach ($blog as $value) {
                        $bData[]  = $value->id;
                    }

                    if (!empty($bData)) {

                        $iFilter['where_in']    = array(array('field' => 'blogId', 'value' => $bData));
                        $blog_images            = $this->blog_images_model->get_rows($iFilter);

                        $attachmentId = array();
                        foreach ($blog_images as $list) {
                            $attachmentId[] = md5($list->image);
                        }
                        $this->attachment_model->removeAttachment($attachmentId);

                        foreach ($bData as $val) {
                            $folder_path = getcwd().'/uploads/blog/'.md5($val);
                            rrmdir($folder_path);
                        }
                        $this->blog_images_model->delete_filter(array('where_in' => array('field' => 'blogId', 'value' => $bData)));

                        $this->blog_model->delete(array('category_id' => $category->id));
                    }
                }

                $this->blog_category_model->delete(array('id' => $category->id));

                $return['status']   = true;
            } else {
                $return['status']   = false;
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }

    public function validate() {
        $title   = $this->input->post('title');
        $id      = $this->input->post('id');
        $filter  = array();
        if (isset($title) && ($title != "")) {
            if (isset($id) && ($id > 0)) {
                $filter['where'] = array('id !=' => $id, 'title' => $title);
            } else {
                $filter['where'] = array('title' => $title);
            }
        }
        $result = $this->cms_model->get_rows($filter,1);
        $return = TRUE;
        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }

    public function category_validate() {
        $name   = $this->input->post('name');
        $id     = $this->input->post('id');
        $filter = array();

        if (isset($name) && ($name != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('md5(id) !=' => $id, 'name' => $name);
            } else {
                $filter['where'] = array('name' => $name);
            }
        }

        $result = $this->blog_category_model->get_rows($filter, 1);
        $return = TRUE;
        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }

    public function deleteBlogImage() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data       = array();
                $image_id   = $this->input->post('image_id');
                $blogId     = $this->input->post('blogId');

                $filter_img['where']    = array('md5(id)' => $image_id);
                $filter_img['row']      = 1;
                $item                   = $this->blog_images_model->get_rows($filter_img);

                if (!empty($item)) {

                    $this->attachment_model->removeAttachment(array(md5($item->image)));

                    $where = array('md5(id)' => $image_id);
                    $this->blog_images_model->delete($where);

                    $data['status']     = true;
                    $data['message']    = "Blog Image deleted successfully.";
                } else {
                    $data['status']     = false;
                    $data['message']    = "No image found.";
                }
                echo json_encode($data);die;
            } else {
                $this->session->set_flashdata('success', 'This method is not allowed.');
            }
            redirect(admin_url().'admin/blog');
        } else {
            redirect(admin_url().'admin');
        }
    }
}
?>