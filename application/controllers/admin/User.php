<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'firstname',
                                    '1' => 'lastname',
                                    '2' => 'email',
                                    '3' => 'phone',
                                    '4' => 'created_date',
                                    '5' => 'is_active'
                                    );
                $filterCount    = $totalCount = $this->user_model->get_rows(array(), true);
                
                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_users.firstname', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_users.lastname', 'value' => $searchString),
                        '1' => array('field' => 'angel_users.email', 'value' => $searchString),
                        '2' => array('field' => 'angel_users.phone', 'value' => $searchString),
                        '3' => array('field' => 'angel_users.created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->user_model->get_rows($filter, true);
                }
//                $filter['where']    = array('id !=' => $this->loginUser['id'], 'user_type' => 0);
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->user_model->get_rows($filter);
                $admin              = array();

                foreach ($query as $row) {
                    $row_data               = array();
                    $row_data['id']         = $row->id;
                    $row_data['firstname']  = $row->firstname;
                    $row_data['lastname']   = $row->lastname;
                    $row_data['email']      = $row->email;
                    $row_data['phone']      = $row->phone;
                    $row_data['created_date']= date('Y/m/d H:i:s', strtotime($row->created_date));
                    $row_data['status']     = ($row->is_active == '0') ? '<a class="badge badge-danger status_checks" data="'.encreptIt($row->id).'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.encreptIt($row->id).'"  title="click to Deactive">Active</a>';
                    $row_data['action']     = ' <a class="color-content" href="'.base_url().'admin/user/setup/'.  encreptIt($row->id).'"><i class="material-icons">edit</i></a>
                                                        <a class="color-content" data-Id="'.encreptIt($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $admin[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $admin;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/user/index');
            }
        } else {
            redirect('admin');
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $data = array();
            if ($id != '') {
                $filter['where']= array('id' => decreptIt($id));
                $filter['row']  = 1;
                $user           = $this->user_model->get_rows($filter);
                if (!empty($user)) {
                    $data['user']   = $user;
                }
            }

            $this->template->view("admin/user/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {
        if ($this->is_logined) {

            $data['firstname']  = $this->input->post('firstname');
            $data['lastname']   = $this->input->post('lastname');
            $data['phone']      = $this->input->post('phone');
            $data['email']      = $this->input->post('email');
            $data['is_active']  = $this->input->post('is_active') == 'on' ? '1' : '0';
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
                if ($id != '') {
                    $password = $this->input->post('password');
                    if ($password != "") {
                        $data['password']           = encreptIt($password);
                        $data['visible_password']   = $password;
                    }

                    $this->user_model->update_table($data, array('id' => decreptIt($id)));

                    $this->session->set_flashdata('success', 'User saved successfully.');
                } else {
//                    $password               = $this->input->post('password');
//                    $data['password']       = encreptIt($password);
//                    $data['visiblePassword']= $password;
//                    $data['created_date']   = date('Y-m-d H:i:s');
//
//                    $id = $this->user_model->insert($data);
//                    $id = md5($id);
//                    if ($id != '') {
//                        $this->session->set_flashdata('success', 'User saved successfully.');
//                    } else {
//                        $this->session->set_flashdata('success', 'Something went worng, Please try again later.');
//                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Please enter valid email address');
            }
            redirect(admin_url().'user');
        } else {
            redirect(admin_url());
        }
    }
    
    public function validate() {
        $email      = $this->input->post('email');
        $id         = $this->input->post('user_id');
        $filter     = array();

        if (isset($email) && ($email != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('id !=' => decreptIt($id), 'email' => $email);
            } else {
                $filter['where'] = array('email' => $email);
            }
        }
        $filter['row']  = 1;
        $result         = $this->user_model->get_rows($filter);
        $return         = TRUE;

        if ($result != '') {
            if ($result->id != $this->loginUser['id']) {
                $return = FALSE;
            }
        }

        echo json_encode($return);
    }

    public function delete() {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('id' => decreptIt($id));
            $uFilter['row']     = 1;
            $user               = $this->user_model->get_rows($uFilter);

            if (!empty($user)) {
                $where = array('id' => decreptIt($id));
                $this->user_model->delete($where);                

                $return['status']   = true;
                $return['message']  = 'Admin User deleted successfully';
            } else {
                $return['status']   = false;
                $return['message']  = 'Somthing went wrong';
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }
    
    public function checkStatus() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter['where']        = array('id' => decreptIt($id));
                $filter['row']          = 1;
                $item                   = $this->user_model->get_rows($filter);
                if (!empty($item)) {
                    if ($item->is_active == '0') {
                        $data['is_active']   = '1';
                    } else {    
                        $data['is_active']   = '0';
                    }

                    $this->user_model->update_table($data, array('id' => $item->id));
                    $data['status']   = TRUE;
                    $data['message']  = 'Admin User updated successfully';
                } else {
                    $data['status']   = FALSE;
                    $data['message']  = 'Somthing went wrong';
                }
                echo json_encode($data);die;
            } else {
                redirect(admin_url());
            }
        }
    }
}
?>