<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promotional extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'title',
                                    '1' => 'promo_code',
                                    '2' => 'type',
                                    '3' => 'discount',
                                    '4' => 'created_date',
                                    '5' => 'status'
                                    );

                $filter['select']   = array('angel_promotional.*');
                $filterCount        = $totalCount = $this->promotional_model->get_rows($filter, true);

                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_promotional.title', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_promotional.discount', 'value' => $searchString),
                        '1' => array('field' => 'angel_promotional.promo_code', 'value' => $searchString),
                        '2' => array('field' => 'angel_promotional.created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->promotional_model->get_rows($filter, true);
                }
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[4];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->promotional_model->get_rows($filter);
                $promotional        = array();

                foreach ($query as $row) {
                    $row_data               = array();
                    $row_data['id']         = $row->id;
                    $row_data['title']      = $row->title;
                    $row_data['promo_code'] = $row->promo_code;
                    $row_data['type']       = ($row->type == '1') ? 'Dollar' : 'percent';
                    $row_data['discount']   = $row->discount;
                    $row_data['created_date']= date('m/d/Y', strtotime($row->created_date));
                    $row_data['status']     = ($row->status == '0') ? '<a class="badge badge-danger status_checks" data="'.$row->id.'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.$row->id.'"  title="click to Deactive">Active</a>';
                    $row_data['action']     = ' <a class="color-content" href="'.base_url().'admin/promotional/setup/'.md5($row->id).'"><i class="material-icons">edit</i></a>
                                                <a class="color-content" data-Id="'.md5($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $promotional[]          = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $promotional;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/promotional/index');
            }
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $data = array();

            if ($id != '') {
                $filter['where']= array('md5(id)' => $id);
                $filter['row']  = 1;
                $promotional    = $this->promotional_model->get_rows($filter);

                if (!empty($promotional)) {
                    $data['promotional']   = $promotional;
                }
            }
            $this->template->view("admin/promotional/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {
        if ($this->is_logined) {
            $this->form_validation->set_rules('title', 'Promotional Title', 'trim|required');
            $this->form_validation->set_rules('promo_code', 'Promotional Code', 'trim|required');
            $this->form_validation->set_rules('use_number', 'Promotional Usage Time', 'trim|required|numeric');
            $this->form_validation->set_rules('type', 'Promotional Type', 'required');
            $this->form_validation->set_rules('discount', 'Discount', 'trim|required|numeric');

            if ($this->form_validation->run() == TRUE) {
                $data['title']          = $this->input->post('title');
                $data['promo_code']     = $this->input->post('promo_code');
                $data['use_number']     = $this->input->post('use_number');
                $data['type']           = $this->input->post('type');
                $data['discount']       = $this->input->post('discount');
                $data['status']         = $this->input->post('status') == 'on' ? '1' : '0';

                if($id != '') {
                    $this->promotional_model->update_table($data, array('md5(id)' => $id));
                    $this->session->set_flashdata('success', 'Promotional updated successfully.');
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $id                     = $this->promotional_model->insert($data);

                    if ($id > 0) {
                        $this->session->set_flashdata('success', 'Promotional saved successfully.');
                    } else {
                        $this->session->set_flashdata('success', 'Something went worng, Please try again later.');
                    }
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());

                if($id != '') {
                    redirect(admin_url().'promotional/setup/'. $id );
                } else {
                    redirect(admin_url().'promotional/setup');
                }
            }
            redirect(admin_url().'promotional');
        } else {
            redirect(admin_url());
        }

        $this->template->view('admin/blog/post', $data);
    }

    public function validate() {
        $promo_code = $this->input->post('promo_code');
        $id         = $this->input->post('id');
        $filter     = array();

        if (isset($promo_code) && ($promo_code != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('md5(id) !=' => $id, 'promo_code' => strtolower($promo_code));
            } else {
                $filter['where'] = array('promo_code' => strtolower($promo_code));
            }
        }

        $result = $this->promotional_model->get_rows($filter, true);
        $return = TRUE;

        if ($result > 0) {
            $return = FALSE;
        }

        echo json_encode($return);
    }

    public function delete() {
        if ($this->is_logined) {
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('md5(id)' => $id);
            $uFilter['row']     = 1;
            $promotional        = $this->promotional_model->get_rows($uFilter);

            if (!empty($promotional)) {

                $where = array('id' => $promotional->id);
                $this->promotional_model->delete($where);

                $return['status']   = true;
            } else {
                $return['status']   = false;
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }

    public function checkStatus() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter['where']        = array('md5(id)' => md5($id));
                $filter['row']          = 1;
                $item                   = $this->promotional_model->get_rows($filter);

                if (!empty($item)) {
                    if ($item->status == '0') {
                        $update_data['status']   = '1';
                    } else {
                        $update_data['status']   = '0';
                    }
                    $this->promotional_model->update_table($update_data, array('md5(id)' => md5($item->id)));
                    $data['status']     = TRUE;
                    $data['value']      = $update_data['status'];
                } else {
                    $data['status']   = FALSE;
                }
                echo json_encode($data);die;
            }
        }
        redirect(admin_url());
    }
}
?>