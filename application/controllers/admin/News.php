<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {
        if ($this->is_logined) {
            $return = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search = $this->input->post('search');

                if($search != "") {
                    $nfilter['search']  = $search;
                }

                $perpage                = 8;
                $start                  = ($start != 0) ? ($start - 1) * $perpage : 0;
                $nfilter['limit']       = array('limit' => $perpage, 'from' => $start);
                $result                 = $this->news_model->getNews($nfilter);
                $data['news']           = $result['news'];
                $data['total']          = $result['total'];

                //pagination
                $config                     = initPagination();
                $config["base_url"]         = base_url() . "admin/news/index/";
                $config["total_rows"]       = $data['total'];
                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = $data['total'];
                $config['attributes']       = array('class' => 'page-link');
                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();
                $data["links"]          = $str_links;

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/news/ajax_news_view', $data, true);
                } else {
                    $return['status'] = false;
                }
                echo json_encode($return);die;
            }
            $this->template->view('admin/news/index');
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $data = array();

            if ($id != '') {
                $filter['where']= array('md5(id)' => $id);
                $filter['row']  = 1;
                $data['news']   = $this->news_model->get_rows($filter);
            }
            $this->template->view("admin/news/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $this->form_validation->set_rules('title', 'News Title', 'trim|required');
//                $this->form_validation->set_rules('url', 'News Url', 'trim|required');
                $this->form_validation->set_rules('description', 'News Description', 'trim|required');

                if ($this->form_validation->run() == TRUE) {
                    $data['title']          = $this->input->post('title');
                    $data['url']            = $this->input->post('url');
                    $data['description']    = $this->input->post('description');
                    $data['slug']           = slug($data['title']);
                    $data['is_active']      = $this->input->post('is_active') == 'on' ? '1' : '0';
                    $insert_id              = 0;

                    if ($id != '') {

                        $filter['select']   = array('id');
                        $filter['where']    = array('md5(id)' => $id);
                        $filter['row']      = 1;
                        $result             = $this->news_model->get_rows($filter);

                        if (!empty($result)) {
                            $insert_id = $result->id;
                            $this->news_model->update_table($data, array('id' => $result->id));
                            $this->session->set_flashdata('success', 'News saved successfully.');
                        } else {
                            $this->session->set_flashdata('error', 'somthing went wrong.');
                        }
                    } else {
                        $data['created_date']   = date('Y-m-d H:i:s', time());
                        $insert_id              = $this->news_model->insert($data);
                        if ($insert_id > 0) {
                            $this->session->set_flashdata('success', 'News saved successfully.');
                        } else {
                            $this->session->set_flashdata('error', 'Something went worng, Please try again later.');
                        }
                    }

                    if (!empty($_FILES["news_image"]['name'])) {

                        $path = getcwd() . '/uploads/news';
                        if (!is_dir($path)) {
                            mkdir($path, 0777, true);
                        }

                        $config['upload_path']  = $path . '/' . md5($insert_id);
                        $upload_path            = '/uploads/news/' . md5($insert_id);
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }

                        $filter_add['where']    = array('id' => $insert_id);
                        $filter_add['row']      = 1;
                        $news                   = $this->news_model->get_rows($filter_add);

                        if (!empty($news) && ($news->image > 0)) {
                            $this->attachment_model->removeAttachment(array('id' => $news->image));
                        }
                        $config['allowed_types']= 'gif|jpg|jpeg|png|GIF|JPEG|JPG|PNG';
//                        $config['max_size']   = 2097152;
                        $config['max_size']     = $this->allowed_file_size;
                        $config['file_name']    = $_FILES["news_image"]['name'];
                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('news_image')) {
    //                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        } else {
                            $upload_data = $this->upload->data();

                            $aData['path']  = $upload_path.'/'.$upload_data['file_name'];
                            $aData['type']  = 'image';
                            $attachmentId   = $this->attachment_model->insert($aData);

                            $updateData['image'] = $attachmentId;
                            $this->news_model->update_table($updateData, array('id' => $insert_id));
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            } else {
                $this->session->set_flashdata('error', 'Something went wrong');
            }
            redirect(admin_url().'news/');
        } else {
            redirect('/admin');
        }
    }

    public function delete() {
        if ($this->is_logined) {
            $data = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');

                $filter_add['where']    = array('md5(id)' => $id);
                $filter_add['row']      = 1;
                $item                   = $this->news_model->get_rows($filter_add);

                if (!empty($item)) {
                    $aFilter['where']   = array('id' => $item->image);
                    $aFilter['row']     = 1;
                    $attachments        = $this->attachment_model->get_rows($aFilter);

                    if (!empty($attachments)) {
                        $image = getcwd() . $attachments->path;
                        if (file_exists($image)) {
                            unlink($image);
                        }

                        $this->attachment_model->delete(array('id' => $attachments->path));

                        $folder_path = getcwd().'/uploads/news/'.$id;
                        rrmdir($folder_path);
                    }
                }
                $this->news_model->delete(array('md5(id)' => $id));

                $data['status']     = true;
                $data['message']    = "News deleted successfully.";
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }
    
    public function updateNewsImage() {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $data       = array();
            $news_id    = $this->input->post('news_id');
            if (!empty($_FILES["news_image"]['name']) && $news_id != '') {

                $filter['select']   = array('angel_news.*', 'a.path');
                $filter['join']     = array(
                                            0 => array('table' => 'angel_attachment as a', 'condition' => 'a.id = angel_news.image', 'type' => 'left')
                                        );
                $filter['where']    = array('md5(angel_news.id)' => $news_id);
                $filter['row']      = 1;
                $result             = $this->news_model->get_rows($filter);

                if (!empty($result)) {
                    $path = getcwd() . '/uploads/news';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }

                    $config['upload_path']  = $path . '/' . md5($result->id);
                    $upload_path            = '/uploads/news/' . md5($result->id);
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }

                    if ($result->image > 0) {
                        $this->attachment_model->removeAttachment(array('id' => $result->image));
                    }
                    $config['allowed_types']= 'gif|jpg|jpeg|png|GIF|JPEG|JPG|PNG';
//                    $config['max_size']     = 2097152;
                    $config['max_size']     = $this->allowed_file_size;
                    $config['file_name']    = $_FILES["news_image"]['name'];
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('news_image')) {
                        $data['status']     = false;
                        $data['message']    = 'Please upload valid file';
//                        $this->session->set_flashdata('error', $this->upload->display_errors());
                    } else {
                        $upload_data    = $this->upload->data();
                        $aData['path']  = $upload_path.'/'.$upload_data['file_name'];
                        $aData['type']  = 'image';
                        $attachmentId   = $this->attachment_model->insert($aData);

                        $updateData['image'] = $attachmentId;
                        $this->news_model->update_table($updateData, array('id' => $result->id));

                        $data['status']     = true;
                        $data['message']    = 'Image uploaded successfully';
                        $data['image']      = base_url().'attachment/image/0/300/'.md5($attachmentId);
                    }
                } else {
                    $data['status']     = false;
                    $data['message']    = 'No record found';
                }
            } else {
                $data['status']     = false;
                $data['message']    = 'something went wrong';
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }
}
?>