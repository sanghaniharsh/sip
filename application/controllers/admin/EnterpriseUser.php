<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EnterpriseUser extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($id = 0) {
        
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                
                
                $id = decreptIt($id);
                
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'firstname',
                                    '1' => 'lastname',
                                    '2' => 'email',
                                    '3' => 'phone',
                                    '4' => 'created_date',
                                    '5' => 'is_active'
                                    );
                
                $filter['where']       = array('enterprise_id' => $id);
                
                $filterCount    = $totalCount = $this->enterprise_user_model->get_rows($filter, true);
                
                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_users.firstname', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_users.lastname', 'value' => $searchString),
                        '1' => array('field' => 'angel_users.email', 'value' => $searchString),
                        '2' => array('field' => 'angel_users.phone', 'value' => $searchString),
                        '3' => array('field' => 'angel_users.created_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->enterprise_user_model->get_rows($filter, true);
                }
//                $filter['where']    = array('id !=' => $this->loginUser['id'], 'user_type' => 0);
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->enterprise_user_model->get_rows($filter);
                $admin              = array();

                foreach ($query as $row) {
                    $row_data               = array();
                    $row_data['id']         = $row->id;
                    $row_data['firstname']  = $row->firstname;
                    $row_data['lastname']   = $row->lastname;
                    $row_data['email']      = $row->email;
                    $row_data['phone']      = $row->phone;
                    $row_data['created_date']= date('Y/m/d H:i:s', strtotime($row->created_date));
                    $row_data['status']     = ($row->is_active == '0') ? '<a class="badge badge-danger status_checks" data="'.encreptIt($row->id).'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data="'.encreptIt($row->id).'"  title="click to Deactive">Active</a>';
                    $row_data['action']     = ' <a class="color-content" href="'.base_url().'admin/enterprise-user/setup/'.encreptIt($row->enterprise_id).'/'.encreptIt($row->id).'"><i class="material-icons">edit</i></a>
                                                        <a class="color-content" data-Id="'.encreptIt($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $admin[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $admin;

                echo json_encode($data);die();
            } else {
                
                $data['enterprise_id'] = $id;
                
                $this->template->view('admin/enterprise_user/index',$data);
            }
        } else {
            redirect('admin');
        }
    }
    
    public function setup($id = '', $user_id = "") {
        if ($this->is_logined) {
            $data = array();
            
            if ($user_id != '') {
                $filter['where']= array('id' => decreptIt($user_id));
                $filter['row']  = 1;
                $user           = $this->enterprise_user_model->get_rows($filter);
                if (!empty($user)) {
                    $data['user']   = $user;
                }
            }
            
            $data['id'] = $id;

            $this->template->view("admin/enterprise_user/form", $data);
        } else {
            redirect(admin_url());
        }
    }
    
    public function validate($enterprise_id = "") {
        
        $email      = $this->input->post('email');
        $id         = $this->input->post('user_id');
        $filter     = array();

        if (isset($email) && ($email != "")) {
            if (isset($id) && ($id != '')) {
                $filter['where'] = array('id !=' => decreptIt($id), 'email' => $email);
            } else {
                $filter['where'] = array('email' => $email);
            }
        }
        $filter['row']  = 1;
        $result         = $this->enterprise_user_model->get_rows($filter);

        if ($result != '') {
            $return = FALSE;
        } else {
            $return = TRUE;
        }
 
        echo json_encode($return);
    }

    public function commit($id = '', $user_id = "") {
        if ($this->is_logined) {

            $data['firstname']      = $this->input->post('firstname');
            $data['lastname']       = $this->input->post('lastname');
            $data['sales_tax']      = $this->input->post('sales_tax');
            $data['email']          = $this->input->post('email');
            $data['enterprise_id']  = decreptIt($id);
            
            $data['is_active']  = $this->input->post('is_active') == 'on' ? '1' : '0';
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
                if ($user_id != '') {
                    $password = $this->input->post('password');
                    if ($password != "") {
                        $data['password']           = encreptIt($password);
                        $data['visible_password']   = $password;
                    }

                    $this->enterprise_user_model->update_table($data, array('id' => decreptIt($user_id)));

                    $this->session->set_flashdata('success', 'User update successfully.');
                } else {
                    $password               = $this->input->post('password');
                    $data['password']       = encreptIt($password);
                    $data['visible_password']= $password;
                    $data['created_date']   = date('Y-m-d H:i:s');

                    $user_id = $this->enterprise_user_model->insert($data);

                    if ($user_id > 0) {
                        $this->session->set_flashdata('success', 'User saved successfully.');
                    } else {
                        $this->session->set_flashdata('success', 'Something went worng, Please try again later.');
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Please enter valid email address');
            }
            redirect(admin_url().'enterprise-user/'.$id);
        } else {
            redirect(admin_url());
        }
    }
    
    public function delete($enterprise_id = '') {
        if ($this->is_logined) {
            
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('id' => decreptIt($id));
            $uFilter['row']     = 1;
            $user               = $this->enterprise_user_model->get_rows($uFilter);

            if (!empty($user)) {
                $where = array('id' => decreptIt($id));
                $this->enterprise_user_model->delete($where);                

                $return['status']   = true;
                $return['message']  = 'User deleted successfully';
            } else {
                $return['status']   = false;
                $return['message']  = 'Somthing went wrong';
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }
    
    
    
}
?>