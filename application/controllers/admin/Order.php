<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Order extends MY_Controller {



    var $is_logined = FALSE;



    public function __construct() {

        parent::__construct();

        $this->is_logined   = $this->get_admin_user();

        $this->loginUser    = $this->getLoginUser();

    }



    public function index() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search         = $this->input->post('search');

                $start          = intval($this->input->post("start"));

                $length         = intval($this->input->post("length"));

                $order          = $this->input->post("order");



                $columnArray    = array(

                                        '0' => 'user_id',

                                        '1' => 'email',

                                        '2' => 'amount',

                                        '3' => 'order_status',

                                        '4' => 'created_date'

                                    );



                $filter['select']   = array('angel_order.*','u.id as u_id','u.firstname','u.lastname','u.email','u.phone');

                $filter['join']     = array(

                                        0 => array('table' => 'angel_users as u', 'condition' => 'u.id = angel_order.user_id', 'type' => 'left')

                                );

                //$filter['where']        = array('angel_order.status' => 'Success');



                $filterCount        = $totalCount = $this->order_model->get_rows($filter, true);



                if (isset($search) && ($search['value'] != '')) {

                    $searchString       = $search['value'];

                    $filter['like']     = array('field' => 'angel_order.amount', 'value' => $searchString);

                    $filter['or_like']  = array(

                        '0' => array('field' => 'angel_order.id', 'value' => $searchString),

                        '1' => array('field' => 'CONCAT("SIP", "", LPAD(angel_order.id, 5, "0"))', 'value' => $searchString),

                        '2' => array('field' => 'u.firstname', 'value' => $searchString),

                        '3' => array('field' => 'u.lastname', 'value' => $searchString),

                        '4' => array('field' => 'u.email', 'value' => $searchString),

                        '5' => array('field' => 'angel_order.created_date', 'value' => str_replace("/", "-", $searchString))

                    );

                    $filterCount    = $this->order_model->get_rows($filter, true);

                }

                $filter['limit']    = array('limit' => $length, 'from' => $start);

                $orderField         = $columnArray[4];

                $orderSort          = 'DESC';

                if (!empty($order)) {

                    if (isset($order[0]['column']) && $order[0]['column'] != '') {

                        $orderField = $columnArray[$order[0]['column']];

                        $orderSort  = $order[0]['dir'];

                    }

                }

                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);

                $query              = $this->order_model->get_rows($filter);

                $order              = array();

                $status             = '';

                foreach ($query as $row) {

                    $row_data                   = array();

                    $row_data['id']             = $row->id;

                    $row_data['order_id']       = display_order($row->id);

                    $row_data['user']           = $row->firstname."  ".$row->lastname;

                    $row_data['email']          = $row->email;

                    $row_data['amount']         = $row->amount;

                    

                    if (isset($row->order_status) && $row->order_status != '') {

                        if($row->order_status == '0') {

                            $status     =   '<span class="btn btn-xs fw-900 fs-11 text-uppercase btn-default flex-1 justify-content-center">In Progress</span>';

                        } else if ($row->order_status == '1') {

                            $status     =   '<span class="btn btn-xs fw-900 fs-11 text-uppercase btn-info flex-1 justify-content-center">Shipped</span>';

                        } else if ($row->order_status == '2') {

                            $status     =   '<span class="btn btn-xs fw-900 fs-11 text-uppercase btn-success flex-1 justify-content-center">Delivered</span>';

                        } else {

                            $status     =   '';

                        }

                    } else {

                        $status = '';

                    }

                    $row_data['order_status']   = $status;

                    $row_data['created_date']   = date('m/d/Y', strtotime($row->created_date));

                    $row_data['action']         = ' <a class="color-content" href="'.base_url().'admin/order/user_orders_detail/'.encreptIt($row->id).'"><i class="material-icons">remove_red_eye</i></a>

                                                    <a class="color-content" data-Id="'.encreptIt($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>

                                                    ';

                    $order[]                    = $row_data;

                }

                $data['recordsTotal']           = $totalCount;

                $data['recordsFiltered']        = $filterCount;

                $data['data']                   = $order;



                echo json_encode($data);die();

            } else {

                $this->template->view('admin/order/index');

            }

        } else {

            redirect(admin_url());

        }

    }



    public function user_orders_detail($order_id = '') {

        if ($this->is_logined) {

            $data       = array();

            $id         = 0;



            if ($order_id != '') {

                $id         = decreptIt($order_id);

            }



            if($id > 0) {



                $filter['select']   = array('angel_order.id as o_id ','oi.*', 'p.name', 'p.price','angel_order.tax','p.modal_number', 'p.weight');

                $filter['join']     = array(

                                        0 => array('table' => 'angel_order_items  as oi', 'condition' => 'oi.order_id = angel_order.id', 'type' => 'left'),

                                        1 => array('table' => 'angel_product  as p', 'condition' => 'p.id = oi.product_id', 'type' => 'left'),

                                );

                $filter['where']        = array('oi.order_id' => $id);

                $filter['groupby']      = array('field' => 'oi.product_id');



                $data['order']          = $this->order_model->get_rows($filter);



                $Ufilter['select']      = array('angel_order.*', 'u.firstname', 'u.lastname', 'u.email', 'u.phone', 'u_a.city', 's.name as state' , 'u_a.street_address' , 'u_a.zipcode');

                $Ufilter['join']        = array(

                                        0 => array('table' => 'angel_users as u', 'condition' => 'u.id = angel_order.user_id', 'type' => 'left'),

                                        1 => array('table' => 'angel_user_address as u_a', 'condition' => 'u_a.user_id = u.id', 'type' => 'left'),

                                        2 => array('table' => 'angel_state as s', 'condition' => 's.code = u_a.state', 'type' => 'left')

                                );

                $Ufilter['row']         = 1;

                $filter['groupby']      = array('field' => 'angel_order.id');

                $Ufilter['where']       = array('angel_order.id' => $id);



                $data['userDetail']     = $this->order_model->get_rows($Ufilter);



                $this->template->view('admin/order/order_item',$data);

            } else {

                $this->template->view('admin/order/index');

            }

        } else {

            redirect(admin_url());

        }

    }

    

    public function update_order_status() {



        if ($this->is_logined) {

            $return     = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $status     = $this->input->post('order_status');

                $order_id   = $this->input->post('order_id');

                $id         = 0;



                if ($order_id != '') {

                    $id         = decreptIt($order_id);

                }



                if ($id > 0  &&  $status != '') {

                    $data['order_status']   = $status;

                    $this->order_model->update_table($data, array('id' => $id));

                    $return['order_status'] = $status;

                    $return['status']       = true;

                    $return['message']      = "Order status updated successfully.";



                } else {

                    $return['status']       = false;

                    $return['message']      = "Please Select Status.";

                }

                echo json_encode($return);die();

            } else {

              $this->template->view('admin/order/index');  

            }

        } else {

            redirect(admin_url());

        }

    } 



    public function delete() {

        if ($this->is_logined && $this->loginUser['user_type'] == 0) {

            $id                 = $this->input->post('id');

            $uFilter['where']   = array('id' => decreptIt($id));

            $uFilter['row']     = 1;

            $user               = $this->order_model->get_rows($uFilter);



            if (!empty($user)) {

                $this->order_model->delete(array('id' => decreptIt($id)));

                $this->order_items_model->delete(array('order_id' => decreptIt($id)));                



                $return['status']   = true;

                $return['message']  = 'Order deleted successfully';

            } else {

                $return['status']   = false;

                $return['message']  = 'Somthing went wrong';

            }

            echo json_encode($return);die;

        } else {

            redirect(admin_url());

        }

    }

}

?>