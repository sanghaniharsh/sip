<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined = $this->get_admin_user();
        $this->loginUser = $this->getLoginUser();
    }

    public function index($category_id = '') {
        if ($this->is_logined) {
            $data['category_id'] = '';
            if ($category_id != '') {
                $data['category_id']    = $category_id;
                $parents                = $this->category_model->getOnlyParents(decreptIt($category_id));
                $data['parents']        = $parents;
            }
             $data["js_files"]   = array("jquery-ui.js");
            $this->template->view('admin/category/index', $data);
        } else {
            redirect(admin_url());
        }
    }

    public function view($category_id = '', $start = 0) {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $search = $this->input->post('search');

            if($search != "") {
                $filter['search']   = $search;
            }

            $perpage                = 200;
            $start                  = ($start != 0) ? ($start - 1) * $perpage : 0;
            $filter['limit']        = array('limit' => $perpage, 'from' => $start);

            $cate_id = -1;
            if ($category_id != '') {
                $cate_id = decreptIt($category_id);
                if ($cate_id > 0) {
                    $filter['parent']   = $cate_id;
                }
            }

            $result             = $this->category_model->getCategory($filter);
            $count              = $result['total'];

            //pagination
            $config = initPagination();
            $config["base_url"]     = base_url() . "admin/category/view/".encreptIt($cate_id).'/';
            $config["per_page"]     = $perpage;
            $config['uri_segment']  = 5;
            $config["total_rows"]   = $count;
            $config['attributes']   = array('class' => 'page-link');
            $this->pagination->initialize($config);

            $view_data['pagination_link']   = $this->pagination->create_links();
            $view_data['category_list']     = $result['category'];

            $data['category_view']      = $this->load->view('admin/category/ajax_category_view', $view_data, true);

            $return['status']           = true;
            $return['category_html']    = $data['category_view'];
            echo json_encode($return);
            die;
        }
    }

    public function commit($category_id = '') {
        if ($this->is_logined && ($this->input->server('REQUEST_METHOD') == 'POST')) {
            $return     = array();
            $this->form_validation->set_rules('cat_name', 'Category', 'required');
            $validate   = $this->form_validation->run();

            if ($validate == true) {
                $data['parent']     = 0;
                if ($category_id != '') {
                    $data['parent'] = decreptIt($category_id);
                }

                $data['name']       = $this->input->post('cat_name');
                $id                 = $this->input->post('category_id');
                $cat_id             = 0;
                $data['slug']       = $this->category_model->create_slug($id , $data['name']);

                if ($id != '') {
                    $cFilter['where']   = array('md5(id)' => $id);
                    $cFilter['row']     = 1;
                    $cdata              = $this->category_model->get_rows($cFilter);

                    if (!empty($cdata)) {

                        $cat_id = $cdata->id;
                        $this->category_model->update_table($data, array('md5(id)' => $id));
                        $return['status']   = TRUE;
                        $return['message']  = "Category updated successfully";
                    }
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s', time());
                    $insert_id              = $this->category_model->insert($data);

                    if ($insert_id > 0) {
                        $cat_id = $insert_id;
                    }

                    $return['status']   = TRUE;
                    $return['message']  = "Category inserted successfully";
                }

                if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {
                    
                    $path = getcwd() . '/uploads/product_category';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }

                    $config['upload_path']  = $path . '/' . md5($cat_id);
                    $upload_path            = '/uploads/product_category/' . md5($cat_id);

                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }

                    $config['allowed_types']    = 'gif|jpg|jpeg|png|PNG|JPG|JPEG';
                    $config['file_name']        = $_FILES["avatar"]['name'];
                    $config['max_size']         = $this->allowed_file_size;
                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('avatar')) {

                        if (!empty($id)) {
                            $cFilter['where']   = array('md5(id)' => $id);
                            $cFilter['row']     = 1;
                            $cData = $this->category_model->get_rows($cFilter);
                            if (!empty($cData) && !empty($cData->image)) {
                                $this->attachment_model->removeAttachment(array($cData->image));
                            }
                        } 

                        $upload_data    = $this->upload->data();
                        $aData['path']  = $upload_path.'/'.$upload_data['file_name'];
                        $aData['type']  = 'image';
                        $attachment_id   = $this->attachment_model->insert($aData);

                        if (!empty($attachment_id)) {

                            $update_attachment = array('image' => $attachment_id);
                            $this->category_model->update_table($update_attachment, array('id' => $cat_id));
                        } else {
                            $return['status']   = FALSE;
                            $return['message']  = "Somthing went wrong";
                        }
                    } else {
                        $return['status']   = FALSE;
                        $return['message']  = $this->upload->display_errors();
                    }
                }
            } else {
                $return['status']   = FALSE;
                $return['message']  = validation_errors();
            }
            echo json_encode($return);
            die;
        } else {
            redirect(admin_url());
        }
    }

    public function setup() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id = $this->input->post('id');
                $editdata = array();

                if (!empty($id)) {
                    $filter['select']   = array('angel_category.*');
                    $filter['where']    = array('md5(angel_category.id)' => $id);
                    $filter['row']      = 1;
                    $editdata['categorydata']   = $this->category_model->get_rows($filter);
                }
                $data['category_html']          = $this->load->view('admin/category/form', $editdata, true);
                echo json_encode($data);
                die;
            } else {
                $this->template->view('admin/category/index');
            }
        } else {
            redirect(admin_url());
        }
    }

    public function delete() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $cat_id     = $this->input->post('id');
                $id         = 0;

                if ($cat_id != '') {
                    $id         = decreptIt($cat_id);
                }

                if ($id > 0) {
                    $cFilter['where']   = array('id' => $id);
                    $cFilter['row']     = 1;
                    $category           = $this->category_model->get_rows($cFilter);

                    if (!empty($category)) {

                        $this->deleteCategory($category->id);

                        $return['status']   = true;
                        $return['message']  = 'Category deleted successfully.';
                    } else {
                        $return['status']   = false;
                        $return['message']  = 'Somthing went wrong';
                    }

                    echo json_encode($return);die;
                }

            } else {
                $this->template->view('admin/category/index');
            }
        } else {
            redirect(admin_url());
        }
    }

    public  function deleteCategory($id = 0) {

        if ($id != '') {

            $cFilter['where']   = array('parent' => $id);
            $category           = $this->category_model->get_rows($cFilter);

            if (!empty($category)) {

                foreach($category as $row) {
                    $parent =  $row->id;
                    $this->deleteCategory($parent);
                } 
            }

            $pFilter['where']   = array('category_id' => $id);
            $product_data       = $this->product_model->get_rows($pFilter);

            if (!empty($product_data)) {

                foreach($product_data as $row) {

                    $filter_img['where']    = array('product_id' => $row->id);
                    $img_data               = $this->product_images_model->get_rows($filter_img);

                    if (!empty($img_data)) {

                        foreach($img_data as $img_row) {

                            $this->attachment_model->removeAttachment(array($img_row->image));   
                            $this->product_images_model->delete(array('id' => $img_row->id));
                        }
                        $folder_path = getcwd().'/uploads/products/'.md5($row->id);
                        rrmdir($folder_path);
                    }
                    $this->cart_model->delete(array('product_id' => $row->id));
                    $this->product_model->delete(array('id' => $row->id));
                } 
            }

            $cFilter['where']   = array('id' => $id);
            $cFilter['row']     = 1;
            $cdata              = $this->category_model->get_rows($cFilter);

            if (!empty($cdata)) {
                $this->attachment_model->removeAttachment(array($cdata->image));
                $folder_path = getcwd().'/uploads/product_category/'.md5($cdata->id);
                rrmdir($folder_path);
            }
            $this->category_model->delete(array('id' => $id));
        }
    }
     public function sorting() {
        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $position       = $this->input->post('position');
                $i              = 1;
                $updateArray    = array();
                if (!empty($position)){
                    foreach($position as $val){
                        $updateArray[] = array( 'id'   => $val,
                                                'sort' => $i
                            );
                        $i++;
                    }
                }
                $this->category_model->update_batch($updateArray, 'id');
                $data['status'] = true;
                echo json_encode($data);
                die;
            } else {
                redirect(admin_url());
            }
        } else {
            redirect(admin_url());
        }
    }

}

?>