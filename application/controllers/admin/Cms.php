<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

     public function index() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'title',
                                    '1' => 'url',
                                    '2' => 'c.username',
                                    '3' => 'created_date',
                                    '4' => 'modify_date',
                                    '5' => 'is_active'
                                    );
                $filterCount    = $totalCount = $this->cms_model->get_rows(array(), true);

                $filter['join'] = array(
                                    '0' => array('table' => 'angel_admin as c', 'condition' => 'c.id = angel_cms.created_by'),
                                    '1' => array('table' => 'angel_admin as m', 'condition' => 'm.id = angel_cms.modify_by')
                                );
                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_cms.title', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_cms.url', 'value' => $searchString),
                        '1' => array('field' => 'c.username', 'value' =>  $searchString),
                        '2' => array('field' => 'm.username', 'value' =>  $searchString),
                        '3' => array('field' => 'angel_cms.created_date', 'value' => str_replace("/", "-", $searchString)),
                        '4' => array('field' => 'angel_cms.modify_date', 'value' => str_replace("/", "-", $searchString))
                    );
                    $filterCount    = $this->cms_model->get_rows($filter, true);
                }
                $filter['select']   = array('angel_cms.*','c.username as created_by','m.username as modify_by');
                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $query              = $this->cms_model->get_rows($filter);
                $cms                = array();

                foreach ($query as $row) {
                    $row_data                   = array();
                    $row_data['id']             = $row->id;
                    $row_data['title']          = $row->title;
                    $row_data['url']            = $row->url;
                    $row_data['created_by']     = $row->created_by;
                    $row_data['created_date']   = date('m/d/Y', strtotime($row->created_date));
                    $row_data['modify_date']    = date('m/d/Y', strtotime($row->created_date));
                    $row_data['status']         = ($row->is_active == '0') ? '<a class="badge badge-danger status_checks" data-id="'.md5($row->id).'" href="javascript:void(0)"  title="click to Active">Deactive</a>' : '<a class="badge badge-success status_checks" href="javascript:void(0)" data-id="'.md5($row->id).'"  title="click to Deactive">Active</a>';
                    $row_data['action']         = ' <a class="color-content" href="'.base_url().'admin/cms/setup/'.md5($row->id).'"><i class="material-icons">edit</i></a>
                                                    <a class="color-content" data-id="'.md5($row->id).'" id="delete" href="javascript:void(0)"><i class="material-icons">delete</i></a>';
                    $cms[]                      = $row_data;
                }
                $data['recordsTotal']           = $totalCount;
                $data['recordsFiltered']        = $filterCount;
                $data['data']                   = $cms;

                echo json_encode($data);die();
            } else {
                $this->template->view('admin/cms/index');
            }
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $data = array();
            if ($id != '') {
                $filter['where']= array('md5(id)' => $id);
                $filter['row']  = 1;
                $cms            = $this->cms_model->get_rows($filter);

                if (!empty($cms)) {
                    $data['cms']   = $cms;
                }
            }
            $this->template->view("admin/cms/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = 0) {
        if ($this->is_logined) {

            $data['title']      = $this->input->post('title');
            $data['url']        = $this->input->post('url');
            $data['content']    = $_POST['content'];
            $data['is_active']  = $this->input->post('is_active') == 'on' ? '1' : '0';
            $data['modify_by']  = $this->loginUser['id'];
            if ($id != '') {
                $this->cms_model->update_table($data, array('md5(id)' => $id));

                $this->session->set_flashdata('success', 'Page saved successfully.');
            } else {
                $data['created_by']     = $this->loginUser['id'];
                $data['created_date']   = date('Y-m-d H:i:s', time());
                $insert_id              = $this->cms_model->insert($data);

                if ($insert_id > 0) {
                    $this->session->set_flashdata('success', 'Page saved successfully.');
                } else {
                    $this->session->set_flashdata('error', 'Something went worng, Please try again later.');
                }
                $id = $insert_id;
            }

            redirect(admin_url().'cms');
        } else {
            redirect(admin_url());
        }
    }

    public function validate() {
        $title   = $this->input->post('title');
        $id      = $this->input->post('id');
        $filter  = array();
        if (isset($title) && ($title != "")) {
            if (isset($id) && ($id > 0)) {
                $filter['where'] = array('id !=' => $id, 'title' => $title);
            } else {
                $filter['where'] = array('title' => $title);
            }
        }
        $result = $this->cms_model->get_rows($filter,1);
        $return = TRUE;
        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }

    public function delete() {
        if ($this->is_logined && $this->loginUser['user_type'] == 0) {
            $id                 = $this->input->post('id');
            $uFilter['where']   = array('md5(id)' => $id);
            $uFilter['row']     = 1;
            $user               = $this->cms_model->get_rows($uFilter);

            if (!empty($user)) {
                $where = array('md5(id)' => $id);
                $this->cms_model->delete($where);

                $return['status']   = true;
                $return['message']  = 'Page deleted successfully.';
            } else {
                $return['status']   = false;
                $return['message']  = 'Somthing went wrong';
            }
            echo json_encode($return);die;
        } else {
            redirect(admin_url());
        }
    }

    public function checkStatus() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter['where']        = array('md5(id)' => $id);
                $filter['row']          = 1;
                $item                   = $this->cms_model->get_rows($filter);

                if (!empty($item)) {
                    if ($item->is_active == '0') {
                        $update_data['is_active']   = '1';
                    } else {
                        $update_data['is_active']   = '0';
                    }

                    $this->cms_model->update_table($update_data, array('id' => $item->id));
                    $data['status']     = TRUE;
                    $data['value']      = $update_data['is_active'];
                } else {
                    $data['status']   = FALSE;
                }
                echo json_encode($data);die;
            }
        }
        redirect(admin_url());
    }
}
?>