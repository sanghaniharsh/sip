<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EnterpriseProduct extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {
        
        if ($this->is_logined) {

            $return = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $searchval          = $this->input->post('searchval');
                $search_enterprise    = $this->input->post('search_enterprise');                

                if($search_enterprise > 0) {
                    $sfilter['enterprise_ids']= $search_enterprise;
                }

                if($searchval  != "") {
                    $sfilter['search']      = $searchval;
                }

                $perpage            = 48;
                $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
                $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);

                $result             = $this->enterprise_product_model->getProducts($sfilter);
                
                $data['products']= $result['product'];
                $data['total']      = $result['total'];

                $config                     = initPagination();
                $config["base_url"]         = base_url() . "admin/enterprise-product/";
                $config["total_rows"]       = $data['total'];

                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 3;

                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = 2;
                $config['attributes']       = array('class' => 'page-link');

                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();

                $data["links"]          = $str_links;
                
                
                if (!empty($data)) {

                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/enterprise_product/ajax_product_view', $data, true);

                } else {
                    $return['status']   = false;
                }

                echo json_encode($return);die;
            }
            

            $data['enterprises']   = $this->enterprise_model->getEnterprises();
            
            $this->template->view('admin/enterprise_product/index',$data);

        } else {

            redirect(admin_url());

        }

    }
    
    public function setup($id = '') {
        if ($this->is_logined) {
            $data['enterprise_ids'] = array();
            
            if ($id != '') {
                $pid = decreptIt($id);

                if ($pid > 0) {
                    $param['pid']       = $pid;
                    $param['row']       = 1;
                    
                    $productData        = $this->enterprise_product_model->getProducts($param);
                    $data['product'] = $productData['product'];
                    
                    $eFilter['select']      = array('GROUP_CONCAT(enterprise_id SEPARATOR ",") as enterprise_ids');
                    $eFilter['where']       = array('enterprise_product_id' => $productData['product']->id);
                    $eFilter['groupby']     = array('field' => 'enterprise_product_id');
                    $eFilter['row']         = 1;
                    $enterprises            = $this->product_enterprise_model->get_rows($eFilter);
                    
                    $iFilter['where']       = array('product_id' => $productData['product']->id);
                    $data['product_images'] = $this->enterprise_images_model->get_rows($iFilter);

                    $data['enterprise_ids'] = (!empty($enterprises) ? explode(',',$enterprises->enterprise_ids) : array());
                }
            }

            $data['enterprises']   = $this->enterprise_model->getEnterprises();
            
            $data['js_files']       = array(
                'select2.min.js'
            );
            $data['css_files']      = array(
                'select2.min.css'
            );

            $this->template->view('admin/enterprise_product/form', $data);
        } else {
            redirect(admin_url());
        }
    }
    
    public function commit($id = '') {
          if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('name', 'Product Name', 'trim|required');

            if ($this->form_validation->run() == TRUE) {

                $enterprises                = $this->input->post('enterprise');
                
                $data['name']               = $this->input->post('name');
                $data['slug']               = slug($data['name']);
                $data['price']              = $this->input->post('awarded_price') == 'on' ? 0 : $this->input->post('price');
                $data['model_number']       = $this->input->post('model_number');
                $data['status']             = $this->input->post('status') == 'on' ? '1' : '0';
                $data['awarded_price']      = $this->input->post('awarded_price') == 'on' ? '1' : '0';
                
                $insert_id                          = 0;

                if ($id != '') {
                    
                    $filter['pid']       = decreptIt($id);
                    
                    $filter['row']      = 1;   
                    $productData        = $this->enterprise_product_model->getProducts($filter);
                        

                    if (!empty($productData) && !empty($productData['product'])) {
                        $insert_id = $productData['product']->id;
                        $this->enterprise_product_model->update_table($data, array('id' => $insert_id));
                        
                        $this->session->set_flashdata('success', 'Product update successfully.');
                    } else {
                        $this->session->set_flashdata('error', 'Please try again letter');
                    }
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $insert_id              = $this->enterprise_product_model->insert($data);
                    $this->session->set_flashdata('success', 'Product saved successfully.');
                }
                
                if(!empty($enterprises) && $insert_id > 0 ) {
                    
                    $this->product_enterprise_model->delete(array('enterprise_product_id' => $insert_id));
                    
                    foreach ($enterprises as $key => $enterprise) {
                        $p_data[] = array(
                                            'enterprise_id'         => $enterprise,
                                            'enterprise_product_id' => $insert_id
                                        );
                    }
                    if(!empty($p_data)) {
                        $this->product_enterprise_model->insert_betch($p_data);
                    }
                    
                } else if(empty($enterprises) && $insert_id > 0) {
                    $this->product_enterprise_model->delete(array('enterprise_product_id' => $insert_id));
                }
                // $return['status']   = true;
                // $return['message']  = 'Product Details saved successfully.';
                
                redirect(admin_url().'enterprise-product/setup/'.encreptIt($insert_id));
                
            } else {
                redirect(admin_url().'enterprise-product/setup/'.encreptIt($insert_id));
            }
        } else {
            redirect(admin_url());
        }
    }
    
    public function commit_specification($id = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            if ($id != '') {

                $param['pid']               = decreptIt($id);
                $param['row']               = 1;
                $productData                = $this->enterprise_product_model->getProducts($param);

                
                if (!empty($productData) && !empty($productData['product'])) {
                    $data['description']    = $_POST['description'];
                    $data['specification']  = $_POST['specification'];
                    $data['warranty']       = $_POST['warranty'];

                    
                    $this->enterprise_product_model->update_table($data, array('id' => $productData['product']->id));

                    if (isset($productData['product']->id) && $productData['product']->id > 0) {
                        $return['status']   = true;
                        $return['message']  = 'Product Details saved successfully.';
                    } else {
                        $return['status']   = false;
                        $return['message']  = 'Something went worng, Please try again later.';
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = 'Something went worng';
                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }
    
    public  function delete($id = 0) {
        $id       = $this->input->post('id');
        
        if ($id != '') {

            $cFilter['where']   = array('id' => decreptIt($id));
            $cFilter['row']     = 1;
            $cdata              = $this->enterprise_product_model->get_rows($cFilter);

            if (!empty($cdata)) {
                
                $filter['where']    = array('product_id' => decreptIt($id));
                $item                   = $this->enterprise_images_model->get_rows($filter);
                
                if (!empty($item)) {
                    $attachmentId = array();
                    foreach ($item as $list) {
                        $attachmentId[] = $list->image;
                    }
                    $this->attachment_model->removeAttachment($attachmentId);

                    $folder_path = getcwd().'/uploads/enterprise_products/'.md5($cdata->id);
                    rrmdir($folder_path);

                    $this->enterprise_images_model->delete(array('product_id' => decreptIt($id)));
                }
            }
            $delete = $this->enterprise_product_model->delete(array('id' => decreptIt($id)));

            if($delete) {
                $return['status']   = TRUE;
                $return['message']   = 'Enterprise Product deleted successfully';
            } else {
                $return['status']   = FALSE;
                $return['message']   = 'Something went wrong';
            }
        } else {
            $return['status']   = FALSE;
            $return['message']   = 'Something went wrong';
        }
        echo json_encode($return);
        die;

    }
    
    public function updateProductImage() {
        $data       = array();
        $product_id = $this->input->post('product_id');

        if ($_FILES['product_images']['name'][0] && $product_id != '') {
            $pid = decreptIt($product_id);

            if ($pid > 0) {
                $param['pid']               = $pid;
                $param['row']               = 1;
                $productData                = $this->enterprise_product_model->getProducts($param);

                if (!empty($productData['product'])) {
                    $id     = $productData['product']->id;
                    $files  = $_FILES['product_images'];

                    foreach ($files['name'] as $i => $file) {
                        $_FILES['product_images']['name']     = $files['name'][$i];
                        $_FILES['product_images']['type']     = $files['type'][$i];
                        $_FILES['product_images']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['product_images']['error']    = $files['error'][$i];
                        $_FILES['product_images']['size']     = $files['size'][$i];

                        $blogPath = getcwd().'/uploads/enterprise_products';
                        if (!is_dir($blogPath)) {
                            mkdir($blogPath, 0777, true);
                        }

                        $file_name  = str_replace(' ', '', $files['name'][$i]);
                        $uploadPath = $blogPath.'/'.md5($id).'/';

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }

                        $config['upload_path']          = $uploadPath;
                        $config['allowed_types']        = 'gif|jpg|jpeg|png|jpeg|JPG|JPEG';
                        $config['file_name']            = $file_name;
                        $config['max_size']             = $this->allowed_file_size;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('product_images')){
                            $fileData                   = $this->upload->data();

                            $attachmentData['path']     = '/uploads/enterprise_products/'.md5($id).'/'.$fileData['file_name'];
                            correctOrientation(getcwd().$attachmentData['path']);
                            
                            $attachmentData['type']     = 'image';
                            $attachment_id              = $this->attachment_model->insert($attachmentData);

                            $uploadData['product_id']   = $id;
                            $uploadData['image']        = $attachment_id;
                            $this->enterprise_images_model->insert($uploadData);
                        } else {
    //                            echo '<pre>';print_r($this->upload->display_errors());die;
                        }
                    }

                    $filter['where']            = array('product_id' => $id);
                    $image                      = $this->enterprise_images_model->get_rows($filter);
                    $view_data['product_image'] = $image;

                    $data['status']     = true;
                    $data['message']    = 'Image uploaded successfully';
                    $data['html']       = $this->load->view('admin/enterprise_product/ajax_image', $view_data, true);
                } else {
                    $data['status']     = false;
                    $data['message']    = 'No record found';
                }
            } else {
                $data['status']     = false;
                $data['message']    = 'something went wrong';
            }
        } else {
            $data['status']     = false;
            $data['message']    = 'Please choose image';
        }
        echo json_encode($data);die;
    }
    
    public function deleteProductImage() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data       = array();
                $image_id   = $this->input->post('image_id');

                $filter_img['where']    = array('md5(id)' => $image_id);
                $filter_img['row']      = 1;
                $item                   = $this->enterprise_images_model->get_rows($filter_img);

                if (!empty($item)) {

                    $this->attachment_model->removeAttachment(array($item->image));

                    $where = array('md5(id)' => $image_id);
                    $this->enterprise_images_model->delete($where);

                    $data['status']     = true;
                    $data['message']    = "Product Image deleted successfully.";
                } else {
                    $data['status']     = false;
                    $data['message']    = "No image found.";
                }
                echo json_encode($data);die;
            } else {
                $this->session->set_flashdata('success', 'This method is not allowed.');
            }
            redirect(admin_url().'admin/blog');
        } else {
            redirect(admin_url().'admin');
        }
    }
    
}
?>