<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auction extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {

        if ($this->is_logined) {
            $return = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search = $this->input->post('search');

                if($search != "") {
                    $sfilter['search']  = $search;
                }
                $perpage                = 200;
                $start                  = ($start != 0) ? ($start - 1) * $perpage : 0;
                $sfilter['limit']       = array('limit' => $perpage, 'from' => $start);
                $result                 = $this->auction_model->getAuction($sfilter);
                $data['auction_list']   = $result['auction'];
                $data['total']          = $result['total'];

                //pagination
                $config                     = initPagination();
                $config["base_url"]         = base_url() . "admin/auction/index/";
                $config["total_rows"]       = $data['total'];
                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = $data['total'];
                $config['attributes']       = array('class' => 'page-link');
                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();
                $data["links"]          = $str_links;

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/auction/ajax_auction_view', $data, true);
                } else {
                    $return['status']   = false;
                }

                echo json_encode($return);die;
            }
            $data["js_files"]   = array("jquery-ui.js");
            $this->template->view('admin/auction/index',$data);
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {

        if ($this->is_logined) {
            $data = array();

            if ($id != '') {
                $a_id               = decreptIt($id);
                $filter['where']    = array('id' => $a_id);
                $filter['row']      = 1;
                $auction            = $this->auction_model->get_rows($filter);

                if (!empty($auction)) {

                    $iFilter['where']       = array('auction_id' => $auction->id);
                    $data['auction_image']  = $this->auction_images_model->get_rows($iFilter);
                    $data['auction']        = $auction;
                }
            }
            $data['css_files']   = array("jquery.timepicker.min.css");
            $data['js_files']   = array("jquery.timepicker.min.js");
            $this->template->view("admin/auction/form", $data);
        } else {
            redirect(admin_url());
        }
    }

    public function commit($id = '') {

        if ($this->is_logined && ($this->input->server('REQUEST_METHOD') == 'POST')) {

//            $this->form_validation->set_rules('title', 'Auction Title', 'trim|required');
//            $this->form_validation->set_rules('a_date', 'Auction Date', 'trim|required');
//            $this->form_validation->set_rules('a_time', 'Auction Time', 'trim|required');
//            $this->form_validation->set_rules('from_time', 'Auction From Time', 'trim|required');
//            $this->form_validation->set_rules('to_time', 'Auction To Time', 'trim|required');
//            $this->form_validation->set_rules('address', 'Auction Address', 'trim|required');
//            $this->form_validation->set_rules('addition_comment', 'Auction Addition Comment', 'trim|required');

//            if ($this->form_validation->run() == TRUE) {
            if (TRUE) {
                $auction_type               = ($this->input->post('auction_type') == 'on' ? '1' : '0');
                $date                       = $this->input->post('a_date');
                $data['a_date']             = $auction_type == '1' ? $date : date('Y-m-d', strtotime($date));
                $data['title']              = $this->input->post('title');
                $data['a_time']             = $auction_type == '1' ? $this->input->post('a_time') : date("H:i", strtotime($this->input->post('a_time'))); 
                $data['from_time']          = date("H:i", strtotime($this->input->post('from_time')));
                $data['to_time']            = date("H:i", strtotime($this->input->post('to_time')));
                $data['address']            = $this->input->post('address');
                $data['video_link']         = $this->input->post('video_link');
                $data['additional_comment'] = $this->input->post('addition_comment');
                $data['slug']               = slug($data['title']);
                $data['is_active']          = $this->input->post('is_active') == 'on' ? '1' : '0';
                $data['auction_type']       = $auction_type;
                $auction_id                 = $this->input->post('auction_id');
                $a_id                       = decreptIt($auction_id);
                $insert_id                  = 0;
                if ($a_id > 0) {

                    $filter['select']   = array('id');
                    $filter['where']    = array('id' => $a_id);
                    $filter['row']      = 1;
                    $result             = $this->auction_model->get_rows($filter);

                    if (!empty($result)) {
                        $insert_id = $result->id;
                        $this->auction_model->update_table($data, array('id' => $result->id));
                        $this->session->set_flashdata('success', 'Auction saved successfully.');
                    } else {
                        $this->session->set_flashdata('error', 'somthing went wrong.');
                    }
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $insert_id              = $this->auction_model->insert($data);

                    if ($insert_id > 0) {
                        $this->session->set_flashdata('success', 'Auction saved successfully.');
                    } else {
                        $this->session->set_flashdata('error', 'Something went worng, Please try again later.');
                    }
                }

                if ($_FILES['equipment_pdf']['name'] != '') {

                    if($a_id > 0) {
                        $filter['select']   = array('equipment_pdf');
                        $filter['where']    = array('id' => $a_id);
                        $filter['row']      = 1;
                        $result             = $this->auction_model->get_rows($filter);

                        if(!empty($result)) {
                            $this->attachment_model->removeAttachment($result->equipment_pdf);
                        }
                    }
                    $auctionPath = getcwd().'/uploads/auction/pdf/';
                    if (!is_dir($auctionPath)) {
                        mkdir($auctionPath, 0777, true);
                    }

                    $uploadPath = $auctionPath.'/'.md5($insert_id).'/';

                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, 0777, true);
                    }

                    $config['file_name']            = $_FILES['equipment_pdf']['name'];
                    $config['upload_path']          = $uploadPath;
                    $config['allowed_types']        = 'pdf';
                    $this->load->library('upload', $config);

                    if($this->upload->do_upload('equipment_pdf')){
                        $fileData                       = $this->upload->data();
                        $attachmentData['path']         = '/uploads/auction/pdf/'.md5($insert_id).'/'.$fileData['file_name'];
                        $attachmentData['type']         = 'pdf';
                        $attachment_id                  = $this->attachment_model->insert($attachmentData);
                        $updateData['equipment_pdf']    = $attachment_id;

                        $this->auction_model->update_table($updateData, array('id' => $insert_id));
                    } else {
                    }
                }

                if ($_FILES['auction_images']['name'][0] != '') {

                    $files = $_FILES['auction_images'];
                    foreach ($files['name'] as $i => $file) {
                        $_FILES['auction_images']['name']     = $files['name'][$i];
                        $_FILES['auction_images']['type']     = $files['type'][$i];
                        $_FILES['auction_images']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['auction_images']['error']    = $files['error'][$i];
                        $_FILES['auction_images']['size']     = $files['size'][$i];

                        $auctionPath = getcwd().'/uploads/auction/image/';
                        if (!is_dir($auctionPath)) {
                            mkdir($auctionPath, 0777, true);
                        }

                        $uploadPath = $auctionPath.'/'.md5($insert_id).'/';
                        $path       = '/uploads/auction/image/'.md5($insert_id).'/';

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }

                        $config['file_name']            = $files['name'][$i];
                        $config['upload_path']          = $uploadPath;
                        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPG|JPEG';
                        $config['max_size']             = $this->allowed_file_size;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if($this->upload->do_upload('auction_images')){
                            $fileData                       = $this->upload->data();
                            $attachmentData['path']         = $path.$fileData['file_name'];
                            $attachmentData['type']         = 'image';
                            $attachment_id                  = $this->attachment_model->insert($attachmentData);

                            $uploadData['auction_id']       = $insert_id;
                            $uploadData['image']            = $attachment_id;
                            $this->auction_images_model->insert($uploadData);
                        } else {
                        }
                    }
                }
 
                redirect(admin_url() . 'auction');
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(admin_url().'auction/');
            }
        } else {
            redirect('/admin');
        }
    }

    public function delete() {

        if ($this->is_logined) {
            $data = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $a_id                   = decreptIt($id);
                $filter_add['where']    = array('auction_id' => $a_id);
                $item                   = $this->auction_images_model->get_rows($filter_add);

                if (!empty($item)) {
                    $attachmentId = array();
                    foreach ($item as $list) {
                        $attachmentId[] = $list->image;
                    }
                    $this->attachment_model->removeAttachment($attachmentId);

                    $folder_path = getcwd().'/uploads/auction/image/'.md5($a_id);
                    rrmdir($folder_path);

                    $this->auction_images_model->delete(array('auction_id' => $a_id));
                } 

                $filter['select']   = array('equipment_pdf');
                $filter['where']    = array('id' => $a_id);
                $filter['row']      = 1;
                $result             = $this->auction_model->get_rows($filter);
                
                if(!empty($result)) {
                    $this->attachment_model->removeAttachment($result->equipment_pdf);
                    $folder_path = getcwd().'/uploads/auction/pdf/'.md5($a_id);
                    rrmdir($folder_path);  
                }

                $this->auction_model->delete(array('id' => $a_id));

                $data['status']     = true;
                $data['message']    = "Auction deleted successfully.";
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }

    public function deleteAuctionImage() {

        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data       = array();
                $id         = $this->input->post('image_id');
                $image_id   = decreptIt($id);
                $filter_img['where']    = array('id' => $image_id);
                $filter_img['row']      = 1;
                $item                   = $this->auction_images_model->get_rows($filter_img);

                if (!empty($item)) {

                    $this->attachment_model->removeAttachment(array($item->image));

                    $where = array('id' => $image_id);
                    $this->auction_images_model->delete($where);

                    $data['status']     = true;
                    $data['message']    = "Auction Image deleted successfully.";
                } else {
                    $data['status']     = false;
                    $data['message']    = "No image found.";
                }
                echo json_encode($data);die;
            } else {
                $this->session->set_flashdata('success', 'This method is not allowed.');
            }
            redirect(admin_url().'admin/auction');
        } else {
            redirect(admin_url().'admin');
        }
    }
      
    public function timevalidate() {
        $t_time     = $this->input->post('to_time');
        $f_time     = $this->input->post('from_time');

        if (strtotime($t_time)  > strtotime($f_time)) {
            $return = true;  
        } else {
            $return= false;
        }
        echo json_encode($return);die;
    }
    public function sorting() {
        if ($this->is_logined) {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $position       = $this->input->post('position');
                $i              = 1;
                $updateArray    = array();
                if (!empty($position)){
                    foreach($position as $val){
                        $updateArray[] = array( 'id'   => $val,
                                                'sort' => $i
                            );
                        $i++;
                    }
                }
                $this->auction_model->update_batch($updateArray, 'id');
                $data['status'] = true;
                echo json_encode($data);
                die;
            } else {
                redirect(admin_url());
            }
        } else {
            redirect(admin_url());
        }
    }
}
?>