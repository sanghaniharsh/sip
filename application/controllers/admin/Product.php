<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined   = $this->get_admin_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function index($start = 0) {
        if ($this->is_logined) {
            $return = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $product_type          =  0;
                $searchval          = $this->input->post('searchval');
                $search_category    = $this->input->post('search_category');                

                if($search_category > 0) {
                    $category_data          = $this->category_model->getChildData($search_category);
                    $category_data[]        = $search_category;
                    $sfilter['category_ids']= $category_data;
                }

                if($searchval  != "") {
                    $sfilter['search']      = $searchval;
                }
                $sfilter['type']    = $product_type;
                $perpage            = 48;
                $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
                $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);
                $result             = $this->product_model->getProducts($sfilter);
                $data['products']   = $result['product'];
                $data['total']      = $result['total'];

                //pagination
                $config                     = initPagination();
                $config["base_url"]         = base_url() . "admin/product/index/";
                $config["total_rows"]       = $data['total'];
                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = 2;
                $config['attributes']       = array('class' => 'page-link');
                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();
                $data["links"]          = $str_links;

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('admin/product/ajax_product_view', $data, true);
                } else {
                    $return['status']   = false;
                }
                echo json_encode($return);die;
            }

            $this->template->view('admin/product/index');
        } else {
            redirect(admin_url());
        }
    }

    public function setup($id = '') {
        if ($this->is_logined) {
            $cFilter['where']       = array('parent' => '0');
            $cFilter['result']      = 1;
            $data['categoryData'][] = $this->category_model->get_rows($cFilter);
            $data['brand']          = $this->brand_model->get_rows();
            $data['js_files']       = array(
                'select2.min.js'
            );
            $data['css_files']      = array(
                'select2.min.css'
            );
            if ($id != '') {
                $pid = decreptIt($id);

                if ($pid > 0) {
                    $param['pid']       = $pid;
                    $productData        = $this->product_model->getProducts($param);

                    if (!empty($productData)) {
                        $data['categoryData']   = $this->category_model->getParents($productData['product']->category_id);
                        $data['product']        = $productData['product'];
                        $iFilter['where']       = array('product_id' => $productData['product']->id);
                        $data['product_images'] = $this->product_images_model->get_rows($iFilter);
                    }
                }
            }

            $this->template->view('admin/product/form', $data);
        } else {
            redirect('/admin');
        }
    }

    public function detail() {
        $data = array();

        $this->template->view('admin/product/detail', $data);
    }

    public function commit($id = '') {

        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('pname', 'Product Name', 'trim|required');

//            $this->form_validation->set_rules('category[]', 'Category', 'callback_validate_category');
//            $this->form_validation->set_rules('pprice', 'Price', 'trim|required');
//            $this->form_validation->set_rules('lprice', 'List Price', 'trim|required');
//            $this->form_validation->set_rules('pModalNumber', 'Modal Number', 'trim|required');
//            $this->form_validation->set_message('validate_category', 'Please choose category');

            if ($this->form_validation->run() == TRUE) {
                $category                   = $this->input->post('category');
                $category_id                = 0;
                foreach($category as $list) {
                    if ($list != '') {
                        $category_id = $list;
                    }
                }

                $data['category_id']        = $category_id;
                $data['name']               = $this->input->post('pname');
                $data['brand_id']           = $this->input->post('brand_id');
                $data['slug']               = slug($data['name']);
                $data['price']              = $this->input->post('pprice');
                $data['list_price']         = $this->input->post('lprice');
                $data['length']             = $this->input->post('length');
                $data['weight']             = $this->input->post('weight');
                $data['height']             = $this->input->post('height');
                $data['width']              = $this->input->post('width');
                $data['is_active']          = $this->input->post('is_active') == 'on' ? '1' : '0';
                $data['call_for_price']     = $this->input->post('call_for_price') == 'on' ? '1' : '0';
                $data['modal_number']       = $this->input->post('pModalNumber');
                $data['video_url']          = $this->input->post('video_url');

                $shipping_option            = $this->input->post('shipping_option');

                $data['shipping_price']             = "0.00";
                $data['is_free_shipping']           = '1';
                $data['display_shipping_details']   = '0';
                $data['ups_shipping']               = '0';
                if ($shipping_option == 'manual') {
                    $data['shipping_price']             = $this->input->post('shipping_price');
                    $data['is_free_shipping']           = '0';
                    $data['display_shipping_details']   = '0';
                    $data['ups_shipping']               = '0';
                } else if ($shipping_option == 'contact_us') {
                    $data['shipping_price']             = "0.00";
                    $data['is_free_shipping']           = '0';
                    $data['display_shipping_details']   = '1';
                    $data['ups_shipping']               = '0';
                } else if ($shipping_option == 'ups_shipping') {
                    $data['shipping_price']             = "0.00";
                    $data['is_free_shipping']           = '0';
                    $data['display_shipping_details']   = '0';
                    $data['ups_shipping']               = '1';
                }
                $data['shipping_option']    = $shipping_option;

                /*
                $is_free_shipping   = $this->input->post('is_free_shipping');
                $shipping_price     = 0;
                if ($is_free_shipping != 'on') {
                    $shipping_price = $this->input->post('shipping_price');
                }

                $data['shipping_price']             = $shipping_price;
                $data['is_free_shipping']           = $this->input->post('is_free_shipping') == 'on' ? '1' : '0';
                $data['display_shipping_details']   = $this->input->post('display_shipping_details') == 'on' ? '1' : '0';
                $data['ups_shipping']               = $this->input->post('ups_shipping') == 'on' ? '1' : '0';
                 * 
                 */
                $insert_id                          = 0;

                if ($id != '') {
                    $param['pid']               = decreptIt($id);
                    $productData                = $this->product_model->getProducts($param);

                    if (!empty($productData) && !empty($productData['product'])) {
                        $insert_id = $productData['product']->id;
                        $this->session->set_flashdata('success', 'Details saved successfully.');
                        $this->product_model->update_table($data, array('id' => $productData['product']->id));
                    } else {
                        $this->session->set_flashdata('error', 'Please try again letter');
                    }
                } else {
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $insert_id              = $this->product_model->insert($data);
                    $this->session->set_flashdata('success', 'Details saved successfully.');
                }

                if ($insert_id > 0) {
                    redirect(admin_url().'product/setup/'.encreptIt($insert_id));
                } else {
                    redirect(admin_url().'product');
                }
            } else {
//                $this->session->set_flashdata('error', validation_errors());
                if ($id != '') {
                    redirect(admin_url().'product/setup/'.encreptIt($insert_id));
                } else {
//                    redirect(admin_url().'product/setup');
                    redirect(admin_url().'product');
                }
            }
        } else {
            redirect('/admin');
        }
    }

    public function commit_specification($id = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            if ($id != '') {

//                $this->form_validation->set_rules('description', 'Product Description', 'trim|required');
//                $this->form_validation->set_rules('specification', 'Specification', 'trim|required');
//                $this->form_validation->set_rules('warranty', 'Warranty', 'trim|required');
//                
//                if ($this->form_validation->run() == TRUE) {
                    $param['pid']               = decreptIt($id);
                    $productData                = $this->product_model->getProducts($param);

                    if (!empty($productData) && !empty($productData['product'])) {
                        $data['description']    = $_POST['description'];
                        $data['specification']  = $_POST['specification'];
                        $data['warranty']       = $_POST['warranty'];

                        if (isset($_FILES['specification_pdf']) && $_FILES['specification_pdf']['name'] != '') {
                            if ($param['pid'] != '') {
                                $filter['select']   = array('specification_pdf');
                                $filter['where']    = array('id' => $param['pid']);
                                $filter['row']      = 1;
                                $result             = $this->product_model->get_rows($filter);

                                if(!empty($result)) {
                                    $this->attachment_model->removeAttachment($result->specification_pdf);
                                }
                            }

                            $auctionPath = getcwd().'/uploads/product/pdf/';
                            if (!is_dir($auctionPath)) {
                                mkdir($auctionPath, 0777, true);
                            }

                            $uploadPath = $auctionPath.'/'.md5($param['pid']).'/';

                            if (!is_dir($uploadPath)) {
                                mkdir($uploadPath, 0777, true);
                            }

                            $config['file_name']            = $_FILES['specification_pdf']['name'];
                            $config['upload_path']          = $uploadPath;
                            $config['allowed_types']        = 'pdf';
                            $this->load->library('upload', $config);

                            if($this->upload->do_upload('specification_pdf')){
                                $fileData                       = $this->upload->data();
                                $attachmentData['path']         = '/uploads/product/pdf/'.md5($param['pid']).'/'.$fileData['file_name'];
                                $attachmentData['type']         = 'pdf';
                                $attachment_id                  = $this->attachment_model->insert($attachmentData);
                                $data['specification_pdf']      = $attachment_id;
                            }
                        }

                        $this->product_model->update_table($data, array('id' => $productData['product']->id));

                        if (isset($productData['product']->id) && $productData['product']->id > 0) {
                            $return['status']   = true;
                            $return['message']  = 'Product Details saved successfully.';
                        } else {
                            $return['status']   = false;
                            $return['message']  = 'Something went worng, Please try again later.';
                        }
                    } else {
                        $return['status']   = false;
                        $return['message']  = 'Something went worng';
                    }
//                } else {
//                    $return['status']   = false;
//                    $return['message']  = 'Please fill required fields';
//                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }

    public function loadQuestion($id = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            if ($id != '') {

                $filter['where']    = array('product_id' => decreptIt($id));
                $filter['orderby']  = array('field' => 'id', 'order' => 'DESC');
                $data['questions']  = $this->question_model->get_rows($filter);

                $return['status']   = true;
                $return['html']     = $this->load->view('admin/product/question_row', $data, true);
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }

    public function openQuestion($id = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            if ($id != '') {
                $data       = array();
                $question_id= $this->input->post('question_id');
                if ($question_id != '') {
                    $filter['where']    = array('id' => decreptIt($question_id));
                    $filter['row']      = 1;
                    $data['question']   = $this->question_model->get_rows($filter);
                }

                $return['status']   = true;
                $return['html']     = $this->load->view('admin/product/question_modal', $data, true);
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }

    public function commitQuestion($id = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            if ($id != '') {

                $this->form_validation->set_rules('question', 'Question', 'trim|required');
                $this->form_validation->set_rules('options[]', 'Options', 'trim|required');
                
                if ($this->form_validation->run() == TRUE) {
                    $question_id            = $this->input->post('question_id');
                    $q_data['product_id']   = decreptIt($id);
                    $q_data['question']     = $this->input->post('question');
                    $q_data['options']      = json_encode($this->input->post('options[]'));
                    
                    if (isset($question_id) && ($question_id != '')) {
                        $this->question_model->update_table($q_data, array('id' => decreptIt($question_id)));
                        $return['status']   = true;
                        $return['message']  = 'Question saved successfully.';
                    } else {
                        $insert_id = $this->question_model->insert($q_data);
                        if ($insert_id > 0) {
                            $return['status']   = true;
                            $return['message']  = 'Question saved successfully.';
                        } else {
                            $return['status']   = false;
                            $return['message']  = 'Something went worng, Please try again later.';
                        }
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = 'Please fill required fields';
                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }
    
    public function removeQuestion() {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('question_id');
            if ($id != '') {
                $filter['where']    = array('id' => decreptIt($id));
                $this->question_model->delete($filter['where']);

                $return['status']   = true;
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }

    public function updateProductImage() {
        $data       = array();
        $product_id = $this->input->post('product_id');

        if ($_FILES['product_images']['name'][0] && $product_id != '') {
            $pid = decreptIt($product_id);

            if ($pid > 0) {
                $param['pid']               = $pid;
                $productData                = $this->product_model->getProducts($param);

                if (!empty($productData['product'])) {
                    $id     = $productData['product']->id;
                    $files  = $_FILES['product_images'];

                    foreach ($files['name'] as $i => $file) {
                        $_FILES['product_images']['name']     = $files['name'][$i];
                        $_FILES['product_images']['type']     = $files['type'][$i];
                        $_FILES['product_images']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['product_images']['error']    = $files['error'][$i];
                        $_FILES['product_images']['size']     = $files['size'][$i];

                        $blogPath = getcwd().'/uploads/products';
                        if (!is_dir($blogPath)) {
                            mkdir($blogPath, 0777, true);
                        }

                        $file_name  = str_replace(' ', '', $files['name'][$i]);
                        $uploadPath = $blogPath.'/'.md5($id).'/';

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath, 0777, true);
                        }

                        $config['upload_path']          = $uploadPath;
                        $config['allowed_types']        = 'gif|jpg|jpeg|png|jpeg|JPG|JPEG';
                        $config['file_name']            = $file_name;
                        $config['max_size']             = $this->allowed_file_size;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('product_images')){
                            $fileData                   = $this->upload->data();

                            $attachmentData['path']     = '/uploads/products/'.md5($id).'/'.$fileData['file_name'];
                            correctOrientation(getcwd().$attachmentData['path']);
                            
                            $attachmentData['type']     = 'image';
                            $attachment_id              = $this->attachment_model->insert($attachmentData);

                            $uploadData['product_id']   = $id;
                            $uploadData['image']        = $attachment_id;
                            $this->product_images_model->insert($uploadData);
                        } else {
    //                            echo '<pre>';print_r($this->upload->display_errors());die;
                        }
                    }

                    $filter['where']            = array('product_id' => $id);
                    $image                      = $this->product_images_model->get_rows($filter);
                    $view_data['product_image'] = $image;

                    $data['status']     = true;
                    $data['message']    = 'Image uploaded successfully';
                    $data['html']       = $this->load->view('admin/product/ajax_image', $view_data, true);
                } else {
                    $data['status']     = false;
                    $data['message']    = 'No record found';
                }
            } else {
                $data['status']     = false;
                $data['message']    = 'something went wrong';
            }
        } else {
            $data['status']     = false;
            $data['message']    = 'Please choose image';
        }
        echo json_encode($data);die;
    }
    
    public function getSubCategory() {

        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $categoryId = $this->input->post('subCategoryId');
            $data       = array();

            if ($categoryId > 0) {
                $view_data['categoryData'] = $this->category_model->getCategoryData($categoryId);

                $data['status'] = true;
                $data['html']   = $this->load->view('admin/product/ajax_category_list', $view_data, true);
            } else {
                $data['status'] = false;
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }

    public function deleteProductImage() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data       = array();
                $image_id   = $this->input->post('image_id');

                $filter_img['where']    = array('md5(id)' => $image_id);
                $filter_img['row']      = 1;
                $item                   = $this->product_images_model->get_rows($filter_img);

                if (!empty($item)) {

                    $this->attachment_model->removeAttachment(array($item->image));

                    $where = array('md5(id)' => $image_id);
                    $this->product_images_model->delete($where);

                    $data['status']     = true;
                    $data['message']    = "Product Image deleted successfully.";
                } else {
                    $data['status']     = false;
                    $data['message']    = "No image found.";
                }
                echo json_encode($data);die;
            } else {
                $this->session->set_flashdata('success', 'This method is not allowed.');
            }
            redirect(admin_url().'admin/blog');
        } else {
            redirect(admin_url().'admin');
        }
    }

    public function removeSpecificationPDF() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data           = array();
                $product_id     = $this->input->post('product_id');

                $filter_img['where']    = array('md5(id)' => $product_id);
                $filter_img['row']      = 1;
                $item                   = $this->product_model->get_rows($filter_img);

                if (!empty($item)) {

                    $this->attachment_model->removeAttachment(array($item->specification_pdf));

                    $where = array('md5(id)' => $product_id);
                    $this->product_model->update_table(array('specification_pdf' => 0), $where);

                    $data['status']     = true;
                    $data['message']    = "Specification PDF deleted successfully.";
                } else {
                    $data['status']     = false;
                    $data['message']    = "No pdf found.";
                }
                echo json_encode($data);die;
            } else {
                $this->session->set_flashdata('success', 'This method is not allowed.');
            }
            redirect(admin_url().'admin/blog');
        } else {
            redirect(admin_url().'admin');
        }
    }

    public function delete() {
        if ($this->is_logined) {
            $data = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter_add['where']    = array('md5(product_id)' => $id);
                $item                   = $this->product_images_model->get_rows($filter_add);

                if (!empty($item)) {
                    $attachmentId = array();
                    foreach ($item as $list) {
                        $attachmentId[] = $list->image;
                    }
                    $this->attachment_model->removeAttachment($attachmentId);

                    $folder_path = getcwd().'/uploads/product/'.$id;
                    rrmdir($folder_path);

                    $this->product_images_model->delete(array('md5(product_id)' => $id));
                }
                $this->cart_model->delete(array('md5(product_id)' => $id));
                $this->product_model->delete(array('md5(id)' => $id));

                $data['status']     = true;
                $data['message']    = "Product deleted successfully.";
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }

    public function validate_category() {
        $category       = $this->input->post('category');
        $return         = false;
        $category_id    = 0;

        if (!empty($category)) {
            foreach ($category as $row) {
                $category_id = $row;
            }

            if ($category_id > 0) {
                $cData = $this->category_model->getChild($category_id);

                if (empty($cData)) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    public function ajax_validate_category() {
        $category       = $this->input->post('category');
        $return         = false;

        if ($category > 0) {
            $cData = $this->category_model->getChild($category);

            if (empty($cData)) {
                $return = true;
            }
        }
        echo json_encode($return);
    }
    public function commit_addon_product($pid = '') {
        if ($this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST') {
            $return         = array();
            $product_id     = decreptIt($pid);
            if ($product_id > 0) {
                $addon_product_id       = $this->input->post('addon_product_id');

                if (!empty($addon_product_id)) {
                    $addon_product_array    = array();
                    if (!empty($addon_product_id)) {
                        foreach($addon_product_id as $a_row) {
                            $addon_product_array[]  = array(
                                   'product_id'         => $product_id,
                                   'addon_product_id'   => $a_row,
                                   'created_date'       => date('Y-m-d H:i:s'),
                            );
                        }
                    }
                    if (!empty($addon_product_array)) {
                        $this->addon_product_model->insert_betch($addon_product_array);
                        $return['status']   = true;
                        $return['message']  = "Addon Product Added successfully.";
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = 'Please fill required fields';
                }
            } else {
                $return['status']   = false;
                $return['message']  = 'Please try again letter';
            }
            echo json_encode($return);die;
        } else {
            redirect('/admin');
        }
    }
    public function addon_product_list() {
        if ($this->is_logined) {
            $return = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $product_type       =  0;
                $pid                = $this->input->post('product_id');
                $searchval          = $this->input->post('searchval');
                $search_category    = $this->input->post('search_category');                
                $product_id         = decreptIt($pid);

                if ($product_id > 0) {
                    $sfilter['product_id']  = $product_id;
                    $sfilter['type']        = $product_type;
                    $result                 = $this->addon_product_model->getAddonProducts($sfilter);
                    $data['products']       = $result['product'];
                    if (!empty($data)) {
                        $return['status']   = true;
                        $return['html']     = $this->load->view('admin/product/ajax_addon_product_view', $data, true);
                    } else {
                        $return['status']   = false;
                    }
                }
                echo json_encode($return);die;
            }
        } else {
            redirect(admin_url());
        }
    }
    public function addon_product_delete() {
        if ($this->is_logined) {
            $data = array();

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                     = $this->input->post('id');
                $filter_add['where']    = array('md5(id)' => $id);
                $item                   = $this->addon_product_model->get_rows($filter_add);
                if (!empty($item)) {
                    $this->addon_product_model->delete(array('md5(id)' => $id));
                    $data['status']     = true;
                    $data['message']    = "Addon Product deleted successfully.";
                } else {
                   $data['status']     = false;
                   $data['message']    = "Something went wrong"; 
                }
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
            }
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }
    public function addon_product_options() {
        if ($this->is_logined) {
            $data = array();
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $id                         = $this->input->post('product_id');
                $product_id                 = decreptIt($id);
                $adFilter['where']          = array('is_active' => '1','product_id' => $product_id);
                $addon_products             = $this->addon_product_model->get_rows($adFilter);
                $exists_addon_products_ids  = array();
                $html                       = '';
                if (!empty($addon_products)) {
                    foreach ($addon_products as $ad_row) {
                        $exists_addon_products_ids[] = $ad_row->addon_product_id;
                    }
                }
                if (!empty($exists_addon_products_ids)) {
                    $aFilter['where']           = array('is_active' => '1','type' => 0);
                    $aFilter['where_not_in']    = array('field' => 'id', 'value' => $exists_addon_products_ids);
                    $addon_products             = $this->product_model->get_rows($aFilter);
                } else {
                    $pFilter['where']       = array('is_active' => '1','type' => 0);
                    $addon_products         = $this->product_model->get_rows($pFilter);
                }
                if (!empty($addon_products)) {
                    foreach ($addon_products as $row) {
                        $html .= '<option value="'.$row->id.'">'.$row->name.'</option>'; 
                    }
                    $data['status']     = true;
                    $data['message']    = "Success";
                } 
            } else {
                $data['status']     = false;
                $data['message']    = "Something went wrong";
                
            }
            $data['html']    = $html;
            echo json_encode($data);die;
        } else {
            redirect('/admin');
        }
    }
}
?>