<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();

    }

    public function index() {
    
    }

    public function check_login() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $remember       = trim($this->input->post('remember_me'));
            $redirect_url   = $_SERVER['HTTP_REFERER'];
            $filter['where']= array('email' => trim($this->input->post('email')), 'password' => trim(encreptIt($this->input->post('password'))));
            $filter['row']  = 2;
            $row            = $this->user_model->get_rows($filter);
            if (!empty($row) > 0) {
                if ($remember == "on") {
                    $cookie_data['user_value']   = $row['email'] . "," . decreptIt($row['password']);
                    $cookie_data['expire_time']  = 30 * 24 * 60 * 60;
                    $this->set_cookie_user($cookie_data);
                }

                $user_session['session_id'] = encreptIt($row['id']);
                $this->set_authorized_user($user_session);

                if ($redirect_url != "") {
                    redirect($redirect_url);
                } else {
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('error', 'Wrong Username or Password.');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong.');
            redirect(base_url());
        }
    }

    public function register() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $this->form_validation->set_rules('firstname', 'First Name', 'trim|required', array('required' => 'Please enter first name'));
            $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required', array('required' => 'Please enter last name'));
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[angel_users.email]|required', array('required' => 'Please enter Email', 'valid_email' => 'Please enter valid Email', 'is_unique' => "Email already register"));
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required', array('required' => 'Please enter phone number'));
            $this->form_validation->set_rules('password', 'Password', 'trim|required', array('required' => 'Please enter password'));
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]', array('required' => 'Please confirm your password', 'matches' => "Password is not match"));
            $this->form_validation->set_rules('register_captcha', 'Register captcha', 'trim|required', array('required' => 'Please verify captch code'));

            if ($this->form_validation->run() == TRUE) {

                $password                           = $this->input->post('password');
                $register_data['firstname']         = $this->input->post('firstname');
                $register_data['lastname']          = $this->input->post('lastname');
                $register_data['email']             = $this->input->post('email');
                $register_data['phone']             = $this->input->post('phone');
                $register_data['password']          = encreptIt($password);
                $register_data['visible_password']  = $password;
                $register_data['is_active']         = '1';
                $register_data['created_date']      = date("Y-m-d H:i:s");

                $user_id    = $this->user_model->insert($register_data);

                if ($user_id > 0) {
                    $user_session['session_id'] = encreptIt($user_id);
                    $this->set_authorized_user($user_session);
                    
                    $return["status"]   = TRUE;
                    $return["message"]  = "Register Successfully";
                    
                    $this->session->set_flashdata('success', 'Register Successfully');
                } else {
                    $return["status"]   = FALSE;
                    $return["message"]  = "Something went wrong";
                }
            } else {
                $return["status"]   = FALSE;
                $return["message"]  = validation_errors();
            }
            echo json_encode($return); die;
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong.');
            redirect(base_url());
        }
    }

    public function forgot_password() {
        if (!$this->is_logined && $this->input->server('REQUEST_METHOD') == 'POST' ) {
            $email  = $this->input->post('email');
            $return = array();
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

                $filter['where']= array('email' => trim($email));
                $filter['row']  = 1;
                $row            = $this->user_model->get_rows($filter);

                if (!empty($row)) {

                    $updateData                     = array();
                    $updateData['activation_code']  = encreptIt(rand());

                    $this->user_model->update_table($updateData, array('email' => trim($email)));

                    $link                       = base_url().'reset-password/'.$updateData['activation_code'];
                    $viewData['email']          = $row->email;
                    $viewData['username']       = ucfirst($row->firstname) . ' ' . ucfirst($row->lastname);
                    $viewData['activation_code']= $link;

                    $sent_data['subject']       = "Password Reset - Restaurant Equipment Paradise";
                    $sent_data['email']         = 'pandya.angelinfotech@gmail.com';
                    $sent_data['message']       = $this->load->view('front/email/forget_password', $viewData, true);
                    $this->sentMail($sent_data);

                    $return['status']   = TRUE;
                    $return['message']  = "You will get password reset link through your email";
                } else {
                    $return['status']   = FALSE;
                    $return['message']  = "Account not exist with given email";
                }
            } else {
                $return['status']   = FALSE;
                $return['message']  = "Please enter valid email address";
            }
            echo json_encode($return); die;
        } else {
            redirect(base_url());
        }
    }

    public function logout() {
        $this->removeUserSession();
        redirect(base_url());
    }

    public function check_user_exists() {
        $email          = $this->input->post('email');
        $filter         = array();
        $filter['where']= array('email' => $email);
        $result         = $this->user_model->get_rows($filter,1);
        $return         = TRUE;

        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }

    public function reset_password($token = '') {
        if ($token != '') {
            $filters['where']   = array('activation_code' => $token);
            $filters['row']     = 1;
            $user_data          = $this->user_model->get_rows($filters);
            if (! empty($user_data)) {
                if ($this->input->server('REQUEST_METHOD') == 'POST') {
                    $password   = $this->input->post('password');
                    $cpassword  = $this->input->post('cpassword');

                    if ($password == $cpassword) {

                        $update_passsword = array(
                            'password'          => encreptIt($password),
                            'visible_password'   => $password,
                            'activation_code'   => ''
                        );

                        $this->user_model->update_table($update_passsword, array('id' => $user_data->id));

                        $this->session->set_flashdata('success', 'Password changed successfully.');
                        redirect(base_url());
                    } else {
                        $this->session->set_flashdata('error', 'Password and Confirm password does not match.');
                    }
                }

                $data               = array('token' => $token);
                $this->template->view('front/reset_password', $data);
            } else {
                $this->session->set_flashdata('error', 'Invalid token.');
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }

    public function check_user_register() {
        $email          = $this->input->post('email');
        $filter         = array();
        $filter['where']= array('email' => $email);
        $result         = $this->user_model->get_rows($filter,1);
        $return         = FALSE;

        if ($result > 0) {
            $return = TRUE;
        }
        echo json_encode($return);
    }
}