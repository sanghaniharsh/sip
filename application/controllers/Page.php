<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }

    public function index($url = "") { 
        
        $data               = array();
        $filter['where']    = array('url' => $url, 'is_active' => '1', 'position' => '0');
        $filter['row']      = 1;
        $result             = $this->cms_model->get_rows($filter);

        if (!empty($result)) {
            $data['page']    = $result;
            $this->template->view('front/page/index', $data);
        } else {
            $this->session->set_flashdata('error', 'Page is not found, Please try again later.');
            redirect('/');
        }
    }

    public function about_us() {
        $data    = array();
        $this->template->view('front/page/about_us', $data);
    }
    
    public function myaccount() {
        $data    = array();
        $this->template->view('front/page/myaccount', $data);
    }

    public function contact_us() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $this->form_validation->set_rules('name', 'Name', 'trim|required', array('required' => 'Please enter name'));
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required', array('required' => 'Please enter Email', 'valid_email' => 'Please enter valid Email'));
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required', array('required' => 'Please enter phone number'));
            $this->form_validation->set_rules('message', 'Message', 'trim|required', array('required' => 'Please enter message'));
            $this->form_validation->set_rules('register_captcha', 'Register captcha', 'trim|required', array('required' => 'Please verify captch code'));

            if ($this->form_validation->run() == TRUE) {
                $viewData['name']       = $this->input->post('name');
                $viewData['email']      = $this->input->post('email');
                $viewData['phone']      = $this->input->post('phone');
                $viewData['message']    = $this->input->post('message');
                
                $sent_data['subject']       = "Contact Details | SIP";
                $sent_data['email']         = "mail@SaveinParadise.com";
                $sent_data['message']       = $this->load->view('front/email/contact', $viewData, true);
                $this->sentMail($sent_data);
                $this->session->set_flashdata('success', 'Contact Detail Send Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Please Field Required.');
            }
        }
        $data    = array();
        $this->template->view('front/page/contact_us', $data);
    }
    
    public function smallwares() {
        $data    = array();
        $this->template->view('front/page/smallwares', $data);
    }
    
    public function greasetraps() {
        $data    = array();
        $this->template->view('front/page/greasetraps', $data);
    }
    
    public function restaurant_design() {
        $data    = array();
        $this->template->view('front/page/restaurant_design', $data);
    }
    
    public function delivery() {
        $data    = array();
        $data['css_files']  = array("ssi-uploader.min.css");
        $data['js_files']   = array("ssi-uploader.js");
        $this->template->view('front/page/delivery', $data);
    }
    
    public function campaign() {
        $data    = array();
        $this->template->view('front/page/campaign', $data);
    }
    
    public function our_work() {
        $data    = array();
        $this->template->view('front/page/our_work', $data);
    }
    
    public function used_equipments() {
        $data    = array();
        $this->template->view('front/page/used_equipments', $data);
    }

    public function delivery_pdf () {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            ini_set("memory_limit", "-1");
            ini_set('max_execution_time', 0); 
            
            $attachment_ids  = array();
            if (!empty($_POST['images'])) { 
                $deliveryPath   = getcwd().'/uploads/delivery/';
                if (!is_dir($deliveryPath)) {
                    mkdir($deliveryPath, 0777, true);
                }
                $images = $_POST['images'];
                foreach ($images as $key => $image_data) {
                    if ($image_data != '') {
                        $type = explode(';', $image_data);
                        list(, $image_data)     = explode(';', $image_data);
                        list(, $image_data)     = explode(',', $image_data);
                        $image_data             = base64_decode($image_data);

                        if($type[0] == "data:image/png"){
                            $new_name               = time() . '_' . $key . '.png';
                        } else {
                            $new_name               = time() . '_' . $key . '.jpg';
                        }
                        $file_path              = $deliveryPath.$new_name;
                        file_put_contents($file_path, $image_data);
                        correctOrientation($file_path);
                        $attachmentData['path'] = '/uploads/delivery/'.$new_name;
                        $attachmentData['type'] = 'image';
                        $attachment_id          = $this->attachment_model->insert($attachmentData);
                        $attachment_ids[]       = $attachment_id;
                    }
                }
            }
//            if(!empty($_FILES['infile'])) {
//                
//                $img_count      = count($_FILES['infile']['name']);
//                $files          = $_FILES['infile'];
//
//                $deliveryPath   = getcwd().'/uploads/delivery/';
//                if (!is_dir($deliveryPath)) {
//                    mkdir($deliveryPath, 0777, true);
//                }
//
//                $uploadPath = $deliveryPath.'/';
//
//                for($i = 0; $i < $img_count; $i++) {
//                    $_FILES['infile']['name']     = $files['name'][$i];
//                    $_FILES['infile']['type']     = $files['type'][$i];
//                    $_FILES['infile']['tmp_name'] = $files['tmp_name'][$i];
//                    $_FILES['infile']['error']    = $files['error'][$i];
//                    $_FILES['infile']['size']     = $files['size'][$i];
//
//                    $config['file_name']            = $_FILES['infile']['name'];
//                    $config['upload_path']          = $uploadPath;
//                    $config['allowed_types']        = 'jpg|png';
//
//                    $this->load->library('upload', $config);
//                    $this->upload->initialize($config);
//                    if($this->upload->do_upload('infile')){
//                        $fileData                       = $this->upload->data();
//                        $attachmentData['path']         = '/uploads/delivery/'.$fileData['file_name'];
//                        $attachmentData['type']         = 'image';
//                        $attachment_id                  = $this->attachment_model->insert($attachmentData);
//                        $attachment_ids[]               = $attachment_id;
//                    }
//                }
//            }

            $this->load->library('m_pdf');
            $memberPDF['business_name']     = "SIP";
            $memberPDF['title']             = "SIP";
            $memberPDF['amount']            = '$';
            $memberPDF['effective_date']    = date('m-d-Y');
            $memberPDF['delivery_data']     = $this->input->post();
            $memberPDF['attachment_imgs']   = $attachment_ids;
            $memberPDF['ip_address']        = getRealIpAddr();
            
            $memberHtml                     = $this->load->view('front/page/delivery_pdf', $memberPDF, true);
            $filePath                       = getcwd() . '/uploads/aggrement-pdf/';

            if (!is_dir($filePath)) {
                mkdir($filePath, 0777, true);
            }
            //ob_clean();
            $pdfFilePath    = $filePath . "-delivery-details.pdf";
            $pdf            = $this->m_pdf->load();
            $pdf->WriteHTML($memberHtml, 2);
            $pdf->Output($pdfFilePath, "F");
            $filter['row']              = "1";
            $site_setting               = $this->sitesettings_model->get_rows($filter);
            $sent_data['subject']       = "Delivery Questionnaire - Restaurant Equipment Paradise";
            $sent_data['email']         = "mail@SaveinParadise.com";
             //$sent_data['email']         = "dhavalg.angelinfotech@gmail.com";
            $sent_data['from_email']    = $site_setting->from_email;
            $sent_data['message']       = "<p>Hello Admin,<p>";
            $sent_data['message']       .= "<p>Please find attached PDF of Delivery Questionnaire for Restaurant Equipment Paradise</p>";
            $sent_data['attachement']   = array($pdfFilePath);
            
            $return_mail = $this->sentMail($sent_data);

            if (isset($return_mail['status']) && ($return_mail['status'] == true)) {
                $this->attachment_model->removeAttachment($attachment_ids);
                redirect(base_url().'delivery-thank-you');
            } else {
                $this->session->set_flashdata('error', $return_mail['message']);
                redirect(base_url());
            }
        } else {
           redirect(base_url());
        }
    }
    
    public function thank_you() {
        $this->template->view('front/page/thank_you');
    }
}

?>