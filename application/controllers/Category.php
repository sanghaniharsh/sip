<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }

    public function index($slug = '') {
        $data['slug'] = '';
        if ($slug != '') {
            $data['slug'] = $slug;
        }
        $this->template->view('front/category/index', $data);
    }

    public function view($slug = '', $start = 0) {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $category_data = array();

            if ($slug != '') {
                $cFilter['select']  = array('id', 'slug');
                $cFilter['where']   = array('slug' => $slug);
                $cFilter['row']     = 1;
                $category_data      = $this->category_model->get_rows($cFilter);
            }

            $perpage            = 200;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0;

            $filter['parent'] = 0;
            if (!empty($category_data) && $category_data->id > 0) {
                $filter['parent'] = $category_data->id;
            }
            $filter['limit']    = array('limit' => $perpage, 'from' => $start);
            $result             = $this->category_model->getCategory($filter);
            $count              = $result['total'];

            //pagination
            $config = initPagination();
            $url    = base_url() . "category/view/";
            if ($slug != '' && !empty($category_data) && $category_data->slug != '') {
                $url = base_url() . "category/view/".$category_data->slug."/";
            }
            $config["base_url"]     = $url;
            $config["per_page"]     = $perpage;
            $config['uri_segment']  = 4;
            $config["total_rows"]   = $count;
            $config['attributes']   = array('class' => 'page-link');
            $this->pagination->initialize($config);

            $view_data['pagination_link']   = $this->pagination->create_links();
            $view_data['category_list']     = $result['category'];
            $parents_class          = new stdClass();
            $parents_class->name    = "RESTAURANT EQUIPMENT";
            $parents_class->slug    = "";
            $parents                = array($parents_class);
            if (isset($category_data->id)) {
                $parents_2  = $this->category_model->getOnlyParents($category_data->id);
                $parents    = array_merge($parents, $parents_2);
            }

            $view_data['parents']       = $parents;
            $data['category_view']      = $this->load->view('front/category/ajax_categories', $view_data, true);
            $return['status']           = true;
            $return['category_html']    = $data['category_view'];

            echo json_encode($return);die;
        }
    }
}

?>