<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_new extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }

    public function index($slug = "") {
        $category = $this->category_model->categoryData();
        
        p($category);
        
        $category_view = '';
        foreach($category as $row) {
            
            $category_view .= $this->build_cat($row);
//            $category_view .= $this->load->view("front/search/partial_category_view", $row, true);
        }
//        
        $data['category'] = $this->category_model->categoryData();
        $this->template->view("front/search/index2", $data);
    }
    
    private function build_cat($data) {
        $html = '';
        if (isset($data['parent']['children']) && ! empty($data['parent']['children'])) {
            foreach($data['parent']['children'] as $child) {
                p($child);
                $html .= $this->build_cat($child);
            }
            
        } else {
            $html .= $this->load->view("front/search/partial_category_view", $data, true);
        }

        return $html;
    }
}

?>