<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();

    }

    public function detail($slug = '') {

        $data['js_files'] = array(
                                'jquery.bxslider.js',
                                'jquery.flexslider.js',
                                'cloud-zoom.js',
                                'select2.min.js'
                            );
        $data['css_files'] = array(
                                'flexslider.css',
                                'select2.min.css'
                            );
        if ($slug != "") {
            $pFilter['select']   = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'COALESCE(c.name, uc.name) as category_name', 'COALESCE(c.id,uc.id) as category_id', 'COALESCE(c.parent,uc.parent) as parentId');
            $pFilter['join']     = array(
                                    array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                    array('table' => 'angel_used_category as uc', 'condition' => 'uc.id = angel_product.category_id', 'type' => 'left'),
                                    array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left')
                                );

            $pFilter['where']   = array('angel_product.slug' => $slug, 'angel_product.is_active' => '1');
            $pFilter['row']     = 1;
            $pFilter['groupby'] = array('field' => 'angel_product.id');
            $result             = $this->product_model->get_rows($pFilter);

            if (!empty($result)) {
                $q_filter['where']  = array('product_id' => $result->id);
                $data['questions']  = $this->question_model->get_rows($q_filter);

                $filter['where']    = array('product_id' => $result->id);
                if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $filter['where']['user_id'] = $this->loginUser['id'];
                } else {
                    $filter['where']['ip_address'] = $this->input->ip_address();
                    $filter['where']['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }

                $filter['row']      = 1;
                $data['cart_data']  = $this->cart_model->get_rows($filter);
                $data['product']    = $result;   

                $parents_class          = new stdClass();
                if ($result->type == 1) {
                    $parents_class->name    = "USED RESTAURANT EQUIPMENT";
                    $parents_class->slug    = "used-equipment";
                } else {
                    $parents_class->name    = "RESTAURANT EQUIPMENT";
                    $parents_class->slug    = "category";
                }
                $parents                = array($parents_class);
                if (isset($result->category_id)) {
                    if ($result->type == 1) {
                        $parents_o      = new stdClass();
                        $parents_o->name= $result->category_name;
                        $parents_o->slug= "used-equipment?search_category=".$result->category_id;
                        $parents_2      = array($parents_o);
                    } else {
                        $parents_2  = $this->category_model->getOnlyParents($result->category_id);
                    }
                    $parents    = array_merge($parents, $parents_2);
                }
                $data['parents']            = $parents;
                $data['meta_title']         = ucfirst($result->name).' at Restaurant Equipment Paradise';
                $data['meta_description']   = html_entity_decode(substr(strip_tags($result->description), 0, 200));
                if (isset($result->product_images) && $result->product_images !="") {
                    $images             = explode (",", $result->product_images);
                    $data['meta_image'] = isset($images[0]) ? $images[0] : '';
                }
                $this->template->view("front/product/detail", $data);
            } else {
                not_found();
            }
        }
    }

    public function related_product() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return         = array();
            $category_id    = $this->input->post('category_id');
            $product_id     = $this->input->post('product_id');
            $product_id     = decreptIt($product_id);
            $category_id    = decreptIt($category_id);
            $limit          = 4;
            if ($category_id > 0) {
                $filter['select']       = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
                $filter['join']         = array(
                                            0 => array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                            1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left')
                                            );

                $filter['where']        = array('angel_product.is_active' => '1', 'angel_product.type' => '0', 'angel_product.id != ' => $product_id , 'c.id'  => $category_id);
                $filter['limit']        = array('limit' => $limit, 'from' => 0);
                $filter['groupby']      = array('field' => 'angel_product.id');
                $product_count          = $this->product_model->get_rows($filter,true);
                $count                  = $limit;

                if ($product_count > 0 && $product_count != $count) {
                    $count              = $limit - $product_count;
                }
                $products               = $this->product_model->get_rows($filter);
                $pdata['product']       = $products;

                if ($limit  >  $count ||  empty($pdata['product'])) {

                    $rdata['limit']     = $count;
                    $rdata['where']     = array("c.id  != " => $category_id, 'angel_product.is_active' => '1');
                    $renodm_product     = $this->product_model->get_random_product($rdata);

                    if (isset($renodm_product['rendom_product']) && !empty($renodm_product['rendom_product'])) {
                        $pdata['product']   =   array_unique(array_merge($products,  $renodm_product['rendom_product']), SORT_REGULAR);
                    }
                }

                if (!empty($pdata['product'])) {
                    $data['product']    = $pdata['product'];
                    $return['html']     = $this->load->view('front/product/relatedproduct_ajax', $data, true);
                    $return['status']   = true;
                } else {
                    $return['status']   = false;
                }
            } else {
                $return['status']   = false;
            }

            echo json_encode($return);die;
        } else {
            not_found();
        }
    }
    
    public function add_cart_block() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data   =   array();
            $return             = array();
            $product_id         = $this->input->post('product_id');
            $product_id         = decreptIt($product_id);
            $data['product_id'] = $product_id;

            $filter['where']    = array('product_id' => $product_id);
            if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $filter['where']['user_id']     = $this->loginUser['id'];
            } else {
                $filter['where']['ip_address']  = $this->input->ip_address();
            }

            $filter['row']      = 1;
            $data['cart_data']  = $this->cart_model->get_rows($filter);

            $pFilter['where']   = array('angel_product.id' =>  $product_id, 'angel_product.is_active' => '1');
            $pFilter['row']     = 1;
            $pFilter['groupby'] = array('field' => 'angel_product.id');
            $data['product']    = $this->product_model->get_rows($pFilter);

            $return['html']     = $this->load->view('front/product/add_cart_ajax_block', $data, true);
            $return['status']   = true;
            echo json_encode($return);die;
        } else {
            not_found();
        }
    }
    public function get_addon_product($product_id ='') {
        $return     = array();
        $html       = '';

        if ($product_id != "") {
            $product_id = decreptIt($product_id);

            if ($product_id > 0) {
                $aPFilter['select']         = array('angel_addon_product.id as addon_id','angel_addon_product.addon_product_id', 'angel_addon_product.is_active as addon_is_active' , 'p.*', 'GROUP_CONCAT(pi.image) as product_images');
                $aPFilter['join']           = array(
                                                array('table' => 'angel_product as p', 'condition' => 'p.id = angel_addon_product.addon_product_id', 'type' => 'left'),
                                                array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = p.id', 'type' => 'left')
                                            );
                $aPFilter['where']          = array('angel_addon_product.is_active' => '1' ,'angel_addon_product.product_id' => $product_id);
                $aPFilter['groupby']        = array('field' => 'p.id');
                $addon_product_result       = $this->addon_product_model->get_rows($aPFilter);

                if (!empty($addon_product_result)) {
                    $card_product_ids = array();
                    foreach ($addon_product_result as $arow) {
                        $card_product_ids[] = $arow->addon_product_id;
                        
                    }
                    $aFilter['where_in']    = array(array('field' => 'product_id', 'value' => $card_product_ids));
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $aFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $aFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $aFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cart_addon_products    = $this->cart_model->get_rows($aFilter);
                    $selected_products      = array();
                    $selected_cart_id       = array();
                    if (!empty($cart_addon_products)) {
                       foreach ($cart_addon_products as  $a_row) {
                           $selected_products[$a_row->product_id]   = $a_row->product_id;
                           $selected_cart_id[$a_row->product_id]    = $a_row->id;
                       }
                    }
                    if (!empty($addon_product_result)) {
                        foreach ($addon_product_result as $arow) {
                            $checked   = '';
                            $cart_id    = '';
                            if (isset($selected_products[$arow->id])) {
                                $checked = 'checked';
                            }
                            if (isset($selected_cart_id[$arow->id])) {
                                $cart_id = encreptIt($selected_cart_id[$arow->id]);
                            }

                            $images[0] =  '';
                            if(isset($arow->product_images) && $arow->product_images != "") {
                                $images = explode(',',$arow->product_images);
                            }

                            $link = base_url().'product/'.$arow->slug;
                            $popover_content = '<span class="enlarge_content"><img src="'.base_url().'attachment/image/213/0/'.md5($images[0]).'" alt="Addon Product" /><a href="'.$link.'" target="_blank">Click here to view full product page</a></span>';
                            $html .='<li class="list-group-item" ><input type="checkbox" class="addon_product_checkbox" cart_id="'.$cart_id.'" value="'.$arow->id.'" name="addon_product_id[]"  '.$checked.' > <span class="add_pro_name">'.$arow->name.'</span> <span class="add_pro_price">$'.$arow->price.'</span>'.$popover_content.'</li>';
                            //$html .='<option cart_id="'.$cart_id.'"  value="'.$arow->id.'" '.$selected.'>'.$arow->name.'</option>';
                        }
                    }
                    $return['status']   = true;
                } else {
                    $return['status']   = false;
                }
                $return['html']     = $html;
                echo json_encode($return);die;
            }
        }
    }
}