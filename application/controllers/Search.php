<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }

    public function index($slug = "") {
        
        $category = $this->category_model->categoryData();
        
//        p($category);

        $category_id        = $this->category_model->getIdFromSlug($slug);
        
        $parents_class          = new stdClass();
        $parents_class->name    = "RESTAURANT EQUIPMENT";
        $parents_class->slug    = "";
        $parents                = array($parents_class);
        if (isset($category_id)) {
            $parents_2  = $this->category_model->getOnlyParents($category_id);
            $parents    = array_merge($parents, $parents_2);
        }
        
        $data['parents']    = $parents;
        $data['category']   = $this->category_model->categoryData();
        $data['slug']       = $slug;
        $this->template->view("front/search/index", $data);
    }
    
    public function getProducts($slug = '', $start = 0) {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $perpage            = 12;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0;

            $cFilter['select']  = array('id');
            $cFilter['row']     = 1;
            $cFilter['where']   = array('slug' => $slug);
            $category           = $this->category_model->get_rows($cFilter);

            $category_data = array();
            if (!empty($category) && isset($category->id) && $category->id > 0) {
                $category_data          = $this->category_model->getChildData($category->id);
                $category_data[]        = $category->id;
                $sfilter['category_ids']= $category_data;
            }

            $search = $this->input->get('q');
            if ($search != '') {
                $sfilter['search'] = $search;
            }
            
            $sfilter['active']      = '1'; 
            $sfilter['type']      = '0';
            $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);

            $result             = $this->product_model->getProducts($sfilter);
            $data['products']   = $result['product'];
            $data['total']      = $result['total'];

            //pagination
            $config     = initPagination();
            $url        = base_url() . "search/getProducts/";
            if ($slug != '') {
                $url = base_url() . "search/getProducts/".$slug."/";
            }
            $config["base_url"]         = $url;
            $config["total_rows"]       = $data['total'];
            $config["per_page"]         = $perpage;
            $config['uri_segment']      = 4;
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $data['total'];
            $config['attributes']       = array('class' => 'page-link');
            $this->pagination->initialize($config);

            $str_links              = $this->pagination->create_links();
            $data["links"]          = $str_links;

            if (!empty($data)) {
                $return['status']   = true;
                $return['html']     = $this->load->view('front/search/ajax_products', $data, true);
            } else {
                $return['status']   = false;
            }
        } else {
            $return['status']   = false;
        }
        echo json_encode($return);die;
    }
}

?>