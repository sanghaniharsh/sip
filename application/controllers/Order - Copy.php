<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('paypal_lib');
    }

    public function cart() {

        $data = array();
        $return = array();
        $this->removeSession(array('applied_promo_code')); 

        $pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
        $pFilter['join']    = array(
                                0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
        );

        $pFilter['where']   = array('p.is_active' => '1', 'angel_cart.ip_address' => $this->input->ip_address());
        $pFilter['groupby'] = array('field' => 'p.id');
        $data['cart']       = $this->cart_model->get_rows($pFilter);
        $return['html']     = $this->load->view('front/order/cart_detail_ajax', $data, true);

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return['status'] = true;
            echo json_encode($return);
            die;
        } else {
            $data['html'] = $return['html'];
            $this->template->view('front/order/cart', $data);
        }
    }

    public function add_cart_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $data['quantity']   = $this->input->post('quantity');
            $product_id         = $this->input->post('product_id');
            $data['product_id'] = decreptIt($product_id);
            $data['ip_address'] = $this->input->ip_address();

            if ($data['product_id'] > 0 && $data['quantity'] > 0) {

                $filter['where'] = array('product_id' => $data['product_id'], 'ip_address' => $data['ip_address']);
                $filter['row'] = 1;
                $result = $this->cart_model->get_rows($filter);

                if (empty($result)) {
                    $this->cart_model->insert($data);
                    $return['status'] = true;
                    $return['message'] = "Product added in your cart";
                } else {
                    $return['status'] = false;
                    $return['message'] = "Already Item exists in cart ";
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_delete_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('id');
            $id = decreptIt($id);

            if ($id > 0) {

                $filter['where'] = array('id' => $id);
                $filter['row'] = 1;
                $cart = $this->cart_model->get_rows($filter);

                if (!empty($cart)) {
                    $this->cart_model->delete(array('id' => $id));
                    $return['message'] = "Cart item deleted successfully";
                    $return['status'] = true;
                } else {
                    $return['message'] = "Something went wrong";
                    $return['status'] = false;
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_update_qty() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('id');
            $id = decreptIt($id);
            $data['quantity'] = $this->input->post('quantity');


            if ($id > 0 && $data['quantity'] > 0) {

                $filter['where'] = array('id' => $id);
                $filter['row'] = 1;
                $cart = $this->cart_model->get_rows($filter);

                if (!empty($cart)) {
                    $this->cart_model->update_table($data, array('id' => $id));
                    $cfilter['select'] = array('SUM(angel_cart.quantity * p.price) AS full_total'); //cart total amount count
                    $cfilter['join'] = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left'),);
                    $cfilter['row'] = 1;
                    $cfilter['where'] = array('angel_cart.ip_address' => $this->input->ip_address());
                    $cresult = $this->cart_model->get_rows($cfilter);

                    if (!empty($cresult)) {
                        $return['full_total'] = number_format($cresult->full_total, 2);
                    }

                    $pfilter['select'] = array('SUM(angel_cart.quantity * p.price) AS total_price');
                    $pfilter['join'] = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left'),);
                    $pfilter['row'] = 1;
                    $pfilter['where'] = array('angel_cart.ip_address' => $this->input->ip_address(), 'angel_cart.id' => $id);
                    $presult = $this->cart_model->get_rows($pfilter);

                    if (!empty($presult)) {
                        $return['total_price'] = number_format($presult->total_price, 2);
                    }

                    $return['message'] = "Quantity updated successfully";
                    $return['status'] = true;
                } else {
                    $return['message'] = "Quantity updated not successfully";
                    $return['status'] = false;
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }

            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_count() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $filter['where'] = array('ip_address' => $this->input->ip_address());
            $total = $this->cart_model->get_rows($filter, true);

            if ($total > 0) {
                $return['status'] = true;
                $return['total'] = $total;
                $return['html'] = '<span class="cart_badge cart_total_item">' . $total . '</span>';
            } else {
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function check_promotional_code() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data = array();
            $return = array();
            $p_code = trim($this->input->post('promotional_code'));

            if ($p_code != '') {

                $cfilter['select'] = array('angel_cart.*', 'p.price', 'SUM(angel_cart.quantity * p.price) AS full_total');
                $cfilter['join'] = array(
                    0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left'),);
                $cfilter['row'] = 1;
                $cfilter['where'] = array('angel_cart.ip_address' => $this->input->ip_address());
                $cresult = $this->cart_model->get_rows($cfilter);
                $pdata['total_amount'] = $cresult->full_total;
                $pdata['promo_code'] = $p_code;
                $promotional = $this->promotional_model->check_promocode($pdata);

                if ($promotional['status'] == true) {

                    $return['status'] = true;
                    $data['total_amount'] = $promotional['total_amount'];
                    $data['total_discount'] = $promotional['total_discount'];
                    $data['promotional_code'] = $p_code;
                    ;
                    $return['html'] = $this->load->view('front/order/discount_detail_ajax', $data, true);
                } else {
                    $return['status'] = false;
                    $return['message'] = "Promo code not valid";
                }
            } else {
                $return['status'] = false;
                $return['message'] = "Please enter promo code";
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function checkout() {
 
        $data                   = array();
        $ip_address             = $this->input->ip_address();
        $cFilter['ip_address']  = $ip_address;
        $result                 = $this->cart_model->getItems($cFilter);
        if (!empty($result)) {
            $amount = 0;
            $discount = 0;
            $data['cart_items'] = $result;
            foreach ($result as $value) {
                $amount += $value->quantity * $value->price;
            }
            $promocode = '';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $promocode = $this->input->post('promotional_code');
                $applied_promo_code['applied_promo_code']   = $promocode;               
                $this->setSession($applied_promo_code);

            } else {
                $promocode      =   $this->getSession('applied_promo_code');
            }

            if ($promocode != '' && !empty($promocode) ) {
                $pData['promo_code']    = $promocode;
                $pData['total_amount']  = $amount;
                $promo_data             = $this->promotional_model->check_promocode($pData);

                if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                    $data['promo_code'] = $promocode;
                    $amount = $promo_data['total_amount'];
                    $discount = $promo_data['total_discount'];
                }
            }
            $data['total_amount'] = $amount;
            $data['total_discount'] = $discount;

            $this->template->view('front/order/checkout', $data);
        } else {
            not_found();
        }
    }

    public function commit() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
            $this->form_validation->set_rules('address', 'Phone', 'trim|required');
            $this->form_validation->set_rules('zipcode', 'Phone', 'trim|required|callback_validate_zipcode');
            $this->form_validation->set_message('validate_zipcode', 'Zipcode is not valid');

            if ($this->form_validation->run() == TRUE) {
                $product = array();
                $promocode = $this->input->post('promocode');
                $ip_address = $this->input->ip_address();
                $cFilter['ip_address'] = $ip_address;
                $cart_details = $this->cart_model->getItems($cFilter);

                if (!empty($cart_details)) {
                    $amount = 0;
                    $discount = 0;
                    foreach ($cart_details as $value) {
                        $amount += $value->quantity * $value->price;
                        $product[] = $value->product_id;
                    }

                    if ($promocode != '') {
                        $pData['promo_code'] = $promocode;
                        $pData['total_amount'] = $amount;
                        $promo_data = $this->promotional_model->check_promocode($pData);

                        if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                            $amount = $promo_data['total_amount'];
                            $discount = $promo_data['total_discount'];
                        }
                    }

                    $email = $this->input->post('email');
                    $user_id = 0;

                    $uFilter['select'] = array('id');
                    $uFilter['where'] = array('email' => $email);
                    $uFilter['row'] = 1;
                    $result = $this->user_model->get_rows($uFilter);

                    if (!empty($result) && isset($result->id) && $result->id > 0) {
                        $user_id = $result->id;
                    } else {
                        $data['email'] = $email;
                        $data['firstname'] = $this->input->post('first_name');
                        $data['lastname'] = $this->input->post('last_name');
                        $data['phone'] = $this->input->post('phone');
                        $password = generateRandomString(10);
                        $data['password'] = encreptIt($password);
                        $data['visible_password'] = $password;
                        $data['is_active'] = '1';

                        $user_id = $this->user_model->insert($data);
                    }

                    $zipcode = $this->input->post('zipcode');
                    $lFilter['select'] = array('city', 'state');
                    $lFilter['where'] = array('zipcode' => $zipcode);
                    $lFilter['row'] = 1;
                    $location_result = $this->location_model->get_rows($lFilter);

                    if (!empty($location_result)) {
                        $user_data['user_id'] = $user_id;
                        $user_data['city'] = $location_result->city;
                        $user_data['state'] = $location_result->state;
                        $user_data['street_address'] = $this->input->post('address');
                        $user_data['zipcode'] = $this->input->post('zipcode');

                        $aFilter['where'] = array('user_id' => $user_id);
                        $aFilter['row'] = 1;
                        $userDetails = $this->user_address_model->get_rows($aFilter);

                        if (!empty($userDetails)) {
                            $this->user_address_model->update_table($user_data, array('id' => $userDetails->id));
                        } else {
                            $this->user_address_model->insert($user_data);
                        }

                        if ($user_id > 0 && $amount > 0 && !empty($product)) {
                            $data['user_id']    = $user_id;
                            $data['product_id'] = $product;
                            $data['amount']     = $amount;
                            $data['discount']   = $discount;
                            $data['promo_code'] = $promocode;
                            $this->payment($data);
                            die;
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Please fill user details');
                        redirect(base_url() . 'order/checkout');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please try again latter');
                    redirect(base_url() . 'order/checkout');
                }
            } else {
                $this->session->set_flashdata('error', "All fields are required");
                redirect(base_url() . 'order/checkout');
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong');
            redirect(base_url() . 'order/checkout');
        }
    }

    public function validate() {
        $email = $this->input->post('email');
        $return = FALSE;

        if ($email != '') {
            $filter['where'] = array('email' => $email);
            $result = $this->user_model->get_rows($filter);

            if (!empty($result)) {
                $return = FALSE;
            } else {
                $return = TRUE;
            }
        }
        echo json_encode($return);
        die;
    }

    private function payment($param = array()) {
        if (!empty($param)) {

            $returnURL = base_url() . 'order/payment_success'; //payment success url
            $cancelURL = base_url() . 'order/payment_cancel';

            $products_data = array(
                'product_id' => $param['product_id'],
                'amount' => $param['amount'],
                'discount' => $param['discount'],
                'user_id' => $param['user_id'],
                'promo_code' => $param['promo_code']
            );

            $custom = base64_encode(json_encode($products_data));
            $finalAmount = $param['amount'];
            $paypalcharge = $this->config->item('PAYPALCHARGE');
            $paypalpayment = $finalAmount + ($finalAmount * ($paypalcharge / 100));
            $logo = front_asset_url() . 'images/logo-main.png';

            $post_fileds['brand_name'] = 'Resturant Equipment';
            $post_fileds['brand_logo'] = $logo;

            $post_fileds['amount'] = $paypalpayment;
            $post_fileds['custom'] = $custom;
            $post_fileds['inv_number'] = 'R_inv_' . time();
            $post_fileds['currencyCode'] = "USD";
            $post_fileds['item'] = array(
                array(
                    'name' => 'Resturant Equipment',
                    'desc' => 'Resturant Equipment',
                    'qty' => 1,
                    'amount' => $paypalpayment
                )
            );
            $post_fileds['payment_desc'] = 'Resturant Equipment';
            $post_fileds['cancel_url'] = $cancelURL;
            $post_fileds['return_url'] = $returnURL;
            $return = $this->paypal_lib->SetExpressCheckout($post_fileds);
            if (!empty($return)) {
                $response = $this->paypal_lib->procced_paypal($return);
                if (!empty($response)) {
                    $this->session->set_flashdata('error', 'Payment not done');
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('error', 'Payment not done');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong, payment failed');
            redirect(base_url());
        }
    }

    public function success() {
        $products = $this->getSession('products');
        $promocode = $products['promo_code'];

        $data['user_id'] = $products['user_id'];
        $data['amount'] = $products['amount'];
        $data['discount'] = $products['discount'];
        $data['created_date'] = date('Y-m-d H:i:s');
        $data['promo_code'] = $products['promo_code'];

        $order_id = $this->order_model->insert($data);

        $order_items = array();
        foreach ($products['product_id'] as $row) {
            $order_items[] = array('order_id' => $order_id, 'product_id' => $row);
        }

        $this->order_items_model->insert_betch($order_items);

        if ($promocode != '') {
            $pFilter['where'] = array('promo_code' => $products);
            $pFilter['row'] = 1;
            $result = $this->promotional_model->get_rows($pFilter);

            if (!empty($result)) {
                $promoData['promo_count'] = $result->promo_count + 1;
                $this->promotional_model->update_table($promoData, array('id' => $result->id));
            }
        }

        $where = array('ip_address' => $this->input->ip_address());
        $this->cart_model->delete($where);

        $this->removeSession(array('products'));
        $this->session->set_flashdata('success', 'Payment successfull.');
        redirect(base_url());
    }

    public function payment_success() {

        if (!empty($_GET) && $_GET['PayerID'] != '' && $_GET['token'] != '') {
            ini_set('max_execution_time', '0');
            $payer_id = $_GET['PayerID'];
            $token = $_GET['token'];
            $requestParams = array('TOKEN' => $_GET['token']);
            $response = $this->paypal_lib->GetExpressCheckoutDetails($requestParams);
            if (!empty($response) && isset($response['CUSTOM']) && ($response['ACK'] == 'Success')) {
                $payerId = $response["PAYERID"]; //Payer id returned by paypal
                $requestParams1 = array(
                    "TOKEN" => $response['TOKEN'],
                    "PAYERID" => $payerId,
                    "PAYMENTREQUEST_0_AMT" => $response['AMT'], //Payment amount. This value should be sum of of item values, if there are more items in order
                    "PAYMENTREQUEST_0_CURRENCYCODE" => "USD", //Payment currency
                    "PAYMENTREQUEST_0_ITEMAMT" => $response['AMT']//Item amount
                );

                $transactionResponse = $this->paypal_lib->DoExpressCheckoutPayment($requestParams1); //Execute transaction
                if (!empty($transactionResponse)) {

                    $products = json_decode(base64_decode($response['CUSTOM']));
                    $promocode = $products->promo_code;

                    $data['user_id']        = $products->user_id;
                    $data['transaction_id'] = $transactionResponse['PAYMENTINFO_0_TRANSACTIONID'];
                    $data['amount']         = $response['AMT'];
                    $data['status']         = $transactionResponse['ACK'];
                    
                    if ($data['status'] == 'Success') {
                        $data['order_status']   = '0';
                    }
                    $data['discount']       = $products->discount;
                    if (isset($products->promo_code) && $products->promo_code != '') {
                        $data['promo_code'] = $products->promo_code;
                    }
                    $data['created_date'] = date('Y-m-d H:i:s');

                    $order_id = $this->order_model->insert($data);
                    $order_items = array();
                    foreach ($products->product_id as $row) {
                        $order_items[] = array('order_id' => $order_id, 'product_id' => $row);
                    }

                    $this->order_items_model->insert_betch($order_items);

                    if ($promocode != '') {
                        $pFilter['where'] = array('promo_code' => $promocode);
                        $pFilter['row'] = 1;
                        $result = $this->promotional_model->get_rows($pFilter);

                        if (!empty($result)) {
                            $promoData['promo_count'] = $result->promo_count + 1;
                            $this->promotional_model->update_table($promoData, array('id' => $result->id));
                        }
                    }

                    $Ufilter['where'] = array('id' => $data['user_id']);
                    $Ufilter['row'] = 1;
                    $Uresult = $this->user_model->get_rows($Ufilter);
                    $Uname = (isset($Uresult->firstname) && isset($Uresult->lastname) ) ? $Uresult->firstname . " " . $Uresult->lastname : '';

                    if (!empty($Uresult)) {
                        $viewData['username'] = $Uname;
                        $viewData['transaction_id'] = $data['transaction_id'];
                        $mailData['message'] = $this->load->view('front/email/order_payment', $viewData, true);
                        $mailData['subject'] = "SIP Payment Successfully";
                        $mailData['email'] = $Uresult->email;
                        @$this->sentMail($mailData);
                    }

                    $this->load->library('m_pdf');
                    $memberPDF['client_name'] = $Uname;
                    $memberPDF['business_name'] = "SIP";
                    $memberPDF['title'] = "SIP";
                    $memberPDF['amount'] = '$' . number_format($data['amount'], 2);
                    $memberPDF['effective_date'] = date('m-d-Y');
                    $memberHtml = $this->load->view('front/sip_license_agreement', $memberPDF, true);
                    $filePath = getcwd() . '/uploads/aggrement-pdf/';

                    if (!is_dir($filePath)) {
                        mkdir($filePath, 0777, true);
                    }

                    $pdfFilePath = $filePath . slug($Uname) . "-aggrement-details.pdf";
                    $pdf = $this->m_pdf->load();
                    $pdf->WriteHTML($memberHtml, 2);
                    $pdf->Output($pdfFilePath, "F");

                    $userData['business_name'] = "SIP";
                    $mailData['message'] = $this->load->view('front/email/license_agreement', $userData, true);
                    $mailData['subject'] = "New Product Purchased";
                    $mailData['attachement'] = array($pdfFilePath);
                    $mailData['email'] = $this->config->item('ADMIN_EMAIL');
                    $this->sentMail($mailData);

                    if (file_exists($pdfFilePath)) {
                        unlink($pdfFilePath);
                    }

                    $where = array('ip_address' => $this->input->ip_address());
                    $this->cart_model->delete($where);

                    $success_payment['thankyou'] = encreptIt($products->user_id);
                    $this->setSession($success_payment);

                    redirect(base_url() . 'order/thankyou');
                } else {
                    $this->session->set_flashdata('error', 'Payment Not completed. Something went wrong.');
                    redirect(base_url() . 'order/cart');
                }
            } else {
                $this->session->set_flashdata('error', 'Property not found Try Again later!');
                redirect(base_url() . 'order/cart');
            }
        } else {
            $this->session->set_flashdata('error', 'Payment not completed, Please try again later');
            redirect(base_url() . 'order/cart');
        }
    }

    public function validate_zipcode() {
        $zipcode = $this->input->post('zipcode');
        $return = TRUE;

        if ($zipcode != '') {
            $filter['where'] = array('zipcode' => $zipcode);
            $result = $this->location_model->get_rows($filter);

            if (empty($result)) {
                $return = FALSE;
            }
        }
        return $return;
    }

    public function check_zipcode() {
        $zipcode = $this->input->post('zipcode');
        $return = true;

        if ($zipcode != '') {
            $filter['where'] = array('zipcode' => $zipcode);
            $result = $this->location_model->get_rows($filter);

            if (empty($result)) {
                $return = false;
            }
        }
        echo json_encode($return);
        die;
    }

    public function payment_cancel() {
        $this->session->set_flashdata('error', 'Payment cancelled');
        redirect("/home");
    }

    public function thankyou() {
        $data = array();
        $thankyou_user = $this->getSession('thankyou');
        $this->removeSession(array('thankyou'));
        if ($thankyou_user != '' && !is_array($thankyou_user)) {
            $user_id = decreptIt($thankyou_user);
            $user_data = $this->user_model->get_detail($user_id);
            if (!empty($user_data)) {
                $data['user_name'] = $user_data->username;
                $this->template->view('front/order/thankyou', $data);
            } else {
                $this->session->set_flashdata('error', 'Somthing went wrong');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Metthod');
            redirect(base_url());
        }
    }

    public function preview_agreement() {

        $data = array();
        $outputv = $this->load->view('front/sip_license_agreement', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = getcwd() . "/uploads/agrement-details.pdf";
        if (file_exists($pdfFilePath)) {
            unlink($pdfFilePath);
        }
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($outputv, 2);
        $pdf->SetTitle('SIP LICENSE AGREEMENT | SIP');
        ob_end_clean();
        $pdf->Output($pdfFilePath, "I");
    }


}

?>