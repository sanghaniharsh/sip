<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
        
    }

    public function index($start = 0) {
        $perpage            = 10;
        $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
        $sfilter['active']  = '1';
        $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);
        $result             = $this->news_model->getNews($sfilter);
        $data['news_list']  = $result['news'];
        $data['total']      = $result['total'];

        //pagination
        $config                     = initPagination();
        $config["base_url"]         = base_url() . "news/";
        $config["total_rows"]       = $data['total'];
        $config["per_page"]         = $perpage;
        $config['uri_segment']      = 2;
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $data['total'];
        $this->pagination->initialize($config);

        $str_links              = $this->pagination->create_links();
        $data["links"]          = $str_links;
        $this->template->view('front/news/index', $data);
    }
    
    public function index1() {
        $data    = array();
        $this->template->view('front/news/index', $data);
    }
}

?>