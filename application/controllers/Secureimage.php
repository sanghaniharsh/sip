<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secureimage extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function check_captcha() {
            $this->load->library('securimage/securimage');
            $securimage = new Securimage();	

            $return = TRUE;
            if( ! $securimage->check($this->input->post('captcha')))
            {
                $return = FALSE;
            }
            
            echo json_encode($return);
    }
    
    public function securimage() {
        $this->load->config('csecurimage');
        $active = $this->config->item('si_active');
        $allsettings = array_merge($this->config->item($active), $this->config->item('si_general'));
        $this->load->library('securimage/securimage');
        $img = new Securimage($allsettings);
        //$img->captcha_type = Securimage::SI_CAPTCHA_MATHEMATIC;

        $img->show(APPPATH . 'libraries/securimage/backgrounds/bg5.png');
    }
}