<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->is_logined       = $this->get_enterprise_user();
        $this->loginUser        = $this->getEnterpriseUserData();
        $this->enterprise_user  = $this->get_enterprise_data();
        
        if(empty($this->enterprise_user) || empty($this->loginUser)) {
            redirect(base_url().'enterprise');
        }
    }

    public function cart() {

        $data = array();
        $return = array();
        
        $pFilter['select']  = array('angel_enterprise_cart.id as cart_id ', 'angel_enterprise_cart.quantity', 'angel_enterprise_cart.ip_address', 'angel_enterprise_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'angel_enterprise_cart.question');
        $pFilter['join']    = array(
                                0 => array('table' => 'angel_enterprise_product as p', 'condition' => 'p.id = angel_enterprise_cart.product_id', 'type' => 'left'),
                                1 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = angel_enterprise_cart.product_id', 'type' => 'left'),
        );

        $pFilter['where']       = array('p.status' => '1');

        if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
            $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
        } else {
            $pFilter['where']['angel_enterprise_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
            $pFilter['where']['angel_enterprise_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        }
        $pFilter['groupby'] = array('field' => 'p.id');
        $data['cart']       = $this->enterprise_cart_model->get_rows($pFilter);

        $data['cart_count'] = $this->enterprise_cart_model->cart_count();

        $return['html'] = $this->load->view('enterprise/order/cart_detail_ajax', $data, true);

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return['status'] = true;
            echo json_encode($return);
            die;
        } else {
            $data['html'] = $return['html'];
            $this->template->view('enterprise/order/cart', $data);
        }
    }

    public function add_cart_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $return             = array();
            
            $data['quantity']       = $this->input->post('quantity');
            $product_id             = $this->input->post('product_id');
            $data['product_id']     = decreptIt($product_id);
            $data['enterprise_id']  = $this->enterprise_user->id;

            if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $data['user_id']    = $this->loginUser['id'];
            } else {
                $data['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            }

            if ($data['product_id'] > 0 && $data['quantity'] > 0) {

                $filter['where']    = array('product_id' => $data['product_id']);
                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $filter['where']['user_id'] = $this->loginUser['id'];
                } else {
                    $filter['where']['ip_address'] = $data['ip_address'];
                    $filter['where']['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $filter['row']      = 1;
                $result             = $this->enterprise_cart_model->get_rows($filter);

                if (empty($result)) {
                    $this->enterprise_cart_model->insert($data);
                    $return['status']       = true;
                    $return['message']      = "Product added in your cart";
                } else {
                    $return['status']   = false;
                    $return['message']  = "Already Item exists in cart ";
                }
                $return['cart_count']   = $this->enterprise_cart_model->cart_count();
            } else {
                $return['message']  = "Something Wrong";
                $return['status']   = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function add_to_cart_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $return             = array();

            $data['quantity']       = $this->input->post('quantity');
            $product_id             = $this->input->post('product_id');
            $data['product_id']     = decreptIt($product_id);
            $data['enterprise_id']  = $this->enterprise_user->id;
            if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $data['user_id']    = $this->loginUser['id'];
            } else {
                $data['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            }

            if ($data['product_id'] > 0 && $data['quantity'] > 0) {
                
                $filter['where']    = array('product_id' => $data['product_id']);
                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $filter['where']['user_id'] = $this->loginUser['id'];
                } else {
                    $filter['where']['ip_address'] = $data['ip_address'];
                    $filter['where']['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $filter['row']          = 1;
                $result                 = $this->enterprise_cart_model->get_rows($filter);
                
                if (empty($result)) {
                    $this->enterprise_cart_model->insert($data);
                    $return['status']       = true;
                    $return['message']      = "Product added in your cart";
                } else {
                    $updateData['quantity']  = $data['quantity'];
                    
                    $this->enterprise_cart_model->update_table($updateData, array('product_id' => $data['product_id']));
                    $return['status']   = true;
                    $return['message']  = "Product added in your cart";
                }
                $return['cart_count']   = $this->enterprise_cart_model->cart_count();
            } else {
                if ($data['product_id'] > 0) {
                    $filter['where']    = array('product_id' => $data['product_id']);
                    $filter['row']      = 1;
                    $cart               = $this->enterprise_cart_model->get_rows($filter);

                    if (!empty($cart)) {
                        $this->enterprise_cart_model->delete(array('id' => $cart->id));
                        $count                      = $this->enterprise_cart_model->cart_count();
                        $return['cart_count']       = '';
                        if ($count > 0) {
                            $return['cart_count']   = $this->enterprise_cart_model->cart_count(); 
                        }
                    }

                    $return['message']      = "Cart item deleted successfully";
                    $return['status']       = true;
                }else {
                    $return['message']  = "Something Wrong";
                    $return['status']   = false;
                }
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }


    public function cart_delete_item() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('id');

            $id = decreptIt($id);

            if ($id > 0) {

                $filter['where']    = array('id' => $id);
                $filter['row']      = 1;
                $cart               = $this->enterprise_cart_model->get_rows($filter);

                if (!empty($cart)) {
                    $this->enterprise_cart_model->delete(array('id' => $id));
                    $count                  = $this->enterprise_cart_model->cart_count();
                    $return['cart_count']   = '';
                    if ($count > 0) {
                       $return['cart_count']   = $this->enterprise_cart_model->cart_count(); 
                    }
                    $return['message']      = "Cart item deleted successfully";
                    $return['status']       = true;
                } else {
                    $return['message']  = "Something went wrong";
                    $return['status']   = false;
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function checkout() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            
            $ip_address             = $_SERVER['REMOTE_ADDR'];
            if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $cFilter['user_id']     = $this->loginUser['id'];
            } else {
                $cFilter['ip_address']  = $ip_address;
            }

            $cart_details   = $this->enterprise_cart_model->getItems($cFilter);
            
            if (!empty($cart_details)) {
                $amount             = 0;
                $discount           = 0;
                $sub_total          = 0;
                $sales_tax          = $this->loginUser['sales_tax'];
                $shipping_total     = 0;
                $user_id            = $this->loginUser['id'];
                $promocode          = "";
                
                foreach ($cart_details as $value) {
                    $amount                                     += $value->quantity * $value->price;
                    $product[$value->product_id]                = array('quantity' => $value->quantity, 'price' => $value->price);
                }
                
                $amount = $amount + $shipping_total;
                $tax    = $amount * $sales_tax /100;
                         
                $data['user_id']        = $user_id;
                $data['status']         = 'success';
                $data['amount']         = $amount+$tax;   

                $data['order_status']   = '0';
                $data['delivery']       = (isset($_POST['delivery']) ? $_POST['delivery'] : 0 );
                $data['term_condition'] = (isset($_POST['terms']) ? $_POST['terms'] : 0 );
                $data['discount']       = $discount;
                $data['tax']            = $tax;
                $data['promo_code']     = $promocode;
                $data['created_date']   = date('Y-m-d H:i:s');
                
                $order_id               = $this->enterprise_order_model->insert($data);
                
                if($order_id > 0) {
                    
                    $order_items    = array();
                    $product_ids    = array();
                    $product_details= array();
                    
                    foreach ($product as $key => $row) {
                        $order_items[$key]                              = array('order_id' => $order_id, 'product_id' => $key , 'quantity' => $row['quantity'],'amount' =>  $row['price'],'shipping_amount' => 0);
                        $product_ids[]                                  = $key;
                        $product_details[$key]['id']                    = $key;
                        $product_details[$key]['quantity']              = $row;
                        
                    }
                    
                    if(!empty($order_items)) {
                        $this->enterprise_order_item_model->insert_betch($order_items);
                    }
                    
                    $this->order_email($order_id);
                    
                    
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $where = array('user_id' => $this->loginUser['id']);
                        $this->enterprise_cart_model->delete($where);

                        $where = array('ip_address' => $_SERVER['REMOTE_ADDR']);
                        $this->enterprise_cart_model->delete($where);
                    } else {
                        $where = array('ip_address' => $_SERVER['REMOTE_ADDR']);
                        $this->enterprise_cart_model->delete($where);
                    }
                    
                    redirect(enterprise_url() . 'thankyou');
                }
                
            } else {
                $this->session->set_flashdata('error', 'Somthing went wrong');
                redirect(enterprise_url() . 'cart');
            }
            
        } else {
            not_found();
        }
    }

    public function order_email($id = "") {
        
        $filter['select'] = array('angel_enterprise_order.id as o_id ', 'oi.*', 'p.name', 'p.price', 'p.awarded_price' ,'angel_enterprise_order.tax', 'p.model_number');

        $filter['join'] = array(
            0 => array('table' => 'angel_enterprise_order_items  as oi', 'condition' => 'oi.order_id = angel_enterprise_order.id', 'type' => 'left'),
            1 => array('table' => 'angel_enterprise_product  as p', 'condition' => 'p.id = oi.product_id', 'type' => 'left'),
        );
        $filter['where'] = array('oi.order_id' => $id);
        $filter['groupby'] = array('field' => 'oi.product_id');


        $data['order'] = $this->enterprise_order_model->get_rows($filter);
        $Ufilter['select'] = array('angel_enterprise_order.*', 'u.firstname', 'u.lastname', 'u.email', 'u.phone');
        $Ufilter['join'] = array(
            0 => array('table' => 'angel_enterprise_users as u', 'condition' => 'u.id = angel_enterprise_order.user_id', 'type' => 'left'),
        );

        $Ufilter['row'] = 1;
        $filter['groupby'] = array('field' => 'angel_enterprise_order.id');
        $Ufilter['where'] = array('angel_enterprise_order.id' => $id);
        
        $data['userDetail'] = $this->enterprise_order_model->get_rows($Ufilter);

        
        $data['enterprise'] = $this->enterprise_user;
        
        
        $afilter['row']     = 1;
        $Uresult            = $this->admin_model->get_rows($afilter);

        $od_filter['where']     = array('angel_enterprise_order.id' => $id);
        $od_filter['groupby']   = array('field' => 'angel_enterprise_order.id');
        $od_filter['row']       = 2;
        $data['order_details']  = $this->enterprise_order_model->get_rows($od_filter);

        $mailData['message']        = $this->load->view('enterprise/email/admin_order', $data, true);
        $mailData['subject']        = "REP - Enterprise Order";
        $mailData['email']          =  array('kswerdlick@saveinparadise.com','rsinger@saveinparadise.com','gricci@saveinparadise.com');
        @$this->sentMail($mailData);
    }
    
    
    public function thankyou() {
        $data = array();
        
        $user_id = $this->loginUser['id'];
        
        $user_data = $this->enterprise_user_model->get_detail($user_id);
        if (!empty($user_data)) {
            $data['user_name'] = $user_data->username;
            $this->template->view('enterprise/order/thankyou', $data);
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong');
            redirect(enterprise_url());
        }
        
    }
    
    public function cart_update_qty() {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $return = array();
            $id = $this->input->post('id');
            $id = decreptIt($id);
            $data['quantity'] = $this->input->post('quantity');

            if ($id > 0 && $data['quantity'] > 0) {
                
                $filter['where'] = array('id' => $id);
                $cart = $this->enterprise_cart_model->get_rows($filter);
                if (!empty($cart)) {
                    $update = $this->enterprise_cart_model->update_table($data, array('id' => $id));

                    if($update) {
                        $return['message']  = "Quantity updated successfully";
                        $return['status']   = true;
                    } else {
                       $return['message']  = "Something went wrong";
                       $return['status']   = true;
                    }
                    
                } else {
                    $return['message']  = "Something went wrong";
                    $return['status']   = false;
                }
            } else {
                $return['message']  = "Something Wrong";
                $return['status']   = false;
            }

            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }
    
    public function reorder($id = '') {
        
        if($id != "") {

            $id = decreptIt($id);
            
            $uFilter['where']   = array('id' => $id);
            $uFilter['row']     = 1;
            $order              = $this->enterprise_order_model->get_rows($uFilter);
            if(!empty($order)) {
                
                $iFilter['where']   = array('order_id' => $id);
                $order_items        = $this->enterprise_order_item_model->get_rows($iFilter);
                
                if(!empty($order_items)) {
                    $old_amount = $order->amount - $order->tax;
                    $sales_tax  = $old_amount * $this->loginUser['sales_tax'] / 100; 
                    $amount     = $old_amount + $sales_tax; 

                    $new_order['user_id']       = $order->user_id;
                    $new_order['amount']        = $amount;
                    $new_order['promo_code']    = $order->promo_code;
                    $new_order['discount']      = $order->discount;
                    $new_order['tax']           = $sales_tax;
                    $new_order['status']        = $order->status;
                    $new_order['order_status']  = 0;
                    $new_order['delivery']       = (isset($_POST['delivery']) ? $_POST['delivery'] : 0 );
                    $new_order['term_condition'] = (isset($_POST['terms']) ? $_POST['terms'] : 0 );
                    $new_order['created_date']  = date('Y-m-d H:i:s');

                    $order_id               = $this->enterprise_order_model->insert($new_order);

                    if($order_id > 0) {
                        
                        $new_order_items = array();
                        
                        foreach ($order_items as $key => $value) {
                            $new_order_items[] = array(
                                                    'order_id' => $order_id, 
                                                    'product_id' => $value->product_id, 
                                                    'quantity' => $value->quantity, 
                                                    'shipping_amount' => $value->shipping_amount
                                                    );
                        }
                        
                        if(!empty($new_order_items)) {
                            $this->enterprise_order_item_model->insert_betch($new_order_items);
                            
                            $this->order_email($order_id);
                            
                            redirect(enterprise_url() . 'thankyou');
                        } 

                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong');
                        redirect(enterprise_url().'myaccount');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Order item not found');
                    redirect(enterprise_url().'myaccount');
                }
                
            } else {
                $this->session->set_flashdata('error', 'Order not found');
                redirect(enterprise_url().'myaccount');
            }
            
        } else {
            $this->session->set_flashdata('error', 'Order not found');
            redirect(enterprise_url().'myaccount');
        }
    }

}
?>