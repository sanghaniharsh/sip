<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_enterprise_user();
        $this->loginUser        = $this->getEnterpriseUserData();
        $this->enterprise_user  = $this->get_enterprise_data();
        
        
        if(empty($this->enterprise_user) || empty($this->loginUser)) {
            redirect(base_url().'enterprise');
        }
        
    }

    public function index() {
        
        $this->template->view("enterprise/product/index");
    }
    
    public function product_list($start = 0) {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            
            $perpage            = 12;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
            $searchval          = $this->input->post('searchval');
            $sort    = $this->input->post('sort');
            
            if($sort != "") {
                $sfilter['order']      = $sort;
            }
            
            if($searchval  != "") {
                $sfilter['search']      = $searchval;
            }
            $sfilter['active']      = '1'; 
            $sfilter['limit']       = array('limit' => $perpage, 'from' => $start);
            if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $pFilter['user_id']     = $this->loginUser['id'];
            } else {
                $pFilter['ip_address']  = $this->input->ip_address();
            }

            $cart       = $this->enterprise_cart_model->getItems($pFilter);
            $cart_item_array = array();
            if(isset($cart)) {
                foreach($cart as $cart_item) {
                    $cart_details[$cart_item->product_id]   =  $cart_item;
                    $cart_item_array    = $cart_details;
                }
            }
            $data['cart_item'] =  $cart_item_array;
            $sfilter['enterprise_ids']  = $this->enterprise_user->id; 


            $result             = $this->enterprise_product_model->getProducts($sfilter);


            $data['products']   = $result['product'];
            // P($data['products']);
            $data['total']      = $result['total'];
            //pagination
            $config     = initPagination();
            $url        = enterprise_url() . "product-list";
            
            $config["base_url"]         = $url;
            $config["total_rows"]       = $data['total'];
            $config["per_page"]         = $perpage;
            $config['uri_segment']      = 4;
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $data['total'];
            $config['attributes']       = array('class' => 'page-link');
            $this->pagination->initialize($config);

            $str_links              = $this->pagination->create_links();
            $data["links"]          = $str_links;

            if (!empty($data)) {
                $return['status']   = true;
                $return['html']     = $this->load->view('enterprise/product/ajax_products', $data, true);
            } else {
                $return['status']   = false;
            }
        } else {
            $return['status']   = false;
        }
        echo json_encode($return);die;
    }

    public function product_details() {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $product_id         = $this->input->post('product_id');

            $sfilter['select']   = array('angel_enterprise_product.*','GROUP_CONCAT(pi.image) as product_images');
            $sfilter['join']     = array(
                1 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = angel_enterprise_product.id', 'type' => 'left')
            );
            $sfilter['active']          = '1'; 
            $sfilter['where']           = array('angel_enterprise_product.id' =>  decreptIt($product_id));

            $sfilter['row']             = 1;
            $data['product_details']    = $this->enterprise_product_model->get_rows($sfilter);

            if (!empty($data)) {
                $return['status']   = true;
                $return['html']     = $this->load->view('enterprise/product/product_modal', $data, true);
            } else {
                $return['status']   = false;
            }
        } else {
            $return['status']   = false;
        }
        echo json_encode($return);die;
    }

    public function product_modal_popup() {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $product_id         = $this->input->post('product_id');
            $sfilter['select']   = array('angel_enterprise_product.*','GROUP_CONCAT(pi.image) as product_images');
            $sfilter['join']     = array(
                1 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = angel_enterprise_product.id', 'type' => 'left')
            );
            $sfilter['active']          = '1'; 
            $sfilter['where']           = array('angel_enterprise_product.id' =>  decreptIt($product_id));

            $sfilter['row']             = 1;
            $product_details            = $this->enterprise_product_model->get_rows($sfilter);
            
            if (!empty($product_details)) {
                $return['product_details']  = $product_details;
                $return['status']           = true;
            } else {
                $return['status']   = false;
            }
        } else {
            $return['status']   = false;
        }
        echo json_encode($return);die;
    }

    public function detail($id) {
        
        $data['js_files'] = array(
                                'jquery.bxslider.js',
                                'jquery.flexslider.js',
                                'cloud-zoom.js',
                                'select2.min.js'
                            );
        $data['css_files'] = array(
                                'flexslider.css',
                                'select2.min.css'
                            );

        if ($id != "") {
            $pFilter['select']   = array('angel_enterprise_product.*', 'GROUP_CONCAT(pi.image) as product_images');
            $pFilter['join']     = array(
                                    array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = angel_enterprise_product.id', 'type' => 'left')
                                );

            $pFilter['where']   = array('angel_enterprise_product.id' =>  decreptIt($id), 'angel_enterprise_product.status' => '1');
            $pFilter['row']     = 1;
            $pFilter['groupby'] = array('field' => 'angel_enterprise_product.id');
            $result             = $this->enterprise_product_model->get_rows($pFilter);

            if (!empty($result)) {
                
                $data['product']    = $result;
                

                $data['meta_title']         = ucfirst($result->name).' at Restaurant Equipment Paradise';
                $data['meta_description']   = html_entity_decode(substr(strip_tags($result->description), 0, 200));
                if (isset($result->product_images) && $result->product_images !="") {
                    $images             = explode (",", $result->product_images);
                    $data['meta_image'] = isset($images[0]) ? $images[0] : '';
                }
                $this->template->view("enterprise/product/detail", $data);
            } else {
                not_found();
            }
        }
        
    }
    
    public function add_cart_block() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data   =   array();
            $return             = array();
            $product_id         = $this->input->post('product_id');
            $product_id         = decreptIt($product_id);
            $data['product_id'] = $product_id;

            $filter['where']    = array('product_id' => $product_id);
            if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $filter['where']['user_id']     = $this->loginUser['id'];
            } else {
                $filter['where']['ip_address']  = $this->input->ip_address();
            }

            $filter['row']      = 1;
            $data['cart_data']  = $this->enterprise_cart_model->get_rows($filter);

            $pFilter['where']   = array('angel_enterprise_product.id' =>  $product_id, 'angel_enterprise_product.status' => '1');
            $pFilter['row']     = 1;
            $pFilter['groupby'] = array('field' => 'angel_enterprise_product.id');
            $data['product']    = $this->enterprise_product_model->get_rows($pFilter);

            $return['html']     = $this->load->view('enterprise/product/add_cart_ajax_block', $data, true);
            $return['status']   = true;
            echo json_encode($return);die;
        } else {
            not_found();
        }
    }

    public function cart_delete_item() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('id');

            $id = decreptIt($id);
            if ($id > 0) {
                
                $filter['where']    = array('id' => $id);
                $filter['row']      = 1;
                $cart               = $this->enterprise_cart_model->get_rows($filter);
                
                if (!empty($cart)) {
                    $this->enterprise_cart_model->delete(array('id' => $id));
                    
                    $filter['where']    = array('product_id' => $cart->product_id);
                    if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $filter['where']['user_id']     = $this->loginUser['id'];
                    } else {
                        $filter['where']['ip_address']  = $this->input->ip_address();
                    }

                    $filter['row']              = 1;
                    $data['cart_data']          = $this->enterprise_cart_model->get_rows($filter);
                    $count                      = $this->enterprise_cart_model->cart_count();
                    $return['html']             = $this->load->view('enterprise/product/add_cart_ajax_block', $data, true);
                    $return['cart_item_count']  = '';
                    if ($count > 0) {
                       $return['cart_item_count'] = $this->enterprise_cart_model->cart_count(); 
                    }
                    $return['message']      = "Cart item deleted successfully";
                    $return['status']       = true;
                } else {
                    $return['message']  = "Something went wrong";
                    $return['status']   = false;
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }
}