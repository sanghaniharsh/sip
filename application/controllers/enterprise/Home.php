<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_enterprise_user();
        $this->loginUser        = $this->getEnterpriseUserData();
        $this->enterprise_user  = $this->get_enterprise_data(); 
    }

    public function index() {
        
        $this->removeEnterpriseSession();
  
        $this->template->view("enterprise/home/index");
    }
    
    public function myaccount() {
        if ($this->is_logined) {
            $data               = array();
            $data['loginUser']  = $this->loginUser;
            

            $data['js_files'] = array("jquery.dataTables.min.js");
            $data['css_files'] = array("jquery.dataTables.min.css", "tabs.css");
            
            $this->template->view('enterprise/user/myaccount', $data);
        } else {
            redirect(base_url());
        }
    }
    
    public function get_user_orders() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'angel_enterprise_order.user_id',
                                    '1' => 'angel_enterprise_order.created_date',
                                    '2' => 'angel_enterprise_order.amount',
                                    '3' => 'total_item'
                                    );
                $filter['where']= array('user_id' => $this->loginUser['id']);
                
                $filterCount    = $totalCount = $this->enterprise_order_model->get_rows($filter, true);

                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_enterprise_order.amount', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_enterprise_order.created_date', 'value' => $searchString)
                    );
                    $filterCount    = $this->enterprise_order_model->get_rows($filter, true);
                }

                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['select']   = array('angel_enterprise_order.*', 'COUNT(oi.id) as total_item');
                $filter['join']     = array( 
                                        0 => array('table' => 'angel_enterprise_order_items as oi', 'condition' => 'oi.order_id = angel_enterprise_order.id', 'type' => 'LEFT')
                                    );
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $filter['groupby']  = array('field' => 'angel_enterprise_order.id');
                $query              = $this->enterprise_order_model->get_rows($filter);
                $orders             = array();
                                // P($query);
                foreach ($query as $k => $row) {
                    $checked                    = ($row->status == 1) ? 'checked' : '';
                    $status                     = $row->status;

                    $row_data                   = array();
                    $row_data['no']             = $k + 1;
                    $row_data['date']           = date("m-d-Y", strtotime($row->created_date));
                    $row_data['invoice_number'] = display_enterprise_order($row->id);
                    $row_data['total_item']     = $row->total_item;
                    $row_data['total_amount']   = '$' .$row->amount;
//                    $row_data['action']         = '
//                                                    <a href="'.  enterprise_url() . 'reorder/' . encreptIt($row->id).'" class="btn btn-primary">Reorder</a>    
//                                                ';
                    $row_data['action']         = '
                                                    <a href="'.  enterprise_url() . 'order-details/' . encreptIt($row->id).'" class="btn btn-default">View and Reorder</a>    
                                                ';

                    $orders[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $orders;

                echo json_encode($data);die();
            } else {
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }
    
    public function order_details($order_id = "") {
        if ($this->is_logined) {

            if ($order_id != "") {
                $filter['where']    = array('angel_enterprise_order.id' => decreptIt($order_id));
                $filter['select']   = array('angel_enterprise_order.*', 'oi.quantity','oi.product_id', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'oi.shipping_amount');
                $filter['join']     = array( 
                                        0 => array('table' => 'angel_enterprise_order_items as oi', 'condition' => 'oi.order_id = angel_enterprise_order.id', 'type' => 'LEFT'),
                                        1 => array('table' => 'angel_enterprise_product as p', 'condition' => 'p.id = oi.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = oi.product_id', 'type' => 'left'),
                                    );
                $filter['groupby']  = array('field' => 'oi.product_id');
                $data['order']      = $this->enterprise_order_model->get_rows($filter);
                
                if (!empty($data['order'])) {

                    $data['item_count']     = count($data['order']);
                    $od_filter['select']    = array('angel_enterprise_order.*', 'u.firstname', 'u.lastname', 'u.email', 'u.phone');
                    
                    $od_filter['join'] = array(
                        0 => array('table' => 'angel_enterprise_users as u', 'condition' => 'u.id = angel_enterprise_order.user_id', 'type' => 'left'),
                    );
                    
                    $od_filter['where']     = array('angel_enterprise_order.id' => decreptIt($order_id));
                    $od_filter['groupby']   = array('field' => 'angel_enterprise_order.id');
                    $od_filter['row']       = 1;
                    $data['order_details']  = $this->enterprise_order_model->get_rows($od_filter);

                    $this->template->view('enterprise/user/order_details', $data);
                } else {
                    redirect(enterprise_url().'myaccount');
                }
            } else{
                redirect(enterprise_url());
            }
        } else {
            redirect(enterprise_url());
        } 
    }
    
    
    
}