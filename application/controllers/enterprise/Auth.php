<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->enterprise_user  = $this->get_enterprise_data();
    }

    public function check_login() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            
            $remember       = trim($this->input->post('remember_me'));
            $redirect_url   = $_SERVER['HTTP_REFERER'];
            
            
            $filter['where']= array('email' => trim($this->input->post('email')), 'password' => trim(encreptIt($this->input->post('password'))), 'is_active' => '1');
            $filter['row']  = 2;
            $row            = $this->enterprise_user_model->get_rows($filter);
            
            if (!empty($row)) {
                if ($remember == "on") {
                    $cookie_data['name']        = $this->enterprise_user->slug;
                    $cookie_data['user_value']   = $row['email'] . "," . decreptIt($row['password']);
                    $cookie_data['expire_time']  = 30 * 24 * 60 * 60;
                    $this->set_cookie_enterprise($cookie_data);
                }

                $e_filter['where']  = array('id' => $row['enterprise_id'], 'status' => '1');
                $enterprise = $this->enterprise_model->getEnterprise($e_filter);

                if(!empty($enterprise)) {

                    $user_session['session_id'] = encreptIt($row['id']);
                    $this->set_enterprise_user($user_session);

                    if ($redirect_url != "") {
                        redirect(base_url().'enterprise/'.$enterprise->slug);
                    } else {
                        redirect(enterprise_url());
                    }
                } else {
                    $this->session->set_flashdata('error', 'Enterprise not activate.');
                    redirect(enterprise_url());
                }
                
            } else {
                $this->session->set_flashdata('error', 'Wrong Username or Password.');
                redirect(enterprise_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong.');
            redirect(enterprise_url());
        }
    }
    
    public function logout() {
        $this->removeEnterpriseSession();
        redirect(enterprise_url());
    }
    
    
}
?>