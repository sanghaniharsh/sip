<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser(); 
    }

    public function index($start = 0) {
        $perpage            = 10;
        $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
        $sfilter['active']  = '1'; 
        $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);
        $result             = $this->blog_model->blogPost($sfilter);
        $data['blog_list']  = $result['blog'];
        $data['total']      = $result['total'];

        //pagination
        $config                     = initPagination();
        $config["base_url"]         = base_url() . "blog/";
        $config["total_rows"]       = $data['total'];
        $config["per_page"]         = $perpage;
        $config['uri_segment']      = 2;
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $data['total'];
        $this->pagination->initialize($config);

        $str_links              = $this->pagination->create_links();
        $data["links"]          = $str_links;

        $cFilter['where']       = array('isActive' => '1');
        $data["blog_category"]  = $this->blog_category_model->get_rows($cFilter);
        $this->template->view('front/blog/index', $data);
    }

    public function view($slug) {
        $sfilter['slug']    = $slug;
        $blog               = $this->blog_model->blogPost($sfilter);

        if (!empty($blog['blog'])) {
            $data['blog_images']    = array();
            if (isset($blog['blog']->blog_images) && $blog['blog']->blog_images != '') {
                $img_data               = explode(',', $blog['blog']->blog_images);
                $data['blog_images']    = $img_data;
            }

            $data['blog']           = $blog['blog'];
            $cFilter['where']       = array('isActive' => '1');
            $data["blog_category"]  = $this->blog_category_model->get_rows($cFilter);

            $vFilter['where']   = array('blog_id' => $blog['blog']->id, 'ip_address' => $this->input->ip_address());
            $checkIp            = $this->blog_view_model->get_rows($vFilter, true);

            if($checkIp == 0) {
                $this->blog_view_model->addView($blog['blog']->id);
            }
            $this->template->view('front/blog/detail', $data);
        } else {
            redirect(base_url().'blog');
        }
    }

    public function category($slug = '', $start = 0) {

        if ($slug != '') {
            $perpage            = 10;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
            $sfilter['category']= $slug;
            $sfilter['limit']   = array('limit' => $perpage, 'from' => $start);
            $result             = $this->blog_model->blogPost($sfilter);
            $data['blog_list']  = $result['blog'];
            $data['total']      = $result['total'];

            //pagination
            $config                     = initPagination();
            $config["base_url"]         = base_url()."blog/category/".$slug;
            $config["total_rows"]       = $data['total'];
            $config["per_page"]         = $perpage;
            $config['uri_segment']      = 4;
            $config['use_page_numbers'] = TRUE;
            $config['num_links']        = $data['total'];
            $this->pagination->initialize($config);

            $str_links              = $this->pagination->create_links();
            $data["links"]          = $str_links;

            $cFilter['where']       = array('isActive' => '1');
            $data["blog_category"]  = $this->blog_category_model->get_rows($cFilter);
            $this->template->view('front/blog/category', $data);
        } else {
            redirect(base_url().'blog');
        }
    }
}

?>