<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auction extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
    }
    public function index($start = 0) {

        $perpage                = 8;
        $start                  = ($start != 0) ? ($start - 1) * $perpage : 0;
        $afilter['active']      = '1'; 
        $afilter['limit']       = array('limit' => $perpage, 'from' => $start);
        $result                 = $this->auction_model->getAuction($afilter);

        $data['auction_list']   = $result['auction'];
        $total                  = $result['total'];

        //pagination
        $config                     = initPagination();
        $config["base_url"]         = base_url() . "auction/";
        $config["total_rows"]       = $total;
        $config["per_page"]         = $perpage;
        $config['uri_segment']      = 2;
        $config['use_page_numbers'] = TRUE;
        $config['num_links']        = $total;
        $config['attributes']       = array('class' => 'page-link');
        $this->pagination->initialize($config);

        $str_links              = $this->pagination->create_links();
        $data["links"]          = $str_links;
        
        $data['js_files'] = array(
                                'jquery.bxslider.js',
                                'jquery.flexslider.js',
                                'cloud-zoom.js'
                            );
        $data['css_files'] = array(
                                'flexslider.css'
                            );

        $this->template->view("front/auction/index",$data);
    }
    
    public function auctionvideo() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id     = $this->input->post('id');
            $a_id   = decreptIt($id);

            $aFilter['where']   = array('id' => $a_id);
            $aFilter['row']     = 1;
            $auction_data       = $this->auction_model->get_rows($aFilter);

            if(isset($auction_data->video_link)) {
                $adata['video_link']= $auction_data->video_link;
                $return['html']     = $this->load->view('front/auction/auction_modal_content', $adata, true);
                $return['status']   = TRUE;
            } else {
                $return['status']   = FALSE;
            }

            echo json_encode($return);
            die;
        } else {
            redirect(base_url());
        }
    }
  
}

?>