<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand extends MY_Controller {

    var $is_logined = FALSE;

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();
    }

    public function index($start = 0) {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $sfilter                    = array();
            $result                     = $this->brand_model->getProductBrand($sfilter);
            $data['brand_list']         = $result['brand'];
            $data['total']              = $result['total'];

            if (!empty($data)) {
                $return['status']   = true;
                $return['html']     = $this->load->view('front/brand/ajax_brand_view', $data, true);
            } else {
                $return['status']   = false;
            }

            echo json_encode($return);die;
        }
        $this->template->view('front/brand/index');
    }

    public function brand_products($slug = "") {
        $data   = array();
        if ($slug != '') {
            $bFilter['select']  = array('id');
            $bFilter['row']     = 1;
            $bFilter['where']   = array('slug' => $slug);
            $brand              = $this->brand_model->get_rows($bFilter);

            if (!empty($brand)) {
                $result                 = $this->brand_model->getProductBrand();
                $data['brand_list']     = $result['brand'];
                $data['slug']           = $slug;
                $this->template->view("front/brand/brand_product_list", $data);
            } else {
                not_found();
            }
        } else {
            not_found();
        }
    }

    public function get_brand_products($slug = '', $start = 0) {
        $return = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $perpage            = 8;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0; 

            $bFilter['select']  = array('id');
            $bFilter['row']     = 1;
            $bFilter['where']   = array('slug' => $slug);
            $brand              = $this->brand_model->get_rows($bFilter);

            if (!empty($brand)) {

                $sfilter['brand_id']        = $brand->id; 
                $sfilter['active']          = '1'; 
                $sfilter['limit']           = array('limit' => $perpage, 'from' => $start);

                $result                     = $this->brand_model->getBrandProducts($sfilter);
                $data['products']           = $result['product'];
                $data['total']              = $result['total'];

                //pagination
                $config                     = initPagination();
                $url                        = base_url() . "brand/get_brand_products/".$slug."/";
                $config["base_url"]         = $url;
                $config["total_rows"]       = $data['total'];
                $config["per_page"]         = $perpage;
                $config['uri_segment']      = 4;
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = $data['total'];
                $config['attributes']       = array('class' => 'page-link');
                $this->pagination->initialize($config);

                $str_links              = $this->pagination->create_links();
                $data["links"]          = $str_links;

                if (!empty($data)) {
                    $return['status']   = true;
                    $return['html']     = $this->load->view('front/brand/ajax_products', $data, true);
                } else {
                    $return['status']   = false;
                }
            } else {
               $return['status']   = false;
            }
        } else {
            $return['status']   = false;
        }
        echo json_encode($return);die;
    }
}
?>