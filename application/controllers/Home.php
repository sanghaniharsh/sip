<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logined       = $this->get_authorized_user();
        $this->loginUser        = $this->getLoginUser();

    }

    public function index($start = 0) {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return             = array();
            $perpage            = 16;
            $start              = ($start != 0) ? ($start - 1) * $perpage : 0;
            $filter['limit']    = array('limit' => $perpage, 'from' => $start);

            $cate_id            = -1;
            $filter['parent']   = $cate_id;
            $result             = $this->category_model->getCategory($filter);
            $count              = $result['total'];

            //pagination
            $config = initPagination();
            $config["base_url"]     = base_url() . "home/index/";
            $config["per_page"]     = $perpage;
            $config['uri_segment']  = 3;
            $config["total_rows"]   = $count;
            $this->pagination->initialize($config);

            $view_data['pagination_link']   = $this->pagination->create_links();
            $view_data['category_list']     = $result['category'];

            $data['category_view']      = $this->load->view('front/home/ajax_categories', $view_data, true);

            $return['status']           = true;
            $return['category_html']    = $data['category_view'];
            echo json_encode($return);die;
        }

        $this->template->view("front/home/index");
    }

    public function search() {
        $this->template->view("front/home/search");
    }
    public function brands() {
        $this->template->view("front/home/brands");
    }
    public function sub_cat() {
        $this->template->view("front/home/sub_cat");
    }
    public function detail() {
        $data['js_files'] = array(
                                'jquery.bxslider.js',
                                'jquery.flexslider.js',
                                'cloud-zoom.js'
                            );
        $data['css_files'] = array(
                                'flexslider.css'
                            );
        $this->template->view("front/home/detail", $data);
    }
    
    public function search_location() {
        $return = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $seach                  = $this->input->post('search');

            $filter['customWhere']  = 'zipcode LIKE "'.$seach.'%" OR city LIKE "'.$seach.'%"';
            $filter['select']       = array('city', 'state', 'zipcode');
            $location               = $this->location_model->get_rows($filter);
            if (! empty($location)) {
                if(!is_numeric($seach)){
                    $city_array     = array();
                    foreach ($location as $key => $loc) {
                        $city_array[$loc->city] = $loc->city;
                    }
                    foreach ($location as $key => $city) {
                        if (isset($city_array[$city->city]) && ($city_array[$city->city] == $city->city)){
                            $new_city           = new stdClass();
                            $new_city->citySearch = true;
                            $new_city->city     = $city->city;
                            $new_city->state    = $city->state;
                            $new_city->zipcode  = $city->city;
                            $insert_city        = $new_city;
                            $location           = $this->array_insert($location, $key, $insert_city);
                            unset($city_array[$city->city]);
                        }
                    }
                }
                $data['location']   = $location;
                $return['html']     = $this->load->view('front/home/location_search', $data, true);
                $return['status']   = true;
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }

        echo json_encode($return);
    }

    public function search_product() {
        $return = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $search   = $this->input->post('search');
            $filter['select']       = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
            $filter['join']         = array(
                                        0 => array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left')
                                    );
            $filter['customWhere']  = 'angel_product.is_active = "1" AND angel_product.type = 0 AND (angel_product.name LIKE "'.$search.'%" OR  modal_number LIKE "'.$search.'%" OR c.name LIKE "'.$search.'%")';
            $filter['groupby']      = array('field' => 'angel_product.id');
            $filter['orderby']      = array('field' => 'angel_product.created_date', 'order' => 'DESC');
            $product                = $this->product_model->get_rows($filter);

            if (!empty($product)) {
                $data['product']    = $product;
                $return['html']     = $this->load->view('front/home/product_search', $data, true);
                $return['status']   = true;
                
            } else {
                $return['status'] = false;
            }
        } else {
            $return['status'] = false;
        }
         echo json_encode($return);
    }

    public function myaccount() {
        if ($this->is_logined) {
            $data               = array();
            $data['loginUser']  = $this->loginUser;
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $password   = $this->input->post('password');
                $this->form_validation->set_rules('firstname', 'First Name', 'trim|required', array('required' => 'Please enter first name'));
                $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required', array('required' => 'Please enter last name'));
                $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required', array('required' => 'Please enter Email', 'valid_email' => 'Please enter valid Email'));
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required', array('required' => 'Please enter phone number'));

                if ($password != "") {
                    $this->form_validation->set_rules('password', 'Password', 'trim|required', array('required' => 'Please enter password'));
                    $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]', array('required' => 'Please confirm your password', 'matches' => "Password is not match"));
                }

                if ($this->form_validation->run() == TRUE) {

                    $updateData['firstname']        = $this->input->post('firstname');
                    $updateData['lastname']         = $this->input->post('lastname');
                    $updateData['email']            = $this->input->post('email');
                    $updateData['phone']            = $this->input->post('phone');

                    if ($password != "") {
                        $updateData['password']          = encreptIt($password);
                        $updateData['visible_password']  = $password;
                    }

                    $this->user_model->update_table($updateData, array('id' => $this->loginUser['id']));

                    $aData['street_address']    = $this->input->post('address');
                    $aData['zipcode']           = $this->input->post('zipcode');

                    $aData['shipping_street_address']   = $this->input->post('shipping_address');
                    $aData['shipping_zipcode']          = $this->input->post('shipping_zipcode');

                    $lFilter['select']  = array('city', 'state');
                    $lFilter['where']   = array('zipcode' => $aData['zipcode']);
                    $lFilter['row']     = 1;
                    $location_result    = $this->location_model->get_rows($lFilter);
                    if (!empty($location_result)) {
                        $aData['city']  = $location_result->city;
                        $aData['state'] = $location_result->state;
                    }

                    $sFilter['select']  = array('city', 'state');
                    $sFilter['where']   = array('zipcode' => $aData['shipping_zipcode']);
                    $sFilter['row']     = 1;
                    $slocation_result    = $this->location_model->get_rows($sFilter);
                    if (!empty($slocation_result)) {
                        $aData['shipping_city']  = $slocation_result->city;
                        $aData['shipping_state'] = $slocation_result->state;
                    }

                    $a_filter['where']  = array('user_id' => $this->loginUser['id']);
                    $check_address      = $this->user_address_model->get_rows($a_filter, true);
                    if ($check_address > 0) {
                        $this->user_address_model->update_table($aData, array('user_id' => $this->loginUser['id']));
                    } else {
                        $aData['user_id'] = $this->loginUser['id'];
                        $this->user_address_model->insert($aData);
                    }

                    $return["status"]   = TRUE;
                    $return["message"]  = "Profile Updated Successfully";
                } else {
                    $return["status"]   = FALSE;
                    $return["message"]  = validation_errors();
                }
                echo json_encode($return);die;
            }

            $data['js_files'] = array("jquery.dataTables.min.js");
            $data['css_files'] = array("jquery.dataTables.min.css", "tabs.css");
            $this->template->view('front/user/myaccount', $data);
        } else {
            redirect(base_url());
        }
    }

    public function check_email_exists() {
        $email          = $this->input->post('email');
        $user_id        = $this->input->post('user_id');

        $filter         = array();
        if ($user_id != "") {
            $filter['where']['id !='] = decreptIt($user_id);
        }
        $filter['where']['email']   = $email;
        $result                     = $this->user_model->get_rows($filter,1);
        $return                     = TRUE;

        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }
    
    public function get_user_orders() {
        if ($this->is_logined) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {

                $search         = $this->input->post('search');
                $start          = intval($this->input->post("start"));
                $length         = intval($this->input->post("length"));
                $order          = $this->input->post("order");
                $columnArray    = array(
                                    '0' => 'angel_order.user_id',
                                    '1' => 'angel_order.created_date',
                                    '2' => 'angel_order.amount',
                                    '3' => 'total_item'
                                    );
                $filter['where']= array('user_id' => $this->loginUser['id']);
                $filterCount    = $totalCount = $this->order_model->get_rows($filter, true);

                if (isset($search) && ($search['value'] != '')) {
                    $searchString       = $search['value'];
                    $filter['like']     = array('field' => 'angel_order.amount', 'value' => $searchString);
                    $filter['or_like']  = array(
                        '0' => array('field' => 'angel_order.created_date', 'value' => $searchString)
                    );
                    $filterCount    = $this->order_model->get_rows($filter, true);
                }

                $filter['limit']    = array('limit' => $length, 'from' => $start);
                $orderField         = $columnArray[0];
                $orderSort          = 'DESC';
                if (!empty($order)) {
                    if (isset($order[0]['column']) && $order[0]['column'] != '') {
                        $orderField = $columnArray[$order[0]['column']];
                        $orderSort  = $order[0]['dir'];
                    }
                }
                $filter['select']   = array('angel_order.*', 'COUNT(oi.id) as total_item');
                $filter['join']     = array( 
                                        0 => array('table' => 'angel_order_items as oi', 'condition' => 'oi.order_id = angel_order.id', 'type' => 'LEFT')
                                    );
                $filter['orderby']  = array('field' => $orderField, 'order' => $orderSort);
                $filter['groupby']  = array('field' => 'angel_order.id');
                $query              = $this->order_model->get_rows($filter);
                $orders             = array();

                foreach ($query as $k => $row) {
                    $checked                    = ($row->status == 1) ? 'checked' : '';
                    $status                     = $row->status;

                    $row_data                   = array();
                    $row_data['no']             = $k + 1;
                    $row_data['date']           = date("m-d-Y", strtotime($row->created_date));
                    $row_data['total_item']     = $row->total_item;
                    $row_data['total_amount']   = '$' .$row->amount;
                    $row_data['action']         = '
                                                    <a href="'.  base_url() . 'order-details/' . encreptIt($row->id).'" class="btn btn-success">Order Detail</a> 
                                                ';

                    $orders[]                = $row_data;
                }
                $data['recordsTotal']       = $totalCount;
                $data['recordsFiltered']    = $filterCount;
                $data['data']               = $orders;

                echo json_encode($data);die();
            } else {
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }
    
    public function order_details($order_id = "") {
        if ($this->is_logined) {
            if ($order_id != "") {
                $filter['where']    = array('angel_order.id' => decreptIt($order_id));
                $filter['select']   = array('angel_order.*', 'oi.quantity','oi.product_id', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'oi.shipping_amount');
                $filter['join']     = array( 
                                        0 => array('table' => 'angel_order_items as oi', 'condition' => 'oi.order_id = angel_order.id', 'type' => 'LEFT'),
                                        1 => array('table' => 'angel_product as p', 'condition' => 'p.id = oi.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = oi.product_id', 'type' => 'left'),
                                        3 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left')
                                    );
                $filter['groupby']  = array('field' => 'oi.product_id');
                $data['order']      = $this->order_model->get_rows($filter);
                if (!empty($data['order'])) {

                    $data['item_count']     = count($data['order']);
                    $od_filter['select']    = array('angel_order.*', 'ua.street_address', 'ua.state', 'ua.city', 'ua.zipcode');
                    $od_filter['join']      = array( 
                                            0 => array('table' => 'angel_user_address as ua', 'condition' => 'ua.user_id = angel_order.user_id', 'type' => 'left')
                                        );
                    $od_filter['where']     = array('angel_order.id' => decreptIt($order_id));
                    $od_filter['groupby']   = array('field' => 'angel_order.id');
                    $od_filter['where']     = array('angel_order.id' => decreptIt($order_id));
                    $od_filter['row']       = 1;
                    $data['order_details']  = $this->order_model->get_rows($od_filter);
                    $this->template->view('front/user/order_details', $data);
                } else {
                    redirect(base_url().'myaccount');
                }
            } else{
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        } 
    }

    public function notFound() {
        redirect(base_url());
    }
}