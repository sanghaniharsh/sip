<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function daily() {
        $this->update_sitemap();
    }

    public function update_sitemap() {
        $finalUrl = array();

        $p_filter['where']  = array('is_active' => '1');
        $p_filter['select'] = array('slug');
        $products           = $this->product_model->get_rows($p_filter);
        if (! empty($products)) {
            foreach($products as $product) {
                $finalUrl[] = base_url().'product/'.$product->slug;
            }
        }

        $b_filter['where']  = array('is_active' => '1');
        $b_filter['select'] = array('slug');
        $blogs           = $this->blog_model->get_rows($b_filter);
        if (! empty($blogs)) {
            foreach($blogs as $blog) {
                $finalUrl[] = base_url().'blog/'.$blog->slug;
            }
        }


        $fileuploadPath     = getcwd()."/sitemap.xml";

        $data['final_url']  = $finalUrl;
        $xmlFileData        = $this->load->view('front/sitemap', $data, true);

//        header("Content-Type: text/xml");
        $dom                = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xmlFileData);
        $dom->save($fileuploadPath);
//        $this->view_xml();
    }

    private function view_xml(){
       header("Content-type: text/xml");
       $fileuploadPath = getcwd()."/sitemap.xml";
       $xml_file        = file_get_contents($fileuploadPath);
       echo $xml_file;
    }

    private function objectsIntoArray($arrObjData, $arrSkipIndices = array()) {
        $arrData = array();

        // if input is object, convert into array
        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }

        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = $this->objectsIntoArray($value, $arrSkipIndices); // recursive call
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }
}
?>