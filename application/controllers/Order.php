<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->is_logined   = $this->get_authorized_user();
        $this->loginUser    = $this->getLoginUser();
    }

    public function cart() {

        $data = array();
        $return = array();
        $this->removeSession(array('applied_promo_code')); 

        $pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'angel_cart.question');
        $pFilter['join']    = array(
                                0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
        );

        $pFilter['where']       = array('p.is_active' => '1');

        if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
            $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
        } else {
            $pFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
            $pFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        }
        $pFilter['groupby']     = array('field' => 'p.id');
        $data['cart']           = $this->cart_model->get_rows($pFilter);

        foreach($data['cart'] as $cart) {
            $cart->questions = array();
            $questions = array();
            
            if ($cart->question != '') {
                $questions = json_decode($cart->question, true);
            }

            if (! empty($questions)) {
                foreach($questions as $id => $answer) {
                    $q_filter['where']  = array('id' => $id);
                    $q_filter['select'] = array('question');
                    $q_filter['row']    = 1;
                    $q_result           = $this->question_model->get_rows($q_filter);
                    if (! empty($q_result)) {
                        $cart->questions[$id]['question']   = $q_result->question;
                        $cart->questions[$id]['answer']     = $answer;   
                    }
                }
            }
        }

        $data['cart_count'] = $this->cart_model->cart_count();
        $return['html'] = $this->load->view('front/order/cart_detail_ajax', $data, true);

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return['status'] = true;
            echo json_encode($return);
            die;
        } else {
            $data['html'] = $return['html'];
            $this->template->view('front/order/cart', $data);
        }
    }

    public function add_cart_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $return             = array();
            $question           = $this->input->post('question');
            $addon_product_id   = $this->input->post('addon_product_id');

            $is_empty = true;
            if (! empty($question)) {
                foreach($question as $row) {
                    if ($row != '') {
                        $is_empty = false;
                    }
                }
            }
            
            $data['question']   = ($is_empty == true) ? '' : json_encode($question);
            $data['quantity']   = $this->input->post('quantity');
            $product_id         = $this->input->post('product_id');
            $data['product_id'] = decreptIt($product_id);

            if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $data['user_id']    = $this->loginUser['id'];
            } else {
                $data['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            }

            if ($data['product_id'] > 0 && $data['quantity'] > 0) {

                $filter['where']    = array('product_id' => $data['product_id']);
                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $filter['where']['user_id'] = $this->loginUser['id'];
                } else {
                    $filter['where']['ip_address'] = $data['ip_address'];
                    $filter['where']['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $filter['row']      = 1;
                $result             = $this->cart_model->get_rows($filter);
                
                 if (!empty($addon_product_id)) {
                    $addon_array    = array();
                    $addon_data     = $data;
                    foreach($addon_product_id as $addon_row) {
                        $addon_data['product_id']   =  $addon_row;
                        $addon_array[]              = $addon_data;

                    }
                    $this->cart_model->insert_betch($addon_array);
                }

                if (empty($result)) {
                    $this->cart_model->insert($data);
                    $return['cart_count']   = $this->cart_model->cart_count();
                    $return['status']       = true;
                    $return['message']      = "Product added in your cart";
                } else {
                    $return['status']   = false;
                    $return['message']  = "Already Item exists in cart ";
                }
            } else {
                $return['message']  = "Something Wrong";
                $return['status']   = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_delete_item() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return = array();
            $id = $this->input->post('id');
            $id = decreptIt($id);

            if ($id > 0) {

                $filter['where']    = array('id' => $id);
                $filter['row']      = 1;
                $cart               = $this->cart_model->get_rows($filter);

                if (!empty($cart)) {
                    $this->cart_model->delete(array('id' => $id));
                    $count                  = $this->cart_model->cart_count();
                    $return['cart_count']   = '';
                    if ($count > 0) {
                       $return['cart_count']   = $this->cart_model->cart_count(); 
                    }
                    $return['message']      = "Cart item deleted successfully";
                    $return['status']       = true;
                } else {
                    $return['message']  = "Something went wrong";
                    $return['status']   = false;
                }
            } else {
                $return['message'] = "Something Wrong";
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_update_qty() {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $return = array();
            $id = $this->input->post('id');
            $id = decreptIt($id);
            $data['quantity'] = $this->input->post('quantity');
            $temp_quantity = $this->input->post('quantity');
            $temp_shipping_total = 0;
            $temp_sub_total = 0;


            if ($id > 0 && $data['quantity'] > 0) {
                
                $filter['where'] = array('id' => $id);
                //$filter['row'] = 1;
                $cart = $this->cart_model->get_rows($filter);
                //var_dump($cart);
                // echo json_encode($cart);
                if (!empty($cart)) {
                    $this->cart_model->update_table($data, array('id' => $id));
                	
                    //$cfilter['select']  = array('SUM(angel_cart.quantity * p.price) AS full_total','SUM(IF(p.is_free_shipping = "0"  and p.display_shipping_details = "0", p.shipping_price, 0)) AS total_shipping_price'); //cart total amount count
                    
                    $cfilter['select']  = array('*');
                    //$cfilter['select']  = array('angel_cart.quantity,p.price, p.shipping_price');


                    $cfilter['join']    = array(
                                            0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                                        );

                    //$cfilter['row']     = 1;

                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cresult = $this->cart_model->get_rows($cfilter);
                    //var_dump($cresult);
                    for($i = 0; $i < count($cresult); $i++){
                        /*$temp_id = $cresult[$i]->id;
                        if($id == $temp_id){
                            $temp_shipping_price = $cresult[$i]->shipping_price;
                            $temp_shipping_total += $temp_shipping_price * $temp_quantity;
                        } else{
                            $temp_shipping_price = $cresult[$i]->shipping_price;
                            //$temp_shipping_total += $temp_shipping_price * $cresult->
                        }*/
                        $temp_sub_total += $cresult[$i]->price * $cresult[$i]->quantity;
                        $temp_shipping_total += $cresult[$i]->shipping_price * $cresult[$i]->quantity;
                        //echo $temp_sub_total . " --- " . $temp_shipping_total . "<br />";
                    }

                    //echo $temp_id;
                    //var_dump($cresult);
                    
                    //echo json_encode($cresult);
                    if (!empty($cresult)) {
                        $tax = 0;
                        // if(isset($this->loginUser['shipping_state']) && $this->loginUser['shipping_state'] == "CT" ){
                            $tax = $temp_sub_total * $this->config->item('TAX_RATE') / 100;
                        // }
                        $return['tax']          = number_format($tax, 2, '.', ',');
                        $return['sub_total']    = number_format($temp_sub_total, 2, '.', ',');
                        $return['total_shipping_price']    = number_format($temp_shipping_total, 2, '.', ',');
                        
                        $return['full_total']   = number_format($temp_sub_total + $tax +  ($temp_shipping_total), 2);
                        
                        
                    }

                    $pfilter['select']  = array('SUM(angel_cart.quantity * p.price) AS total_price', 'SUM(IF(p.is_free_shipping = "0" and p.display_shipping_details = "0", p.shipping_price, 0)) AS total_shipping_price');
                    $pfilter['join']    = array(
                                            0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                                        );
                    $pfilter['row']     = 1;
                    $pfilter['where']   = array('angel_cart.id' => $id);
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $pfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $pfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $pfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
//                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
//                        $pfilter['where']['angel_cart.user_id']    = $this->loginUser['id'];
//                    } else {
//                        $pfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
//                    }

                    $presult = $this->cart_model->get_rows($pfilter);

                    //echo json_encode($presult);
                    if (!empty($presult)) {
                        $return['total_price'] = number_format($presult->total_price, 2);
                    }
                    
                    $return['message']  = "Quantity updated successfully";
                    $return['status']   = true;
                } else {
                    $return['message']  = "Quantity updated not successfully";
                    $return['status']   = false;
                }
            } else {
                $return['message']  = "Something Wrong";
                $return['status']   = false;
            }

            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function cart_count() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return  =  '';
            $count   = $this->cart_model->cart_count();

            if ($count != '') {
                $return['status']   = true;
                $return['count']    = $count;
            } else {
                $return['status'] = false;
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function check_promotional_code() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data = array();
            $return = array();
            $p_code = trim($this->input->post('promotional_code'));
            $device = trim($this->input->post('device'));

            if ($p_code != '') {
                /*
                $cfilter['select']  = array('angel_cart.*', 'p.price', 'SUM(angel_cart.quantity * p.price) AS full_total','SUM(IF(p.is_free_shipping = "0", IF(p.display_shipping_details = 1, p.shipping_price, 0), 0)) AS total_shipping_price', 'p.shipping_option');
                $cfilter['join']    = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                                    );
                $cfilter['row']     = 1;
                if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                } else {
                    $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $cresult                = $this->cart_model->get_rows($cfilter);
                p($cresult);
                */
                
                $pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'angel_cart.question');
                $pFilter['join']    = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
                );

                $pFilter['where']       = array('p.is_active' => '1');

                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                } else {
                    $pFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $pFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $pFilter['groupby']     = array('field' => 'p.id');
                $cresult           = $this->cart_model->get_rows($pFilter);

                if (!empty($cresult)) {
                    $ups_shipping_class = "hide";
                    $full_total         = 0;
                    $total_shipping     = 0;
                    foreach($cresult as $row) {
                        $full_total += $row->price * $row->quantity;
                        if (isset($row->shipping_option) && $row->shipping_option == 'ups_shipping') {
                            $ups_shipping_class= "";
                        }
                        if (isset($row->shipping_option) && $row->shipping_option == 'manual') {
                            $total_shipping += $row->shipping_price;
                        }
                    }

                    $pdata['total_amount']  = $full_total;
                    $pdata['promo_code']    = $p_code;
                    $promotional            = $this->promotional_model->check_promocode($pdata);

                    if ($promotional['status'] == true) {

                        $return['status']           = true;
                        $tax                        = 0;
                        if(isset($this->loginUser['shipping_state']) && $this->loginUser['shipping_state'] == "CT" ){
                            $tax = $full_total * $this->config->item('TAX_RATE') / 100;
                        }
                        $data['sub_total']          = number_format($full_total, 2, '.', ',');
                        $data['tax']                = number_format($tax, 2, '.', ',');
                        $data['total_amount']       = number_format($promotional['total_amount'] + $tax + $total_shipping, 2, '.', ',');
                        $data['shipping_price']     = number_format($total_shipping, 2, '.', ',');
                        $data['total_discount']     = number_format($promotional['total_discount'], 2, '.', ',');
                        $data['promotional_code']   = $p_code;
                        $data['ups_shipping_class'] = $ups_shipping_class;
                        if ($device == 'mobile') {
//                            $return['html'] = $this->load->view('front/order/mobile_discount_detail_ajax', $data, true);
                            $return['html'] = $this->load->view('front/order/discount_detail_ajax', $data, true);
                        } else {
                            $return['html'] = $this->load->view('front/order/discount_detail_ajax', $data, true);
                        }
                    } else {
                        $return['status']   = false;
                        $return['message']  = "Promo code not valid";
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = "Promo code not valid";
                }
            } else {
                $return['status']   = false;
                $return['message']  = "Please enter promo code";
            }
            echo json_encode($return);
            die;
        } else {
            not_found();
        }
    }

    public function checkout() {

        $data               = array();
        $pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'angel_cart.question');
        $pFilter['join']    = array(
                                0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
        );

        $pFilter['where']       = array('p.is_active' => '1');

        if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
            $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
        } else {
            $pFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
            $pFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        }
        $pFilter['groupby']     = array('field' => 'p.id');
        $result           = $this->cart_model->get_rows($pFilter);

        if (!empty($result)) {
            $amount     = 0;
            $discount   = 0;
            $tax        = 0;
            $data['cart_items'] = $result;
            $param          = array();
            $total_shipping = 0;
            $check_ups      = false;
            $weights        = 0; 
            foreach ($result as $value) {
                $amount += $value->quantity * $value->price;
                if ($value->shipping_option == 'ups_shipping') {
                    // $param[] = array(
                    //     'length' => $value->length,
                    //     'width' => $value->width,
                    //     'height' => $value->height,
                    //     'weight' => $value->weight
                    // );
                    $total_shipping += $value->shipping_price * $value->quantity;
                    // $check_ups  = true;
                } else if ($value->shipping_option == 'manual') {
                    $total_shipping += $value->shipping_price * $value->quantity;
                }
                if($value->weight != ""){
                    $weights += $value->weight;
                }
            }
            $zipcode        = $this->input->post('shipping_zipcoe');
            $address_type   = $this->input->post('address_type');
            if ($zipcode == '' && ($check_ups == true)) {
                $this->session->set_flashdata('error', 'Please enter shipping zipcode');
                redirect(base_url().'order/cart');
            } else {
                if (! empty($param)) {
                    $this->load->library('ups');
                    $shipping_return = $this->ups->check_shipping($zipcode, $address_type, $param);

                    if ($shipping_return['status'] == true) {
                        $total_shipping += $shipping_return['shipping_rate'];
                    } else {
                        $this->session->set_flashdata('error', $shipping_return['message']);
                        redirect(base_url().'order/cart');
                    }
                }
            }

            $data['weights']        = $weights;
            $data['tax']            = $tax;
            $data['sub_total']      = $amount;
            $promocode = '';
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $promocode = $this->input->post('promotional_code');
                $applied_promo_code['applied_promo_code']   = $promocode;               
                $this->setSession($applied_promo_code);
            } else {
                $promocode      =   $this->getSession('applied_promo_code');
            }
            if ($this->is_logined) {
                $od_filter['select']    = array('angel_users.*', 'ua.street_address', 'ua.state', 'ua.city', 'ua.zipcode','ua.shipping_street_address', 'ua.shipping_state', 'ua.shipping_city', 'ua.shipping_zipcode');
                $od_filter['join']      = array( 
                                            0 => array('table' => 'angel_user_address as ua', 'condition' => 'ua.user_id = angel_users.id', 'type' => 'left')
                                        );
                $od_filter['where']     = array('angel_users.id' => $this->loginUser['id']);
                $od_filter['groupby']   = array('field' => 'angel_users.id');
                $od_filter['row']       = 1;
                $data['user_details']   = $this->user_model->get_rows($od_filter);
                
                if ($zipcode == '') {
                    $zipcode = $data['user_details']->shipping_zipcode;
                }
            }

            $shFilter['select']  = array('city', 'state');
            $shFilter['where']   = array('zipcode' => $zipcode);
            $shFilter['row']     = 1;
            $shlocation_result   = $this->location_model->get_rows($shFilter);
            $tax = 0;
            // if(isset($shlocation_result->state) && $shlocation_result->state == "CT" ){
                $tax = $data['sub_total'] * $this->config->item('TAX_RATE') / 100;
            // }

            if ($promocode != '' && !empty($promocode) ) {
                $pData['promo_code']    = $promocode;
                $pData['total_amount']  = $amount;
                $promo_data             = $this->promotional_model->check_promocode($pData);

                if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                    $data['promo_code'] = $promocode;
                    $amount             = $promo_data['total_amount'];
                    $discount           = $promo_data['total_discount'];
                }
            }
//            $data['total_amount']   = number_format($amount, 2, '.', ',');
            $data['total_tax']          = number_format($tax, 2, '.', ',');
            $data['total_amount']       = $amount + $tax;
            $data['total_discount']     = number_format($discount, 2, '.', ',');
            $data['shipping_total']     = $total_shipping;

            if ($zipcode != '') {
                $lFilter['select']  = array('city', 'state');
                $lFilter['where']   = array('zipcode' => $zipcode);
                $lFilter['row']     = 1;
                $location_result    = $this->location_model->get_rows($lFilter);

                if (! empty($location_result)) {
                    $data['address_type']       = $address_type;
                    $data['shipping_zipcode']   = $zipcode;
                    $data['shipping_city_state']= $location_result->city.', '.$location_result->state;
                }
            }
            // var_dump($data);
            $data['order_summary_view'] = $this->load->view('front/order/order_summary_view', $data, true);

            $this->template->view('front/order/checkout', $data);
        } else {
            not_found();
        }
    }

    public function commit() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $validate = false;

            if (!$this->is_logined) {
                $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
                $this->form_validation->set_rules('address', 'Phone', 'trim|required');
                $this->form_validation->set_rules('zipcode', 'Phone', 'trim|required|callback_validate_zipcode');
                $this->form_validation->set_message('validate_zipcode', 'Zipcode is not valid');
                
                $validate = $this->form_validation->run();
            } else {
                $validate = TRUE;
            }

            if ( $validate == TRUE) {
                $product                = array();
                $promocode              = $this->input->post('promocode');
                $ip_address             = $_SERVER['REMOTE_ADDR'];

                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $cFilter['user_id']     = $this->loginUser['id'];
                } else {
                    $cFilter['ip_address']  = $ip_address;
                }
                $cart_details   = $this->cart_model->getItems($cFilter);
                if (!empty($cart_details)) {
                    $amount             = 0;
                    $discount           = 0;
                    $sub_total          = 0;
                    $tax                = 0;
                    $shipping_total     = 0;
                    foreach ($cart_details as $value) {
                        if ($value->is_free_shipping == '0' && $value->shipping_price > 0 ){
                            $shipping_total = $shipping_total + $value->shipping_price;
                        }
                        $amount                                     += $value->quantity * $value->price;
                        $product[$value->product_id]                = $value->quantity;
                    }
                    $sub_total = $amount;
                    if ($promocode != '') {
                        $pData['promo_code']    = $promocode;
                        $pData['total_amount']  = $amount;
                        $promo_data             = $this->promotional_model->check_promocode($pData);

                        if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                            $amount     = $promo_data['total_amount'];
                            $discount   = $promo_data['total_discount'];
                        }
                    }
                    $amount = $amount + $shipping_total;
                    $result = array();
                    if (!$this->is_logined) {
                        $email      = $this->input->post('email');
                        $user_id    = 0;

                        $uFilter['select']  = array('id');
                        $uFilter['where']   = array('email' => $email);
                        $uFilter['row']     = 1;
                        $result             = $this->user_model->get_rows($uFilter);
                    }
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $user_id    = $this->loginUser['id'];
                    } else if (!empty($result) && isset($result->id) && $result->id > 0) {
                        $user_id    = $result->id;
                    } else {
                        $data['email']              = $email;
                        $data['firstname']          = $this->input->post('first_name');
                        $data['lastname']           = $this->input->post('last_name');
                        $data['phone']              = $this->input->post('phone');
                        $password                   = trim($this->input->post('password'));
                        $data['password']           = encreptIt($password);
                        $data['visible_password']   = $password;
                        $data['is_active']          = '1';
                        $data['created_date']       = date('Y-m-d H:i:s');
                        $user_id                    = $this->user_model->insert($data);

//                        $zipcode            = $this->input->post('zipcode');
//                        $lFilter['select']  = array('city', 'state');
//                        $lFilter['where']   = array('zipcode' => $zipcode);
//                        $lFilter['row']     = 1;
//                        $location_result    = $this->location_model->get_rows($lFilter);
//
//                        $shipping_zipcode   = $this->input->post('shipping_zipcode');
//                        $shFilter['select']  = array('city', 'state');
//                        $shFilter['where']   = array('zipcode' => $shipping_zipcode);
//                        $shFilter['row']     = 1;
//                        $shlocation_result   = $this->location_model->get_rows($shFilter);

//                        if (!empty($location_result) && $user_id > 0) {
//                            $user_data['user_id']           = $user_id;
//                            $user_data['city']              = $location_result->city;
//                            $user_data['state']             = $location_result->state;
//                            $user_data['street_address']    = $this->input->post('address');
//                            $user_data['zipcode']           = $this->input->post('zipcode');
//
//                            $user_data['shipping_city']              = $shlocation_result->city;
//                            $user_data['shipping_state']             = $shlocation_result->state;
//                            $user_data['shipping_street_address']    = $this->input->post('shipping_address');
//                            $user_data['shipping_zipcode']           = $this->input->post('shipping_zipcode');
//
//                            $aFilter['where']   = array('user_id' => $user_id);
//                            $aFilter['row']     = 1;
//                            $userDetails        = $this->user_address_model->get_rows($aFilter);
//
//                            if (!empty($userDetails)) {
//                                $this->user_address_model->update_table($user_data, array('id' => $userDetails->id));
//                            } else {
//                                $this->user_address_model->insert($user_data);
//                            }
//                        } else {
//                            $this->session->set_flashdata('error', 'Please fill user details');
//                            redirect(base_url() . 'order/checkout');
//                        }
                    }
                    if($user_id > 0) {
                        $zipcode            = $this->input->post('zipcode');
                        $shipping_zipcode   = $this->input->post('shipping_zipcode');
                        if ($zipcode != "" && $shipping_zipcode != "") {
                            $lFilter['select']  = array('city', 'state');
                            $lFilter['where']   = array('zipcode' => $zipcode);
                            $lFilter['row']     = 1;
                            $location_result    = $this->location_model->get_rows($lFilter);

                            $shFilter['select']  = array('city', 'state');
                            $shFilter['where']   = array('zipcode' => $shipping_zipcode);
                            $shFilter['row']     = 1;
                            $shlocation_result   = $this->location_model->get_rows($shFilter);
                            if (!empty($location_result) && !empty($shlocation_result)) {
                                $user_data['user_id']           = $user_id;
                                $user_data['city']              = $location_result->city;
                                $user_data['state']             = $location_result->state;
                                $user_data['street_address']    = $this->input->post('address');
                                $user_data['zipcode']           = $this->input->post('zipcode');

                                $user_data['shipping_city']              = $shlocation_result->city;
                                $user_data['shipping_state']             = $shlocation_result->state;
                                $user_data['shipping_street_address']    = $this->input->post('shipping_address');
                                $user_data['shipping_zipcode']           = $this->input->post('shipping_zipcode');

                                $aFilter['where']   = array('user_id' => $user_id);
                                $aFilter['row']     = 1;
                                $userDetails        = $this->user_address_model->get_rows($aFilter);

                                if (!empty($userDetails)) {
                                    $this->user_address_model->update_table($user_data, array('id' => $userDetails->id));
                                } else {
                                    $this->user_address_model->insert($user_data);
                                }
                            } else {
                                $this->session->set_flashdata('error', 'Please fill user details');
                                redirect(base_url() . 'order/checkout');
                            }
                        }
                    }

                    if ($user_id > 0 && !empty($product)) {
                        if (!$this->is_logined) {
                            $user_session['session_id'] = encreptIt($user_id);
                            $this->set_authorized_user($user_session);
                            $this->loginUser    = $this->getLoginUser();
                        }
                        $tax                        = 0;
                        if(isset($this->loginUser['shipping_state']) && $this->loginUser['shipping_state'] == "CT" ){
                            $tax = $sub_total * $this->config->item('TAX_RATE') / 100;
                        }
                        $amount = $amount + $tax;
                        $products_data = array(
                            'product_data'  => $product,
                            'amount'        => $amount,
                            'discount'      => $discount,
                            'user_id'       => $user_id,
                            'tax'           => $tax,
                            'promo_code'    => $promocode
                        );
                        $custom = base64_encode(json_encode($products_data));
                        if ($amount > 0) {
                            $returnURL      = base_url() . 'order/payment_success'; //payment success url
                            $cancelURL      = base_url() . 'order/payment_cancel';

                            $paypalpayment  = round($amount, 2);
                            $logo           = front_asset_url() . 'images/logo-main.png';

                            $post_fileds['brand_name']      = 'Resturant Equipment';
                            $post_fileds['brand_logo']      = $logo;

                            $post_fileds['amount']          = $paypalpayment;
                            $post_fileds['custom']          = $custom;
                            $post_fileds['inv_number']      = 'SIP_inv_' . time();
                            $post_fileds['currencyCode']    = "USD";
                            $post_fileds['item'] = array(
                                array(
                                    'name'  => 'Resturant Equipment',
                                    'desc'  => 'Resturant Equipment',
                                    'qty'   => 1,
                                    'amount'=> $paypalpayment
                                )
                            );
                            $post_fileds['payment_desc']    = 'Resturant Equipment';
                            $post_fileds['cancel_url']      = $cancelURL;
                            $post_fileds['return_url']      = $returnURL;
                            $return = $this->paypal_lib->SetExpressCheckout($post_fileds);
                            if (!empty($return)) {
                                $response = $this->paypal_lib->procced_paypal($return);
                                if (!empty($response)) {
                                    $this->session->set_flashdata('error', 'Payment not done');
                                    redirect(base_url());
                                }
                            } else {
                                $this->session->set_flashdata('error', 'Payment not done');
                                redirect(base_url());
                            }
                        } else {
                            $order_data['CUSTOM']       = $custom;
                            $order_data['TRANSACTIONID']= time();
                            $order_data['AMT']          = 0;
                            $order_data['ACK']          = 'Success';
                            $this->set_order_details($order_data);
                            die;
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong');
                        redirect(base_url() . 'order/checkout');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please try again latter');
                    redirect(base_url() . 'order/checkout');
                }
            } else {
                $this->session->set_flashdata('error', "All fields are required");
                redirect(base_url() . 'order/checkout');
            }
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong');
            redirect(base_url() . 'order/checkout');
        }
    }

    public function validate() {
        $email = $this->input->post('email');
        $return = FALSE;

        if ($email != '') {
            $filter['where']    = array('email' => $email);
            $result             = $this->user_model->get_rows($filter);

            if (!empty($result)) {
                $return = FALSE;
            } else {
                $return = TRUE;
            }
        }
        echo json_encode($return);
        die;
    }

    public function payment_success() {

        if (!empty($_GET) && $_GET['PayerID'] != '' && $_GET['token'] != '') {
            ini_set('max_execution_time', '0');
            $payer_id       = $_GET['PayerID'];
            $token          = $_GET['token'];
            $requestParams  = array('TOKEN' => $_GET['token']);
            $response       = $this->paypal_lib->GetExpressCheckoutDetails($requestParams);
            if (!empty($response) && isset($response['CUSTOM']) && ($response['ACK'] == 'Success')) {
                $payerId        = $response["PAYERID"]; //Payer id returned by paypal
                $requestParams1 = array(
                    "TOKEN"     => $response['TOKEN'],
                    "PAYERID"   => $payerId,
                    "PAYMENTREQUEST_0_AMT"          => $response['AMT'], //Payment amount. This value should be sum of of item values, if there are more items in order
                    "PAYMENTREQUEST_0_CURRENCYCODE" => "USD", //Payment currency
                    "PAYMENTREQUEST_0_ITEMAMT"      => $response['AMT']//Item amount
                );

                $transactionResponse = $this->paypal_lib->DoExpressCheckoutPayment($requestParams1); //Execute transaction
                if (!empty($transactionResponse)) {
                    if (isset($transactionResponse['ACK']) && $transactionResponse['ACK'] == 'Success') {
                        $order_data['CUSTOM']       = $response['CUSTOM'];
                        $order_data['TRANSACTIONID']= $transactionResponse['PAYMENTINFO_0_TRANSACTIONID'];
                        $order_data['AMT']          = $response['AMT'];
                        $order_data['ACK']          = $transactionResponse['ACK'];
                        $this->set_order_details($order_data);
                        die;
                    } else {
                        $this->session->set_flashdata('error', 'Payment Not completed. Something went wrong.');
                        redirect(base_url() . 'order/cart');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Payment Not completed. Something went wrong.');
                    redirect(base_url() . 'order/cart');
                }
            } else {
                $this->session->set_flashdata('error', 'Property not found Try Again later!');
                redirect(base_url() . 'order/cart');
            }
        } else {
            $this->session->set_flashdata('error', 'Payment not completed, Please try again later');
            redirect(base_url() . 'order/cart');
        }
    }

    private function set_order_details($order_data = array()) {

        if (!empty($order_data) && isset($order_data['CUSTOM']) && isset($order_data['TRANSACTIONID']) && isset($order_data['ACK']) && isset($order_data['AMT']) ) {

            $products   = json_decode(base64_decode($order_data['CUSTOM']));

            if (isset($products->product_data)  &&  !empty($products->product_data)  &&  isset($products->user_id)  &&  $products->user_id > 0 ) {

                $promocode              = $products->promo_code;
                $data['user_id']        = $products->user_id;
                $data['transaction_id'] = $order_data['TRANSACTIONID']; 
                $data['status']         = $order_data['ACK'];
                $data['amount']         = $order_data['AMT'];   

                if (isset($data['status']) && $data['status'] == 'Success') {
                    $data['order_status']   = '0';
                }

                $data['discount']           = $products->discount;
                $data['tax']                = $products->tax;
                if (isset($products->promo_code) && $products->promo_code != '') {
                    $data['promo_code']     = $products->promo_code;
                }
                $data['created_date']       = date('Y-m-d H:i:s');
                $order_id                   = $this->order_model->insert($data);
                if ($order_id > 0) {

                    $order_items    = array();
                    $product_ids    = array();
                    $product_details= array();
                    foreach ($products->product_data as $key => $row) {
                        $order_items[$key]                              = array('order_id' => $order_id, 'product_id' => $key , 'quantity' => $row, 'shipping_amount' => 0);
                        $product_ids[]                                  = $key;
                        $product_details[$key]['id']                    = $key;
                        $product_details[$key]['quantity']              = $row;
                        
                    }
                    if (!empty($order_items)) {
                        $iFilter['select']      = array('id', 'name', 'price','modal_number','is_free_shipping','shipping_price','display_shipping_details');
                        $iFilter['where_in']    = array(array('field' => 'id', 'value' => $product_ids));
                        $product_data           = $this->product_model->get_rows($iFilter);
                        if (!empty($product_data)) {
                            foreach ($product_data as $prow) {
                                $shipping_amount                            = 0;
                                $product_details[$prow->id]['modal_number'] = $prow->modal_number;
                                $product_details[$prow->id]['name']         = $prow->name;
                                $product_details[$prow->id]['price']        = $prow->price;
                                if ($prow->is_free_shipping == '0' && $prow->display_shipping_details == '0' && $prow->shipping_price > 0 ){
                                    $shipping_amount = $prow->shipping_price;
                                }
                                $product_details[$prow->id]['shipping_amount']  = $shipping_amount;
                                $order_items[$prow->id]['shipping_amount']      = $shipping_amount;
                            }
                        }
                        $this->order_items_model->insert_betch($order_items);
                    }
                    if ($promocode != '') {
                        $pFilter['where']   = array('promo_code' => $promocode);
                        $pFilter['row']     = 1;
                        $result             = $this->promotional_model->get_rows($pFilter);

                        if (!empty($result)) {
                            $promoData['promo_count'] = $result->promo_count + 1;
                            $this->promotional_model->update_table($promoData, array('id' => $result->id));
                        }
                    }

                    $Ufilter['select']  = array('angel_users.email','angel_users.firstname', 'CONCAT(angel_users.firstname , " ", angel_users.lastname) as username', 'ua.street_address', 'ua.state', 'ua.city', 'ua.zipcode');
                    $Ufilter['where']   = array('angel_users.id' => $data['user_id']);
                    $Ufilter['join']    = array(
                                            0 => array('table' => 'angel_user_address as ua', 'condition' => 'ua.user_id = angel_users.id', 'type' => 'left')
                                        );
                    $Ufilter['row']     = 1;
                    $Uresult            = $this->user_model->get_rows($Ufilter);
                    $Uname              = isset($Uresult->username) ? $Uresult->username : '';
                    if (!empty($Uresult)) {
                        $viewData['user_details']   = $Uresult;
                        $viewData['product_details']= $product_details;
                        $viewData['order_id']       = display_order($order_id);
                        $viewData['order_details']  = $data;
                        $mailData['message']        = $this->load->view('front/email/order_payment', $viewData, true);
                        $mailData['subject']        = "Thank you for your order - Restaurant Equipment Paradise";
                        $mailData['email']          = $Uresult->email;
                        $this->sentMail($mailData);
                    }
//                    $this->load->library('m_pdf');
//                    $memberPDF['client_name']       = $Uname;
//                    $memberPDF['business_name']     = "SIP";
//                    $memberPDF['title']             = "SIP";
//                    $memberPDF['amount']            = '$' . number_format($data['amount'], 2);
//                    $memberPDF['effective_date']    = date('m-d-Y');
//                    $memberHtml                     = $this->load->view('front/sip_license_agreement', $memberPDF, true);
//                    $filePath                       = getcwd() . '/uploads/aggrement-pdf/';
//
//                    if (!is_dir($filePath)) {
//                        mkdir($filePath, 0777, true);
//                    }
//
//                    $pdfFilePath    = $filePath . slug($Uname) . "-aggrement-details.pdf";
//                    $pdf            = $this->m_pdf->load();
//                    $pdf->WriteHTML($memberHtml, 2);
//                    $pdf->Output($pdfFilePath, "F");

                    $userData['product_details']= $product_details;
                    $userData['order_details']  = $data;
                    $userData['business_name']  = "SIP";
                    $mailData['message']        = $this->load->view('front/email/license_agreement', $userData, true);
                    $mailData['subject']        = "New Product Purchased";
//                    $mailData['attachement']    = array($pdfFilePath);
                    $mailData['email']          =  array('kswerdlick@saveinparadise.com','rsinger@saveinparadise.com','sgenova@saveinparadise.com');;
                    $this->sentMail($mailData);
//                    if (file_exists($pdfFilePath)) {
//                        unlink($pdfFilePath);
//                    }

                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $where = array('user_id' => $this->loginUser['id']);
                        $this->cart_model->delete($where);

                        $where = array('ip_address' => $_SERVER['REMOTE_ADDR']);
                        $this->cart_model->delete($where);
                    } else {
                        $where = array('ip_address' => $_SERVER['REMOTE_ADDR']);
                        $this->cart_model->delete($where);
                    }


                    $success_payment['thankyou'] = encreptIt($products->user_id);
                    $this->setSession($success_payment);

                    redirect(base_url() . 'order/thankyou'); 

                } else {
                    $this->session->set_flashdata('error', 'Somthing went wrong123');
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('error', 'Somthing went wrong123');
                redirect(base_url());
            } 
        } else {
            $this->session->set_flashdata('error', 'Somthing went wrong456');
            redirect(base_url());
        }
    }

    public function validate_zipcode() {
        $zipcode = $this->input->post('zipcode');
        $return = TRUE;

        if ($zipcode != '') {
            $filter['where'] = array('zipcode' => $zipcode);
            $result = $this->location_model->get_rows($filter);

            if (empty($result)) {
                $return = FALSE;
            } else {
                $return = TRUE;
            }
        }
        return $return;
    }

    public function check_zipcode() {
        $zipcode = $this->input->post('zipcode');
        $return = true;

        if ($zipcode != '') {
            $filter['where'] = array('zipcode' => $zipcode);
            $result = $this->location_model->get_rows($filter);

            if (empty($result)) {
                $return = false;
            } else {
                $return = true;
            }
        }
        echo json_encode($return);
        die;
    }

    public function payment_cancel() {
        $this->session->set_flashdata('error', 'Payment cancelled');
        redirect("/home");
    }

    public function thankyou() {
        $data = array();
        $thankyou_user = $this->getSession('thankyou');
        $this->removeSession(array('thankyou'));
        if ($thankyou_user != '' && !is_array($thankyou_user)) {
            $user_id = decreptIt($thankyou_user);
            $user_data = $this->user_model->get_detail($user_id);
            if (!empty($user_data)) {
                $data['user_name'] = $user_data->username;
                $this->template->view('front/order/thankyou', $data);
            } else {
                $this->session->set_flashdata('error', 'Somthing went wrong');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Metthod');
            redirect(base_url());
        }
    }

    public function preview_agreement() {

        $data = array();
        $outputv = $this->load->view('front/sip_license_agreement', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = getcwd() . "/uploads/agrement-details.pdf";
        if (file_exists($pdfFilePath)) {
            unlink($pdfFilePath);
        }
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($outputv, 2);
        $pdf->SetTitle('SIP LICENSE AGREEMENT | SIP');
        ob_end_clean();
        $pdf->Output($pdfFilePath, "I");
    }
    
    public function check_email_exists() {
        $email          = $this->input->post('email');

        $filter                     = array();
        $filter['where']['email']   = $email;
        $result                     = $this->user_model->get_rows($filter,1);
        $return                     = TRUE;

        if ($result > 0) {
            $return = FALSE;
        }
        echo json_encode($return);
    }

    public function check_state_tax() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data       = array();
            $return         = array();
            $zipcode        = trim($this->input->post('zipcode'));
            $address_type   = trim($this->input->post('address_type'));

            if ($zipcode != '') {
                $pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'angel_cart.question');
                $pFilter['join']    = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
                );

                //$pFilter['where']       = array('p.is_active' => '1');

                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                } else {
                    $pFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $pFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $pFilter['groupby'] = array('field' => 'p.id');
                $result = $this->cart_model->get_rows($pFilter);
                if (!empty($result)) {
                    $amount             = 0;
                    $discount           = 0;
                    $tax                = 0;
                    $data['cart_items'] = $result;
                    $param              = array();
                    $shipping_total     = 0;
                    foreach ($result as $value) {
                        $amount += $value->quantity * $value->price;
                        if ($value->shipping_option == 'ups_shipping') {
                            // $param[] = array(
                            //     'length' => $value->length,
                            //     'width' => $value->width,
                            //     'height' => $value->height,
                            //     'weight' => $value->weight
                            // );
                            $shipping_total += $value->shipping_price * $value->quantity;
                        } else if ($value->shipping_option == 'manual') {
                            $shipping_total += $value->shipping_price * $value->quantity;
                        }
                    }

                    if (! empty($param)) {
                        $this->load->library('ups');
                        $shipping_return = $this->ups->check_shipping($zipcode, $address_type, $param);

                        if ($shipping_return['status'] == true) {
                            $shipping_total     += $shipping_return['shipping_rate'];
                        }
                    }

                    $shFilter['select']  = array('city', 'state');
                    $shFilter['where']   = array('zipcode' => $zipcode);
                    $shFilter['row']     = 1;
                    $shlocation_result   = $this->location_model->get_rows($shFilter);
                    if(isset($shlocation_result->state) && $shlocation_result->state == "CT" ){
                        $tax = $amount * $this->config->item('TAX_RATE') / 100;
                    }

                    $data['tax']                = $tax;
                    $data['sub_total']          = $amount;
                    $promocode                  = $this->getSession('applied_promo_code');
                    if ($promocode != '' && !empty($promocode)) {
                        $pData['promo_code']    = $promocode;
                        $pData['total_amount']  = $amount;
                        $promo_data             = $this->promotional_model->check_promocode($pData);

                        if (isset($promo_data['total_amount']) && isset($promo_data['total_discount'])) {
                            $data['promo_code'] = $promocode;
                            $amount             = $promo_data['total_amount'];
                            $discount           = $promo_data['total_discount'];
                        }
                    }

                    $data['total_tax']              = number_format($tax, 2, '.', ',');
                    $data['total_amount']           = $amount + $tax;
                    $data['total_discount']         = number_format($discount, 2, '.', ',');
                    $data['shipping_total']         = $shipping_total;

                    $return['order_summary_view']   = $this->load->view('front/order/order_summary_view', $data, true);

                    $return['status']               = true;

                } else {
                    $return['status']   = false;
                }
            } else {
                $return['status']   = false;
            }
            echo json_encode($return); die;
        } else {
            not_found();
        }
    }

    public function shipping_price(){

            
        
         if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return     = array();
            $zipcode    = trim($this->input->post('zipcode'));
            $isResidential  = trim($this->input->post('isResidential'));
            $city       = trim($this->input->post('city'));
            $state      = trim($this->input->post('state'));

            // echo "<pre>";
            // print_r($_POST);



            // echo "city : " . $type;

            if ($zipcode != '') {

                $cfilter['select']  = array('*');
                    $cfilter['join']    = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                    );
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cresult = $this->cart_model->get_rows($cfilter);
                    $temp_shipping_charges = 0;
                    $sub_total_shipping = 0;
                    $sub_total_amount = 0;


                    $temp_shipping_charges = 0;
                    for($i=0;$i < count($cresult); $i++){

                        if($cresult[$i]->shipping_option == "ups_shipping"){
                            //take length, width, height and weight
                            
                            $temp_id = $cresult[$i]->id;
                            $temp_length = $cresult[$i]->length;
                            $temp_width = $cresult[$i]->width;
                            $temp_height = $cresult[$i]->height;
                            $temp_weight = $cresult[$i]->weight;                                                            
                            $temp_qty = $cresult[$i]->quantity;


                             $this->load->library('Fedex_tracking_lib');

                             if ($temp_weight > 150) {
                                 # code...
                                $serviceType = 'FEDEX_3_DAY_FREIGHT';

                             }else{
                                $serviceType = 'FEDEX_GROUND';
                                 $isResidential = false;
                             }

                            $temp_shipping_charges = $this->fedex_tracking_lib->rates($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode,$serviceType,$isResidential); 

                             // echo "<pre>";
                             // echo "rates";
                             // print_r($temp_shipping_charges);

                            // if( $temp_weight > 150){
                            // $temp_shipping_charges = $this->fedex_calculator($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode, $city, $state);

                            // // echo "<pre>";
                            // // print_r($temp_shipping_charges);
                            //     // echo "------------------";
                            // }else{

                            //     $temp_shipping_charges = $this->rates($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode);

                            //     // echo "<pre>";
                            //     // echo "rates";
                            //     // print_r($temp_shipping_charges);
                            // }

                            //echo "Calculated price : " . $temp_shipping_charges;
                            $temp_data['shipping_price'] = $temp_shipping_charges;


                            // echo "<pre>";
                            // print_r($temp_data['shipping_price']);


                            $this->product_model->update_table($temp_data, array('id' => $temp_id));
                            $sub_total_shipping += $temp_shipping_charges;
                            $sub_total_amount += $cresult[$i]->price * $temp_qty;
                        }
                    }



                    $tax = $sub_total_amount * $this->config->item('TAX_RATE') / 100;
                    $return['tax'] = number_format($tax, 2);
                    $return['subtotal'] = $sub_total_shipping;
                    $return['status'] = true;
                    $return['message'] = "Calculation done";
                    $return['shipping'] = $temp_shipping_charges;
                    echo json_encode($return);
                    die();
        }
    }
    }

    public function shipping_price_old(){


         if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return     = array();
            $zipcode    = trim($this->input->post('zipcode'));
            $type       = trim($this->input->post('address_type'));
            $city       = trim($this->input->post('city'));
            $state      = trim($this->input->post('state'));
            // echo "city : " . $city;
            if ($zipcode != '') {

                $cfilter['select']  = array('*');
                    $cfilter['join']    = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                    );
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cresult = $this->cart_model->get_rows($cfilter);

                    // echo "<pre>";
                    // print_r($cresult);
                    // echo "-----d---------d-";

                    $isManyProduct = count($cresult);
                    if ($isManyProduct > 1) {
                         $totalProductWeight = $this->productWeight($cresult);
                    }else{
                        $totalProductWeight = 1;
                    }
                   


                    $temp_shipping_charges = 0;
                    $sub_total_shipping = 0;
                    $sub_total_amount = 0;

                    // for($i=0;$i < count($cresult); $i++){

                    for($i=0;$i < 1; $i++){

                        if($cresult[$i]->shipping_option == "ups_shipping"){
                            //take length, width, height and weight
                            
                            $temp_id = $cresult[$i]->id;
                            $temp_length = $cresult[$i]->length;
                            $temp_width = $cresult[$i]->width;
                            $temp_height = $cresult[$i]->height;
                            $temp_weight = $cresult[$i]->weight;                                                            
                            $temp_qty = $cresult[$i]->quantity;

                           if ($totalProductWeight>150) {
                                $temp_weight = $totalProductWeight;
                                # code...
                            } 





                            if( $temp_weight > 150){

                            // $temp_shipping_charges = $this->fedex_calculator($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode, $city, $state);
                            $temp_shipping_charges = $this->fedex_calculator($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode, $city, $state);

                            // echo "<pre>";
                            // print_r($temp_shipping_charges);
                            // echo "------------------";
                            }else{

                                $temp_shipping_charges = $this->rates($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode);

                                // echo "<pre>";
                                // echo "rates";
                                // print_r($temp_shipping_charges);
                            }


                            //echo "Calculated price : " . $temp_shipping_charges;
                            $temp_data['shipping_price'] = $temp_shipping_charges;


                            $this->product_model->update_table($temp_data, array('id' => $temp_id));
                            $sub_total_shipping += $temp_shipping_charges;
                            $sub_total_amount += $cresult[$i]->price * $temp_qty;
                        }
                    }



                    $tax = $sub_total_amount * $this->config->item('TAX_RATE') / 100;
                    $return['tax'] = number_format($tax, 2);
                    $return['subtotal'] = $sub_total_shipping;
                    $return['status'] = true;
                    $return['message'] = "Calculation done";
                    $return['shipping'] = $temp_shipping_charges;
                    echo json_encode($return);
                    die();
        }
    }
    }


    public function productWeight($pram){

        if (is_array($pram)) {

            $sumWeight = 0;

            for ($i=0; $i <count($pram) ; $i++) { 

                if (is_numeric($pram[$i]->weight)) {

                      $sumWeight += $pram[$i]->weight;
                }

            }

            return $sumWeight;
        }

    }

    public function shipping_price__________(){
        
         if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return     = array();
            $zipcode    = trim($this->input->post('zipcode'));
            $type       = trim($this->input->post('address_type'));
            $city       = trim($this->input->post('city'));
            $state      = trim($this->input->post('state'));
            // echo "city : " . $city;
            if ($zipcode != '') {

                $cfilter['select']  = array('*');
                    $cfilter['join']    = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                    );
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cresult = $this->cart_model->get_rows($cfilter);
                    $temp_shipping_charges = 0;
                    $sub_total_shipping = 0;
                    $sub_total_amount = 0;
                    for($i=0;$i < count($cresult); $i++){

                        if($cresult[$i]->shipping_option == "ups_shipping"){
                            //take length, width, height and weight
                            
                            $temp_id = $cresult[$i]->id;
                            $temp_length = $cresult[$i]->length;
                            $temp_width = $cresult[$i]->width;
                            $temp_height = $cresult[$i]->height;
                            $temp_weight = $cresult[$i]->weight;															
                            $temp_qty = $cresult[$i]->quantity;

                            if( $temp_weight > 150){
                            $temp_shipping_charges = $this->fedex_calculator($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode, $city, $state);

                            // echo "<pre>";
                            // print_r($temp_shipping_charges);
                            // echo "------------------";
                        }else{

                            $temp_shipping_charges = $this->rates($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode);

                            // echo "<pre>";
                            // echo "rates";
                            // print_r($temp_shipping_charges);
                        }
                            //echo "Calculated price : " . $temp_shipping_charges;
                            $temp_data['shipping_price'] = $temp_shipping_charges;


                            $this->product_model->update_table($temp_data, array('id' => $temp_id));
                            $sub_total_shipping += $temp_shipping_charges;
                            $sub_total_amount += $cresult[$i]->price * $temp_qty;
                        }
                    }
                    $tax = $sub_total_amount * $this->config->item('TAX_RATE') / 100;
                    $return['tax'] = number_format($tax, 2);
                    $return['subtotal'] = $sub_total_shipping;
                    $return['status'] = true;
                    $return['message'] = "Calculation done";
                    $return['shipping'] = $temp_shipping_charges;
                    echo json_encode($return);
                    die();
        }
    }
    }

    public function check_shipping_price() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $return     = array();
            $shipping_price    = trim($this->input->post('shipping_price'));
            $type       = trim($this->input->post('address_type'));
            // $city       = trim($this->input->post('city'));
            // $state      = trim($this->input->post('state'));
            //echo "city : " . $city;
            if ($shipping_price != '') {

                $cfilter['select']  = array('*');
                    $cfilter['join']    = array(
                        0 => array('table' => 'angel_product as p', 'condition' => 'angel_cart.product_id = p.id', 'type' => 'left')
                    );
                    if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                        $cfilter['or_where'] = array('angel_cart.user_id' => $this->loginUser['id'], 'angel_cart.ip_address' => $_SERVER['REMOTE_ADDR']);
                    } else {
                        $cfilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $cfilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    }
                    $cresult = $this->cart_model->get_rows($cfilter);
                    $temp_shipping_charges = str_replace(',', '', $shipping_price);
                    $sub_total_shipping = 0;
                    $sub_total_amount = 0;
                    for($i=0;$i < count($cresult); $i++){

                        if($cresult[$i]->shipping_option == "ups_shipping"){
                            //take length, width, height and weight
                            $temp_id = $cresult[$i]->id;
                            $temp_length = $cresult[$i]->length;
                            $temp_width = $cresult[$i]->width;
                            $temp_height = $cresult[$i]->height;
                            $temp_weight = $cresult[$i]->weight;
                            $temp_qty = $cresult[$i]->quantity;
                           
                            //echo "Calculated price : " . $temp_shipping_charges;
                            $temp_data['shipping_price'] = $temp_shipping_charges;
                            $this->product_model->update_table($temp_data, array('id' => $temp_id));
                            $sub_total_shipping += $temp_shipping_charges;
                            $sub_total_amount += $cresult[$i]->price * $temp_qty;
                        }
                    }
                    $tax = $sub_total_amount * $this->config->item('TAX_RATE') / 100;
                    $return['tax'] = number_format($tax, 2);
                    $return['subtotal'] = $sub_total_shipping;
                    $return['status'] = true;
                    $return['message'] = "Calculation done";
                    $return['shipping'] = $temp_shipping_charges;
                    echo json_encode($return);
                    die();



                /*$pFilter['select']  = array('angel_cart.id as cart_id ', 'angel_cart.quantity ', 'angel_cart.ip_address', 'angel_cart.product_id as cart_productId', 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'angel_cart.question');
                $pFilter['join']    = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_cart.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
                );

                $pFilter['where']       = array('p.is_active' => '1');

                if (isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                    $pFilter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                } else {
                    $pFilter['where']['angel_cart.ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $pFilter['where']['angel_cart.user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                }
                $pFilter['groupby'] = array('field' => 'p.id');
                $result             = $this->cart_model->get_rows($pFilter);
                
                if (! empty($result)) {
                    $param          = array();
                    $sub_total      = 0;
                    $total_weight   = 0;
                    foreach($result as $row) {
                        $sub_total += $row->quantity * $row->price;
                        if ($row->shipping_option == 'ups_shipping') {
                            $param[]= array(
                                'length' => $row->length,
                                'width' => $row->width,
                                'height' => $row->height,
                                'weight' => $row->weight
                            );
                            $total_weight   += $row->weight;
                        }
                    }

                    if (! empty($param)) {

                        $this->load->library('ups');
                        /*
                        $return = array();
                        if ($total_weight > 150) {
                            $return = $this->ups->check_ltl_shipping($zipcode, $type, $param[0]);
                        } else {
                        }
                        
                        $return = $this->ups->check_shipping($zipcode, $type, $param);

                        $shFilter['select'] = array('city', 'state');
                        $shFilter['where']  = array('zipcode' => $zipcode);
                        $shFilter['row']    = 1;
                        $shlocation_result  = $this->location_model->get_rows($shFilter);
                        $tax                = 0;
                        if(isset($shlocation_result->state) && $shlocation_result->state == "CT" ){
                            $tax = $sub_total * $this->config->item('TAX_RATE') / 100;
                        }
                        $return['tax'] = number_format($tax, 2);
                    } else {
                        $return['status'] = false;
                        $return['message'] = "Cart empty";
                    }
                } else {
                    $return['status'] = false;
                    $return['message'] = "Cart empty";
                }*/
            } else {
                $return['status']   = false;
            }

            echo json_encode($return); die;
        } else {
            not_found();
        }
    }

   public function fedex_calculator($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode, $city, $state){
   	$value = 0;

        // echo "<pre>";
        // echo "----------------------";
        // print_r($temp_weight);
        // echo "----------------------";

        // $zip_code = $this->input->post('zipcode');
        // $city = $this->input->post('city');
        // $state = $this->input->post('state');
        // echo json_encode(array($zipcode, $state, $city));
    	// $length, $width, $height, $weight,
        require("freight/fedex-common.php5");
         
        $newline = "<br />";
        //The WSDL is not included with the sample code.
        //Please include and reference in $path_to_wsdl variable.
        // $path_to_wsdl = "https://saveinparadise.com/assets/RateService_v26.wsdl";
        $path_to_wsdl = "https://saveinparadise.com/assets/RateService_v26.wsdl";

        ini_set("soap.wsdl_cache_enabled", "0");
         
        $opts = array(
        	  'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        	);
        $client = new SoapClient($path_to_wsdl, array('trace' => 1,'stream_context' => stream_context_create($opts)));  // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

        $request['WebAuthenticationDetail'] = array(
        	'ParentCredential' => array(
        		'Key' => getProperty('parentkey'),
        		'Password' => getProperty('parentpassword')
        	),
        	'UserCredential' =>array(
        		'Key' => getProperty('key'), 
        		'Password' => getProperty('password')
        	)
        ); 
        $request['ClientDetail'] = array(
        	'AccountNumber' => getProperty('shipaccount'), 
        	'MeterNumber' => getProperty('meter')
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
        $request['Version'] = array(
        	'ServiceId' => 'crs', 
        	'Major' => '26', 
        	'Intermediate' => '0', 
        	'Minor' => '0'
        );
        $request['ReturnTransitAndCommit'] = true;
        $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        $request['RequestedShipment']['ShipTimestamp'] = date('c');
        // $request['RequestedShipment']['ServiceType'] = 'FEDEX_FREIGHT_ECONOMY'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        

        $request['RequestedShipment']['ServiceType'] = 'FEDEX_3_DAY_FREIGHT'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
        $request['RequestedShipment']['Shipper'] = getProperty('shipperbilling');
        $request['RequestedShipment']['Recipient'] = $this->addRecipient($zipcode,$city, $state);
        $request['RequestedShipment']['ShippingChargesPayment'] = $this->addShippingChargesPayment();
        $request['RequestedShipment']['FreightShipmentDetail'] = array(
        	'FedExFreightAccountNumber' => getProperty('freightaccount'),
        	'FedExFreightBillingContactAndAddress' => getProperty('freightbilling'),
        	'PrintedReferences' => array(
        		'Type' => 'SHIPPER_ID_NUMBER',
        		'Value' => 'RBB1057'
        	),
        	'Role' => 'SHIPPER',
        	'PaymentType' => 'PREPAID',
        	'CollectTermsType' => 'STANDARD',
        	'DeclaredValuePerUnit' => array(
        		'Currency' => 'USD',
        		'Amount' => 50
        	),
        	'LiabilityCoverageDetail' => array(
        		'CoverageType' => 'NEW',
        		'CoverageAmount' => array(
        			'Currency' => 'USD',
        			'Amount' => '50'
        		)
        	),
        	'TotalHandlingUnits' => 15,
        	'ClientDiscountPercent' => 0,
        	'PalletWeight' => array(
        		'Units' => 'LB',
        		'Value' => 20
        	),
        	'ShipmentDimensions' => array(
        		'Length' => $temp_length,
        		'Width' => $temp_width,
        		'Height' => $temp_height,
        		'Units' => 'IN'
        	),
        	'LineItems' => array(
        		'Id' => 1,
        		'FreightClass' => 'CLASS_050',
        		'ClassProvidedByCustomer' => false,
        		'HandlingUnits' => 5,
        		'Packaging' => 'PALLET',
        		'BillOfLaddingNumber' => 'BOL_12345',
        		'PurchaseOrderNumber' => 'PO_12345',
        		'Description' => 'Heavy Stuff',
        		'Weight' => array(
        			'Value' => $temp_weight,
        			'Units' => 'LB'
        		),
        		'Dimensions' => array(
        			'Length' => $temp_length,
        			'Width' => $temp_width,
        			'Height' => $temp_height,
        			'Units' => 'IN'
        		),
        		'Volume' => array(
        			'Units' => 'CUBIC_FT',
        			'Value' => 30
        		)
        	)
        );
        $request['RequestedShipment']['RequestedPackageLineItems'] = array(
        	'SequenceNumber' => 1,
        	'GroupNumber' => 1,
        	'GroupPackageCount' => 1,
        	'Weight' => array(
        		'Units' => 'LB',
        		'Value' => $temp_weight,
        	),
        	'Dimensions' => array(
        		'Length' => $temp_length,
                    'Width' => $temp_width,
                    'Height' => $temp_height,
        		'Units' => 'IN'
        	),
        	'PhysicalPackaging' => 'PALLET',
        	'AssociatedFreightLineItems' => array(
        		'Id' => 1
        	)
        );
        $request['RequestedShipment']['PackageCount'] = '1';

//echo json_encode($client);

        try{
        	if(setEndpoint('changeEndpoint')){
        		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
        	}
        	
        	$response = $client -> getRates($request);

            // echo "<pre>";
            // print_r($response);

            $data1 = [];
            //echo "checking response";
            //var_dump($response);
            if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){  	
            	$rateReply = $response -> RateReplyDetails;
            	//echo '<table border="1">';
            	//echo '<tr><th>Rate Details</th><th>&nbsp;</th></tr>';
                //echo'[';
                $data =  json_encode($rateReply, true);
                $data1 = json_decode($data, true);
                //var_dump($data1);
                //echo $data1['RatedShipmentDetails']['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'];
                //echo "ahmad was here";
                //echo $data['RatedShipmentDetails']['ShipmentRateDetail'];
        		//echo "]";
         		//print_r(json_decode($data,true));
        		
        		// echo(count($data));
        		// echo '</table>';
               
                printSuccess($client, $response);
            }else{
                printError($client, $response);
            } 
            
            writeToLog($client);    // Write to log file   
        	} catch (SoapFault $exception) {
           		printFault($exception, $client);        
        	}
        	// echo $data1['RatedShipmentDetails']['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'];
            $value = [];
            if(!empty($data1)){
        	   $value = $data1['RatedShipmentDetails']['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'];
            }else{
                $value = 0;
            }
        	$value_shipping = $value;
        	return $value_shipping;
            }

       	public function addRecipient($zipcode, $city, $state){
            $recipient = array(
                'Contact' => array(
                    'PersonName' => 'Ahmad',
                    'CompanyName' => 'Sender Company Name',
                    'PhoneNumber' => '1234567890'
                ),
                'Address' => array(
                    // 'StreetLines' => array('Address Line 1'),
                    'City' => $city,
                    'StateOrProvinceCode' => $state,
                    'PostalCode' => $zipcode,
                    'CountryCode' => 'US'
                )
            );
            return $recipient;
        }
       public function addRecipient2($zipcode){
            $recipient = array(
                'Contact' => array(
                    'PersonName' => 'Ahmad',
                    'CompanyName' => 'Sender Company Name',
                    'PhoneNumber' => '1234567890'
                ),
                'Address' => array(
                    // 'StreetLines' => array('Address Line 1'),
                    // 'City' => $city,
                    // 'StateOrProvinceCode' => $state,
                    'PostalCode' => $zipcode,
                    'CountryCode' => 'US'
                )
            );
            return $recipient;
        }
        public function addShippingChargesPayment(){
            $shippingChargesPayment = array(
                'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
                'Payor' => array(
                    'ResponsibleParty' => array(
                        'AccountNumber' => getProperty('freightaccount'),
                        'CountryCode' => 'US')
                )
            );
            return $shippingChargesPayment;
        }


        public function rates($temp_length, $temp_width, $temp_height, $temp_weight, $zipcode){
      // echo $temp_length.'------'.$temp_width.'------'.$temp_height.'------'.$temp_weight ;
            $value = 0;
            // Copyright 2009, FedEx Corporation. All rights reserved.

            // Version 12.0.0

            // $arr= [];

            // $payload = file_get_contents("php://input");

            // $data = json_decode( $payload ); 

            // echo "<per>";
            // print_r($temp_weight);  
            // echo "rate";




            require("freight/fedex-common2.php5");
            // require("freight/fedex-common.php5");



            //The WSDL is not included with the sample code.

            //Please include and reference in $path_to_wsdl variable.

            // $path_to_wsdl = "https://saveinparadise.com/assets/RateService_v28.wsdl";
            $path_to_wsdl = "https://saveinparadise.com/assets/RateService_v26.wsdl";



            ini_set("soap.wsdl_cache_enabled", "0");

             

            $opts = array(

                  'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)

                );

            $client = new SoapClient($path_to_wsdl, array('trace' => 1,'stream_context' => stream_context_create($opts)));  // Refer to http://us3.php.net/manual/en/ref.soap.php for more information



            $request['WebAuthenticationDetail'] = array(

                'ParentCredential' => array(

                    'Key' => getProperty('parentkey'),

                    'Password' => getProperty('parentpassword')

                ),

                'UserCredential' => array(

                    'Key' => getProperty('key'), 

                    'Password' => getProperty('password')

                )

            ); 

            $request['ClientDetail'] = array(

                'AccountNumber' => getProperty('shipaccount'), 

                'MeterNumber' => getProperty('meter')

            );

            $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Available Services Request using PHP ***');

            $request['Version'] = array(

                'ServiceId' => 'crs', 

                'Major' => '26', 

                'Intermediate' => '0', 

                'Minor' => '0'

            );

            $request['ReturnTransitAndCommit'] = true;

            $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...

            $request['RequestedShipment']['ShipTimestamp'] = date('c');

            // Service Type and Packaging Type are not passed in the request
            $request['RequestedShipment']['ServiceType'] = 'FEDEX_GROUND'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
            $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...

            $request['RequestedShipment']['Shipper'] = getProperty('shipperbilling');

            $request['RequestedShipment']['Recipient'] = $this->addRecipient2($zipcode);

            $request['RequestedShipment']['ShippingChargesPayment'] = array(

                'PaymentType' => 'SENDER',

                'Payor' => array(

                    'ResponsibleParty' => array(

                        'AccountNumber' => getProperty('billaccount'),

                        'Contact' => null,

                        'Address' => array(

                            'CountryCode' => 'US'

                        )

                    )

                )

            );                                                              

            $request['RequestedShipment']['PackageCount'] = '1';

            $request['RequestedShipment']['RequestedPackageLineItems'] = array(

                '0' => array(

                    'SequenceNumber' => 1,

                    'GroupPackageCount' => 1,

                    'Weight' => array(

                        'Value' =>  $temp_weight,

                        'Units' => 'LB'

                    ),

                    'Dimensions' => array(

                        'Length' => $temp_length,

                        'Width' => $temp_width,

                        'Height' => $temp_height,

                        'Units' => 'IN'

                    )

                )

            );

       //  try {
       //      if(setEndpoint('changeEndpoint')){
       //          $newLocation = $client->__setLocation(setEndpoint('endpoint'));
       //      }
       //      $count = 1;
       //      $data1 = [];
       //      $response = $client ->getRates($request);
                
       //      if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){

       //          // $rateReply = $response -> RateReplyDetails;
       //          // $data = $this->printRateReplyDetails($rateReply,$count,count($response->RateReplyDetails));
       //          // $data =  json_encode($rateReply, true);
       //          // $data1 = json_decode($data, true);
       // // $data1 .= "[";

       //  if(is_array($response->RateReplyDetails)){

       //      foreach ($response->RateReplyDetails as $rateReply){

       //          $data1 += $this->printRateReplyDetails($rateReply,$count,count($response -> RateReplyDetails));

       //          $count++;

       //      }

       //  }else{

       //       $data1 += $this->printRateReplyDetails($response->RateReplyDetails,$count,count($response -> RateReplyDetails));

       //       $count++;       

       //  }

       //  // $data1 .= "]";
       //          printSuccess($client, $response);
       //      }else{
       //          printError($client, $response); 
       //      } 
            
       //      writeToLog($client);    // Write to log file   
       //  } catch (SoapFault $exception) {
       //     printFault($exception, $client);        
       //  }
       //  $value = str_replace(',', '', $data1[1]);

       //      return $value;


                
    try {
        if(setEndpoint('changeEndpoint')){
            $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        }
        
        $response = $client ->getRates($request);

        
            
        if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
           $data = '';
            if(is_array($response -> RateReplyDetails)){
                foreach ($response -> RateReplyDetails as $rateReply){
                   $data = $this->printRateReplyDetails($rateReply);
                }
            }else{
                   $data = $this->printRateReplyDetails($response -> RateReplyDetails);          
            }
            printSuccess($client, $response);
        }else{
            printError($client, $response); 
        } 
        
        writeToLog($client);    // Write to log file   
    } catch (SoapFault $exception) {
       printFault($exception, $client);        
    }
        return $data;
    }

    public function printRateReplyDetails($rateReply){
        
        $table = [];
    $serviceType = $rateReply -> ServiceType;
    if($rateReply->RatedShipmentDetails && is_array($rateReply->RatedShipmentDetails)){
        $amount =  number_format($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount,2,".",",");
    }elseif($rateReply->RatedShipmentDetails && ! is_array($rateReply->RatedShipmentDetails)){
        $amount =  number_format($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount,2,".",",");
    }
    if(array_key_exists('DeliveryTimestamp',$rateReply)){
        $deliveryDate=  $rateReply->DeliveryTimestamp;
    }else{
        $deliveryDate=  $rateReply->TransitTime;
    }
    if($serviceType == 'FEDEX_GROUND'){
            return $amount;
        }
    
    }

    public function send_order_mail () {

        $viewData['email']          = 'pandya.angelinfotech@gmail.com';
                    $viewData['username']       = ucfirst('jay') . ' ' . ucfirst('pandya');
                    $viewData['activation_code']= 'dsdsdd';

                    $sent_data['subject']       = "Password Reset - Restaurant Equipment Paradise";
                    $sent_data['email']         = 'pandya.angelinfotech@gmail.com';
                    $sent_data['message']       = $this->load->view('front/email/forget_password', $viewData, true);
                    @$this->sentMail($sent_data); 


        // p('dad');
//                     $userData['product_details']= array();
//                     $userData['order_details']  = array();
//                     $userData['business_name']  = "SIP";
//                     $mailData['message']        = $this->load->view('front/email/license_agreement', $userData, true);
//                     $mailData['subject']        = "New Product Purchased";
// //                    $mailData['attachement']    = array($pdfFilePath);
//                     $mailData['email']          = 'pandya.angelinfotech@gmail.com';
//                     $this->sentMail($mailData);
    } 


}
?>