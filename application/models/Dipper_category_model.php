<?php
    class Dipper_category_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_dipper_category';
            $this->children = array();
        }

        public function getCategory($rFilter = array()) {

            $return = array();

            $filter['select']   = array('angel_dipper_category.*');
            if(isset($rFilter['search'])) {
                $filter['like'] = array('field' => 'angel_dipper_category.name', 'value' => $rFilter['search']);
            }
            $filter['groupby']  = array('field' => 'angel_dipper_category.id');
            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_dipper_category.slug'] = $rFilter['slug'];
            }
            $return['total']        = $this->dipper_category_model->get_rows($filter, true);

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row']  = 1;
            } else if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }
            $filter['orderby'] = array('field' => 'angel_dipper_category.created_date', 'order' => 'DESC');
            $return['category'] = $this->dipper_category_model->get_rows($filter);
            return $return;
        }

        public function getIdFromSlug($slug = '') {
            $data   = 0;

            if ($slug != '') {

                $filter['where']    = array('slug' => $slug);
                $filter['select']   = array('id');
                $filter['row']      = 1;
                $cData              = $this->dipper_category_model->get_rows($filter);

                if (!empty($cData) && isset($cData->id)) {
                    $data = $cData->id;
                }
            }

            return $data;
        }

        public function create_slug($id, $name) {
            $count      = 0;
            $name       = slug($name);
            $slug_name  = $name;

            while(true) {
                $filter['select']       = array('id');
                $filter['where']        = array('slug' => $slug_name ,'md5(id)  != ' => $id);
                $slugcount              = $this->dipper_category_model->get_rows($filter,true);

                if ($slugcount == 0) {
                    break;
                }
                $slug_name = $name . '_' . (++$count);
            }
            return $slug_name;
        }
        
         public function getCategoryNames($cid) {
            $data = array();
            if ($cid != '') {

                $filter['where']    = array('id' => $cid);
                $filter['select']   = array('name');
                $filter['row']      = 1;
                $data               = $this->dipper_category_model->get_rows($filter);

            }

            return $data;
        }
        
        
    }
?>