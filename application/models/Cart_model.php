<?php
    class Cart_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_cart';
        }
        
        public function getItems($param = array()) {
            $return = array();

            if (!empty($param)) {
                
                $filter['select']   = array('angel_cart.*', 'p.id as product_id', 'p.name as product_name', 'p.price as price', 'p.length', 'p.width', 'p.height', 'p.weight', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId', 'p.shipping_price',  'p.is_free_shipping', 'p.display_shipping_details', 'p.shipping_option','p.modal_number');
                $filter['join']     = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = p.id', 'type' => 'left')
                                    );
                $filter['groupby']  = array('field' => 'angel_cart.id');
                
//                if (isset($param['user_id']) && $param['user_id'] > 0 && isset($param['ip_address']) && $param['ip_address'] > 0) {
//                    $filter['where'] = array('user_id' => $param['user_id'], 'ip_address' => $param['ip_address']);
//                } else if (isset($param['ip_address']) && $param['ip_address'] > 0) {
//                    $filter['where'] = array('ip_address' => $param['ip_address']);
//                }

                if (isset($param['user_id']) && $param['user_id'] > 0) {
                    $filter['or_where']             = array('user_id' => $param['user_id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                } else if (isset($param['ip_address']) && $param['ip_address'] > 0) {
                    $filter['where']                = array('ip_address' => $param['ip_address'], 'user_agent' => $_SERVER['HTTP_USER_AGENT']);
                }
                $filter['orderby']  = array('field' => 'angel_cart.created_date', 'order' => 'DESC');
                $return             = $this->cart_model->get_rows($filter);
            }
            return $return;
        }

        public function cart_count() {
            $return             = 0;
            $filter['join']     = array(
                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_cart.product_id', 'type' => 'left'),
                                        );
            if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
//                $filter['where']['user_id'] = $this->loginUser['id'];
                $filter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
            } else {
                $filter['where']['ip_address']  = $_SERVER['REMOTE_ADDR'];
                $filter['where']['user_agent']  = $_SERVER['HTTP_USER_AGENT'];
                $filter['where']['product_id >'] = 0;
            }

            $filter['groupby']  = array('field' => 'p.id');
            $filter['select']   = array('*', 'shipping_price');
            $count              = $this->cart_model->get_rows($filter, true);

            if ($count > 0) {
                $return = $count;
//                $return = '<span class="cart_badge cart_total_item">' . $count . '</span>';
            }
            return $return;
        }
    }
?>