<?php

class Enterprise_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_enterprise';
    }
    
    
    public function getEnterprises($param = array()) {
        
        $filter['select']   = array('angel_enterprise.*');
        $filter['where']    = array('angel_enterprise.status' => '1');

        $data = $this->enterprise_model->get_rows($filter);
        
        return $data;
        
    }

    public function getEnterprise($param = array()) {
        
        $filter['select']   = array('angel_enterprise.*');
       
        if(!empty($param)) {
            $filter['where']    = $param['where'];
        }
        $filter['row']  = 1;
        
        $data = $this->enterprise_model->get_rows($filter);
        return $data;
        
    }
    
}

?>