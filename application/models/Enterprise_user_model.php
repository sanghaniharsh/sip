<?php

class Enterprise_user_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_enterprise_users';
    }
    
    public function get_detail($id = 0) {
        $return = array();
        if ($id > 0) {
            
            $filter['select']   = array('angel_enterprise_users.*', 'CONCAT(angel_enterprise_users.firstname , " ", angel_enterprise_users.lastname) as username ');
            $filter['where']    = array('angel_enterprise_users.id' => $id);
            $filter['row']      = 1;
            $return             = $this->enterprise_user_model->get_rows($filter);
        }
        return $return;
    }
    
}

?>