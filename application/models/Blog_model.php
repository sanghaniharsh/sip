<?php
    class Blog_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_blog';
        }
        
        public function blogPost($rFilter = array()) {

            $return = array();

            $filter['select']       = array('angel_blog.*', 'GROUP_CONCAT(i.image) as blog_images', 'c.name as category_name', 'c.slug as category_slug');
            $filter['join']         = array(
                                        0 => array('table' => 'angel_blog_category as c', 'condition' => 'c.id = angel_blog.category_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_blog_image as i', 'condition' => 'i.blogId = angel_blog.id', 'type' => 'left')
                                    );

            if(isset($rFilter['search'])) {
                $filter['like']     = array('field' => 'angel_blog.title', 'value' => $rFilter['search']);
                $filter['or_like']  = array(
                    '0' => array('field' => 'c.name', 'value' => $rFilter['search'])
                );
            }
            $filter['groupby']      = array('field' => 'angel_blog.id');
            
            if (isset($rFilter['category'])) {
                $filter['where']['c.slug'] = $rFilter['category'];
            }

            if (isset($rFilter['active'])) {
                $filter['where']['angel_blog.is_active'] = $rFilter['active'];
            }

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_blog.slug'] = $rFilter['slug'];
                $filter['row']                      = 1;
            }

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row']  = 1;
            } 
            $return['total']    = $this->blog_model->get_rows($filter, true);

            if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }

            $filter['orderby']  = array('field' => 'angel_blog.created_date', 'order' => 'DESC');

            $return['blog']     = $this->blog_model->get_rows($filter);
            return $return;
        }
    }
?>