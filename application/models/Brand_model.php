<?php
    class Brand_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_brand';
            $this->children = array();
        }

        public function getBrand($rFilter = array()) {

            $return = array();

            $filter['select']       = array('angel_brand.*');

            if (isset($rFilter['search'])) {
                $filter['like']     = array('field' => 'angel_brand.name', 'value' => $rFilter['search']);
            }

            $filter['groupby']      = array('field' => 'angel_brand.id');

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_brand.slug']  = $rFilter['slug'];
                $filter['row']  = 1;
            }

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row']  = 1;
            } 

            $return['total']    = $this->brand_model->get_rows($filter, true);

            if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }

            $filter['orderby'] = array('field' => 'angel_brand.sort', 'order' => 'ASC');

            $return['brand'] = $this->brand_model->get_rows($filter);
            return $return;
        }

        public function getProductBrand($rFilter = array()) {

            $return = array();

            $filter['select']       = array('angel_brand.*');
//            $filter['join']         = array(
//                                        0 => array('table' => 'angel_product as p', 'condition' => 'p.brand_id = angel_brand.id', 'type' => 'right'),
//                                        );

            if (isset($rFilter['search'])) {
                $filter['like']     = array('field' => 'angel_brand.name', 'value' => $rFilter['search']);
            }
//            $filter['where']['p.brand_id !=']       = 0;
            $filter['where']['angel_brand.id !=']   = 0;
            $filter['groupby']      = array('field' => 'angel_brand.id');

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_brand.slug']    = $rFilter['slug'];
                $filter['row']                          = 1;
            }

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row'] = 1;
            } 

            $return['total']    = $this->brand_model->get_rows($filter, true);

            if (isset($rFilter['limit'])) {
                $filter['limit']    = array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }

            $filter['orderby']      = array('field' => 'angel_brand.sort', 'order' => 'ASC');

            $return['brand']        = $this->brand_model->get_rows($filter);
            return $return;
        }

        public function getBrandProducts($param = array()) {

            $filter['select']   = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
            $filter['join']     = array(
                                    0 => array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                    1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left')
                                );

            if (isset($param['active'])) {
                $filter['where']['angel_product.is_active'] = $param['active'];
            }

            if (isset($param['brand_id'])) {
                $filter['where']['angel_product.brand_id'] = $param['brand_id'];
            }
            $filter['groupby']  = array('field' => 'angel_product.id');

            $return['total']    = $this->product_model->get_rows($filter, true);

            if (isset($param['limit'])) {
                $filter['limit']= array('limit' => $param['limit']['limit'], 'from' => $param['limit']['from']);
            }

            $filter['orderby']  = array('field' => 'angel_product.created_date', 'order' => 'DESC');

            $return['product']  = $this->product_model->get_rows($filter);
            return $return;
        }
 
        public function create_slug($id, $name)
        {
            $count      = 0;
            $name       = slug($name);
            $slug_name  = $name;

            while(true) {
                $filter['select']       = array('id');
                $filter['where']        = array('slug' => $slug_name, 'id  != ' => $id);
                $slugcount              = $this->brand_model->get_rows($filter,true);

                if ($slugcount == 0) {
                    break;
                }
                $slug_name = $name . '_' . (++$count);
            }
            return $slug_name;
        }
    }
?>