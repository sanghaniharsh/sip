<?php
    class Auction_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_auction';
        }

        public function getAuction($rFilter = array()) {

            $return = array();

            $filter['select']       = array('angel_auction.*', 'GROUP_CONCAT(i.image) as auction_images');
            $filter['join']         = array(
                                        0 => array('table' => 'auction_images as i', 'condition' => 'i.auction_id = angel_auction.id', 'type' => 'left')
                                    );

            if(isset($rFilter['search'])) {
                $filter['like']     = array('field' => 'angel_auction.title', 'value' => $rFilter['search']);
            }

            if (isset($rFilter['active'])) {
                $filter['where']['angel_auction.is_active'] = $rFilter['active'];
            }

            $filter['groupby']      = array('field' => 'angel_auction.id');

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_auction.slug']  = $rFilter['slug'];
                $filter['row']                          = 1;
            }

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row'] = 1;
            } 
            
            $return['total']    = $this->auction_model->get_rows($filter, true);

            if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }

            $filter['orderby'] = array('field' => 'angel_auction.sort', 'order' => 'ASC');

            $return['auction'] = $this->auction_model->get_rows($filter);
            return $return;
        }

    }
?>