<?php

class Location_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_location';
    }
    
    public function getLocation($zipcode = '') {
        $location = '';

        if ($zipcode != '') {
            $filter['where']    = array('zipcode' => $zipcode);;
            $filter['select']   = array('city', 'state', 'zipcode');
            $filter['row']      = 1;
            $locationResult     = $this->location_model->get_rows($filter);
            
            if (!empty($locationResult)) {
                $location = $locationResult->city.', '.$locationResult->state.' '.$locationResult->zipcode;
            }
        }

        return $location;
    }
    public function getCityState($zipcode = '') {
        $result = array();

        if ($zipcode != '') {
            $filter['where']    = array('zipcode' => $zipcode);;
            $filter['select']   = array('city', 'state', 'zipcode');
            $filter['row']      = 1;
            $locationResult     = $this->location_model->get_rows($filter);

            if (!empty($locationResult)) {
                $result['city']     = $locationResult->city;
                $result['state']    = $locationResult->state;
            }
        }

        return $result;
    }
}

?>