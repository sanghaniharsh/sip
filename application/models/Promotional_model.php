<?php

class Promotional_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_promotional';
    }

    public function check_promocode($data = array()) {
        $return = array();

        if (!empty($data)) {
            $pfilter['where']   = array('promo_code' => $data['promo_code']);
            $pfilter['row']     = 1;
            $promotional        = $this->promotional_model->get_rows($pfilter);
            if (!empty($promotional)) {

                $full_total         = $data['total_amount'];
                $discount           = $promotional->discount;
                $total_discount     = 0;
                $final_amount       = $data['total_amount'];

                if ($promotional->use_number  >=  $promotional->promo_count) {

                    if (isset($promotional->type)) {
                        if ($promotional->type == '0') {
                            $total_discount   = $full_total * $discount / 100;

                            if ($total_discount >= $full_total) {
                                $total_discount = $full_total;
                            }
                            $final_amount       = ($full_total - $total_discount);
                        } else if($promotional->type == '1') {
                           $total_discount   = $discount; 

                            if ($total_discount >= $full_total) {
                                $total_discount = $full_total;
                            }
                            $final_amount    = ($full_total - $total_discount);
                        }
                        $status                         = true;

                        $return['total_amount']         = $final_amount;
                        $return['total_discount']       = $total_discount;
                    } else {
                       $status  = false;
                    }
                } else {
                    $status   = false;
                } 
            } else {
                $status   = false;
            } 
        } else {
            $status   = false;
        }
        $return['status']   =   $status;
        return $return;
    }
}

?>