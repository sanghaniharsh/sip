<?php

class Enterprise_product_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_enterprise_product';
    }
    
    public function getProducts($param = array()) {

            $filter['select']   = array('angel_enterprise_product.*','GROUP_CONCAT(pi.image) as product_images');
            $filter['join']     = array( 
                                        0 => array('table' => 'angel_product_enterprise as pe', 'condition' => 'pe.enterprise_product_id = angel_enterprise_product.id', 'type' => 'LEFT'),
                                        1 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = angel_enterprise_product.id', 'type' => 'left')
                                    );
           
            
            if(isset($param['search'])) {
                $filter['like']     = array('field' => 'angel_enterprise_product.name', 'value' => $param['search']);
                $filter['or_like']  = array(
                    '0' => array('field' => 'angel_enterprise_product.price', 'value' => $param['search']),
                    '1' => array('field' => 'angel_enterprise_product.name', 'value' => $param['search'])
                );
            }

            if(isset($param['enterprise_ids'])) {
                $filter['where']['pe.enterprise_id'] = $param['enterprise_ids'];
            }

            if (isset($param['status'])) {
                $filter['where']['angel_enterprise_product.status'] = $param['status'];
            }

             
            $filter['groupby']  = array('field' => 'angel_enterprise_product.id');

            if (isset($param['slug']) && $param['slug'] != '') {
                $filter['where']['angel_enterprise_product.slug']  = $param['slug'];
            }

            if (isset($param['pid']) && $param['pid'] > 0) {
                $filter['where']['angel_enterprise_product.id']    = $param['pid'];
            }
            
            if(isset($param['row'])) {
                $filter['row'] = $param['row'];
            }
            
            $return['total']    = $this->enterprise_product_model->get_rows($filter, true);

            if (isset($param['limit'])) {
                $filter['limit']= array('limit' => $param['limit']['limit'], 'from' => $param['limit']['from']);
            }
            
            $sort_field = 'angel_enterprise_product.created_date';
            $sort_order = 'DESC';
            
            if(isset($param['order'])) {
                if($param['order'] == 'name') {
                    $sort_field = 'angel_enterprise_product.name';
                    $sort_order = 'ASC';
                } else if($param['order'] == 'price') {
                    $sort_field = 'angel_enterprise_product.price';
                    $sort_order = 'ASC';
                } else if($param['order'] == 'latest') {
                    $sort_field = 'angel_enterprise_product.created_date'; 
                }
                
            } 
            
            $filter['orderby']  = array('field' => $sort_field, 'order' => $sort_order);

            $return['product']  = $this->enterprise_product_model->get_rows($filter);
            
            return $return;
        }
    
}

?>