<?php

class Attachment_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_attachment';
    }
    
    public function getRecord($md5_id) {
        $return = array();
        
        if ($md5_id != '') {
            $filter['where']= array('md5(id)' => $md5_id);
            $filter['row']  = 1;
            $return         = $this->attachment_model->get_rows($filter);
        }

        return $return;
    }

    public function removeAttachment($md5_ids = array()) {

        if (!empty($md5_ids)) {
            $aFilter['where_in']    = array(array('field' => 'id', 'value' => $md5_ids));
            $img_result             = $this->attachment_model->get_rows($aFilter);

            if (!empty($img_result)) {                
                $attachmentId = array();
                foreach ($img_result as $row) {
                    $attachmentId[] = $row->id;
                    $blog_image     = getcwd() . $row->path;

                    if (file_exists($blog_image)) {
                        unlink($blog_image);
                    }
                }

                $this->attachment_model->delete_filter(array('where_in' => array('field' => 'id', 'value' => $attachmentId)));
            }
        }
    }
}

?>