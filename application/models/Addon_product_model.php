<?php
class Addon_product_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_addon_product';
    }
    public function getAddonProducts($param = array()) {

        $filter['select'] = array('angel_addon_product.id as addon_id','angel_addon_product.addon_product_id', 'angel_addon_product.is_active as addon_is_active' , 'p.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
        $filter['join'] = array(
            0 => array('table' => 'angel_product as p', 'condition' => 'p.id = angel_addon_product.addon_product_id', 'type' => 'left'),
            1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = p.id', 'type' => 'left'),
            2 => array('table' => 'angel_category as c', 'condition' => 'c.id = p.category_id', 'type' => 'left'),
        );

        if (isset($param['search'])) {
            $filter['like'] = array('field' => 'p.name', 'value' => $param['search']);
            $filter['or_like'] = array(
                '0' => array('field' => 'p.modal_number', 'value' => $param['search']),
                '1' => array('field' => 'c.name', 'value' => $param['search'])
            );
        }

        if (isset($param['type'])) {
            $filter['where']['p.type'] = $param['type'];
        }

        if (isset($param['category_ids']) && !empty($param['category_ids'])) {

            $filter['where_in'] = array(array('field' => 'p.category_id', 'value' => $param['category_ids']));
        } else if (isset($param['category'])) {

            $filter['where']['p.category_id'] = $param['category'];
        }

        if (isset($param['product_id'])) {
            $filter['where']['angel_addon_product.product_id'] =  $param['product_id'];
        }
        if (isset($param['active'])) {
            $filter['where']['angel_addon_product.is_active'] = $param['active'];
        }
        $filter['groupby']  = array('field' => 'angel_addon_product.id');
        $return['total']    = $this->addon_product_model->get_rows($filter, true);

        if (isset($param['limit'])) {
            $filter['limit'] = array('limit' => $param['limit']['limit'], 'from' => $param['limit']['from']);
        }

        $filter['orderby'] = array('field' => 'p.created_date', 'order' => 'DESC');

        $return['product'] = $this->addon_product_model->get_rows($filter);
        return $return;
    }

}
?>