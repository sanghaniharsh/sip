<?php
    class Enterprise_cart_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_enterprise_cart';
        }
        
        public function getItems($param = array()) {
            $return = array();

            if (!empty($param)) {
                
                $filter['select']   = array('angel_enterprise_cart.*', 'p.id as product_id', 'p.name as product_name', 'p.price as price', 'GROUP_CONCAT(pi.image) as product_images','p.model_number');
                $filter['join']     = array(
                                        0 => array('table' => 'angel_enterprise_product as p', 'condition' => 'p.id = angel_enterprise_cart.product_id', 'type' => 'left'),
                                        2 => array('table' => 'angel_enterprise_product_images as pi', 'condition' => 'pi.product_id = p.id', 'type' => 'left')
                                    );
                $filter['groupby']  = array('field' => 'angel_enterprise_cart.id');

                if (isset($param['user_id']) && $param['user_id'] > 0) {
                    $filter['or_where']             = array('user_id' => $param['user_id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
                } else if (isset($param['ip_address']) && $param['ip_address'] > 0) {
                    $filter['where']                = array('ip_address' => $param['ip_address'], 'user_agent' => $_SERVER['HTTP_USER_AGENT']);
                }
                $filter['orderby']  = array('field' => 'angel_enterprise_cart.created_date', 'order' => 'DESC');
                $return             = $this->enterprise_cart_model->get_rows($filter);
            }
            return $return;
        }

        public function cart_count() {
            $return             = 0;
            $filter['join']     = array(
                                        0 => array('table' => 'angel_enterprise_product as p', 'condition' => 'p.id = angel_enterprise_cart.product_id', 'type' => 'left'),
                                        );
            if ( isset($this->loginUser['id']) && $this->loginUser['id'] != "" ) {
                $filter['or_where'] = array('user_id' => $this->loginUser['id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
            } else {
                $filter['where']['ip_address']  = $_SERVER['REMOTE_ADDR'];
                $filter['where']['user_agent']  = $_SERVER['HTTP_USER_AGENT'];
                $filter['where']['product_id >'] = 0;
            }

            $filter['groupby']  = array('field' => 'p.id');
            $filter['select']   = array('*');
            $count              = $this->enterprise_cart_model->get_rows($filter, true);

            if ($count > 0) {
                $return = $count;
            }
            return $return;
        }
    }
?>