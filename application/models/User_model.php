<?php

class user_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'angel_users';
    }

    public function validate_user($data = array()) {
        $username   = $data['email'];
        $password   = $data['password'];
        $return     = array();

        if (!empty($username) && !empty($password)) {
            $filter['where']    = array('password' => md5(trim($password)), 'email' => $username);
            $filter['row']      = 2;
            $result             = $this->user_model->get_rows($filter);

            if (!empty($result)) {
                $return = $result;
            }
        }

        return $return;
    }

    public function get_detail($id = 0) {
        $return = array();
        if ($id > 0) {
            
            $filter['select']   = array('angel_users.*', 'CONCAT(angel_users.firstname , " ", angel_users.lastname) as username ');
            $filter['where']    = array('angel_users.id' => $id);
            $filter['row']      = 1;
            $return             = $this->user_model->get_rows($filter);
        }
        return $return;
    }
    
    public function remove_avatar($avatar = '') {
        $return = TRUE;
        
        $filter['where']    = array('md5(avatar)' => $avatar);
        $filter['row']      = 1;
        $query              = $this->user_model->get_rows($filter);

        if ($avatar != '') {
            $aFilter['where']   = array('id' => $query->avatar);
            $aFilter['row']     = 1;
            $result             = $this->attachment_model->get_rows($aFilter);

            if (!empty($result) && $result->path != '') {
                $logo  = getcwd() .''. $result->path;
                if (file_exists($logo)) {
                    unlink($logo);
                }

                $temp_path  = getcwd() . '/uploads/temp/'.md5($result->id).'_150_150.*';
                $temp_result= glob ($temp_path);
                if (!empty($temp_result) && file_exists($temp_result[0])) {
                    unlink($temp_result[0]);
                }

                $this->attachment_model->delete(array('id' => $query->avatar));
                $this->user_model->update_table(array('avatar' => '0'), array('id' => $query->id));
            } else {
                $result = FALSE;
            }
        } else {
            $result = FALSE;
        }
        return $return;
    }
}

?>