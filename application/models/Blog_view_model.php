<?php
    class Blog_view_model extends MY_Model
    {
        public function __construct() {
            parent::__construct();
            $this->tableName = 'angel_blog_view';
        }

        public function addView($blogId) {
            $data['blog_id']    = $blogId;
            $data['ip_address'] = $this->input->ip_address();

            $this->blog_view_model->insert($data);
        }
    }
?>