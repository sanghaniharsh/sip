<?php
    class News_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_news';
        }
        
        public function getNews($rFilter = array()) {

            $return                 = array();

            if(isset($rFilter['search'])) {
                $filter['like'] = array('field' => 'angel_news.title', 'value' => $rFilter['search']);
            }

            if (isset($rFilter['active'])) {
                $filter['where']['angel_news.is_active'] = $rFilter['active'];
            }
            $filter['groupby']      = array('field' => 'angel_news.id');

            if (isset($rFilter['news_id']) && $rFilter['news_id'] != '') {
                $filter['where']['md5(angel_news.id)']  = $rFilter['news_id'];
                $filter['row']                          = 1;
            }

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_news.slug'] = $rFilter['slug'];
                $filter['row']                      = 1;
            }

            $return['total']        = $this->news_model->get_rows($filter, true);

            if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }

            $filter['orderby']  = array('field' => 'angel_news.created_date', 'order' => 'DESC');

            $return['news']     = $this->news_model->get_rows($filter);
            return $return;
        }
    }
?>