<?php
    class Category_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_category';
            $this->children = array();
        }

        public function getCategory($rFilter = array()) {

            $return = array();

            $filter['select']   = array('angel_category.*', ' count(c.id) as child');
            $filter['join']     = array(
                                    0 => array('table' => 'angel_category as c', 'condition' => 'angel_category.id = c.parent', 'type' => 'left')
                                );

            if(isset($rFilter['search'])) {
                $filter['like'] = array('field' => 'angel_category.name', 'value' => $rFilter['search']);
            }
            $filter['groupby']  = array('field' => 'angel_category.id');
            $filter['where']    = array('angel_category.parent' => 0);

            if (isset($rFilter['slug']) && $rFilter['slug'] != '') {
                $filter['where']['angel_category.slug'] = $rFilter['slug'];
            }

            if (isset($rFilter['parent']) && $rFilter['parent'] > 0) {
                $filter['where']['angel_category.parent'] = $rFilter['parent'];
            }

            $return['total']        = $this->category_model->get_rows($filter, true);

            if (isset($rFilter['row']) && $rFilter['row'] == 1) {
                $filter['row']  = 1;
            } else if (isset($rFilter['limit'])) {
                $filter['limit']= array('limit' => $rFilter['limit']['limit'], 'from' => $rFilter['limit']['from']);
            }
            $filter['orderby'] = array('field' => 'angel_category.sort', 'order' => 'ASC');
            $return['category'] = $this->category_model->get_rows($filter);
            return $return;
        }

        public function getParents($categoryId = 0) {
            $data   = array();

            if ($categoryId > 0) {
                $continue = true;

                while($continue) {
                    $sql        = "SELECT * FROM `angel_category` WHERE `parent` = (SELECT parent FROM `angel_category` WHERE `id` = ".$categoryId.") AND parent != 0";
                    $results    = $this->db->query($sql);
                    $f_result   = $results->result_array();
                    
                    if (! empty($f_result)) {
                        $data[$categoryId] = $f_result;
                        
                        if (isset($f_result[0]['parent'])) {
                            if ($f_result[0]['parent'] > 0) {
                                $categoryId = $f_result[0]['parent'];
                            } else {
                                $continue = false;
                            }
                        } else {
                            $continue = false;
                        }
                    } else {
                        $continue = false;
                    }
                }

                $filter['where']    = array('parent' => 0);
                $filter['result']   = 1;
                $cData              = $this->category_model->get_rows($filter);

                if (!empty($cData)) {
                    $data[$categoryId] = $cData;
                }

                $data = array_reverse($data, true);
            }

            return $data;
        }

        public function getOnlyParents($categoryId = 0) {
            $data   = array();

            if ($categoryId > 0) {
                $temp       = $categoryId;
                $continue   = true;

                while($continue) {
                    $sql        = "SELECT * FROM `angel_category` WHERE `id` = (SELECT parent FROM `angel_category` WHERE `id` = ".$categoryId.")";
                    $results    = $this->db->query($sql);
                    $f_result   = $results->row();

                    if (! empty($f_result)) {
                        $data[$f_result->id] = $f_result;
                        
                        if (isset($f_result->parent)) {
                            if ($f_result->parent > 0) {
                                $categoryId = $f_result->parent;
                            } else {
                                $continue = false;
                            }
                        } else {
                            $continue = false;
                        }
                    } else {
                        $s_sql      = "SELECT * FROM `angel_category` WHERE `id` = ".$categoryId;
                        $s_results  = $this->db->query($s_sql);
                        $s_result   = $s_results->row();
                        if (! empty($s_result)) {
                            $data[$s_result->id] = $s_result;
                        }

                        $continue = false;
                    }
                }

                $filter['where']    = array('id' => $temp);
                $filter['row']      = 1;
                
                $cData              = $this->category_model->get_rows($filter);

                ksort($data);
                if (!empty($cData)) {
                    $data[$cData->id] = $cData;
                }
            }

            return $data;
        }

        public function categoryData($id = 0) {
            $children       = array();
            $filter['where']= array('parent' => '0');

            if ($id > 0) {
                $filter['where'] = array('parent' => $id);
            }
            $filter['result']   = 1;
            $filter['orderby']  = array('field' => 'name', 'order' => 'ASC');
            $result             = $this->category_model->get_rows($filter);

            foreach($result as $key => $row) {
                $children[$key]                         = array();
                $children[$key]['parent']               = $row;
                $children[$key]['parent']['children']   = $this->categoryData($row['id']);
            }

            return $children;
        }

        public function getChildData($ids = array()) {

            if (is_array($ids) && !empty($ids)) {
                $filter['where_in'] = array(array('field' => 'parent', 'value' => $ids));
            } else {
                $filter['where'] = array('parent' => $ids);
            }
            $filter['select']   = array('id', 'parent');
            $filter['result']   = 1;
            $result             = $this->category_model->get_rows($filter);

            if (!empty($result)) {
                $temp = array();
                foreach($result as $row) {
                    $temp[]             = $row['id'];
                    $this->children[]   = $row['id'];
                }

                if (!empty($temp)) {
                    $this->getChildData($temp);
                }
            }
            return $this->children;
        }
        
        public function getCategoryData($categoryId = 0) {
            $data   = array();

            if ($categoryId > 0) {

                $data               = $this->category_model->getParents($categoryId);
                $filter['where']    = array('parent' => $categoryId);
                $filter['result']   = 1;
                $cData              = $this->category_model->get_rows($filter);

                if (!empty($cData)) {
                    $data[0] = $cData;
                }
            }

            return $data;
        }

        public function getChild($categoryId = 0) {
            $data   = array();

            if ($categoryId > 0) {

                $filter['where']    = array('parent' => $categoryId);
                $filter['result']   = 1;
                $cData              = $this->category_model->get_rows($filter);

                if (!empty($cData)) {
                    $data = $cData;
                }
            }
            return $data;
        }
        
        public function getIdFromSlug($slug = '') {
            $data   = 0;

            if ($slug != '') {

                $filter['where']    = array('slug' => $slug);
                $filter['select']   = array('id');
                $filter['row']      = 1;
                $cData              = $this->category_model->get_rows($filter);

                if (!empty($cData) && isset($cData->id)) {
                    $data = $cData->id;
                }
            }

            return $data;
        }

        public function create_slug($id, $name) {
            $count      = 0;
            $name       = slug($name);
            $slug_name  = $name;

            while(true) {
                $filter['select']       = array('id');
                $filter['where']        = array('slug' => $slug_name ,'md5(id)  != ' => $id);
                $slugcount              = $this->category_model->get_rows($filter,true);

                if ($slugcount == 0) {
                    break;
                }
                $slug_name = $name . '_' . (++$count);
            }
            return $slug_name;
        }
        
         public function getCategoryNames($cid) {
               if ($cid != '') {

                $filter['where']    = array('id' => $cid);
                $filter['select']   = array('name');
                $filter['row']      = 1;
                $data              = $this->category_model->get_rows($filter);

            }

            return $data;
        }
        
        
    }
?>