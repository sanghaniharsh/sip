<?php
    class Product_model extends MY_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->tableName = 'angel_product';
        }

        public function getProducts($param = array()) {

            $filter['select']   = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId','angel_cart.quantity as cart_quantity');
            $filter['join']     = array(
                                    0 => array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                    1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left'),
                                    2 => array('table' => 'angel_cart', 'condition' => 'angel_cart.product_id = angel_product.id', 'type' => 'left'),
                                 );
            if(isset($param['type'])) {
                if($param['type'] == 1) {
                    $filter['join'][0] = array('table' => 'angel_used_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left');
                } else if($param['type'] == 2) {
                    $filter['join'][0] = array('table' => 'angel_dipper_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left');
                }
            }
            if(isset($param['search'])) {
                $filter['like']     = array('field' => 'angel_product.name', 'value' => $param['search']);
                $filter['or_like']  = array(
                    '0' => array('field' => 'angel_product.modal_number', 'value' => $param['search']),
                    '1' => array('field' => 'c.name', 'value' => $param['search'])
                );
            }

            if(isset($param['category_ids']) && !empty($param['category_ids'])) {
                $filter['where_in'] = array(array('field' => 'angel_product.category_id', 'value' => $param['category_ids']));
            } else if(isset($param['category'])) {
                $filter['where']['angel_product.category_id'] = $param['category'];
            }

            if (isset($param['active'])) {
                $filter['where']['angel_product.is_active'] = $param['active'];
            }

            if (isset($param['brand'])) {
                $filter['where']['angel_product.brand_id'] = $param['brand'];
            }
             if (isset($param['type'])) {
                $filter['where']['angel_product.type'] = $param['type'];
            }
            $filter['groupby']  = array('field' => 'angel_product.id');

            if (isset($param['slug']) && isset($param['row']) && $param['slug'] != '' && $param['row'] != '') {
                $filter['where']['angel_product.slug']  = $param['slug'];
                $filter['row']                          = 1;
            } else if (isset($param['slug']) && $param['slug'] != '') {
                $filter['where']['c.slug']    = $param['slug'];
            }

            if (isset($param['pid']) && $param['pid'] > 0) {
                $filter['where']['angel_product.id']    = $param['pid'];
                $filter['row']                          = 1;
            }
            $return['total']    = $this->product_model->get_rows($filter, true);

            if (isset($param['limit'])) {
                $filter['limit']= array('limit' => $param['limit']['limit'], 'from' => $param['limit']['from']);
            }

            $filter['orderby']  = array('field' => 'angel_product.created_date', 'order' => 'DESC');

            $return['product']  = $this->product_model->get_rows($filter);
            return $return;
        }

        public function get_random_product($Pfilter = array()) {
            $return                 = array();
            $filter['select']       = array('angel_product.*', 'GROUP_CONCAT(pi.image) as product_images', 'c.name as category_name', 'c.id as category_id', 'c.parent as parentId');
            $filter['join']         = array(
                                        0 => array('table' => 'angel_category as c', 'condition' => 'c.id = angel_product.category_id', 'type' => 'left'),
                                        1 => array('table' => 'angel_product_images as pi', 'condition' => 'pi.product_id = angel_product.id', 'type' => 'left')
                                        );
            if (isset($Pfilter['where'])) {
                $filter['where']        = $Pfilter['where'];
            }

            if (isset($Pfilter['limit'])) { 
                $filter['limit']        = array('limit' => $Pfilter['limit'], 'from' => 0);
            }
            $filter['groupby']          = array('field' => 'angel_product.id');
            $result['rendom_product']   = $this->product_model->get_rows($filter);
            $return['rendom_product']   = $result['rendom_product'];
            return $return;
        }

        public function isUsedProduct($Pfilter = array()) {
            $return = false;
            if (isset($Pfilter['slug'])) {
                $filter['where']        = array('angel_product.slug' => $Pfilter['slug']);
            }
            if (isset($Pfilter['product_id'])) {
                $filter['where']        = array('angel_product.id' => $Pfilter['product_id']);
            }

            $filter['groupby']  = array('field' => 'angel_product.id');
            $filter['row']      = 1;
            $product            = $this->product_model->get_rows($filter);
            if (isset($product->type) && $product->type == 1 ) {
                $return = true;
            }
            return $return;
        }
        
       
    }
    
?>