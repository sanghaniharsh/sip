<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_controller extends CI_Controller {

    public $sitename            = "direct_intro";
    public $allowed_filesize_mb = 5; // define filesize in MB
    public $allowed_file_size   = 1024 * 5; // define filesize in MB
    private $method             = "";

    public function __construct() {
        parent::__construct();
        $this->config->set_item('SITE_NAME', $this->sitename);
        $template       = $this->checkTemplate();
        $this->metod    = $this->router->fetch_method();
        if ($template == 'admin') {
            $this->config->set_item('ADMIN_URL', base_url() . 'admin/');
            $this->template->set_template('admin/template');
        }
        if($template == 'enterprise') {
            
            $enterprise['enterprise']   = $this->uri->segment('2');     
            
            if($enterprise['enterprise'] != "" && $enterprise['enterprise'] != "check_login") {
                
                $this->setSession($enterprise);

                $this->config->set_item('ENTERPRISE_URL', base_url() . 'enterprise/');
                $this->template->set_template('enterprise/template');
            } else {
                $this->removeSession(array('enterprise'));
            }
            
        }
        $this->defineGlobal();
    }

    public function defineGlobal() {

        $filter['where']= array('id' => 1);
        $filter['row']  = 2;
        $result         = $this->sitesettings_model->get_rows($filter);
        if (!empty($result)) {
            foreach ($result as $key => $value) {   
                $this->config->set_item(strtoupper($key), $value);
            }
        }
    }

    public function checkTemplate() {
        $current_url    = explode('/', base_url(uri_string()));
        $template       = 'front';
        if (in_array('admin', $current_url)) {
            $template   = 'admin';
        } else if(in_array('enterprise', $current_url)) {
            $template   = 'enterprise';
        }
        return $template;
    }
    
    public function setSession($data) {

        $oldData = $this->getSession();
        $sessionData = $data;
        if (!empty($oldData)) {
            $sessionData = array_merge($oldData, $data);
        }

        $this->session->set_userdata($this->sitename, $sessionData);
    }

    public function getSession($data = array()) {
        if (empty($data)) {
            return isset($this->session->userdata[$this->sitename]) ? $this->session->userdata[$this->sitename] : array();
        } else {
            return isset($this->session->userdata[$this->sitename][$data]) ? $this->session->userdata[$this->sitename][$data] : array();
        }
    }

    public function removeSession($data = array()) {
        $this->load->library('session');
        if (empty($data)) {
            $this->session->unset_userdata($this->sitename);
        } else {
            $sessionData = $this->getSession();
            
            

            foreach ($data as $row) {
                if (is_array($row)) {
                    foreach($row as $value) {
                        unset($sessionData[$value]);
                    }
                }
                unset($sessionData[$row]);
            }
            $this->session->set_userdata($this->sitename, $sessionData);    
        }
    }
    
    public function getLoginUser() {
        if ($this->checkTemplate() == 'admin') {
            return isset($this->session->userdata[$this->sitename]['adminUser']) ? $this->session->userdata[$this->sitename]['adminUser'] : FALSE;
        } else {
            return $this->getLoginUserData();
        }
    }
    
    public function getLoginUserData() {
        $user_data = array();

        if (isset($this->session->userdata[$this->sitename]['loginUser']['session_id'])) {
            $user_id            = decreptIt($this->session->userdata[$this->sitename]['loginUser']['session_id']);
            $filter['select']   = array('angel_users.*', 'a.zipcode', 'a.city', 'a.state', 'a.street_address','a.shipping_zipcode', 'a.shipping_city', 'a.shipping_state', 'a.shipping_street_address');
            $filter['where']    = array('angel_users.id' => $user_id);
            $filter['row']      = 2;
            $filter['join']     = array(
                                    array('table' => 'angel_user_address as a', 'condition' => 'a.user_id = angel_users.id', 'type' => 'LEFT')
                                );
            $filter['groupby']  = array('field' => 'angel_users.id');
            $user_data          = $this->user_model->get_rows($filter);
        }
        
        return $user_data;
    }
    
    public function getEnterpriseUserData() {
        $user_data = array();

        if (isset($this->session->userdata[$this->sitename]['enterpriseUser']['session_id'])) {
            $user_id            = decreptIt($this->session->userdata[$this->sitename]['enterpriseUser']['session_id']);
            $filter['select']   = array('angel_enterprise_users.*');
            $filter['where']    = array('angel_enterprise_users.id' => $user_id);
            $filter['row']      = 2;
            
            $filter['groupby']  = array('field' => 'angel_enterprise_users.id');
            $user_data          = $this->enterprise_user_model->get_rows($filter);
        }
        
        return $user_data;
    }
    

    public function set_authorized_user($user = array()) {
        $this->setSession(array('loginUser' => $user));
    }

    public function get_authorized_user() {
        $return = false;
        $user   = $this->getLoginUserData();

        if (!empty($user)) {
            $return = true;
        }

        return $return;
    }
    
    public function set_enterprise_user($user = array()) {
        $this->setSession(array('enterpriseUser' => $user));
    }
    
    public function get_enterprise_user() {
        $return = false;
        $user   = $this->getEnterpriseUserData();

        if (!empty($user)) {
            $return = true;
        }

        return $return;
    }
    
    public function get_admin_user() {
        return (bool) isset($this->session->userdata[$this->sitename]['adminUser']['id']) ? TRUE : FALSE;
    }

    public function set_admin_user($user = array()) {
        $this->setSession(array('adminUser' => $user));
    }
    
    public function get_enterprise_data() {
        
        $user_data = array();
        $sessionData = $this->getSession('enterprise');
        
        if(!empty($sessionData) && $sessionData != "") {
            
            $filter['select']   = array('angel_enterprise.*');
            $filter['where']    = array('angel_enterprise.slug' => $sessionData, 'status' => '1');
            $filter['row']      = 1;
            $user_data          = $this->enterprise_model->get_rows($filter);
        }
        
        return $user_data;
    }

    public function removeAdminSession() {
        $sessionData = $this->getSession();
        foreach ($sessionData['adminUser'] as $key => $value) {
            unset($sessionData['adminUser'][$key]);
        }
        unset($sessionData['adminUser']);
        $this->session->set_userdata($this->sitename, $sessionData);
    }
    
    public function removeUserSession() {
        $sessionData = $this->getSession();
        if (isset($sessionData['loginUser'])) {
            foreach ($sessionData['loginUser'] as $key => $value) {
                if (isset($sessionData['loginUser'][$key])) {
                    unset($sessionData['loginUser'][$key]);
                }
            }
            unset($sessionData['loginUser']);
        }

        $this->session->set_userdata($this->sitename, $sessionData);
    }
    
    public function removeEnterpriseSession() {
        $sessionData = $this->getSession();
        if (isset($sessionData['enterpriseUser'])) {
            foreach ($sessionData['enterpriseUser'] as $key => $value) {
                if (isset($sessionData['enterpriseUser'][$key])) {
                    unset($sessionData['enterpriseUser'][$key]);
                }
            }
            unset($sessionData['enterpriseUser']);
        }

        $this->session->set_userdata($this->sitename, $sessionData);
    }
    

    public function set_cookie_user($user = array()) {
        $user_cookie = array(
                    'name'      => $this->sitename,
                    'value'     => $user['user_value'],
                    'expire'    => $user['expire_time'],
                    );
        $this->input->set_cookie($user_cookie);
    }

    public function get_cookie_user() {
        $usercookie = $this->input->cookie($this->sitename);
        return $usercookie;
    }
    
    public function set_cookie_enterprise($user = array()) {
        $user_cookie = array(
                    'name'      => $user['name'],
                    'value'     => $user['user_value'],
                    'expire'    => $user['expire_time'],
                    );
        $this->input->set_cookie($user_cookie);
    }

    public function get_cookie_enterprise() {
        $usercookie = $this->input->cookie($this->sitename);
        return $usercookie;
    }
    

    public function set_cookie($user = array()) {
        $cookie = array(
                    'name'      => $user['name'],
                    'value'     => $user['value'],
                    'expire'    => $user['expire'],
                    );
        $this->input->set_cookie($cookie);
    }

    public function get_cookie($name) {
        $cookie = $this->input->cookie($name);
        return $cookie;
    }

    private function validateToken() {
        $headers = $this->input->request_headers();
        if (isset($headers['token']) && ($headers['token'] != '')) {

            $filter['where']= array('token' => $headers['token']);
            $check          = $this->token_model->get_rows($filter, true);

            if ($check == 0) {
                $return['status']   = 0;
                $return['message']  = "Invalid token";
                header('Content-Type: application/json');
                echo json_encode($return);die;
            }
        } else {
            $return['status']   = 0;
            $return['message']  = "Invalid token";
            header('Content-Type: application/json');
            echo json_encode($return);die;
        }
    }

    public function sentMail($data) {

        $return = array();

        if (!empty($data)) {
//            $from_email = $this->config->item('FROM_EMAIL');
//            $from_name  = $this->config->item('FROM_NAME');
            $from_email = "info@saveinparadise.com";
            $from_name  = "SIP";

            if (isset($data['from_name']) && isset($data['from_email'])) {
                $from_email = $data['from_email'];
                $from_name  = $data['from_name'];
            }

            $type       = "sendmail";

            if ($type == "gmail") {
                $this->email->from($from_email, $from_name);
                $this->email->to($data['email']);
                $this->email->subject($data['subject']);
                $this->email->message($data['message']);
                $this->email->set_mailtype('html');
                if (isset($data['attachement']) && (! empty($data['attachement']))) {
                    foreach($data['attachement'] as $attachment) {
                        $this->email->attach($attachment);
                    }
                }
                /*
                  $config = Array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'ssl://smtp.googlemail.com',
                  'smtp_port' => 465,
                  'smtp_user' => 'xxx',
                  'smtp_pass' => 'xxx',
                  'mailtype'  => 'html',
                  'charset'   => 'iso-8859-1'
                  );

                  $this->load->library('email', $config);
                 * *
                 */
                if ($this->email->send()) {
                    $return['status'] = true;
                    $return['message'] = "Mail sent successfully.";
                } else {
                    $return['status'] = false;
                    $return['message'] = "Something went wrong, Please try again later.";
                }
            }  else if ($type == 'sendmail') {
                $this->load->library('email');
                
                $config = array();
                $config['useragent']           = "CodeIgniter";
                $config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
                $config['protocol']            = "sendmail";
                $config['smtp_host']           = "localhost";
                $config['smtp_port']           = "25";
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['newline']  = "\r\n";
                $config['wordwrap'] = TRUE;
                
                $this->email->initialize($config);

                $this->email->from($from_email, $from_name);
                $this->email->to($data['email']);
                $this->email->subject($data['subject']);
                $this->email->message($data['message']);
                $this->email->set_mailtype('html');
                if (isset($data['attachement']) && (! empty($data['attachement']))) {
                    foreach($data['attachement'] as $attachment) {
                        $this->email->attach($attachment);
                    }
                }

                if ($this->email->send()) {
                    $return['status'] = true;
                    $return['message'] = "Mail sent successfully.";
                } else {
//                    p($this->email->print_debugger());
                    $return['status'] = false;
                    $return['message'] = "Something went wrong, Please try again later.";
                }
            } else if ($type == "sendgrid") {
//                $sandfrid_username = $this->config->item('SANDGRID_USERNAME');
//                $sandfrid_password = $this->config->item('SANDGRID_PASSWORD');
                // $sandfrid_username = "resteqparadise20";
                // $sandfrid_password = 'Hockey12$Puck';
                $sandfrid_username = "apikey";
                $sandfrid_password = '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';

                $this->email->initialize(array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.sendgrid.net',
                    'smtp_user' => $sandfrid_username, 
                    'smtp_pass' => $sandfrid_password, 
                    'smtp_port' => 587,
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
                ));

                $this->email->from($from_email, $from_name);
                $this->email->to($data['email']);
                $this->email->subject($data['subject']);
                $this->email->message($data['message']);
                $this->email->set_mailtype('html');

                if (isset($data['attachement']) && (! empty($data['attachement']))) {
                    foreach($data['attachement'] as $attachment) {
                        $this->email->attach($attachment);
                    }
                }

                if ($this->email->send()) {
                    $return['status'] = true;
                    $return['message'] = "Mail sent successfully.";
                } else {
//                    p($this->email->print_debugger());
                    $return['status'] = false;
                    $return['message'] = "Something went wrong, Please try again later.";
                }
            } else if($type == "sendinblue") {
                
                $sandfrid_username = "dhavalk.angelinfotech@gmail.com";
                $sandfrid_password = 'PLyEWbVOJnAZ8K7c';

                $this->email->initialize(array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp-relay.sendinblue.com',
                    'smtp_user' => $sandfrid_username, 
                    'smtp_pass' => $sandfrid_password, 
                    'smtp_port' => 587,
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
                ));
                
                $this->email->from($from_email, $from_name);
                $this->email->to($data['email']);
                $this->email->subject($data['subject']);
                $this->email->message($data['message']);
                $this->email->set_mailtype('html');

                if (isset($data['attachement']) && (! empty($data['attachement']))) {
                    foreach($data['attachement'] as $attachment) {
                        $this->email->attach($attachment);
                    }
                }

                if ($this->email->send()) {
                    $return['status'] = true;
                    $return['message'] = "Mail sent successfully.";
                } else {
                    $return['status'] = false;
                    $return['message'] = "Something went wrong, Please try again later.";
                }
                
            }
        } else {
            $return['status'] = false;
            $return['message'] = "Parameters not found";
        }

        return $return;
    }

    public function dologin($data) {

        $return = array();
        if (! empty($data)) {
            $userRow        = $this->user_model->validate_user($data);
            if (! empty($userRow)) {
                $b_filter['where'] = array('user_id' => $userRow['id']);
                $checkBlock = $this->blocked_user_model->get_rows($b_filter, true);
                
                if ($checkBlock == 0) {
                    if ($userRow['is_active'] == '1') {
                        $loginData['last_login']     = date('Y-m-d H:i:s');
                        $this->user_model->update_table($loginData, array('id' => $userRow['id']));

                        $sessionData['session_id']  = md5($userRow['id']);

                        if ($userRow['user_type'] == 2) {
                            $allowedPermission = $this->staff_permissions_model->getAllowedPermissions($userRow['id']);
                            if (!empty($allowedPermission)) {
                                $sessionData['allowed_permission'] = $allowedPermission;
                            }
                        }

                        $this->set_authorized_user($sessionData);

                        if ((isset($data['remember'])) && ($data['remember'] == "1")) {
                            $cookie_data['user_name']    = 'usercookie';
                            $cookie_data['user_value']   = $data['email'] . "," . $data['password'];
                            $cookie_data['expire_time']  = 30 * 24 * 60 * 60;
                            $this->set_cookie_user($cookie_data);
                        }

                        $return['status'] = true;
                    } else {
                        $return['status']   = false;
                        $return['message']  = "Account is not active.";
                    }
                } else {
                    $return['status']   = false;
                    $return['message']  = "Your account is blocked.";
                }
            } else {
                $return['status']   = false;
                $return['message']  = "Email & password not valid";
            }
        }

        return $return;
    }
    function p($data) {
       echo "<pre>"; print_r($data); exit;
    }
}

?>