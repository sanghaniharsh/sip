<?php 
    function p($data, $continue = false) {
        echo '<pre>'; print_r($data);
        if (!$continue) {
            die;
        }
    }

    function check_login() {
        $ci     = & get_instance();
        if (!$ci->get_authorized_user()) {
            redirect(base_url());
        }
    }

    function initPagination() {
        $config                     = array();
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="pagination pagination-lg d-flex justify-content-center">';
        $config['full_tag_close']   = '</ul>';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active page-item"><a class="page-link" href="javascript:void()">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        return $config;
    }
    
    function slug($str) {
        $str = preg_replace('/[^a-z0-9[:space:]]/', '', strtolower($str));
        $str = str_replace(' ', '-', $str);
        return $str;
    }

    function admin_url() {
        return base_url().'admin/';
    }
    
    function enterprise_url() {
        $ci             = & get_instance();
        $enterprise     = $ci->uri->segment('2');

        if($enterprise != "" && $enterprise != "check_login") {
            return base_url().'enterprise/'.$enterprise.'/';
        } else {
             return base_url().'enterprise/';
            
        }
        
    }
    
    function admin_img_url() {
        return base_url().'assets/admin-side/images/';
    }

    function asset_url() {
        return base_url().'assets/admin-side/';
    }

    function enterprise_asset_url() {
        return base_url().'assets/enterprise-side/';
    }
    
    function front_asset_url() {
        return base_url().'assets/front-side/';
    }
    

    function description($description, $length) {
        $str        = '';
        $description= preg_replace("/<img[^>]+\>/i", "", $description);
        $description= preg_replace("/&lt;iframe.*?>/", "", $description);
        $description= preg_replace("/<iframe.*?>/", "", $description);
        if (strlen($description) > $length) {
            $str = substr($description, 0, $length).'...';
        } else {
            $str = $description;
        }
        return $str;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    function encreptIt($encrept_id = '') {
        $ci             = & get_instance();
        $encrept_str    = '';
        if ($encrept_id != '') {
            $encrept_str = urlencode(base64_encode($ci->config->item('SITE_NAME')).base64_encode($encrept_id));
        }
        return $encrept_str;
    }

    function decreptIt($encrept_id = '') {
        $ci             = & get_instance();
        $decrept_id     = '';
        if ($encrept_id != '') {
            $temp_id        = urldecode($encrept_id);
            $encoded_site   = base64_encode($ci->config->item('SITE_NAME'));
            $temp_var       = explode($encoded_site, $temp_id);
            $decrept_id     = base64_decode($temp_var[1]);
        }

        return $decrept_id;
    }

    function correctOrientation($file_path) {
        $file_info = pathinfo($file_path);

        if (! empty($file_info) && $file_info['extension'] == 'jpg') {
            ini_set('memory_limit', '1028M');

            $exif = @exif_read_data($file_path);
            if (!empty($exif['Orientation'])) {
                $imageResource = imagecreatefromjpeg($file_path); // provided that the image is jpeg. Use relevant function otherwise
                switch ($exif['Orientation']) {
                    case 3:
                        $image = imagerotate($imageResource, 180, 0);
                        break;
                    case 6:
                        $image = imagerotate($imageResource, -90, 0);
                        break;
                    case 8:
                        $image = imagerotate($imageResource, 90, 0);
                        break;
                    default:
                        $image = imagerotate($imageResource, 0, 0);;
                } 

                $fullpath = $file_info['dirname'].'/'.$file_info['basename'];
                imagejpeg($image, $fullpath, 100);
                imagedestroy($imageResource);
                imagedestroy($image);
            }
        }
    }
    
    function not_found() {
        $ci     = & get_instance();
        $data   = array();
        $ci->template->view("front/not_found", $data);
    }

    function display_order($id = 0) {
        $return = '';
        if ($id > 0) {
           $return = "SIP" . str_pad($id,5,"0",STR_PAD_LEFT);
        }
        return $return;
    }
    
    function display_enterprise_order($id = 0) {
        $return = '';
        if ($id > 0) {
           $return = "ENTSIP" . str_pad($id,5,"0",STR_PAD_LEFT);
        }
        return $return;
    }

    function display_amount($amount) {
        return ($amount > 0) ? number_format($amount, 2, '.', ',') : 0;
    }
    function getRealIpAddr() {
        $ip = '';

        if ( isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy        
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
 ?>