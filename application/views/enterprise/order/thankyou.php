<div class="main container">
    <div class="col-main thank-you-page">
        <div class="row">
            <div class="main-heading">
                <h1>Order Successful</h1>
                <hr>
                <h2>Hello <span><?php echo (isset($user_name)) ? trim(ucfirst($user_name)) : ''; ?></span></h2>
            </div>
        </div>
        <div class="row sales_info">
            <div class="col-md-12 text-center successfull_block">
                <i class="fa fa-check"></i>
                <h3>
                    We have received your order, Thank you
                </h3>
                <p>
                    We will contact you if we have any questions about your order.<br>
                    A team member will contact you about an estimated delivery or pickup date.
                </p>
                <hr>
                <a href="<?php echo enterprise_url(); ?>" class="btn btn-default">Back to Home</a>
            </div>
        </div>
    </div>
</div>