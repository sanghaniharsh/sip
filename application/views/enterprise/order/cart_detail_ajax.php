<?php if (isset($cart) && empty($cart)) { ?>
  

    <div class="page-order">

        <div class="heading-counter">

            <div class="row empty-cart-block">

                <div class="col-sm-12">

                    <i class="fa fa-shopping-cart"></i>

                    <h3>Your shopping cart is empty</h3>

                    <hr>

                    <a class="btn btn-default btn-lg" href="<?php echo enterprise_url(); ?>product">

                        SEARCH FOR PRODUCTS

                    </a>

                </div>

            </div>

        </div>

    </div>

<?php } else { ?>

    <div class="page-order">

        <div class="heading-counter warning">

            <div class="cart_contains">

                Your shopping cart contains :  

                <strong>

                    <span class="total_items"><?php echo isset($cart_count) ? $cart_count :'' ?></span>

                    <span> Product</span>

                </strong>

            </div>
        </div>

        <div class="order-detail-content">

            <form action="<?php echo enterprise_url().'checkout'; ?>" id="cart_details_form" method="post" autocomplete="nope">

                <div class="table-responsive">

                    <table class="table table-bordered cart_summary">

                        <thead>

                            <tr>

                                <th class="cart_product">Product Image</th>

                                <th>Description</th>

                                <th style="text-align: center;">Unit price</th>

                                <th style="text-align: center;">Qty</th>

                                <!-- <th style="text-align: center;">Shipping</th> -->

                                <th style="text-align: center;">Total</th>

                                <th class="action"><i class="fa fa-trash-o"></i></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php  
                                $tempcount = 0;

                                $totalAll           = 0; 

                                $shipping_price     = 0;

                                $ground_amount      = '';

                                $other_amount       = '';

                                $ups_shipping_class = "hide";
                                
                                $weights=0;

                            ?>
                            <?php if (isset($cart) && !empty($cart)) { 

                            // var_dump($cart);

                            foreach ($cart as $row) { ?>

                                        <?php 
                                        

                                            $totalAll   = $totalAll + ($row->quantity * $row->price );
                                            if(isset($row->weight) && $row->weight != ""){
                                                $weights += $row->weight;
                                            }
                                            if(isset($row->shipping_option) && $row->shipping_option == 'free'){
                                                  $shipping_price = 0;
                                            }

                                            if (isset($row->shipping_option) && $row->shipping_option == 'manual') {?> 

                                                    <input type="hidden" class="manual_rate_<?php echo $row->slug; ?>" name="manual_rate" id="manual_rate" value="<?php echo $row->shipping_price; ?>">
                                                    <?php


                                                $shipping_price   = $shipping_price + ($row->quantity * $row->shipping_price) ;

                                            }

                                            

                                            if (isset($row->shipping_option) && $row->shipping_option == 'ups_shipping') {

                                                $ups_shipping_class = "";

                                            }

                                        ?>



                                        <tr>

                                            <td class="cart_product">

                                                <?php 
                                                $images[0] =  '';

                                                if (isset($row->product_images) && $row->product_images != "") {

                                                    $images = explode(',',$row->product_images);

                                                }

                                                ?>

                                                <a href="<?php echo  enterprise_url(); ?>product/<?php echo isset($row->id) && $row->id != '' ? encreptIt($row->id) : ''; ?>"><img src="<?php echo base_url().'attachment/image/71/0/'.md5($images[0]); ?>" alt="Product"></a>

                                            </td>

                                            <td class="cart_description">

                                                <p class="product-name">

                                                    <a href="<?php echo  enterprise_url(); ?>product/<?php echo isset($row->id) && $row->id != '' ? encreptIt($row->id) : ''; ?>"><?php echo isset($row->name) && $row->name != '' ? $row->name : ''; ?> </a>

                                                </p>

                                                <small class="view_options">

                                                    <!-- <a href="#">Category : <?php echo isset($row->category_name) && $row->category_name != '' ? $row->category_name : ''; ?></a> -->

                                                    <?php if (isset($row->questions) && ! empty($row->questions)) { ?>

                                                        <a href="#" data-toggle="modal" data-target="#options_modal_<?php echo $row->cart_id; ?>">View list of options I selected</a>

                                                    <?php } ?>

                                                </small>

                                            <td class="price">

                                                <span>
                                                    <?php if($row->awarded_price == '1') { ?>
                                                        Best Pricing Will Be Awarded
                                                    <?php } else {  ?>
                                                        $<?php echo isset($row->price) ? display_amount($row->price) : 0; ?>
                                                    <?php } ?>
                                                </span>

                                            </td>

                                            <td class="qty">
                                                    <input id="quantity_change <?php echo 'prd_'. $tempcount;?>" class="form-control input-sm quantity_change quantity_<?php echo $row->slug; ?>"data-rel="<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>" data-id="<?php echo isset($row->cart_id) && $row->cart_id != '' ? encreptIt($row->cart_id) : ''; ?>" type="number" min="1" value="<?php echo isset($row->quantity) && $row->quantity != '' ? $row->quantity : ''; ?>">
                                                <input type="hidden" name="qty_slug" id="qty_slug" value="<?php echo $row->slug; ?>">
                                            </td>

                                            <!-- <td class="shipping price">

                                                <input type="hidden" class="ship_type_<?php echo $row->slug; ?>" name="ship_type_" id="ship_type_" value="<?php echo (isset($row->shipping_option) ? $row->shipping_option : ''); ?>">

                                                <span id="total_shipping" class="total_shipping_<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">

                                                    <?php if (isset($row->shipping_option) && $row->shipping_option == 'contact_us') { ?>

                                                        <p class="" style="font-size: 12px;"><a href="javascript:;" style="font-weight: bold;color: #6089ff;" data-toggle="modal" data-target="#shippingDetailsModal">Contact us </a>for the best shipping price.</p>

                                                    <?php } else if (isset($row->shipping_option) && $row->shipping_option == 'ups_shipping'){ ?>

                                                        Calculate by Fedex

                                                    <?php } else if (isset($row->shipping_option) && $row->shipping_option == 'manual'){ ?>

                                                       <span class="shipping in_shipping_total ship_<?php echo $row->slug; ?>" data-amount="<?php echo (isset($row->shipping_price) && $row->shipping_price > 0 ) ?  number_format($row->shipping_price, 2) : '0.00'; ?>">$<?php echo (isset($row->shipping_price) && $row->shipping_price > 0 ) ?  number_format(($row->quantity * $row->shipping_price), 2) : '0.00'; ?></span> 
                                                    <?php } else { ?>

                                                        <?php echo $shipping_price; ?>

                                                    <?php } ?>

                                                </span>

                                            </td> -->

                                            <td class="price">

                                                <span class="total_price_<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">

                                                    <?php 

                                                    $product_total = $row->price;

                                                    if (isset($row->quantity) && isset($row->price)) {
                                                        $product_total = ($row->quantity * $row->price);
                                                    }

                                                    ?>

                                                   $<?php echo ($product_total > 0) ?  number_format($product_total, 2) : '0'; ?>

                                                </span>

                                            </td>

                                            <td class="action">

                                                <a href="#" class="delete_cart_item" data-id="<?php echo isset($row->cart_id) && $row->cart_id != '' ? encreptIt($row->cart_id) : ''; ?>"><i class="icon-close"></i></a>

                                            </td>

                                        </tr>

                                <?php $tempcount++; } ?>

                            <?php } else { ?>

                                        <tr>

                                            <td colspan="6">

                                                <center><p>Cart Is Empty</p></center>

                                            </td>

                                        </tr>

                            <?php } ?>         

                        </tbody>

                        <tfoot class="discount_block">

                            <tr>

                                <td colspan="2" rowspan="4" class="calculate_discount">


                                </td>

                                <td colspan="3">Sub Total</td>

                                <td colspan="2" class="text-right in_final_total sub_total_text" data-amount="<?php echo ($totalAll > 0) ? $totalAll : 0; ?>"><?php echo ($totalAll > 0) ? "$" . number_format($totalAll, 2) : "Free" ; ?> </td>

                            </tr>

                            <?php 

                            // $tax        = 0; 
                            $sales_tax  = 0; 


                            // if(isset($this->loginUser['shipping_state']) && $this->loginUser['shipping_state'] == "CT" ){
                            //     $tax = $totalAll * $this->config->item('TAX_RATE') / 100;
                            // }
                            if(isset($this->loginUser['sales_tax']) && $this->loginUser['sales_tax'] > 0){
                                $sales_tax = $totalAll * $this->loginUser['sales_tax'] / 100;
                            }

                            ?>

                            <!-- <tr class="">

                                <td colspan="3">Tax</td>

                                <td colspan="2" class="text-right total_tax in_final_total" data-amount="<?php echo $tax; ?>">$<?php echo number_format($tax, 2); ?> </td>

                            </tr> -->

                            <tr class="">

                                <td colspan="3">Sales Tax</td>

                                <td colspan="2" class="text-right total_tax in_final_total" data-amount="<?php echo $sales_tax; ?>">$<?php echo number_format($sales_tax, 2); ?> </td>

                            </tr>

                            <tr class="hide">

                                <td colspan="3"><strong>Discount Amount</strong></td>

                                <td colspan="2" class="text-right"><strong>0.00 </strong></td>

                            </tr>

                            <tr>

                                <!-- <td colspan="3" class="shipping_calculator">

                                    <span class="calc_title">Shipping Amount</span>

                                         <span class="calc_input <?php echo $ups_shipping_class; ?>">

                                             <select class="form-control" name="address_type">

                                            <option value="0">Commercial</option>

                                            <option value="1">Residential</option>
                                    
                                        </select> -->

                                        <!-- <div class="row">
                                            <?php if($weights > 140){ ?>

                                         This two line has been removed by kzis1987
                                            <!-- <input type="text" name="shipping_city" class="form-control" placeholder="Enter City" autocomplete="sasasa" style="width: 100px;"> -->
                                            <!-- <input type="text" name="shipping_state" class="form-control" placeholder="State Code" autocomplete="sasasa"style="width: 80px;"> -->
                                            <!-- <input type="text" name="shipping_zipcoe" class="form-control" placeholder="ZIP Code" autocomplete="sasasa"style="width: 80px;"  -->
                                            >

                                            <!-- <select class="form-control" id="isResidential" name="isResidential"> -->

                                                <!-- <option value="0">Commercial</option>
                                                <option value="1">Residential</option> -->
                                    
                                            <!-- </select> -->

                                        <!-- <?php }else{ ?>  -->
                                       <!--  <input type="text" name="shipping_city" class="form-control" placeholder="Enter City" autocomplete="sasasa" style="width: 100px;">
                                        <input type="text" name="shipping_state" class="form-control" placeholder="State Code" autocomplete="sasasa"style="width: 80px;"> -->
                                        <!-- <input type="text" name="shipping_zipcoe" class="form-control" placeholder=" ZIP Code" autocomplete="sasasa"style="width: 80px;" > -->
                                    <!-- <?php } ?> -->
                                        <!-- data-toggle="modal" data-target="#myModal" -->
                                        <!-- <button class="calculate_shipping_btn" >Calculate</button> -->
                                    <!-- </div> -->
                                    <!-- </span> -->

                                <!-- </td> -->

                                <!--<td colspan="3" class="text-right"><?php echo ($shipping_price > 0) ? "$" . number_format($shipping_price, 2) : "Free" ; ?> </td>-->

                                <!-- <td colspan="2" class="text-right">$<span class="total_shipping_amount in_final_total" id="total_shipping_amount" data-amount="<?php echo ($shipping_price > 0) ? number_format($shipping_price, 2) : "0.00" ; ?>"><?php echo ($shipping_price > 0) ? number_format($shipping_price, 2) : "0.00" ; ?></span></td> -->



                            <!-- </tr> -->

                            <tr>

                                <td colspan="3"><strong>Total</strong></td>

                                <td colspan="2" class="text-right">$<strong class="total_amount"><?php echo number_format($totalAll + $sales_tax +$shipping_price, 2); ?></strong></td>

                            </tr>

                        </tfoot>

                    </table>
                    <div class="box-border ent_terms_box">
                        <div class="delivery_info_ent">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="1" name="delivery">
                                <b>I will pick up the items and I do not require delivery</b>
                            </label>
                            <div class="cart_info">
                                <span>
                                    <i class="fa fa-info-circle"></i>
                                </span>
                                <span>
                                    Delivery fees will be calculated and presented prior to delivery.
                                </span>
                            </div>
                        </div>
                        <ul>
                            <li style="margin: 0;">
                                <div class="">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" name="terms">
                                        I agree to the <a href="<?php echo base_url() ?>page/user-agreement" target="_blank" class="color_blue">User/Purchase Agreement</a> & <a href="<?php echo base_url() ?>page/privacy-policy" class="color_blue" target="_blank">Privacy Policy</a>
                                    </label>
                                    <div class="checkbox_error"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="cart_navigation">

                        <a class="continue-btn btn btn-primary" href="<?php echo enterprise_url(); ?>product">

                            <i class="fa fa-arrow-left"> </i>&nbsp; Continue shopping

                        </a>

                        <button type="submit" class="checkout-btn">

                            <i class="fa fa-check"></i> Place an Order

                        </button>

                    </div>

                </div>

            </form>

        </div>

    </div>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Fedex Shipping Details</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
              <thead>
                  <tr>
                      <th>DELIVERED BY</th>
                      <th>DELIVERED ON</th>
                      <th>PRICE</th>
                  </tr>
              </thead>
              <tbody id="shipping-data">
                  
              </tbody>
          </table>
        </div>
        <span id="shipping-data"></span>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<?php } ?>

<div id="shippingDetailsModal" tabindex="-1" role="dialog" aria-labelledby="shippingDetailsModallLabel" aria-hidden="true" class="modal fade">

    <div class="modal-dialog modal-login">

        <div class="modal-content">

            <div class="modal-header">				

                <h4 class="modal-title">Contact Details</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

            <div class="modal-body">				

                <div class="row">

                    <div id="contact_form_map" class="col-xs-12 col-sm-12">

                        <!--<h3 class="page-subheading">Let's get in touch</h3>-->

                        <ul class="store_info">

                            <li><i class="fa fa-phone"></i><span><a href="tel:860-282-8733">860-282-8733</a></span></li>

                            <li><i class="fa fa-envelope"></i>Email: <span><a href="javascript:void(0)">Info@SaveInParadise.com</a></span></li>

                        </ul>

                    </div>

                </div>

            </div>

            <div class="modal-footer">

                <button type="button" name="" data-dismiss="modal" aria-hidden="true" class="btn btn-primary pull-right">Cancel</button>

            </div>

        </div>

    </div>

</div>