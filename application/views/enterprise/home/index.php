<div class="main-slideshow enterprise_login">
    <div class="slider_text_block">
        <div class="container ent_login">
            <form name="loginForm" id="loginForm" method="POST" action="<?php echo enterprise_url() . 'check_login' ?>">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="modal-title">Enterprise Portal Login</h3>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="pull-left">Email</label>
                            <input type="text" class="form-control" name="email" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="clearfix">
                                <label class="pull-left">Password</label>
                                <!--<a href="javascript:void();" class="pull-right text-muted forgot_password "><small>Forgot password?</small></a>-->
                            </div>
                            <input type="password" class="form-control" name="password" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label  class="pull-left">Enter Captcha</label>
                            <input class="form-control register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <img id="captcha_code" src="<?php echo base_url() . 'secureimage/securimage'; ?>" style="width: 170px">
                    <span class="reload_captcha">
                        <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                    </span>
                </div>
                <!-- <div class="form-group">
                    <label>Enter Captcha</label>
                    <input class="form-control register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text" >
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <label class=" pull-left"><input type="checkbox" name="remember_me"> Remember me</label>
                        <button type="submit" name="login_btn" id="login_btn" class="btn btn-primary pull-right">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- .fullwidth -->

<!-- COLLECTION-AREA-START -->
<section class="testimonials">
    <div class="page-header-wrapper">
        <div class="container">
            <div class="page-header text-center wow fadeInUp">
                <h2 class="blue">est. 1998</h2>
            </div>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            <p class="lead" style="color: #fff;text-align: center;"> 
                Restaurant Equipment Paradise has been in business for more than 20 years.
            </p>
            <p class="lead" style="color: #fff;text-align: center;">We have helped thousands of clients with their Restaurant and Commercial Kitchen  needs.</p>
        </div>
    </div>
</section>
<!-- home contact -->
<section id="contact" class="gray">
    <div class="container"> 

        <!-- Begin page header-->
        <div class="page-header-wrapper">
            <div class="container">
                <div class="page-header text-center wow fadeInUp">
                    <h2 class="blue">Contact Us</h2>
                    <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                    <p class="lead text-gray" style="margin: 0;"> Hours of operation:</p>
                    <p class="lead text-gray" style="margin: 0;"> Monday - Friday: 8am to 5pm (we are open)</p>
                    <p class="lead text-gray"> Saturday:  closed until further notice</p>
                </div>
            </div>
        </div>
        <!-- End page header-->

        <div class="row">
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-home fa-2x"></i>
                <h3>Our Address</h3>
                <span class="font-l">465 Park Avenue <br> East Hartford, CT 06108</span> 
            </div>
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-comment fa-2x"></i>
                <h3>Our mail</h3>
                <span class="font-l">Info (at) SaveInParadise.com</span> </div>
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-phone fa-2x"></i>
                <h3>Call Us</h3>
                <span class="font-l">860.282.8733</span> </div>
        </div>
    </div>
</section>