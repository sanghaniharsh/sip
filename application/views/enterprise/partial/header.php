<div class="top-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-6 mobile_none">
                <!-- <h4><i class="fa fa-truck"></i> Free Shipping on Many Items</h4> -->
            </div>
            <div class="col-md-3 text-right">
                <h4><a href="mailto:info@saveinparadise.com" style="color: #fff;"><i class="fa fa-envelope"></i> info@saveinparadise.com</a></h4>
            </div>
            <div class="col-md-2 text-right">
                <h4><a href="tel:123-456-7890" style="color: #fff;"><i class="fa fa-phone"></i> 860-282-8733</a></h4>
            </div>
        </div>
    </div>
</div>
<header class="enterprise_header">
    <div class="container">
        <div class="header-container">
            <?php if(isset($enterprise_user->image)) { ?>
            <div class="logo ent_logo">
                <a title="e-commerce" href="<?php echo enterprise_url(); ?>" class="logo-color">
                    <img class="category_img" src="<?php echo base_url().'attachment/image/0/0/'.md5($enterprise_user->image); ?>" alt="">
                    <label>&nbsp;<?php echo $enterprise_user->name; ?></label>
                </a> 
            </div>
            <div class="site_logo">
                <a title="" href="<?php echo base_url(); ?>" target="_blank">
                    <img src="<?php echo front_asset_url() ?>images/logo-main.png">
                    <label>Exclusive Enterprise Client Portal</label>
                </a>
            </div>
            <?php } else { ?>
            <img src="<?php echo front_asset_url() ?>images/logo-main.png" class="default_logo">
            <?php } ?>
            <div class="right_enterprise">
                <div class="header_login_btns text-right">
                    <?php if(isset($loginUser) && !empty($loginUser) ) { ?>
                    <div class="language-currency-wrapper">
                        <div class="inner-cl">
                            <div class="block block-currency">
                                <div class="item-cur"> <span>Hi, <?php echo ucfirst($loginUser['firstname']); ?> </span> <i class="fa fa-angle-down"></i></div>
                                <ul>
                                    <!-- <li> <a href="<?php echo enterprise_url() ?>myaccount">My Account</a> </li> -->
                                    <li> <a href="<?php echo enterprise_url() ?>myaccount">Order History</a> </li>
                                    <li> <a href="<?php echo enterprise_url() ?>logout">Logout</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <!--<button class="button do_login_class" style="width: 25%;">Login</button>-->
                    <?php } ?>
                </div>
                <?php if(isset($loginUser) && !empty($loginUser) ) { ?>
                <div class="" style="padding-right: 0;">
                    <div class="mm-toggle-wrap">
                        <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
                    </div>
                    <div class="top-cart-contain">
                        <div class="mini-cart">
                            <div class="basket dropdown-toggle text-right"> 
                                <a href="<?php echo enterprise_url() ?>cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="cart-title">View Cart</span>
                                </a>
                                <span class="cart_total_item">
                                    <?php if (isset($cart_count) && ($cart_count > 0) ) { ?>
                                        <span class="cart_badge cart_total_item"> <?php echo $cart_count; ?> </span>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</header>
<script>
    var cart_count_header = <?php echo isset($cart_count) && ($cart_count > 0) ?  $cart_count : 0; ?> 
</script>