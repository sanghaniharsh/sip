<div class="modal-header">
    <!-- <h4 class="modal-title">R</h4> -->
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="product-item">
    <div class="item-inner">
        <div class="product-thumbnail">
            <div class="pr-img-area">
                <figure>
                    <?php
                    $img_data[0] = '';
                    if (isset($product_details->product_images) && $product_details->product_images != '') {
                        $img_data = explode(',', $product_details->product_images);
                    }
                    ?>
                    <a href="<?php echo enterprise_url(); ?>product/<?php echo isset($product_details->id) && $product_details->id != '' ? encreptIt($product_details->id) : ''; ?>" target="_blank">
                        <img class="first-img" src="<?php echo base_url() . 'attachment/image/0/350/' . md5($img_data[0]); ?>" alt="">
                    </a>
                </figure>
                <a href="<?php echo enterprise_url(); ?>product/<?php echo isset($product_details->id) && $product_details->id != '' ? encreptIt($product_details->id) : ''; ?>" class="add-to-cart-mt" target="_blank">
                    <i class="fa fa-shopping-cart"></i>
                    <span> View details</span>
                </a>
            </div>
        </div>
        <div class="item-info">
            <div class="item-title">
                <a href="<?php echo enterprise_url(); ?>product/<?php echo isset($product_details->id) && $product_details->id != '' ? encreptIt($product_details->id) : ''; ?>" target="_blank">
                    <?php echo isset($product_details->name) && $product_details->name != '' ? $product_details->name : ''; ?>
                </a>
            </div>
            <div class="item-price">
                <?php if (isset($product_details->call_for_price) && ($product_details->call_for_price == '1')) { ?>
                    <h6 class="call_for_price">Call for discounted price</h6>
                <?php } else { ?>
                    <p class="special-price">
                        <span class="price">
                            $<?php echo isset($product_details->price) ? display_amount($product_details->price) : 0; ?>
                        </span>
                    </p>
                    <?php if (isset($product_details->product_price) && $product_details->product_price > 0) { ?>
                        <p class="old-price">
                            <span class="price">
                                $<?php echo isset($product_details->product_price) ? display_amount($product_details->product_price) : 0; ?>
                            </span>
                        </p>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>