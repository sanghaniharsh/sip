<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="product-name">

                <h1 style="margin-top: 10px;"><?php echo (isset($product->name) && $product->name != '') ? $product->name : ''; ?></h1>

            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-main">

            <div class="product-view-area">

                <div class="product-big-image col-xs-12 col-sm-8 col-lg-8 col-md-8">

                    <?php 

                    $images[0] =  '';

                    if(isset($product->product_images) && $product->product_images != "") {

                        $images = explode(',',$product->product_images);

                    }


                    ?>

                    <div class="large-image"> 

                        <a href="<?php echo base_url().'attachment/image/0/394/'.md5($images[0]); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 

                            <img class="zoom-img" src="<?php echo base_url().'attachment/image/0/394/'.md5($images[0]); ?>" alt="products"> 

                        </a> 

                    </div>

                    <div class="flexslider flexslider-thumb">

                        <ul class="previews-list slides">

                            <?php if(isset($images) && !empty($images)) { ?>

                                <?php foreach($images as $image) { ?>

                                    <li>

                                        <a href='<?php echo base_url().'attachment/image/0/394/'.md5($image);?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo base_url().'attachment/image/0/394/'.md5($image);?>' ">

                                            <img src="<?php echo base_url().'attachment/image/0/89/'.md5($image);?>" alt = "Thumbnail 2"/>

                                        </a>

                                    </li>

                                <?php } ?>

                            <?php } ?>

                        </ul>

                    </div>
                </div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5947de0d00e5fb05"></script>
                <div class="col-xs-12 col-sm-4 col-lg-4 col-md-4">

                    <div class="product-details-area">

                        <?php if (isset($product->call_for_price) && ($product->call_for_price == '1')) { ?>

                            <h4 class="call_for_price_details">Call <a href="tel:860-282-8733">860-282-8733</a> for Discounted Price</h4>

                        <?php } else { ?>

                            <div class="price-box">

                                <p class="special-price"> <span class="price-label">Special Price</span> 

                                <?php if($product->awarded_price == '1') { ?>
                                    <h4>Best Pricing Will Be Awarded</h4>
                                <?php } else {  ?>
                                    <span class="price">$<?php echo isset($product->price) ? display_amount($product->price) : 0; ?></span> 
                                <?php } ?>



                                </p>
                                <!-- <?php echo($product->weight); ?> -->

                                <?php if (isset($product->list_price) && $product->list_price > 0 ) { ?>

                                    <p class="old-price"> <span class="price-label">Regular Price:</span> 

                                        <span class="price">$<?php echo isset($product->list_price) ? display_amount($product->list_price) : 0; ?></span>

                                    </p>

                                <?php } ?>

                            </div>

                        <?php } ?>


                        <hr>

                        <h4>MODEL #: <?php echo isset($product->model_number) && $product->model_number != '' ? $product->model_number : ''; ?></h4>

                        <form action="" method="post" class="add-cart-frm">

                            <?php if (isset($questions) && (! empty($questions))) { ?>

                                <?php foreach($questions as $question) { 

                                    $options = json_decode($question->options, true);

                                    ?>

                                    <div class="product_questions">

                                        <label><?php echo $question->question; ?></label>

                                        <select class="input form-control" name="question[<?php echo $question->id; ?>]">

                                            <option value="">Select option</option>

                                            <?php foreach($options as $option) { ?>

                                                <option value="<?php echo $option; ?>"><?php echo $option; ?></option>

                                            <?php } ?>

                                        </select>

                                    </div>

                                <?php } ?>

                            <?php } ?>

                            <hr>

                            <div class="addon_block" style="display:none;">

                                <h4 style="color: #054988">Check to add item(s) to your cart</h4>

                                <ul class="list-group addon_products" style="margin: 0;">

                                </ul>

                                <input type="hidden" class="product_id" value="<?php echo isset($product->id) && $product->id != '' ? encreptIt($product->id) : ''; ?>">

                                <hr>

                            </div>

                            <div class="product-variation add_to_cart_custom_block  add_to_cart_block" data-id="<?php echo isset($product->id) ? encreptIt($product->id) :'';?>">

                                <?php if(isset($cart_data) && !empty($cart_data) ) { ?>
                                    <h5 class="added_in_text"><i class="fa fa-check"></i> Added to your cart</h5>

                                    <button type="button" data-id="<?php echo isset($cart_data->id) && $cart_data->id != '' ? encreptIt($cart_data->id) : ''; ?>" class="btn btn-default delete_cart_item"  title="Remove Cart" type="button">

                                        <span><i class="fa fa-remove"></i> Remove from Cart</span>

                                    </button>

                                    <a href="<?php echo base_url() ?>order/cart" class="btn btn-default">

                                    <i class="fa fa-shopping-cart"></i> View Cart

                                    </a>
                                <?php } else {  ?>

                                    <?php if (isset($product->call_for_price) && ($product->call_for_price == '1')) { ?>

                                        <button class="button pro-add-to-cart contact_us_btn" title="Call for discounted price"><span><i class="fa fa-envelope"></i> Contact Us</span></button>

                                    <?php } else { ?>

                                        <div class="cart-plus-minus" style="display:<?php echo isset($product->type) && ($product->type == 1) ? 'none;': '' ?>">

                                            <label for="qty">Quantity:</label>

                                            <div class="numbers-row">

                                                <!--<div onClick="var result = document.getElementById('qty'); var qty = result.value; if (!isNaN(qty) & amp; & amp; qty & gt; 0) result.value--; return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>-->

                                                <div onClick="var result = document.getElementById('qty');

                                                    var qty = result.value;

                                                    if (!isNaN(qty) && qty > 1 ) {
                                                        result.value--;
                                                    }       
                                                    return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i>

                                                </div>

                                                <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="quantity">

                                                <div onClick="var result = document.getElementById('qty');

                                                    var qty = result.value;

                                                    if (!isNaN(qty))

                                                        result.value++;

                                                    return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i>

                                                </div>

                                            </div>

                                        </div>

                                        <input type="hidden" name="product_id" value="<?php echo isset($product->id) && $product->id != '' ? encreptIt($product->id) : ''; ?>">

                                        <button type="submit" class="button pro-add-to-cart add-to-cart-btn" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>

                                    <?php } ?>

                                <?php } ?>

                            </div>

                        </form>

                        <!-- <div class="row support-client sidebar_free_shipping">

                            <div class="col-md-12 col-sm-10">

                                <div class="box-container free-shipping">

                                    <div class="box-inner">

                                        <?php $shipping_option = (isset($product->shipping_option)) ? $product->shipping_option : 'free'; ?>

                                        

                                        <?php if ($shipping_option == 'contact_us') { ?>

                                            <p><a href="javascript:;" style="color: #054988;text-decoration: underline;" class="display_popup">Contact us </a>for the best shipping price.</p>

                                        <?php } else if ($shipping_option == 'manual') { ?>

                                            <h2><?php echo '$'.$product->shipping_price; ?></h2>

                                            <p>Shipping charges will apply</p>

                                        <?php } else if ($shipping_option == 'ups_shipping') { ?>

                                            <h2>Shipping charges </h2>

                                            <p>will calculate on cart page</p>

                                        <?php } else { ?>

                                            <h2>FREE SHIPPING ON THIS ITEM</h2>

                                            <p>No delivery charge</p>

                                        <?php } ?>

                                        

                                        <?php /*if(isset($product->is_free_shipping) && $product->is_free_shipping != '' && $product->is_free_shipping == '1') { ?>

                                            <h2>FREE SHIPPING ON THIS ITEM</h2>

                                            <p>No delivery charge</p>

                                        <?php } else if(isset($product->display_shipping_details) && $product->display_shipping_details == '1') { ?>

                                            <p><a href="javascript:;" style="color: #054988;text-decoration: underline;" class="display_popup">Contact us </a>for the best shipping price.</p>

                                        <?php } else { ?>

                                            <h2><?php echo '$'.$product->shipping_price; ?></h2>

                                            <p>Shipping charges will apply</p>

                                        <?php } */?>

                                    </div>

                                </div>

                            </div>

                        </div> -->

                        <?php if (isset($product->video_url) && $product->video_url != '') { ?>

                            <div class="row">

                                <div class="col-md-12 text-center watch_video_block">

                                    <a href="#" class="btn btn-default product_video_btn"><i class="fa fa-video-camera" aria-hidden="true"></i> Watch video</a>

                                </div>

                            </div>

                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>

        <div class="product-overview-tab wow fadeInUp">



            <div class="col-xs-12">

                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">

                    <li class="active"> 

                        <a href="#description" data-toggle="tab"> Description </a> 

                    </li>

                    <li>

                        <a href="#specification" data-toggle="tab">Specifications</a>

                    </li>
                    
                    <li> 

                        <a href="#warranty" data-toggle="tab">Warranty</a> 

                    </li>

                </ul>

                <div id="productTabContent" class="tab-content">

                    <div class="tab-pane fade in active" id="description">

                        <?php if (isset($product->description) && $product->description != '') {

                                echo $product->description; 

                        } else { ?>

                            <p class="text-justify">

                                        No Description Available.

                            </p>

                        <?php } ?>

                    </div>

                    <div class="tab-pane fade" id="specification">

                        <?php if (isset($product->specification) && $product->specification != '') {

                            echo $product->specification; 

                        } else { ?>

                            <p class="text-justify">

                                No Specifications Available.

                            </p>

                        <?php } ?>


                    </div>
                    
                    <div class="tab-pane fade" id="warranty">

                        <?php if (isset($product->warranty) && $product->warranty != '') {

                                echo $product->warranty; 

                        } else { ?>

                                No Warranty Available.

                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="col-main">

        <div class="col-md-12 text-right">

            <div class="attention_ca">

                <span><img src="<?php echo front_asset_url() ?>images/ca_res.png"> For CA Residents: <a href="#" data-toggle="modal" data-target="#cadetails_modal">Click here for Prop 65 Warning</a></span>

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="product_video_model" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title">Video</h4>

            </div>

            <div class="modal-body">

                <?php if (isset($product->video_url) && $product->video_url != '') {



                    $video_string = '<iframe width="100%" height="350" src="' . $product->video_url . '" frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';



                    if (preg_match('/watch/', $product->video_url)) {

                        $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe src=\"https://www.youtube.com/embed/$2\" width=\"100%\" height=\"350\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>", $product->video_url);

                    }

                    echo $video_string;

                }

                ?>

            </div>

        </div>



    </div>

</div>

<div id="shippingDetailsModal" tabindex="-1" role="dialog" aria-labelledby="shippingDetailsModallLabel" aria-hidden="true" class="modal fade">

    <div class="modal-dialog modal-login">

        <div class="modal-content">

            <div class="modal-header">				

                <h4 class="modal-title">Contact Details</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

            <div class="modal-body">				

                <div class="row">

                    <div id="contact_form_map" class="col-xs-12 col-sm-12">

                        <!--<h3 class="page-subheading">Let's get in touch</h3>-->

                        <ul class="store_info">

                            <li><i class="fa fa-phone"></i><span><a href="tel:860-282-8733">860-282-8733</a></span></li>

                            <li><i class="fa fa-envelope"></i>Email: <span><a href="javascript:void(0)">Info@SaveInParadise.com</a></span></li>

                        </ul>

                    </div>

                </div>

            </div>

            <div class="modal-footer">

                <button type="button" name="" data-dismiss="modal" aria-hidden="true" class="btn btn-primary pull-right">Cancel</button>

            </div>

        </div>

    </div>

</div>

<div id="cadetails_modal" tabindex="-1" role="dialog" aria-labelledby="cadetails_modal" aria-hidden="true" class="modal fade">

    <div class="modal-dialog modal-login">

        <div class="modal-content">

            <div class="modal-header">				

                <h4 class="modal-title">Attention CA Residents</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>

            <div class="modal-body">				

                <div class="row">

                    <div class="col-md-12">

                        <p>

                            <img src="<?php echo front_asset_url() ?>images/ca_res.png"><strong>WARNING:</strong> This product can expose you to chemicals including lead, 

                            which are known to the State of California to cause cancer, birth defects, 

                            or other reproductive harm. For more information, go to <a href="https://www.p65warnings.ca.gov/" target="_blank">www.p65warnings.ca.gov</a>

                        </p>

                    </div>

                </div>

            </div>


        </div>

    </div>

</div>