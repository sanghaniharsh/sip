<?php if (isset($products) && !empty($products)) { ?>
    <div class="product-grid-area">
        <ul class="products-grid">
            <?php foreach ($products as $key => $list) { ?>
            <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                    <div class="item-inner">
                        <div class="product-thumbnail">
                            <div class="pr-img-area">
                                <figure class="product">
                                    <div class="ent_quickinfo ent_product product_info_<?php echo isset($key ) && $key  != '' ? $key  : '0'; ?>" data-toggle="popover" id="ent_product" data-id="<?php echo isset($list->id) && $list->id != '' ? encreptIt($list->id) : '';  ?>" data-key="<?php echo isset($key) && $key != '' ? $key : '0';  ?>"><i class="fa fa-info-circle"></i><span class="quick_text">Quick Info</span></div>
                                    <?php
                                        $img_data[0] = '';
                                        if (isset($list->product_images) && $list->product_images != '') {
                                            $img_data = explode(',', $list->product_images);
                                        }
                                    ?>
                                    <a href="<?php echo enterprise_url();?>product/<?php echo isset($list->id) && $list->id != '' ? encreptIt($list->id) : ''; ?>" target="_blank">
                                        <img class="first-img" src="<?php echo base_url().'attachment/image/0/350/'.md5($img_data[0]); ?>" alt="">
                                    </a>
                                    <input type="hidden"  class="product_id" value="<?php echo isset($list->id) && $list->id != '' ? encreptIt($list->id) : ''; ?>">
                                </figure>
                                <a href="<?php echo enterprise_url();?>product/<?php echo isset($list->id) && $list->id != '' ? encreptIt($list->id) : ''; ?>" class="add-to-cart-mt" target="_blank">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span> View details</span>
                                </a>
                                <div class="add_quantity">
                                    <button class="btn product-add" title="Add to Cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span>Add To Cart</span>
                                    </button>
                                    <div class="product-action">
                                        <button class="action-minus" title="Quantity Minus">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <input class="action-input quantity" title="Quantity Number" id="quantity" data-id="<?php echo isset($list->id) && $list->id != '' ? $list->id : ''; ?>" type="text" name="quantity" readonly value="<?php echo isset($cart_item[$list->id]) && $list->id == $cart_item[$list->id]->product_id ? $cart_item[$list->id]->quantity : 0 ; ?>"/>
                                        <button class="action-plus" title="Quantity Plus">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item-info">
                            <div class="item-title"> 
                                <a href="<?php echo enterprise_url();?>product/<?php echo isset($list->id) && $list->id != '' ? encreptIt($list->id) : ''; ?>" target="_blank">
                                    <?php echo isset($list->name) && $list->name != '' ? $list->name : ''; ?>
                                    <input type="hidden" class="product_name" value="<?php echo isset($list->name) && $list->name != '' ? $list->name : ''; ?>">
                                    <input type="hidden" class="details" value="<?php echo isset($list->description) && $list->description != '' ? $list->description : ''; ?>">
                                    <input type="hidden" class="specification" value="<?php echo isset($list->specification) && $list->specification != '' ? $list->specification : ''; ?>">
                                </a>
                            </div>
                            <?php if($list->awarded_price == '1') {  ?>
                                <div class="item-price">
                                    <p class="special-price">
                                        <span class="price best_price">
                                            Best Pricing Will Be Awarded
                                        </span>
                                    </p>
                                </div>
                            <?php } ?>
                            <?php if($list->awarded_price == '0') { ?>
                                <div class="item-price">
                                    <p class="special-price">
                                        <span class="price">
                                            $<?php echo isset($list->price) ? display_amount($list->price) : 0; ?>
                                        </span>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
<?php } else { ?>
    <div class="row no-product-found">
        <div class="col-sm-12">
            <i class="fa fa-shopping-cart"></i>
            <h3>No products found for your restaurant</h3>
            <p>Please contact admin</p>
        </div>
    </div>
<?php } ?>
<div class="pagination-area wow fadeInUp">
    <?php echo isset($links) ? $links : ''; ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('mouseenter','.ent_product',function(e) {
            e.preventDefault;
            var product_id          = $(this).parents(".product-item").find('.product_id').val();
            var product_name        = $(this).parents(".product-item").find('.product_name').val();
            var descritption        = $(this).parents(".product-item").find('.details').val();
            var Specification       = $(this).parents(".product-item").find('.specification').val();
            var product_data_key    = $(this).attr('data-key');

            var detail          = "";
            // console.log(product_id);
            //  detail         += '<h5>Description</h5><p>'+descritption+'</p>';
            //  detail         += '<h5>Specification</h5><p>'+Specification+'</p>';
            if  (descritption != '') {
                detail         += '<h5>Description</h5><p>'+descritption+'</p>'
            } else {
                detail         += '<h5>Description</h5><p>No Descritption Available</p>';
            }
            if  (Specification != '') {
                detail         += '<h5>Specification</h5><p>'+Specification+'</p>'
            } else {
                detail         += '<h5>Specification</h5><p>No Specification Available</p>';
            }

        //    $('.ent_product').popover({
        //         title       : '<h4>'+product_name+'</h4>' + '<button onclick="$(this).closest(\'div.popover\').popover(\'hide\');" type="button" class="close" aria-hidden="true">&times;</button>', 
        //         // content: detail,
        //         html        : true,
        //         container   : 'body',
        //         placement   : 'bottom',
        //         trigger     : 'manual'

        //     }).on("mouseenter", function () {
        //     $('.ent_product').on("mouseenter", function () {
        //         var _this = this;
        //         $(this).popover("show");
        //         $(this).siblings(".popover").on("mouseleave", function () {
        //             $(_this).popover('hide');
        //         });
        //     }).on("mouseleave", function () {
        //         var _this = this;
        //         setTimeout(function () {
        //             if (!$(".popover:hover").length) {
        //                 $(_this).popover("hide")
        //             }
        //         }, 100);
        //     });

            // $.ajax({
            //     type : "POST",
            //     data : { product_id : product_id },    
            //     url  : enterprise_url+'product-details/product-info-modal',
            //     success: function(data) {   
            //         response    = $.parseJSON(data);
            //         if (response.status == true) {
            //             product_title   = response.product_details.name;
            //             var detail      = "";
            //             if  (response.product_details.description != '') {
            //                 detail         += '<h5>Description</h5><p>'+response.product_details.description+'</p>'
            //             } else {
            //                 detail         += '<h5>Description</h5><p>No Descritption </p>';
            //             }
            //             if  (response.product_details.specification != '') {
            //                 detail         += '<h5>Description</h5><p>'+response.product_details.specification+'</p>'
            //             } else {
            //                 detail         += '<h5>Specification</h5><p>No Specification </p>';
            //             }
            //             // detail         += '<h5>Specification</h5><p>'+response.product_details.specification+'</p>';
            //             // init_popover($(this),product_title,detail,product_data_key);
            //             //     detail         += '<h5>Specification</h5><p>'+specification+'</p>';
            //             // $('.product_info_'+product_data_key).popover({
            //             $('.product_info_'+product_data_key).popover({
            //                 title       : '<h4>'+product_title+'</h4>' + '<button onclick="$(this).closest(\'div.popover\').popover(\'hide\');" type="button" class="close" aria-hidden="true">&times;</button>', 
            //                 content     : detail,
            //                 html        : true,
            //                 container   : 'body',
            //                 placement   : 'bottom',
            //                 trigger     : 'manual'
            //             }).on("mouseenter", function () {
            //                 var _this = this;
            //                 $(this).popover("show");
            //                 $(this).siblings(".popover").on("mouseleave", function () {
            //                     $(_this).popover('hide');
            //                 });
            //             }).on("mouseleave", function () {
            //                 var _this = this;
            //                 setTimeout(function () {
            //                     if (!$(".popover:hover").length) {
            //                         $(_this).popover("hide")
            //                     }
            //                 }, 100);
            //             });
            //         }
            //     }
            // });

                // $('.product_info_'+product_data_key).popover({
                $('.product_info_'+product_data_key).popover({
                    title       : '<h4>'+product_name+'</h4>' + '<button onclick="$(this).closest(\'div.popover\').popover(\'hide\');" type="button" class="close" aria-hidden="true">&times;</button>', 
                    content     : detail,
                    html        : true,
                    container   : 'body',
                    placement   : 'bottom',
                    trigger     : 'manual'
                }).popover("show").on("mouseenter", function () {
                    var _this = this;
                    // $(this).popover("show");
                    $(this).siblings(".popover").on("mouseleave", function () {
                        $(_this).popover('hide');
                    });
                }).on("mouseleave", function () {
                    var _this = this;
                    setTimeout(function () {
                        if (!$(".popover:hover").length) {
                            $(_this).popover("hide")
                        }
                    }, 100);
                });

        });
        $(document).on('mouseleave','.popover-content',function(e) {
                $(".popover").popover('hide');
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });

</script>