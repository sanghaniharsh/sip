
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
    <body>
        <table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
            <tr>
                <td bgcolor="" style="">
                    <table width="100%" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" style="padding:20px 0px;background-color: #fff;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 50%;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="" style="padding:20px; ">
                                <table width="100%" cellspacing="0" cellpadding="0" style="">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello <?php echo isset($user_details->firstname) ? ucfirst($user_details->firstname) : '' ?>,</td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Order has created by <b><?php echo isset($enterprise->name) ? $enterprise->name : '';?></b> user, Please see order details below</td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" style="font-family: Arial,sans-serif;font-size: 14px;line-height: 22px;">
                                                            <h4 style="margin: 0;padding-bottom: 10px;font-size: 18px;">To</h4>
                                                            <p style="margin: 0;"><?php echo (isset($userDetail->firstname) || isset($userDetail->lastname)) ? $userDetail->firstname . ' ' . $userDetail->lastname : ''; ?></p>
                                                            <p style="margin: 0;"><?php echo isset($userDetail->email) ? $userDetail->email : '';?></p>
                                                            <p style="margin: 0;"><?php echo isset($userDetail->phone) ? $userDetail->phone : '';?></p>
                                                        </td>
                                                        <td valign="top" style="font-family: Arial,sans-serif;font-size: 14px;line-height: 22px;text-align: right;">
                                                            <h4 style="margin: 0;padding-bottom: 10px;font-size: 18px;">Details</h4>
                                                            <p style="margin: 0;"><strong>Date :</strong> <?php echo isset($userDetail->created_date) ? date("F d  , Y", strtotime($userDetail->created_date)) : ''; ?></p>
                                                            <p style="margin: 0;"><strong>Order ID :</strong> <?php echo isset($userDetail->id) ? display_order($userDetail->id) : ''; ?></p>
                                                            <p style="margin: 0;"><strong>Amount Due :</strong> $<?php echo isset($userDetail->amount) ? display_amount($userDetail->amount) : 0.00; ?></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Product Name</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Unit Price</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Quantity</th>
                                                        <!--<th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Shipping</th>-->
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $sub_total = 0; ?>
                                                    <?php $shipping_total = 0; ?>
                                                    <?php if(isset($order) && !empty($order) ) {
                                                        foreach ($order as $row) { ?>
                                                            <tr>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">
                                                                    <?php echo $row->name; ?><br/> MODEL #:<?php echo isset($row->model_number) ? $row->model_number : ''; ?>
                                                                </td>
                                                                <?php if($row->awarded_price == '1') { ?>
                                                                    <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">Best Pricing Will Be Awarded</td>
                                                                <?php } else {  ?>
                                                                    <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">$<?php echo number_format($row->price, 2); ?></td>
                                                                <?php } ?>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;"><?php echo $row->quantity; ?></td>
                                                                <!--<td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;"><?php echo ($row->shipping_amount > 0) ? "$" . $row->shipping_amount : 'Free'; ?></td>-->
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">$<?php echo number_format(($row->quantity * $row->price) + $row->shipping_amount, 2); ?></td>
                                                            </tr>
                                                        <?php
                                                            $sub_total      = $sub_total + ($row->quantity * $row->price) + $row->shipping_amount;
                                                            $shipping_total = $shipping_total + $row->shipping_amount;
                                                        }
                                                    } ?>
                                                    <tr>
                                                        <td valign="top" colspan="3" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;border: 1px solid #ddd;">Sub Total</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;line-height:20px;border: 1px solid #ddd;">$<?php echo number_format($sub_total, 2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" colspan="3" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;border: 1px solid #ddd;">Sales Tax</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;line-height:20px;border: 1px solid #ddd;">$<?php echo isset($order_details['tax']) ? number_format($order_details['tax'], 2) : '0.00' ?></td>
                                                    </tr>
<!--                                                    <tr>
                                                        <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #7cc9e1; line-height:20px;border: 1px solid #ddd;">Shipping Amount</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #7cc9e1; line-height:20px;border: 1px solid #ddd;"><?php echo ($shipping_total > 0) ? "$" . $shipping_total : 'Free'; ?></td>
                                                    </tr>-->
                                                    <tr>
                                                        <td valign="top" colspan="3" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #fffed3; line-height:20px;border: 1px solid #ddd;">Total Amount Paid</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #fffed3; line-height:20px;border: 1px solid #ddd;">$<?php echo isset($userDetail->amount) ? display_amount($userDetail->amount) : 0.00; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php if($order_details['delivery'] == 1) { ?>                                        
                                        <tr>
                                            <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;"> 
                                                <!-- <span> -->
                                                    <i class="fa fa-info-circle"></i>
                                                <!-- </span> -->
                                                <b>*I will pick up the items and I do not require delivery.</b>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 40%;" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>