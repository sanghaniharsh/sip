<div class="main container">
    <div class="col-main">
        <div class="cart">
            <div class="cart_block">
                <?php if (isset($order) && !empty($order)) { ?>
                    <div class="page-order">
                        <form action="<?php echo enterprise_url() . 'reorder/'.encreptIt($order_details->id);?>" id="re_order_form" method="post" autocomplete="nope">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-border ent_terms_box">
                                        <div class="delivery_info_ent">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" name="delivery">
                                                <b>I will pick up the items and I do not require delivery </b>
                                            </label>
                                            <div class="cart_info">
                                                <span>
                                                    <i class="fa fa-info-circle"></i>
                                                </span>
                                                <span>
                                                    Delivery fees will be calculated and presented prior to delivery.
                                                </span>
                                            </div>
                                        </div>
                                        <ul>
                                            <li style="margin: 0;">
                                                <div class="">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="1" name="terms">
                                                        I agree to the <a href="<?php echo base_url() ?>page/user-agreement" target="_blank" class="color_blue">User/Purchase Agreement</a> & <a href="<?php echo base_url() ?>page/privacy-policy" class="color_blue" target="_blank">Privacy Policy</a>
                                                    </label>
                                                    <div class="checkbox_error"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>            
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-circle btn-lg btn-primary">Reorder</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="heading-counter warning">
                                    <h4 style="margin: 0;padding-bottom: 10px;font-size: 18px;">Invoice to</h4>
                                    <p style="margin: 0;"><?php echo ( isset($this->loginUser['firstname']) && isset($this->loginUser['lastname']) ) ? ucfirst($this->loginUser['firstname']) . " " . ucfirst($this->loginUser['lastname']) : '' ?></p>
                                    <p style="margin: 0;"><?php echo isset($order_details->email) ? $order_details->email : '';?></p>
                                    <p style="margin: 0;"><?php echo isset($order_details->phone) ? $order_details->phone : '';?></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12 pull-right">
                                <div class="heading-counter warning">
                                    <h4>Details</h4>
                                    <p style="margin: 0;"><strong>Date :</strong> <?php echo (isset($order_details->created_date) && ($order_details->created_date != '') ) ? date("m/d/Y", strtotime($order_details->created_date)) : '' ?></p>
                                    <p style="margin: 0;"><strong>Order ID :</strong> <?php echo isset($order_details->id) ? display_enterprise_order($order_details->id) : '' ?></p>
                                    <p style="margin: 0;"><strong>Amount Paid :</strong> 
                                        $<?php echo isset($order_details->amount) ? number_format($order_details->amount, 2) : '0.00' ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="order-detail-content">
                            <div class="table-responsive">
                                <table class="table table-bordered cart_summary">
                                    <thead>
                                        <tr>
                                            <th class="cart_product">Product Image</th>
                                            <th>Description</th>
                                            <th style="text-align: center;">Unit price</th>
                                            <th style="text-align: center;">Qty</th>
                                            <!-- <th style="text-align: center;">Shippinsg</th> -->
                                            <th style="text-align: center;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  $totalAll = 0; ?>
                                        <?php  $shipping_total = 0; ?>
                                        <?php if (isset($order) && !empty($order)) { ?>
                                            <?php foreach ($order as $row) { ?>
                                                    <?php $totalAll         = $totalAll + ($row->quantity * $row->price ) + $row->shipping_amount; ?>
                                                    <?php $shipping_total   = $shipping_total + $row->shipping_amount; ?>
                                                    <tr>
                                                        <td class="cart_product">
                                                            <?php 
                                                            $images[0] =  '';
                                                            if (isset($row->product_images) && $row->product_images != "") {
                                                                $images = explode(',',$row->product_images);
                                                            }
                                                            ?>
                                                            <!-- <a href="<?php echo  base_url(); ?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>"><img src="<?php echo base_url().'attachment/image/71/100/'.md5($images[0]); ?>" alt="Product"></a> -->
                                                            <a href="<?php echo  enterprise_url(); ?>product/<?php echo isset($row->id) && $row->id != '' ? encreptIt($row->id) : ''; ?>"><img src="<?php echo base_url().'attachment/image/71/100/'.md5($images[0]); ?>" alt="Product"></a>
                                                        </td>
                                                        <td class="cart_description">
                                                            <p class="product-name">
                                                                <!-- <a href="<?php echo  base_url(); ?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>"><?php echo isset($row->name) && $row->name != '' ? $row->name : ''; ?> </a> -->
                                                                <a href="<?php echo  enterprise_url(); ?>product/<?php echo isset($row->id) && $row->id != '' ?  encreptIt($row->id) : ''; ?>"><?php echo isset($row->name) && $row->name != '' ? $row->name : ''; ?> </a>
                                                            </p>
                                                            <small class="view_options">
                                                                <!--<a href="#">Category : <?php echo isset($row->category_name) && $row->category_name != '' ? $row->category_name : ''; ?></a>-->
                                                                <?php if (isset($row->questions) && ! empty($row->questions)) { ?>
                                                                    <a href="#" data-toggle="modal" data-target="#options_modal_<?php echo $row->cart_id; ?>">View list of options I selected</a>
                                                                <?php } ?>
                                                            </small>
                                                        <td class="price">
                                                            <span>
                                                                <?php if($row->awarded_price == '1') { ?>
                                                                    Best Pricing Will Be Awarded
                                                                <?php } else {  ?>
                                                                    $<?php echo isset($row->price) ? display_amount($row->price) : 0; ?>
                                                                <?php } ?>
                                                            </span>
                                                        </td>
                                                        <td class="qty">
                                                            <?php echo isset($row->quantity) && $row->quantity != '' ? $row->quantity : ''; ?>
                                                        </td>
                                                        <!-- <td class="shipping">
                                                            <span>
                                                                <?php echo (isset($row->shipping_amount) && $row->shipping_amount > 0) ? "$" . $row->shipping_amount : 'Free'; ?>
                                                            </span>
                                                        </td> -->
                                                        <td class="price">
                                                            <span class="total_price_<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">
                                                               $<?php echo (isset($row->quantity) && isset($row->price)) ?  number_format(($row->quantity * $row->price) + $row->shipping_amount , 2) : ''; ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                            <?php } ?>
                                        <?php } ?>         
                                    </tbody>
                                    <tfoot class="discount_block hidden-xs">
                                        <tr>
                                            <td colspan="2" rowspan="5">
                                            </td>
                                            <td colspan="2">Sub Total</td>
                                            <td colspan="2">$<?php echo number_format($totalAll, 2);  ?> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Sales Tax</td>
                                            <td colspan="2">$<?php echo number_format($row->tax, 2);  ?> </td>
                                        </tr>
                                        <?php
                                        $discount_class = "hide";
                                        if ($row->discount > 0) {
                                            $discount_class = "";
                                        }
                                        ?>
                                        <tr class="<?php echo $discount_class; ?>">
                                            <td colspan="2"><strong>Discount Amount</strong></td>
                                            <td colspan="2"><strong>$<?php echo $row->discount; ?> </strong></td>
                                        </tr>
                                        <!-- <tr class="">
                                            <td colspan="3">Shipping Total</td>
                                            <td colspan="2"><?php echo ($shipping_total > 0) ? "$" . number_format($shipping_total, 2) : "Free"; ?> </td>
                                        </tr> -->
                                        <tr>
                                            <td colspan="2"><strong>Total Amount</strong></td>
                                            <td colspan="2">$<strong class="total_amount"><?php echo number_format($row->amount, 2); ?></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <table class="table table-bordered discount_block visible-xs" data-device="mobile">
                                        <tr>
                                            <th>Sub Total</th>
                                            <td class="sub_total_text text-right">$<?php echo number_format($totalAll, 2);  ?></td>
                                        </tr>
                                        <tr>
                                            <th>Tax</th>
                                            <td class="text-right">$0.00</td>
                                        </tr>
                                        <?php
                                        $discount_class = "hide";
                                        if ($row->discount > 0) {
                                            $discount_class = "";
                                        }
                                        ?>
                                        <tr class="<?php echo $discount_class; ?>">
                                            <th>Discount Amount</th>
                                            <td class="text-right">$<?php echo $row->discount; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Total</th>
                                            <td class="text-right"><?php echo ($shipping_total > 0) ? "$" . number_format($shipping_total, 2) : "Free"; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <td class="text-right">$<?php echo number_format($row->amount, 2); ?></strong></td>
                                        </tr>
                                    </table>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>