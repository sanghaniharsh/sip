<div class="container">
    <div class="account_page">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h2>Order History</h2>
                </div>
            </div>
        </div>
        <div class="row accounts_tabs">
            <div class="col-md-12">
                <ul class="tabs side" id="tabs">
                    <li><a href="#my_orders" class="my_order_tab" target="_self"><i class="fa fa-shopping-cart"></i> My Orders</a></li>  
                </ul>
                <div class="tabs-content">
                    <div id="my_orders" class="tabs-panel">
                        <h1><strong>Your Order History</strong></h1>
                        <div style="text-align : right; margin-bottom: 12px">
                            <a href="<?php echo (base_url().'enterprise/'.$this->enterprise_user->slug) ?>" class="btn btn-primary mb-5">Search For Products</a></li>
                        </div>
                        <div class="table-responsive">
                            <table id="user_order_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice No.</th>
                                        <th>Created Date</th>
                                        <th>Total Item</th>
                                        <th>Total Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

