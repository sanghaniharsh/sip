<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Promotional</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/promotional/commit';

                        if (isset($promotional->id) && ($promotional->id > 0))
                        {
                            $url = base_url().'admin/promotional/commit/'.md5($promotional->id);
                        }
                    ?>
                    <form id="promotionalForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="name">Promotional Title</label>
                            <div class="col-md-3">
                                <input class="form-control" name="title" placeholder="enter promotional title" type="text" value="<?php echo isset($promotional->title) ? $promotional->title : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="name"> Promotional Code</label>
                            <div class="col-md-3">
                                <input class="form-control" name="promo_code" placeholder="enter promo code" type="text" value="<?php echo isset($promotional->promo_code) ? $promotional->promo_code : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="name"> Promotional Usage Time</label>
                            <div class="col-md-3">
                                <input class="form-control numeric" name="use_number" placeholder="enter promo usage time" type="text" value="<?php echo isset($promotional->use_number) ? $promotional->use_number : ''; ?>">
                            </div>
                        </div>
                        <?php $is_bypass = isset($promotional->bypass_payment) && $promotional->bypass_payment == '1' ? 'hide' : ''; ?>
                        <div class="bypass_box <?php echo $is_bypass; ?>">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="type">Promotional Type</label>
                                <div class="col-md-3">
                                    <select name="type" id="type" class="form-control" >
                                        <option value="">-- Select Type --</option>
                                        <option value="0" <?php echo isset($promotional->type) && $promotional->type == '0' ? 'selected="selected"' : ''; ?> >Percent</option>
                                        <option value="1" <?php echo isset($promotional->type) && $promotional->type == '1' ? 'selected="selected"' : ''; ?> >Dollar</option>
                                    </select>
                                </div>
                            </div>
                            <?php 
                            $discountSign = '';
                            if (isset($promotional->type)) {
                                if ($promotional->type == '0') {
                                    $discountSign = '%';
                                } else if($promotional->type == '1') {
                                    $discountSign = '$';
                                }
                            }
                            ?>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="name"> Discount <span class="discount-type-text">In(<?php echo $discountSign; ?>)</span></label>
                                <div class="col-md-3">
                                    <input class="form-control numeric" name="discount" placeholder="enter discount" type="text" value="<?php echo isset($promotional->discount) ? $promotional->discount : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($promotional->status) && ($promotional->status == 1)) || !isset($promotional)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="status" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/promotional' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="promo_id" id="promo_id" value="<?php echo isset($promotional->id) ? md5($promotional->id) : 0; ?>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>