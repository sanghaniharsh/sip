<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Promotional</h6>
        <a href="<?php echo base_url().'admin/promotional/setup'; ?>" class="btn btn-primary ripple pull-right">Add Promotional</a>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="promotionalDataTable">
                            <thead>
                                <tr>
                                    <th class="sorting">Promotional Title</th>
                                    <th class="sorting">Promotional Code</th>
                                    <th class="sorting">Promotional Type</th>
                                    <th class="sorting">Discount</th>
                                    <th class="sorting">Created Date</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
