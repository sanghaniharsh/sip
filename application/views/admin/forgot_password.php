<div class="container-min-full-height justify-content-center align-items-center" style="min-height: 80vh;">
    <div class="text-center mb-4 login_logo">
        <a href="javascript:void(0);">
            <h3>Reset Password</h3>
        </a>
    </div>
    <div class="login-center">
        <form class="form-signin forgot_password" id="forgot_password" method="POST">
            <p class="text-center text-muted">Enter your email address and we'll send you an email with instructions to reset your password.</p>
            <div class="form-group">
                <label for="example-email" class="col-md-12 mb-1">Email</label>
                <input type="email" placeholder="johndoe@site.com" class="form-control form-control-line" name="email" id="email">
                
            </div>
            <span class="mail-error"></span>
            <div class="form-group mb-5">
                <button class="btn btn-block btn-lg btn-info text-uppercase fs-12 fw-600" type="submit">Submit</button>
            </div>
        </form>
        <footer class="col-sm-12 text-center">
            <p>Back to <a href="<?php echo base_url(); ?>admin" class="text-primary m-l-5"><b>Login</b></a>
            </p>
        </footer>
    </div>
</div>