<?php 
    $currentClass   = $this->router->fetch_class();
    $currentMethod  = $this->router->fetch_method();
    
    $userActive     = '';
    if (($currentClass == 'admin') && in_array($currentMethod, array('users', 'add', 'edit')) || ($currentClass == 'sales_rep') && in_array($currentMethod, array('index', 'add', 'edit')) || ($currentClass == 'applicant' && $currentMethod != 'flaggedVideo') || ($currentClass == 'business_user') || ($currentClass == 'blocked')) {
        $userActive     = 'active';
    }
    
    $settingActive = '';
    if (($currentClass == 'admin' && $currentMethod == 'site_settings') || ($currentClass == 'package') || ($currentClass == 'promotional') || ($currentClass == 'referrer')) {
        $settingActive = 'active';
    }
?>
<aside class="site-sidebar" data-suppress-scroll-x="true">
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            <li class="<?php echo ($currentClass == 'admin') && !in_array($currentMethod, array('users', 'add', 'edit', 'site_settings')) ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'dashboard'; ?>">
                    <i class="list-icon fa fa-dashboard"></i>
                    <span class="hide-menu">Dashboard</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'admin') && in_array($currentMethod, array('users', 'add', 'edit')) ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'admin-users'; ?>">
                    <i class="list-icon feather feather-users"></i>
                    <span class="hide-menu">Admin Users</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'user') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'user'; ?>">
                    <i class="list-icon feather feather-users"></i>
                    <span class="hide-menu">Users</span>
                </a>
            </li>
<!--            <li class="<?php echo ($currentClass == 'category') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'category'; ?>">
                    <i class="list-icon feather feather-layers"></i>
                    <span class="hide-menu">Categories</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'product') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'product'; ?>">
                    <i class="list-icon fa fa-shopping-cart"></i>
                    <span class="hide-menu">Products</span>
                </a>
            </li>-->
            <li class="<?php echo ($currentClass == 'brand') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'brand'; ?>">
                    <i class="list-icon feather feather-layers"></i>
                    <span class="hide-menu">Brands</span>
                </a>
            </li>
            <li class="current-page menu-item-has-children <?php echo in_array($currentClass, array('enterprise','enterpriseProduct','enterpriseOrder')) ? 'active' : ''; ?>">
                <a href="#">
                    <i class="list-icon feather feather-briefcase"></i>
                    <span class="hide-menu">Enterprise</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li class="<?php echo ($currentClass == 'enterprise') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'enterprise'; ?>">
                            <span class="hide-menu">Enterprise Clients</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'enterpriseProduct') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'enterprise-product'; ?>">
                            <span class="hide-menu">Products</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'enterpriseOrder') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'enterprise-order'; ?>">
                            <span class="hide-menu">Orders</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="current-page menu-item-has-children <?php echo in_array($currentClass, array('category', 'product')) ? 'active' : ''; ?>">
                <a href="#">
                    <i class="list-icon feather feather-briefcase"></i>
                    <span class="hide-menu">New Equipment</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li class="<?php echo ($currentClass == 'category') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'category'; ?>">
                            <span class="hide-menu">Category</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'product') && in_array($currentMethod, array('index', 'setup', 'commit', 'delete')) ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'product'; ?>">
                            <span class="hide-menu">Product</span>
                        </a>
                    </li>
                </ul>
            </li>
            
            <li class="current-page menu-item-has-children <?php echo in_array($currentClass, array('usedcategory', 'oldproduct')) ? 'active' : ''; ?>">
                <a href="#">
                    <i class="list-icon feather feather-briefcase"></i>
                    <span class="hide-menu">Used Equipment</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li class="<?php echo ($currentClass == 'usedcategory') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'usedcategory'; ?>">
                            <span class="hide-menu">Category</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'oldproduct') && in_array($currentMethod, array('index', 'setup', 'commit', 'delete')) ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'oldproduct'; ?>">
                            <span class="hide-menu">Product</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="current-page menu-item-has-children <?php echo in_array($currentClass, array('dippercategory', 'dipper_product')) ? 'active' : ''; ?>">
                <a href="#">
                    <i class="list-icon feather feather-briefcase"></i>
                    <span class="hide-menu">Big Dipper Parts</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li class="<?php echo ($currentClass == 'dippercategory') ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'dippercategory'; ?>">
                            <span class="hide-menu">Category</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'dipper_product') && in_array($currentMethod, array('index', 'setup', 'commit', 'delete')) ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'dipper_product'; ?>">
                            <span class="hide-menu">Product</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="current-page menu-item-has-children <?php echo ($currentClass == 'blog') ? 'active' : ''; ?>">
                <a href="#">
                    <i class="list-icon feather feather-briefcase"></i>
                    <span class="hide-menu">Blog</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li class="<?php echo ($currentClass == 'blog') && in_array($currentMethod, array('category', 'category_setup', 'category_commit', 'delete_category')) ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'blog/category'; ?>">
                            <span class="hide-menu">Blog Category</span>
                        </a>
                    </li>
                    <li class="<?php echo ($currentClass == 'blog') && in_array($currentMethod, array('index', 'setup', 'commit', 'delete')) ? 'active' : ''; ?>">
                        <a href="<?php echo admin_url().'blog/'; ?>">
                            <span class="hide-menu">Blog Posts</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($currentClass == 'news') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'news'; ?>">
                    <i class="list-icon fa fa-newspaper-o"></i>
                    <span class="hide-menu">News</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'auction') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'auction'; ?>">
                    <i class="list-icon fa fa-buysellads"></i>
                    <span class="hide-menu">Auctions</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'order') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'order'; ?>">
                    <i class="list-icon fa fa-list-alt" aria-hidden="true"></i>
                    <span class="hide-menu">All Orders</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'promotional') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'promotional'; ?>">
                    <i class="list-icon fa fa-dollar"></i>
                    <span class="hide-menu">Discount Codes</span>
                </a>
            </li>
            <li class="<?php echo ($currentClass == 'cms') ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'cms'; ?>">
                    <i class="list-icon fa fa-file-pdf-o"></i>
                    <span class="hide-menu">CMS Pages</span>
                </a>
            </li>
            <li class="<?php echo (($currentClass == 'admin') && ($currentMethod == 'site_settings')) ? 'active' : ''; ?>">
                <a href="<?php echo admin_url().'site-settings'; ?>">
                    <i class="list-icon fa fa-cogs"></i>
                    <span class="hide-menu">Site Settings</span>
                </a>
            </li>
        </ul>
        <div class="row mt-5">
            <div class="col-md-12">
                <a href="<?php echo admin_url().'logout' ?>" class="btn btn-block btn-danger ripple"><i class="feather feather-power"></i> Logout</a>
            </div>
        </div>
    </nav>
</aside>