<nav class="navbar">
    <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-brand">
            <!--<img src="<?php echo admin_img_url() ?>logo.png" class="logo" alt="">-->
            <h3 style="color: #fff;">SIP Admin</h3>
        </a>
    </div>
    <div class="spacer"></div>
    <ul class="nav navbar-nav">
        <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple" data-toggle="dropdown"><span class="avatar thumb-xs2"><img src="<?php echo admin_img_url().'user.png'; ?>" class="rounded-circle" alt=""> <i class="feather feather-chevron-down list-icon"></i></span></a>
            <div
                class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                <div class="card">
                    <header class="card-header d-flex mb-0">
                        <a href="<?php echo admin_url().'profile'; ?>" class="col-md-4 text-center"><i class="feather feather-user-plus align-middle"></i> </a>
                        <a href="<?php echo admin_url().'profile'; ?>" class="col-md-4 text-center"><i class="feather feather-settings align-middle"></i> </a>
                        <a href="<?php echo admin_url().'logout' ?>" class="col-md-4 text-center"><i class="feather feather-power align-middle"></i> </a>
                    </header>
                    <ul class="list-unstyled card-body">
                        <li><a href="<?php echo admin_url().'profile'; ?>"><span><span class="align-middle">Manage Accounts</span></span></a>
                        </li>
                        <li><a href="<?php echo admin_url().'logout' ?>"><span><span class="align-middle">Sign Out</span></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</nav>
