<div id="wrapper" class="wrapper">
    <!--header start-->
    <?php echo isset($header) ? $header : ''; ?>
    <!--header end-->
    <div class="content-wrapper">

        <!--sidebar start-->
        <?php echo isset($left_sidebar) ? $left_sidebar : ''; ?>
        <!--sidebar end-->

        <!--main content start-->
        <main class="main-wrapper clearfix">
            <?php
                $success= $this->session->flashdata('success');
                $error  = $this->session->flashdata('error');
            ?>
            <?php if (isset($success) && $success != '') { ?>
                <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert" style="margin-top: 10px">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="material-icons list-icon">check_circle</i>
                    <?php echo $success; ?>
                </div>
            <?php } ?>
            <?php if (isset($error) && $error != '') { ?>
                <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert" style="margin-top: 10px">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="material-icons list-icon">check_circle</i>
                    <?php echo $error; ?>
                </div>
            <?php } ?>

            <?php echo isset($content) ? $content : ''; ?>

        </main>
    </div>
    <footer class="footer">
        <span class="heading-font-family">Copyright @ <?php echo date('Y'); ?>. All rights reserved by SIP </span>
    </footer>
</div>