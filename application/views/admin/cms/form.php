<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage CMS</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/cms/commit';

                        if (isset($cms->id) && ($cms->id > 0))
                        {
                            $url = base_url().'admin/cms/commit/'.md5($cms->id);
                        }
                    ?>
                    <form id="CmsForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="title">Title</label>
                            <div class="col-md-10">
                                <input class="form-control" name="title" id="title" placeholder="enter title" type="text" value="<?php echo isset($cms->title) ? $cms->title : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="url">URL</label>
                            <?php
                                $url = "";
                                if (isset($cms->url) && ($cms->url != '')) {
                                    $url = base_url().'page/'.$cms->url;
                                }
                            ?>
                            <input class="form-control " id="url" name="url" readonly="" type="hidden" value="<?php echo isset($cms->url) ? $cms->url : '' ?>"/>
                            <div class="col-md-10">
                                <input class="form-control" id="urlText" readonly  type="text" value="<?php echo $url; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="content">Content</label>
                            <div class="col-md-10">
                                <textarea class="form-control ckeditor" name="content" id="content" type="text" ><?php echo isset($cms->content) ? $cms->content : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($cms->is_active) && ($cms->is_active == 1)) || !isset($cms)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                </label>
                            </div>
                        </div>
                        <input type="hidden" name="cmsId" id="cmsId" value="<?php echo isset($cms->id) ? $cms->id : 0; ?>">
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/cms' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>