<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage CMS</h6>
        <a href="<?php echo base_url().'admin/cms/setup'; ?>" class="btn btn-primary ripple pull-right">Add CMS</a>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="cmsDataTable">
                            <thead>
                                <tr>
                                    <th class="sorting">Title</th>
                                    <th class="sorting">URL</th>
                                    <th class="sorting">Created by</th>
                                    <th class="sorting">Created Date</th>
                                    <th class="sorting">Modify Date</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting"></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
