<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Dashboard</h6>
        <p class="page-title-description mr-0 d-none d-md-inline-block">Users, Orders, Products and more</p>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="widget-holder widget-sm col-md-3 widget-full-height">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="counter-w-info media">
                        <div class="media-body">
                            <p class="text-muted mr-b-5">Total Users</p><span class="counter-title color-primary"><span class="counter" id="counter-0"><?php  echo isset($total_users) ? $total_users : '0'; ?> </span> </span>
                        </div>
                        <!-- /.media-body -->
                        <div class="pull-right align-self-center"><i class="list-icon feather feather-user-plus bg-primary"></i>
                        </div>
                    </div>
                    <!-- /.counter-w-info -->
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
        <div class="widget-holder widget-sm col-md-3 widget-full-height">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="counter-w-info media">
                        <div class="media-body">
                            <p class="text-muted mr-b-5">Total Orders</p><span class="counter-title color-info"><span class="counter" id="counter-1"><?php  echo isset($total_orders) ? $total_orders : '0'; ?></span></span>
                        </div>
                        <!-- /.media-body -->
                        <div class="pull-right align-self-center"><i class="list-icon feather feather-shopping-cart bg-info"></i>
                        </div>
                    </div>
                    <!-- /.counter-w-info -->
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
        <div class="widget-holder widget-sm col-md-3 widget-full-height">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="counter-w-info media">
                        <div class="media-body">
                            <p class="text-muted mr-b-5">Total Sales</p><span class="counter-title color-pink">$<span class="counter" id="counter-2"><?php  echo isset($total_income) ? $total_income : '0'; ?></span> </span>
                        </div>
                        <!-- /.media-body -->
                        <div class="pull-right align-self-center"><i class="list-icon feather feather-briefcase bg-pink"></i>
                        </div>
                    </div>
                    <!-- /.counter-w-info -->
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
        <div class="widget-holder widget-sm col-md-3 widget-full-height">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="counter-w-info media">
                        <div class="media-body">
                            <p class="text-muted mr-b-5">Total Products</p><span class="counter-title"><span class="counter" id="counter-3"><?php  echo isset($total_products) ? $total_products : '0'; ?></span> </span>
                            
                        </div>
                        <div class="pull-right align-self-center"><i class="list-icon feather feather-clock bg-warning"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="widget-holder widget-outside-header col-lg-7">
            <div class="widget-heading">
                <h5 class="widget-title">All Orders</h5>
                <div class="widget-actions order_dash_filter">
                    <select class="form-control order_day_filter">
                        <option value="">All</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="oneWeekago">Last 7 days</option>
                        <option value="oneMonthago">Last 30 days</option>
                    </select>
                </div>
            </div>
            
            <div class="widget-bg order_widget">
                <div class="widget-body pb-0">
                    <table class="widget-invoice-table table mb-0 headings-font-family fs-13" valign="center">
                        <thead class="lh-43 fs-12">
                            <tr>
                                <th>Order ID</th>
                                <th class="w-30 text-center">Email</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody class="order_block">
                            <?php echo isset($order_html) ? $order_html : ''; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="widget-holder widget-outside-header col-lg-5">
            <div class="widget-heading">
                <h5 class="widget-title">State wise Orders</h5>
            </div>
            <div class="widget-bg state_wise_widget">
                <div class="widget-body pb-0">
                    <table class="widget-invoice-table table mb-0 headings-font-family fs-13" valign="center">
                        <thead class="lh-43 fs-12">
                            <tr>
                                <th>State Code</th>
                                <th class="w-30">State Name</th>
                                <th class="text-center">Orders</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($state_orders) && !empty($state_orders)) { ?>
                                    <?php foreach($state_orders as $row) {?>
                                        <tr>
                                            <td><span class="headings-color"><?php echo  isset($row->code) ? $row->code :'';?></span>
                                            </td>
                                            <td class="w-25"><span class="headings-color fw-bold"><?php echo  isset($row->name) ? $row->name :'';?></span>
                                            </td>
                                            <td style="text-align: center;"><span class="text-muted"><?php echo  isset($row->orderCount) ? $row->orderCount :'0';?></span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>