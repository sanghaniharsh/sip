<div class="container-min-full-height justify-content-center align-items-center" style="min-height: 80vh;">
    <div class="text-center login_logo">
        <a href="javascript:void(0);">
            <img src="<?php echo admin_img_url() ?>logo-login.png" class="logo" alt="">
            <h3>SIP Admin Panel</h3>
        </a>
    </div>
    <div class="login-center-bdr">
        <div class="login-center">
        
        <?php
            $success= $this->session->flashdata('success');
            $error  = $this->session->flashdata('error');
        ?>
        <?php if (isset($success) && $success != '') { ?>
            <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert" style="margin-top: 10px">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="material-icons list-icon">check_circle</i>
                <?php echo $success; ?>
            </div>
        <?php } ?>
        <?php if (isset($error) && $error != '') { ?>
            <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert" style="margin-top: 10px">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="material-icons list-icon">check_circle</i>
                <?php echo $error; ?>
            </div>
        <?php } ?>
        <form class="form-signin" action="<?php echo base_url(); ?>admin/login" method="POST">
            <div class="form-group">
                <label for="example-email">Username</label>
                <input type="text" class="form-control form-control-line" name="username" id="user_email">
            </div>
            <div class="form-group">
                <label for="example-password">Password</label>
                <input type="password" id="password" name="password" class="form-control form-control-line">
            </div>
            <div class="form-group">
                <label>Enter Captcha</label>
                <input class="form-control register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text" >
            </div>
            <div class="form-group">
                <img id="captcha_code" src="<?php echo base_url() . 'secureimage/securimage'; ?>" style="width: 170px">
                <span class="reload_captcha">
                    <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                </span>
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-lg btn-info text-uppercase fs-12 fw-600" type="submit">Login</button>
            </div>
            <div class="form-group no-gutters mb-0">
                <div class="col-md-12 d-flex">
                    <div class="checkbox checkbox-primary mr-auto mr-0-rtl ml-auto-rtl">
                        <label class="d-flex">
                            <input type="checkbox"> <span class="label-text">Remember me</span>
                        </label>
                    </div><a href="<?php echo base_url(); ?>admin/forgot-password" id="to-recover" class="my-auto pb-2 text-right"><i class="material-icons mr-2 fs-18">lock</i> Forgot Password?</a>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>