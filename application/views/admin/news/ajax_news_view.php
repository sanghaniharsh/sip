<?php if (isset($news) && !empty($news)) { ?>
    <div class="row">
        <?php foreach ($news as $list) { ?>
            <div class="col-md-4 mr-b-30">
                <div class="card blog-post-new news_block">
                    <div class="card-header sub-heading-font-family border-bottom-0 p-0">
                        <figure class="text-center">
                            <a href="javascript:void(0);">
                                <img src="<?php echo base_url().'attachment/image/200/0/'.md5($list->image); ?>" alt="" class="blog_thumb">
                            </a>
                        </figure>
                    </div>
                    <div class="card-body sub-heading-font-family">
                        <h5 class="card-title sub-heading-font-family mb-3"><?php echo isset($list->title) && $list->title != '' ? $list->title : ''; ?></h5>
                        <p class="card-text text-muted" align="justify">
                            <?php echo isset($list->description) && $list->description != '' ? description($list->description, 90) : ''; ?>
                        </p>
                    </div>
                    <div class="card-action d-flex border-0">
                        <a href="javascript:void(0);" class="card-link fw-300 mr-auto mr-0-rtl ml-auto-rtl"><?php echo isset($list->created_date) && $list->created_date != '' ? date('M d, Y', strtotime($list->created_date)) : ''; ?></a>
                        <a class="btn btn-xs btn-outline-default ripple" href="<?php echo isset($list->id) && $list->id > 0 ? base_url().'admin/news/setup/'.md5($list->id) : ''; ?>">
                            <i class="fa fa-pencil"></i> Edit
                        </a> &nbsp;
                        <a class="btn btn-xs btn-outline-danger ripple delete" data-id="<?php echo isset($list->id) && $list->id > 0 ? md5($list->id) : ''; ?>" href="javascript:void(0);">
                            <i class="fa fa-trash"></i> Delete
                        </a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card blog-post-new">
                <h5 align="center">No records found</h5>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row pagination-block">
    <div class="col-md-12">
        <nav aria-label="Page navigation">
            <?php echo isset($links) ? $links : ''; ?>
        </nav>
    </div>
</div>