<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage News</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="alert alert-icon alert-success border-success fade show success-msg" role="alert" style="display:none;"></div>
                <div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/news/commit';

                        if (isset($news->id) && ($news->id > 0))
                        {
                            $url = base_url().'admin/news/commit/'.md5($news->id);
                        }
                    ?>
                    <form class="cmxform form-horizontal tasi-form" id="newsForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="title">News Title</label>
                            <div class="col-md-10">
                                <input class="form-control blog_category_slug" name="title" placeholder="Enter news title" type="text" value="<?php echo isset($news->title) ? $news->title : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="slug"> Slug</label>
                            <div class="col-md-10">
                                <?php
                                    $slug = "";
                                    if (isset($news->slug) && ($news->slug != '')) {
                                        $slug = $news->slug;
                                    }
                                ?>
                                <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($news->slug) ? $news->slug : '' ?>"/>
                                <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="url">News URL</label>
                            <div class="col-md-10">
                                <input class="form-control" name="url" placeholder="Enter news url" type="text" value="<?php echo isset($news->url) ? $news->url : ''; ?>">
                                <input type="hidden" class="blog_id" name="blog_id" value="<?php echo isset($news->id) && ($news->id > 0) ? md5($news->id) : ''; ?>">
                            </div>
                        </div>
                        <?php if (isset($news->image) && $news->image > 0) { ?>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="avatar">Image</label>
                                <div class="col-md-10">
                                    <img class="news-image" width="100" src="<?php echo base_url().'attachment/image/0/300/'.md5($news->image); ?>">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="avatar">News Image</label>
                            <div class="col-md-10">
                                <div class="edit-profile-photo fl-wrap">
                                    <div class="change-photo-btn photoUpload" style="margin-top: 5px; margin-bottom: 0px;">
                                        <div class="">
                                            <input type="file" class="news_image" accept="image/*" name="news_image">
                                        </div>
                                        <label class="avatar-error text-danger"></label>
                                    </div>
                                    <div class="img-error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="description">Description</label>
                            <div class="col-md-10">
                                <div class="ckeditor fl-wrap">
                                    <textarea class="form-control ckeditor" name="description" id="description"><?php echo isset($news->description) ? $news->description : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($news->is_active) && ($news->is_active == 1)) || !isset($news)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                    <input type="hidden" class="news_id" name="news_id" value="<?php echo isset($news->id) && ($news->id > 0) ? md5($news->id) : ''; ?>">
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/blog/' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>