<?php if (! empty($questions)) { ?>
    <div class="accordion" id="accordion-5" role="tablist" aria-multiselectable="true">
        <?php foreach($questions as $q => $question) {
            $options = json_decode($question->options, true);
            ?>
            <div class="card">
                <div class="card-header" role="tab" id="heading<?php echo $q; ?>">
                    <h6 class="card-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-5" href="#collapse<?php echo $q; ?>" aria-expanded="false" aria-controls="collapseSix"><?php echo $question->question;; ?></a>
                        <div class="btn-group questions_accordian_btns">
                            <button type="button" class="btn btn-xs btn-primary btn-outline-default ripple edit-question" data-id="<?php echo encreptIt($question->id); ?>">Edit</button>
                            <button type="button" class="btn btn-xs btn-danger btn-outline-default ripple remove-question" data-id="<?php echo encreptIt($question->id); ?>">Delete</button>
                        </div>
                    </h6>
                </div>
                <div id="collapse<?php echo $q; ?>" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $q; ?>">
                    <div class="card-body">
                        <?php foreach($options as $key => $option) { ?>
                            <div><?php echo $key+1; ?> <?php echo $option; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div>
        <h4 class="text-center">No Question found</h4>
    </div>
<?php } ?>