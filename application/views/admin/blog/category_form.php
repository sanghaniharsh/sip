<div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
<div class="form-group row">
    <label class="col-md-2 col-form-label" for="name">Title</label>
    <div class="col-md-10">
        <input class="form-control blog_category_slug" name="name" placeholder="enter category title" type="text" value="<?php echo isset($blog_category->name) ? $blog_category->name : ''; ?>">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-form-label" for="name">Slug</label>
    <div class="col-md-10">
        <?php
        $slug = "";
        if (isset($blog_category->slug) && ($blog_category->slug != '')) {
            $slug = $blog_category->slug;
        }
        ?>
        <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($news->slug) ? $news->slug : '' ?>"/>
        <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
    </div>
</div>
<div class="form-actions">
    <div class="form-group row">
        <div class="col-md-9 ml-md-auto btn-list">
            <input type="hidden" name="blogCategory" value="<?php echo isset($blog_category->id) && $blog_category->id > 0 ? md5($blog_category->id) : ''; ?>">
            <button class="btn btn-primary btn-rounded" type="submit">Submit</button>
            <button class="btn btn-outline-default btn-rounded close-modal">Cancel</button>
        </div>
    </div>
</div>