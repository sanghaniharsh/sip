<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Blog</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/blog/commit';

                        if (isset($blog->id) && ($blog->id > 0))
                        {
                            $url = base_url().'admin/blog/commit/'.md5($blog->id);
                        }
                    ?>
                    <form class="cmxform form-horizontal tasi-form" id="blogForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="title">Blog Title</label>
                            <div class="col-md-10">
                                <input class="form-control blog_category_slug" name="title" placeholder="Enter blog title" type="text" value="<?php echo isset($blog->title) ? $blog->title : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="slug"> Slug</label>
                            <div class="col-md-10">
                                <?php
                                    $slug = "";
                                    if (isset($blog->slug) && ($blog->slug != '')) {
                                        $slug = $blog->slug;
                                    }
                                ?>
                                <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($news->slug) ? $news->slug : '' ?>"/>
                                <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="category">Category</label>
                            <div class="col-md-3">
                                <select name="category" class="form-control">
                                    <option value="">-- Select Category --</option>
                                    <?php if (isset($blog_category)) {  ?>
                                        <?php foreach($blog_category as $row) {  ?>
                                            <option value="<?php echo isset($row->id) ? $row->id : ''; ?>" <?php echo isset($blog->category_id) && $blog->category_id == $row->id ? 'selected="selected"' : ''; ?> <?php echo isset($business->industry) && $business->industry == $row->id ? 'selected="selected"' : ''; ?>><?php echo isset($row->name) ? $row->name : ''; ?></option>
                                        <?php }  ?>
                                    <?php }  ?>
                                </select>
                            </div>
                        </div>
                        <?php if (isset($blog_image) && !empty($blog_image)) { ?>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="avatar">Blog Images</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        <?php
                                            foreach ($blog_image as $list) {
                                                 ?>
                                                     <div class="col-md-2">
                                                         <img width="100" src="<?php echo base_url().'attachment/image/0/300/'.md5($list->image); ?>">
                                                         <button rel="<?php echo md5($list->id); ?>" data-itemid="<?php echo md5($list->blogId); ?>" type="button" class="badge bg-important blog-image delete-blog-image" aria-label="Close">
                                                             <span class="badge bg-important delete-image" data-rel="<?php echo md5($list->id); ?>">
                                                                 <i class="fa fa-times icon-delete"></i>
                                                             </span>
                                                         </button>
                                                     </div>
                                                 <?php
                                             }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="avatar">Blog Images</label>
                            <div class="col-md-10">
                                <div class="edit-profile-photo fl-wrap">
                                    <div class="change-photo-btn photoUpload" style="margin-top: 5px; margin-bottom: 0px;">
                                        <div class="file-loading">
                                            <input type="file" class="input-44 blog_images" accept="image/*" name="blog_images[]" multiple="true">
                                        </div>
                                    </div>
                                    <div class="img-error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="description">Description</label>
                            <div class="col-md-10">
                                <div class="ckeditor fl-wrap">
                                    <textarea class="form-control ckeditor" name="description" id="description" ><?php echo isset($blog->description) ? $blog->description : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($blog->is_active) && ($blog->is_active == 1)) || !isset($blog)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                    <input type="hidden" class="blog_id" name="blog_id" value="<?php echo isset($blog->id) && ($blog->id > 0) ? md5($blog->id) : ''; ?>">
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/blog/' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>