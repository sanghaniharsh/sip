<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-4">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Blog posts</h6>
            </div>
            <div class="col-sm-4">
                <input class="form-control top_search blogpost_search" name="blog_search" type="text" placeholder="Search something here">
            </div>
            <div class="col-sm-4">
                <a href="<?php echo base_url().'admin/blog/setup'; ?>" class="btn btn-primary pull-right">Create New Blog</a>
            </div>
        </div>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <!-- Card with Image -->
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="blog_post_block">
                        <div class="ajax_loader absolute">
                            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
