<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Blog Category</h6>
        <a href="<?php echo base_url().'admin/blog/category_setup'; ?>" class="btn btn-primary ripple addBlogCategory pull-right">Add Blog Category</a>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="blogCategoryDataTable">
                            <thead>
                                <tr>
                                    <th class="sorting">Category Name</th>
                                    <th class="sorting">Slug</th>
                                    <th class="sorting">Created Date</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="blogCategoryModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myLargeModalLabel">Add New Category</h5>
            </div>
            <div class="modal-body">
                <form class="cmxform form-horizontal tasi-form" id="blogCategoryForm" method="POST">
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    setTimeout(function() {
        loadBlogCategory();
    }, 1000);
</script>