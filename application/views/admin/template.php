<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo asset_url() ?>img/favicon.png">
        <title>SIP - Admin Panel</title>

        <!-- Dynamic Load CSS files starts -->
        <?php echo isset($css_files) ? $css_files : '';?>
        <!-- Dynamic Load CSS files ends -->

        <link href="<?php echo asset_url() ?>fonts/monoserat.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/feather.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/material-icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/jquery.toast.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/slick-theme.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/slick.min.css" rel="stylesheet" type="text/css">
        <!--<link href="<?php echo asset_url() ?>css/pace.css" rel="stylesheet" type="text/css">-->
        <link href="<?php echo asset_url() ?>css/fileinput.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/toastr.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo asset_url() ?>css/custom.css" rel="stylesheet" type="text/css">

        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            var ADMIN_URL = '<?php echo base_url().'admin'; ?>';
            var allowed_filesize_mb = <?php echo isset($this->allowed_filesize_mb) ? $this->allowed_filesize_mb : 5; ?>;
        </script>
    </head>
    <body class="sidebar-dark sidebar-expand navbar-brand-dark header-light <?php echo isset($body_class) ? $body_class : ''; ?>">
          
        <?php echo isset($main_content) ? $main_content : ''; ?>

        <script src="<?php echo asset_url() ?>js/jquery.min.js"></script>
        <script src="<?php echo asset_url() ?>js/popper.min.js"></script>
        <script src="<?php echo asset_url() ?>js/bootstrap.min.js"></script>
        <script src="<?php echo asset_url() ?>js/metisMenu.min.js"></script>    
        <script src="<?php echo asset_url() ?>js/perfect-scrollbar.jquery.js"></script>    
        <script src="<?php echo asset_url() ?>js/widgets.js"></script>
        <script src="<?php echo asset_url() ?>js/jquery.toast.min.js"></script>
        <script src="<?php echo asset_url() ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo asset_url() ?>js/jquery.dataTables.min.js"></script>
        <script src="<?php echo asset_url() ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo asset_url() ?>js/bootstrap-datepicker.js"></script>
        <script src="<?php echo asset_url() ?>js/bootstrap-clockpicker.min.js"></script>
        <script src="<?php echo asset_url() ?>js/bootstrap-toggle.min.js"></script>
        <script src="<?php echo asset_url() ?>js/slick.min.js"></script>
        <script src="<?php echo asset_url() ?>js/theme.js"></script>
        <script src="<?php echo asset_url() ?>js/fileinput.min.js"></script>
        <!--<script src="<?php echo asset_url() ?>js/pace.min.js" data-pace-options='{ "ajax": false, "selectors": [ "img" ]}'></script>-->
        <script src="<?php echo asset_url() ?>js/ckeditor/ckeditor.js" ></script>
        <script src="<?php echo asset_url() ?>js/toastr.min.js" ></script>
        <script src="<?php echo asset_url() ?>js/jquery.formatter.min.js"></script>
        <script src="<?php echo asset_url() ?>js/custom.js"></script>
        <script src="<?php echo asset_url() ?>js/bootstrap-multiselect.js"></script>

        <script type="text/javascript">
            var errorToaster = "";
            var successToaster = "";

            if (successToaster != '') {
                showMessage('success', successToaster);
            }
            if (errorToaster != '') {
                showMessage('danger', errorToaster);
            }
        </script>
        <!-- Dynamic Load JS files starts -->
            <?php echo isset($js_files) ? $js_files : '';?>
        <!-- Dynamic Load JS files ends -->
    </body>
</html>