<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-4">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Brands</h6>
            </div>
            <div class="col-sm-4">
                <input class="form-control top_search brand_search" name="brand_search" type="text" placeholder="Search By Brand Name">
            </div>
            <div class="col-sm-4">
                <a href="#" data-id="" class="brand_button btn btn-primary pull-right" data-toggle="modal" data-target="#brand-modal">Add Brand</a>
            </div>
        </div>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder widget-full-content border-all px-0">
            <div class="alert alert-icon alert-danger border-danger fade show error-message-danger" role="alert" style="display:none;"></div>
            <div class="alert alert-icon alert-success border-success fade show error-message-success" role="alert" style="display:none;"></div>
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row no-gutters">
                        <div class="col-lg-12 col-md-12 mail-inbox">
                            <div class="brandlist">
                                <div class="ajax_loader absolute">
                                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="brand-modal" class="modal fade brand-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myLargeModalLabel">Add New Brand</h5>
            </div>
            <div class="modal-body ">
                <form action="<?php echo admin_url().'brand/commit/'?>" method="POST" class="brand-frm" enctype="multipart/form-data">
                </form>
            </div>
        </div>
    </div>
</div>
