<div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
<input type="hidden" name="brand_id"  class="brand_id" value="<?php echo isset($branddata->id) ? encreptIt($branddata->id) : '';?>">
<div class="form-group">
    <label for="brand_name">Brand Name</label>
    <input class="form-control input_error" name="brand_name" type="text" placeholder="Type Name here" value="<?php echo (isset($branddata)) ? $branddata->name : '';?>">
</div>
<div class="form-group">
    <label for="avatar">Brand Image</label>
    <div class="edit-profile-photo fl-wrap">
        <input class="brandimage" name="avatar" type="file" accept="image/*">
    </div>
    <label class="avatar-error text-danger"></label>
</div>
<?php 
if(isset($branddata)) { ?>
    <div class="form-group">
        <img class="brand_img" src="<?php echo base_url().'attachment/image/85/85/'.md5($branddata->image); ?>" alt="">
    </div>
<?php 
}?>
<div class="text-center mr-b-30">
    <button class="btn btn-rounded btn-success ripple" type="submit">Save Brand</button>
</div>
