<div class="row category_gids row_position" id="sortable">
<?php if (isset($brand_list) && !empty($brand_list)) {
        foreach ($brand_list as $row) {
            ?>
            <div class="col-md-2" id="<?php echo $row->id; ?>" >
                <div class="contact-info brands_boxes">
                    <header>
                        <figure class="inline-block thumb-md">
                            <img src="<?php echo base_url().'attachment/image/0/85/'.md5($row->image); ?>" alt="">
                        </figure>
                        <h5 class="fw-500"><?php echo isset($row->name) ? $row->name : ''; ?></h5>
                    </header>
                    <div class="d-flex justify-content-center mt-3">
                        <a class="btn btn-xs btn-outline-danger brand_remove_btn mr-2" href="#" data-id="<?php echo encreptIt($row->id);?>"><i class="fa fa-trash" ></i></a>
                        <a class="btn btn-xs btn-outline-default ripple brand_button  pull-right" data-id="<?php echo encreptIt($row->id);?>" href=""><i class="fa fa-pencil"></i></a>
<!--                        <a href="javascript:void(0);" class="btn btn-rounded btn-primary mx-1"><i class="feather feather-check-circle list-icon mr-r-10 mr-r-0-rtl mr-l-10-rtl"></i>&nbsp;</a>
                        <a href="javascript:void(0);" class="btn btn-rounded btn-default mx-1">&nbsp;</a>-->
                    </div>
                </div>
<!--                <div class="icon-box icon-box-side icon-box-circle-bg">
                    <header class="align-self-end">
                        <img src="<?php echo base_url().'attachment/image/0/85/'.md5($row->image); ?>" alt="">
                    </header>
                    <section>
                        <h6 class="icon-box-title"><?php echo isset($row->name) ? $row->name : ''; ?></h6>
                        <a class="btn btn-xs ripple brand_remove_btn" href="#" data-id="<?php echo encreptIt($row->id);?>"><i class="fa fa-trash" ></i> Delete</a>
                        <a class="btn btn-xs btn-outline-default ripple brand_button  pull-right" data-id="<?php echo encreptIt($row->id);?>" href=""><i class="fa fa-pencil"></i> Edit</a>
                    </section>
                </div>-->
            </div>
         <?php       
        }
    } else { 
       echo  "Sorry No record found";
    } ?>
</div>
<hr>
<!--<div class="row px-4 mt-5 mb-5">
    <div class="col-7 text-muted mt-1">
    </div>
    <div class="col-5" id="pagination">
        <div class="btn-group float-right">
            <?php 
            echo isset($pagination_link) ? $pagination_link : '';
            ?>
        </div>
    </div>
</div>-->
