<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Add Enterprise</h5>
        </div>
        <div class="modal-body ">
            <form action="<?php echo admin_url().'enterprise/commit'; ?>" method="POST" class="enterprise-frm" enctype="multipart/form-data">
                <input type="hidden" name="enterprise_id"  class="enterprise_id" value="<?php echo isset($enterprise->id) ? encreptIt($enterprise->id) : ''; ?>">
                <div class="form-group">
                    <label for="cat_name">Name</label>
                    <input class="form-control input_error enterprise-name" name="name" type="text" placeholder="Type Name here" value="<?php echo isset($enterprise->name) ? $enterprise->name : ''; ?>">
                    <input type="hidden" class="slug" name="slug" value="<?php echo isset($enterprise->slug) ? $enterprise->slug : ''; ?>">
                </div>
                <div class="form-group">
                    <label for="cat_name">URL</label>
                    <input class="form-control input_error url_name" name="url_name" type="text" placeholder="Type URL Name here" value="<?php echo isset($enterprise->url_name) ? $enterprise->url_name : ''; ?>" readonly="readonly">
                </div>
                <div class="form-group">
                    <label for="avatar">Restaurant logo</label>
                    <div class="edit-profile-photo fl-wrap">
                        <input class="enterpriseImage" name="avatar" type="file" accept="image/*">
                    </div>
                    <label class="avatar-error text-danger"></label>
                </div>
                <div class="form-group category_img_parent <?php echo (isset($enterprise) && $enterprise->image != "") ? '' : 'd-none';?>">
                    <img class="category_img" style="height:100px;width: 100px;" src="<?php echo (isset($enterprise) && $enterprise->image != "") ? base_url().'attachment/image/0/0/'.md5($enterprise->image) : ''; ?>" alt="">
                    <button  data-id="<?php echo isset($enterprise->image) ? encreptIt($enterprise->id) : '' ;?>" type="button" class="badge bg-important blog-image delete-restaurant-image" aria-label="Close">
                        <span class="badge bg-important delete-image">
                            <i class="fa fa-times icon-delete"></i>
                        </span>
                    </button>
                </div>

                <div class="form-group">
                    <input type="checkbox" id="status" name="status" <?php echo (isset($enterprise->status) && ($enterprise->status == 1) ? "checked='checked'" : '');?> />&nbsp;&nbsp;<span class="label-text">Active</span>
                </div>
                <div class="text-center mr-b-30">
                    <button class="btn btn-rounded btn-success ripple" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>