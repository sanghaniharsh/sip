<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-6">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Enterprise</h6>
            </div>
            <div class="col-sm-6">
                <a href="#" class="enterprise-button btn btn-primary pull-right">Add Enterprise</a>
            </div>
        </div>
    </div>
</div>

<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder widget-full-content border-all px-0">
            <div class="alert alert-icon alert-danger border-danger fade show error-message-danger" role="alert" style="display:none;"></div>
            <div class="alert alert-icon alert-success border-success fade show error-message-success" role="alert" style="display:none;"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="enterprise_table">
                            <thead>
                                <tr>
                                    <th class="sorting">Name</th>
                                    <th class="sorting">Created date</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting">Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="enterprise-modal" class="modal fade enterprise-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
</div>