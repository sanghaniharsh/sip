<?php if (isset($order) && !empty($order)) { ?>
    <?php foreach ($order as $row) { ?>
        <tr>
            <td class="">
                <span class="headings-color" title="<?php echo isset($row->id) ? display_order($row->id) : ''; ?>">
                    <?php echo isset($row->id) ? display_order($row->id) : ''; ?> 
                </span>
            </td>
            <td class="w-25 order_name">
                <span class="headings-color fw-bold" title="<?php echo isset($row->email) ? $row->email : ''; ?>">
                    <?php echo isset($row->email) ? $row->email : ''; ?>
                </span>
            </td>
            <td>
                <span class="text-muted">
                    <?php echo isset($row->created_date) ? date('m/d/Y', strtotime($row->created_date)) : ''; ?>
                </span>
            </td>
            <td>
                <div class="d-flex">
                    <?php if (isset($row->order_status)) { ?>

                        <?php if ($row->order_status == '0') { ?>
                                <a href="<?php echo base_url() . 'admin/order/user_orders_detail/' . encreptIt($row->id); ?>" class="btn btn-xs px-3 fw-900 fs-9 text-uppercase btn-outline-default flex-1 justify-content-center">In Progress</a>  
                        <?php } ?>

                        <?php if ($row->order_status == '1') { ?>
                                <a href="<?php echo base_url() . 'admin/order/user_orders_detail/' . encreptIt($row->id); ?>" class="btn btn-xs px-3 fw-900 fs-9 text-uppercase  btn-outline-info flex-1 justify-content-center">Shipped</a>  
                        <?php } ?>

                        <?php if ($row->order_status == '2') { ?>
                                <a href="<?php echo base_url() . 'admin/order/user_orders_detail/' . encreptIt($row->id); ?>" class="btn btn-xs px-3 fw-900 fs-9 text-uppercase btn-outline-success flex-1 justify-content-center">Delivered</a>  
                        <?php } ?>
                    <?php } ?>
                    <!--<a href="<?php echo base_url() . 'admin/order/user_orders_detail/' . encreptIt($row->id); ?>" class="btn btn-xs px-0 content-color flex-2"><i class="fa fa-chevron-right"></i></a>-->
                </div>
                <!-- /.d-flex -->
            </td>
        </tr>
    <?php } ?>
<?php } else { ?>
        <div class="no_order_found_dash">
            <i class="list-icon feather feather-shopping-cart"></i>
            <h4 class="text-muted">No Order Found</h4>
        </div>
<?php } ?>