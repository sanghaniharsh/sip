<ul class="business_search blocked_dropdown">
    <?php foreach($users as $row) { ?>
        <li class="listing-li" data-id="<?php echo md5($row->id); ?>" data-slug="<?php echo $row->first_name.' '.$row->last_name; ?>">
            <p class="list_item"><?php echo $row->first_name.' '.$row->last_name; ?> <span>&lt;<?php echo $row->email; ?>&gt;</span> </p>
            <h5>
                <small><?php echo ($row->business == 0) ? 'Applicant Account' : 'Business Account'; ?></small>
            </h5>
        </li>
    <?php } ?>
</ul>