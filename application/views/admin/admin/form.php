<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Admin user</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/admin-users/commit';

                        if (isset($user->id) && ($user->id > 0))
                        {
                            $url = base_url().'admin/admin-users/commit/'.md5($user->id);
                        }
                    ?>
                    <form id="userForm" method="POST" action="<?php echo $url; ?>">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="username">Username</label>
                            <div class="col-md-9">
                                <input class="form-control" name="username" placeholder="enter your username" type="text" value="<?php echo isset($user->username) ? $user->username : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input class="form-control" name="email" placeholder="enter username" type="text" value="<?php echo isset($user->email) ? $user->email : ''; ?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input class="form-control" id="password" name="password" placeholder="enter username" type="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="cpassword">Confirm Password</label>
                            <div class="col-md-9">
                                <input class="form-control" id="cpassword" name="cpassword" placeholder="enter username" type="password">
                            </div>
                        </div>
                        <input type="hidden" name="userId" id="userId" value="<?php echo isset($user->id) ? md5($user->id) : 0; ?>">
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($user->is_active) && ($user->is_active == 1)) || !isset($user)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/admin-users'; ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>