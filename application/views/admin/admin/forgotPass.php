<table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
    <tr>
        <td bgcolor="" style="">
            <table width="100%" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" style="padding:20px 0px;background-color: #fff;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 50%;" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td valign="top" bgcolor="" style="padding:20px; ">
                        <table width="100%" cellspacing="0" cellpadding="0" style="">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">You are receiving this email as you have requested to change your password on your account.</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Please click the button below to reset your password.</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo $activation_code; ?>" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif;border:1px solid #000;border-radius:8px;padding:10px 5px;color:#000;text-decoration:none;display:block;width:200px;margin:0 auto;text-align:center">Reset Password</a>
                                </td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you,</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">SIP</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 40%;" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>