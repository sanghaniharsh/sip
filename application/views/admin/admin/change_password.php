<div class="row page-title" style="height: 2em;">
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>Change password</h5>
                </div>
                <div class="widget-body clearfix">
                    <form id="changePasswordForm" method="POST">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="username">Password</label>
                            <div class="col-md-9">
                                <input class="form-control" name="password" id="password" placeholder="enter your password" type="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">Confirm Password</label>
                            <div class="col-md-9">
                                <input class="form-control" name="cpassword" placeholder="enter your confirm password" type="password" />
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/dashboard'; ?>" class="btn btn-outline-default btn-rounded">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>