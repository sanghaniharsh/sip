<div class="row category_gids row_position" id="category_sort">
<?php if (isset($category_list) && !empty($category_list)) {
        foreach ($category_list as $row) {
            ?>
            <div class="col-md-3" id="<?php echo $row->id; ?>">
                <div class="icon-box icon-box-circle-bg">
                    <header class="align-self-end">
                        <img src="<?php echo base_url().'attachment/image/0/85/'.md5($row->image); ?>" alt="">
                    </header>
                    <section>
                        <h6 class="icon-box-title"><?php echo isset($row->name) ? $row->name : ''; ?></h6>
                        <a class="btn btn-xs ripple category-remove-btn" href="#" data-id="<?php echo encreptIt($row->id);?>"><i class="fa fa-trash" ></i> Delete</a>
                        <a class="btn btn-xs btn-outline-default ripple categorybutton pull-right" data-id="<?php echo md5($row->id);?>" href=""><i class="fa fa-pencil"></i> Edit</a>
                        <a class="btn btn-xs btn-outline-info  ripple pull-right" href="<?php echo admin_url().'category/index/'.encreptIt($row->id); ?>"><i class="fa fa-eye" ></i> View</a>
                    </section>
                </div>
            </div>
         <?php       
        }
    } else { 
       echo  "Sorry No record found";
    } ?>
</div>
<hr>
<div class="row px-4 mt-5 mb-5">
    <div class="col-7 text-muted mt-1">
    </div>
    <div class="col-5" id="pagination">
        <div class="btn-group float-right">
            <?php 
            echo isset($pagination_link) ? $pagination_link : '';
            ?>
        </div>
    </div>
</div>
