
<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-8">
                <h6 class="page-title-heading mr-0 mr-r-5">Add Product</h6>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
<div class="alert alert-icon alert-success border-success fade show success-msg" role="alert" style="display:none;"></div>

<div class="tabs product_add_page">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active tabs-block" href="#home-tab" data-toggle="tab" aria-expanded="true">General Information</a>
        </li>
        <?php
        $tab = '';
        if (isset($product) && !empty($product)) {
            $tab = 'data-toggle="tab"';
        }
        ?>
        <li class="nav-item">
            <a class="nav-link tabs-block" href="<?php echo isset($product) && !empty($product) ? '#profile-tab' : 'javascript:void(0)'; ?>" <?php echo $tab; ?> aria-expanded="false">Product Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabs-block" href="<?php echo isset($product) && !empty($product) ? '#messages-tab' : 'javascript:void(0)'; ?>" <?php echo $tab; ?> aria-expanded="false">Images</a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabs-block" href="<?php echo isset($product) && !empty($product) ? '#questionary-tab' : 'javascript:void(0)'; ?>" <?php echo $tab; ?> aria-expanded="false">Questions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabs-block" href="<?php echo isset($product) && !empty($product) ? '#addon-product-tab' : 'javascript:void(0)'; ?>" <?php echo $tab; ?> aria-expanded="false">Addon Product</a>
        </li>
        
    </ul>
    <!-- /.nav-tabs -->
    <div class="tab-content">
        <div class="tab-pane active" id="home-tab" aria-expanded="true">
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="">
                            <div class="widget-body clearfix">
                                <?php
                                $url = base_url() . 'admin/product/commit';

                                if (isset($product->id) && ($product->id > 0)) {
                                    $url = base_url() . 'admin/product/commit/' . encreptIt($product->id);
                                }
                                ?>
                                <form class="cmxform form-horizontal tasi-form productForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="category">Category</label>
                                        <div class="col-sm-10 category-block">
                                            <?php if (isset($categoryData) && !empty($categoryData)) { ?>
                                                <?php
                                                $n = 0;
                                                foreach ($categoryData as $key => $parent) {
                                                    $n++;

                                                    $class = 'pcategory';
                                                    if ($n > 1) {
                                                        $class = 'sub-category';
                                                    }
                                                    ?>
                                                    <div class="category_select_ajax">
                                                        <select name="category[]" class="form-control <?php echo $class; ?>">
                                                            <option value="">-- Select Category --</option>
                                                            <?php foreach ($parent as $child) { ?>
                                                                <option value="<?php echo isset($child['id']) ? $child['id'] : ''; ?>" <?php echo isset($child['id']) && $child['id'] == $key ? 'selected="selected"' : ''; ?> ><?php echo isset($child['name']) ? $child['name'] : ''; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="brand">Brand</label>
                                        <div class="col-md-10">
                                            <select name="brand_id" class="form-control">
                                                <option value="">-- Select Brand --</option>
                                                <?php if (isset($brand) && !empty($brand)) { ?>
                                                    <?php foreach ($brand as $list) { ?>
                                                        <option value="<?php echo isset($list->id) ? $list->id : '0'; ?>" <?php echo (isset($product->brand_id) && $product->brand_id == $list->id) ? 'selected="selected"' : ''; ?> ><?php echo isset($list->name) ? $list->name : ''; ?></option>
                                                    <?php } ?>
                                                <?php } ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pname">Product Name</label>
                                        <div class="col-md-10">
                                            <input class="form-control blog_category_slug" name="pname" placeholder="Enter Product Name" type="text" value="<?php echo isset($product->name) ? $product->name : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="slug"> URL</label>
                                        <div class="col-md-10">
                                            <?php
                                            $slug = "";
                                            if (isset($product->slug) && ($product->slug != '')) {
                                                $slug = $product->slug;
                                            }
                                            ?>
                                            <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($product->slug) ? $product->slug : '' ?>"/>
                                            <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
                                            <input class="form-control pcategoryId" name="pcategoryId" type="hidden" value="<?php echo isset($product->category_id) && ($product->category_id > 0) ? $product->category_id : 0; ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pprice">Model Number</label>
                                        <div class="col-md-3">
                                            <input class="form-control" name="pModalNumber" placeholder="Enter Product Model Number" type="text" value="<?php echo isset($product->modal_number) ? $product->modal_number : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pprice">Price</label>
                                        <div class="col-md-3">
                                            <input class="form-control" name="pprice" placeholder="Enter Product Price" type="text" value="<?php echo isset($product->price) ? $product->price : ''; ?>">
                                        </div>
                                        <label class="col-md-2 text-right col-form-label" for="lprice">List Price</label>
                                        <div class="col-md-3">
                                            <input class="form-control" name="lprice" placeholder="Enter List Price" type="text" value="<?php echo isset($product->list_price) ? $product->list_price : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">&nbsp;</label>
                                        <div class="checkbox checkbox-primary col-md-3" style="padding-top: 0.5625rem">
                                            <label>
                                                <?php
                                                $call_checked = "";
                                                if ((isset($product->call_for_price) && ($product->call_for_price == '1'))) {
                                                    $call_checked = "checked='checked'";
                                                }
                                                ?>
                                                <input type="checkbox" name="call_for_price" <?php echo $call_checked; ?> /><span class="label-text">Call for discounted price</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pprice">Video URL</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="video_url" placeholder="Enter Video URL" type="text" value="<?php echo isset($product->video_url) ? $product->video_url : ''; ?>">
                                        </div>
                                    </div>
                                    <?php if (isset($product->video_url) && $product->video_url != '') { ?>

                                        <?php
                                        $video_string = '<iframe width="350" height="200" src="' . $product->video_url . '" frameborder="0" allowfullscreen></iframe>';
                                        if (preg_match('/watch/', $product->video_url)) {
                                            $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe src=\"https://www.youtube.com/embed/$2\" width=\"350\" height=\"200\" frameborder=\"0\" allowfullscreen></iframe>", $product->video_url);
                                        }
                                        ?>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label"></label>
                                            <div class="col-md-10">
                                                <!--<iframe width="150" height="100" frameborder="0" src="<?php // echo isset($product->video_url) && $product->video_url != '' ? $product->video_url : '';  ?>" allowfullscreen></iframe>-->
                                                <?php echo $video_string; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php $shipping_option = isset($product->shipping_option) ? $product->shipping_option : 'free'; ?>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="">Shipping</label>
                                        <div class="radiobox">
                                            <label>
                                                <input type="radio" name="shipping_option" value="free" <?php echo ($shipping_option == 'free') ? 'checked' : ''; ?>> 
                                                <span class="label-text">Free Shipping</span>
                                            </label>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">&nbsp;</label>
                                        <div class="radiobox">
                                            <label>
                                                <input type="radio" name="shipping_option" value="manual" <?php echo ($shipping_option == 'manual') ? 'checked' : ''; ?>> 
                                                <span class="label-text">Manual Shipping</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-3 shipping_price_box <?php echo ($shipping_option == 'manual') ? '' : 'hide'; ?>">
                                            <input class="form-control" style="width: 75%;" name="shipping_price" placeholder="Enter Shipping Price" type="text" value="<?php echo isset($product->shipping_price) && ($product->shipping_price != '0') ? $product->shipping_price : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">&nbsp;</label>
                                        <div class="radiobox">
                                            <label>
                                                <input type="radio" name="shipping_option" value="contact_us" <?php echo ($shipping_option == 'contact_us') ? 'checked' : ''; ?>> 
                                                <span class="label-text">Contact us for the best shipping price.</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">&nbsp;</label>
                                        <div class="radiobox">
                                            <label>
                                                <input type="radio" name="shipping_option" value="ups_shipping" <?php echo ($shipping_option == 'ups_shipping') ? 'checked' : ''; ?>> 
                                                <span class="label-text">Calculate shipping using Fedex.</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ups_box <?php echo ($shipping_option == 'ups_shipping') ? '' : 'hide'; ?>">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="length">Length (IN)</label>
                                            <div class="col-md-3">
                                                <input class="form-control" name="length" placeholder="Enter product length" type="text" value="<?php echo isset($product->length) ? $product->length : ''; ?>">
                                            </div>
                                            <label class="col-md-2 text-right col-form-label" for="weight">Weight (LBS)</label>
                                            <div class="col-md-3">
                                                <input class="form-control" name="weight" placeholder="Enter product weight" type="text" value="<?php echo isset($product->weight) ? $product->weight : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="height">Height (IN)</label>
                                            <div class="col-md-3">
                                                <input class="form-control" name="height" placeholder="Enter product height" type="text" value="<?php echo isset($product->height) ? $product->height : ''; ?>">
                                            </div>
                                            <label class="col-md-2 text-right col-form-label" for="width">Width (IN)</label>
                                            <div class="col-md-3">
                                                <input class="form-control" name="width" placeholder="Enter product width" type="text" value="<?php echo isset($product->width) ? $product->width : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">Status</label>
                                        <div class="checkbox checkbox-primary col-md-3" style="padding-top: 0.5625rem">
                                            <label>
                                                <?php
                                                $active_checked = "";
                                                if ((isset($product->is_active) && ($product->is_active == '1')) || !isset($product)) {
                                                    $active_checked = "checked='checked'";
                                                }
                                                ?>
                                                <input type="checkbox" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                            </label>
                                        </div>
                                        <!--<label class="col-md-2 col-form-label" for=""></label>-->
                                        
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12 ml-md-auto btn-list text-center">
                                                <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                                <!--<a href="<?php echo base_url() . 'admin/product/' ?>" class="btn btn-outline-default ripple">Cancel</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (isset($product) && !empty($product)) { ?>
            <div class="tab-pane" id="profile-tab" aria-expanded="false">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="widget-body clearfix">
                                    <?php
                                    if (isset($product->id) && $product->id > 0) {
                                        $url = base_url() . 'admin/product/commit_specification/' . encreptIt($product->id);
                                    }
                                    ?>
                                    <form class="cmxform form-horizontal tasi-form productDetailForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="description">Description</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor description" name="description" id="description"><?php echo isset($product->description) ? $product->description : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="specification">Specification</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor" name="specification" id="specification"><?php echo isset($product->specification) ? $product->specification : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="specification_pdf">Specification  Pdf</label>
                                            <div class="col-md-10">
                                                <input class="form-control equipmentpdf" name="specification_pdf" type="file" accept=".pdf">
                                            </div>
                                        </div>
                                        <?php if(isset($product) && $product->specification_pdf > 0) { ?>
                                            <div class="form-group row">
                                                <label class="col-md-2 col-form-label"></label>
                                                <div class="col-md-10">
                                                    <a target="_blink" href="<?php echo base_url().'attachment/image/85/85/'.md5($product->specification_pdf); ?>" ><i class="fa fa-file-pdf-o" aria-hidden="true"  style="font-size:60px;color:red"></i></a>
                                                    <button class="btn btn-danger remove_specification_pdf" data-id="<?php echo md5($product->id); ?>"><i class="fa fa-times-circle"></i> Remove PDF</button>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="warranty">Warranty</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor" name="warranty" id="warranty"><?php echo isset($product->warranty) ? $product->warranty : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="questionary-tab" aria-expanded="false">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="float-right">
                                            <button class="btn btn-block btn-primary ripple add_question_btn">Add Question</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-body clearfix question_box">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="addon-product-tab" aria-expanded="false">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="widget-body clearfix">
                                    <?php
                                    $url = base_url() . 'admin/product/commit_addon_product/' . encreptIt($product->id);
                                    ?>
                                    <form class="cmxform form-horizontal tasi-form addon_product_form" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                                        <input class="addon_product_id" type="hidden" name="product_id" value="<?php echo isset($product->id) && $product->id > 0 ? encreptIt($product->id) : ''; ?>"/>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="brand">Search and add Addon</label>
                                            <div class="col-md-8">
                                                <select name="addon_product_id[]" class="form-control select2 addon_product" multiple>
                                                    <option value="">-- Select Addon Product --</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2 ml-md-auto btn-list text-center">
                                                <button class="btn btn-success ripple btn-md" type="submit">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <div class="addon-product-block">
                                    <!-- Products List -->
                                        <div class="ajax_loader absolute">
                                            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="messages-tab" aria-expanded="false">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="widget-body clearfix">
                                    <?php
                                    if (isset($product->id) && $product->id > 0) {
                                        $url = base_url() . 'admin/product/commit_images/' . encreptIt($product->id);
                                    }
                                    ?>
                                    <form class="cmxform form-horizontal tasi-form productImageForm" method="POST" enctype="multipart/form-data">
                                        <div class="form-group row product_image_block">
                                            <?php if (isset($product_images) && !empty($product_images)) { ?>
                                                <label class="col-md-2 col-form-label" for="avatar">Images</label>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <?php
                                                        foreach ($product_images as $list) {
                                                            ?>
                                                            <div class="col-md-2">
                                                                <img width="100" src="<?php echo base_url() . 'attachment/image/0/300/' . md5($list->image); ?>">
                                                                <button rel="<?php echo md5($list->id); ?>" data-itemid="<?php echo md5($list->product_id); ?>" type="button" class="badge bg-important blog-image delete-product-image" aria-label="Close">
                                                                    <span class="badge bg-important delete-image" data-rel="<?php echo md5($list->id); ?>">
                                                                        <i class="fa fa-times icon-delete"></i>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="avatar">Product Images</label>
                                            <div class="col-md-10">
                                                <div class="edit-profile-photo fl-wrap">
                                                    <div class="change-photo-btn photoUpload" style="margin-top: 5px; margin-bottom: 0px;">
                                                        <div class="file-loading">
                                                            <input type="file" class="input-44 product_images" accept="image/*" name="product_images[]" multiple="true">
                                                        </div>
                                                    </div>
                                                    <div class="img-error"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                                    <input class="product_id" type="hidden" name="product_id" value="<?php echo isset($product->id) && $product->id > 0 ? encreptIt($product->id) : ''; ?>"/>
                                                    <!--<button class="btn btn-success ripple btn-lg" type="submit">Submit</button>-->
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <!-- /.tab-content -->
</div>
<div class="modal fade question_modal" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel2" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title" id="myMediumModalLabel2">Manage Your question</h5>
            </div>
            <?php
            $q_url = base_url() . 'admin/product/commitQuestion/';
            if (isset($product->id) && $product->id > 0) {
                $q_url = base_url() . 'admin/product/commitQuestion/' . encreptIt($product->id);
            }
            ?>
            <form class="question_form" method="POST" action="<?php echo $q_url; ?>">
                <div class="alert alert-icon alert-danger border-danger fade show q-error-msg" role="alert" style="display:none;"></div>
                <div class="modal-body">
                </div>
                <div class="text-center mr-b-30">
                    <button type="submit" class="btn btn-success btn-rounded ripple text-left">Save Question</button> 
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    var product_id = "<?php echo (isset($product->id) && $product->id > 0) ? encreptIt($product->id) : ''; ?>";
    setTimeout(function(){
        loadQuestion();
    }, 1000);
    setTimeout(function() {
        loadAddonProducts();
        loadAddonOptions();
    }, 1000);
</script>
    