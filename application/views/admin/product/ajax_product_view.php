<?php if (isset($products) && !empty($products)) { ?>
    <ul class="ecommerce-products list-unstyled row">
        <?php foreach ($products as $list) { ?>
            <?php 
            $class    = '';

            if (isset($list->is_active) && $list->is_active == '0') {
                $class  = 'pro_deactivated';
            } ?> 

            <li class="col-12 col-sm-3 col-md-3  <?php echo $class; ?>">
                <div class="card border-0">
                    <div class="card-header p-0 border-0">
                        <a href="#">
                            <?php
                                $img_data[0] = '';
                                if (isset($list->product_images) && $list->product_images != '') {
                                    $img_data = explode(',', $list->product_images);
                                }
                            ?>
                            <img src="<?php echo base_url().'attachment/image/200/0/'.md5($img_data[0]); ?>" alt="" class="pro_thumb">
                        </a>
                        <span class="badge badge-danger pro_deactivate_badge">This product is deactivated</span>
                    </div>
                    <div class="card-body">
                        <section class="d-flex">
                            <h5 class="sub-heading-font-family mt-0 mr-auto mr-0-rtl ml-auto-rtl">
                                <?php echo isset($list->name) && $list->name != '' ? $list->name : ''; ?>
                            </h5>
                            <span class="h5 mt-0 sub-heading-font-family text-primary">
                                <?php echo isset($list->price) && $list->price != '' ? '$'.number_format($list->price, 2) : ''; ?>
                            </span>
                        </section>
                        <span class="text-muted sub-heading-font-family">
                            <?php echo isset($list->name) && $list->category_name != '' ? $list->category_name : ''; ?>
                        </span>
                    </div>
                    <div class="card-footer d-flex justify-content-between p-0">
<!--                        <div class="col-md-4 sub-heading-font-family text-center p-3">
                            <a class="btn btn-xs btn-outline-success ripple" href="<?php echo isset($list->id) && $list->id != '' ? '#' : ''; ?>">
                                <i class="fa fa-eye"></i> View
                            </a>
                        </div>-->
                        <div class="col-md-6 sub-heading-font-family text-center p-3">
                            <a class="btn btn-xs btn-outline-default ripple" href="<?php echo isset($list->id) && $list->id != '' ? base_url().'admin/product/setup/'.encreptIt($list->id) : ''; ?>">
                                <i class="fa fa-pencil"></i> Edit
                            </a>
                        </div>
                        <div class="col-md-6 sub-heading-font-family text-center p-3">
                            <a href="javascript:void(0);" class="btn btn-xs btn-outline-danger ripple deleteProduct" data-id="<?php echo isset($list->id) && $list->id != '' ? md5($list->id) : ''; ?>">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        <?php } ?>
    </ul>
<?php } else { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card blog-post-new">
                <h5 align="center">No records found</h5>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row pagination-block">
    <div class="col-md-12">
        <nav aria-label="Page navigation">
            <?php echo isset($links) ? $links : ''; ?>
        </nav>
    </div>
</div>