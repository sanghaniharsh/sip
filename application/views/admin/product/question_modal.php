<?php
    $options = array();
    if (isset($question->options)) {
        $options = json_decode($question->options, true);
    }
?>
<div class="form-group row">
    <label class="col-md-2 col-form-label" for="question">Question</label>
    <div class="col-md-10">
        <input class="form-control blog_category_slug" name="question" placeholder="Enter your question here" type="text" value="<?php echo isset($question->question) ? $question->question : ''; ?>" autocomplete="off">
    </div>
</div>
<div class="option_box">
    <?php if (! empty($options)) { ?>
        <?php foreach($options as $key => $option) { ?>
            <div class="form-group row">
                <label class="col-md-2 col-form-label" for="options[]">Option</label>
                <div class="col-md-8">
                    <input class="form-control blog_category_slug" name="options[]" placeholder="Enter option" type="text" value="<?php echo $option; ?>" autocomplete="off">
                </div>
                <div class="col-md-2">
                    <?php if ($key == 0) { ?>
                        <button type="button" class="btn btn-primary ripple add_more_option">Add More</button>
                    <?php } else {?>
                        <button class="btn btn-danger ripple remove_option">Remove</button>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div class="form-group row">
            <label class="col-md-2 col-form-label" for="options[]">Option</label>
            <div class="col-md-8">
                <input class="form-control blog_category_slug" name="options[]" placeholder="Enter option" type="text" value="" autocomplete="off">
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary ripple add_more_option">Add More</button>
            </div>
        </div>
    <?php } ?>
    
    <?php if (isset($question->id)) { ?>
        <input type="hidden" name="question_id" value="<?php echo encreptIt($question->id); ?>">
    <?php } ?>
</div>