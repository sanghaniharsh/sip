<?php if (isset($categoryData) && !empty($categoryData)) { ?>
    <?php $n = 0;
        foreach ($categoryData as $key => $parent) { 
            $n++;

            $class = 'pcategory';
            if ($n > 1) {
                $class = 'sub-category';
            }
            ?>
            <div class="category_select_ajax">
                <select name="category[]" class="form-control <?php echo $class; ?>">
                    <option value="">-- Select Category --</option>
                    <?php foreach ($parent as $child) { ?>
                        <option value="<?php echo isset($child['id']) ? $child['id'] : ''; ?>" <?php echo isset($child['id']) && $child['id'] == $key ? 'selected="selected"' : ''; ?> ><?php echo isset($child['name']) ? $child['name'] : ''; ?></option>
                    <?php } ?>
                </select>
            </div>
    <?php } ?>
<?php } ?>