<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Profile</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <form id="profileForm" method="POST">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="username">Username</label>
                            <div class="col-md-9">
                                <input class="form-control" name="username" placeholder="enter your username" type="text" value="<?php echo isset($user->username) ? $user->username : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input class="form-control" name="email" placeholder="enter username" type="text" value="<?php echo isset($user->email) ? $user->email : ''; ?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="username">Password</label>
                            <div class="col-md-9">
                                <input class="form-control" name="password" id="password" placeholder="enter your password" type="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">Confirm Password</label>
                            <div class="col-md-9">
                                <input class="form-control" name="cpassword" placeholder="enter your confirm password" type="password" />
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/dashboard'; ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>