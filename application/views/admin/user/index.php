<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Users</h6>
    </div>
</div>

<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="users_table">
                            <thead>
                                <tr>
                                    <th class="sorting">First Name</th>
                                    <th class="sorting">Last Name</th>
                                    <th class="sorting">Email</th>
                                    <th class="sorting">Phone</th>
                                    <th class="sorting">Created date</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>