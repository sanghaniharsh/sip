<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>Site Settings</h5>
                </div>
                <div class="widget-body clearfix">
                    <form id="siteSettingsForm" method="POST">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="from_name">From Name</label>
                            <div class="col-md-9">
                                <input class="form-control" name="from_name" placeholder="Enter from name" type="text" value="<?php echo isset($settings->from_name) ? $settings->from_name : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="from_email">From Email</label>
                            <div class="col-md-9">
                                <input class="form-control" name="from_email" placeholder="Enter from email" type="text" value="<?php echo isset($settings->from_email) ? $settings->from_email : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="paypal_username">Paypal Username</label>
                            <div class="col-md-9">
                                <input class="form-control" name="paypal_username" placeholder="enter your paypal username" type="text" value="<?php echo isset($settings->paypalUsername) ? $settings->paypalUsername : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="paypal_signature">Paypal Signature</label>
                            <div class="col-md-9">
                                <input class="form-control" name="paypal_signature" placeholder="enter your paypal signature" type="text" value="<?php echo isset($settings->paypalSignature) ? $settings->paypalSignature : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="paypal_password">Paypal Password</label>
                            <div class="col-md-9">
                                <input class="form-control" name="paypal_password" placeholder="enter your paypal username" type="text" value="<?php echo isset($settings->paypalPassword) ? $settings->paypalPassword : ''; ?>">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto btn-list">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/dashboard'; ?>" class="btn btn-outline-default btn-rounded">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>