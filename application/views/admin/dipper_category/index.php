<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-4">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Big Dipper Parts Category</h6>
            </div>
            <div class="col-sm-4">
                <input class="form-control top_search category_search" name="category_search" type="text" placeholder="Search something here">
            </div>
            <div class="col-sm-4">
                <a href="#" data-id="" class="categorybutton btn btn-primary pull-right" data-toggle="modal" data-target="#category-modal">Add Category</a>
            </div>
        </div>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder widget-full-content border-all px-0">
            <div class="alert alert-icon alert-danger border-danger fade show error-message-danger" role="alert" style="display:none;"></div>
            <div class="alert alert-icon alert-success border-success fade show error-message-success" role="alert" style="display:none;"></div>
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row no-gutters">
                        <div class="col-lg-12 col-md-12 mail-inbox">
                            <div class="categorylist">
                                <div class="ajax_loader absolute">
                                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="category-modal" class="modal fade category-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myLargeModalLabel">Add New Category</h5>
            </div>
            <div class="modal-body ">
                <form action="<?php echo admin_url().'dippercategory/commit/'.$category_id; ?>" method="POST" class="category-frm" enctype="multipart/form-data">
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mainURL = base_url+'admin/dippercategory/view/<?php echo $category_id; ?>';
    pageURL = base_url+'admin/dippercategory/view/<?php echo $category_id; ?>';
</script>