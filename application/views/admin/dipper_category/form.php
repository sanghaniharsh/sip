<div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
<input type="hidden" name="category_id"  class="category_id" value="<?php echo isset($categorydata->id) ? md5($categorydata->id) : ''; ?>">
<div class="form-group">
    <label for="cat_name">Category Name</label>
    <input class="form-control input_error" name="cat_name" type="text" placeholder="Type Name here" value="<?php echo (isset($categorydata)) ? $categorydata->name : '';?>">
</div>
<div class="form-group">
    <label for="avatar">Category Image</label>
    <div class="edit-profile-photo fl-wrap">
        <input class="categoryImage" name="avatar" type="file" accept="image/*">
    </div>
    <label class="avatar-error text-danger"></label>
</div>
<?php 
if(isset($categorydata)) { ?>
    <div class="form-group">
        <img class="category_img" src="<?php echo base_url().'attachment/image/85/85/'.md5($categorydata->image); ?>" alt="">
    </div>
<?php 
}?>
<div class="text-center mr-b-30">
    <button class="btn btn-rounded btn-success ripple" type="submit">Save Category</button>
</div>
