<?php if (isset($auction_list) && !empty($auction_list)) { ?>
    <div class="row row_position" id="auction_sort">
        <?php foreach ($auction_list as $list) { ?>
            <div class="col-md-4 mr-b-30" id="<?php echo $list->id; ?>">
                <div class="card blog-post-new news_block">
                    <div class="card-header sub-heading-font-family border-bottom-0 p-0">
                        <figure>
                            <?php
                                $img_data[0] = '';
                                if (isset($list->auction_images) && $list->auction_images != '') {
                                    $img_data = explode(',', $list->auction_images);
                                }
                            ?>
                            <a href="javascript:void(0);">
                                <img src="<?php echo base_url().'attachment/image/200/300/'.md5($img_data[0]); ?>" alt="" class="blog_thumb">
                            </a>
                        </figure>
                        <?php if($list->auction_type != 1) { ?>
                            <span class="badge badge-danger text-uppercase"><?php echo isset($list->a_date) && $list->a_date != '' ? date('M d, Y', strtotime($list->a_date)) : ''; ?></span>
                        <?php } else { ?>
                            <span class="badge badge-success online-badge text-uppercase">Online</span>
                        <?php } ?>
                        
                    </div>
                    <div class="card-body sub-heading-font-family">
                        <h5 class="card-title sub-heading-font-family mb-3"><?php echo isset($list->title) && $list->title != '' ? $list->title : ''; ?></h5>
                        <p class="card-text text-muted" align="justify">
                            <?php echo isset($list->address) && $list->address != '' ? description($list->address, 70) : ''; ?>
                        </p>
                    </div>
                    <div class="card-action d-flex border-0">
                        <?php if($list->auction_type != 1) { ?>
                            <a href="javascript:void(0);" class="card-link fw-300 mr-auto mr-0-rtl ml-auto-rtl"><?php echo isset($list->a_time) && $list->a_time != '' ? date('h:i A', strtotime($list->a_time)) : ''; ?></a>
                        <?php } ?>
                        <a class="btn btn-xs btn-outline-default ripple" href="<?php echo isset($list->id) && $list->id > 0 ? base_url().'admin/auction/setup/'.encreptIt($list->id) : ''; ?>">
                            <i class="fa fa-pencil"></i> Edit
                        </a> &nbsp;
                        <a class="btn btn-xs btn-outline-danger ripple delete-auction" data-id="<?php echo isset($list->id) && $list->id > 0 ? encreptIt($list->id) : ''; ?>" href="javascript:void(0);">
                            <i class="fa fa-trash"></i> Delete
                        </a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card blog-post-new">
                <h5 align="center">No records found</h5>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row pagination-block">
    <div class="col-md-12">
        <nav aria-label="Page navigation">
            <?php echo isset($links) ? $links : ''; ?>
        </nav>
    </div>
</div>