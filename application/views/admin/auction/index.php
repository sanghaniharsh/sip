<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-4">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Auctions</h6>
            </div>
            <div class="col-sm-4">
                <input class="form-control top_search auction_search" name="" type="text" placeholder="Search something here">
            </div>
            <div class="col-sm-4">
                <a href="<?php echo base_url().'admin/auction/setup'; ?>" class="btn btn-primary pull-right">Create New Auction</a>
            </div>
        </div>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <!-- Card with Image -->
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body">
                    <div class="auction_block">
                        <div class="ajax_loader absolute">
                            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
