<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>Add Auction</h5>
                </div>
                <hr>
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/auction/commit';

                        if (isset($auction->id) && ($auction->id > 0))
                        {
                            $url = base_url().'admin/auction/commit/'.md5($auction->id);
                        }
                        
                        $auction_type = (isset($auction->auction_type) && $auction->auction_type == 1 ? $auction->auction_type : 0);
                        
                    ?>
                    <form class="cmxform form-horizontal tasi-form auctionForm" id="auctionForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="title">Auction Title</label>
                            <div class="col-md-10">
                                <input class="form-control auction_slug" autocomplete="adsdad" name="title" placeholder="Enter Auction Title" type="text" value="<?php echo isset($auction->title) ? $auction->title : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="slug"> Slug</label>
                            <div class="col-md-10">
                                <?php
                                    $slug = "";
                                    if (isset($auction->slug) && ($auction->slug != '')) {
                                        $slug = $auction->slug;
                                    }
                                ?>
                                <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($auction->slug) ? $auction->slug : '' ?>"/>
                                <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="slug"> Online</label>
                            <div class="col-md-10">
                                <label class="toggle_switch">
                                    <input type="checkbox" name="auction_type" class="auction_type" <?php echo ($auction_type == 1 ? "checked='checked'" : '');?>>
                                  <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <?php  if (isset($auction_image) && !empty($auction_image)) { ?>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="avatar">Auction Images</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        <?php
                                            foreach ($auction_image as $list) {
                                                ?>
                                                <div class="col-md-2">
                                                    <img width="100" src="<?php echo base_url().'attachment/image/0/300/'.md5($list->image); ?>">
                                                    <button rel="<?php echo encreptIt($list->id); ?>"  type="button" class="badge bg-important auction-image delete-auction-image" aria-label="Close">
                                                        <span class="badge bg-important delete-image" data-rel="<?php echo encreptIt($list->id); ?>">
                                                            <i class="fa fa-times icon-delete"></i>
                                                        </span>
                                                    </button>
                                                </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="avatar">Auction Images</label>
                            <div class="col-md-10">
                                <div class="edit-profile-photo fl-wrap">
                                    <div class="change-photo-btn photoUpload" style="margin-top: 5px; margin-bottom: 0px;">
                                        <div class="file-loading">
                                            <input type="file" class="input-44 auction_images" accept="image/*" name="auction_images[]" multiple="true">
                                        </div>
                                    </div>
                                    <div class="img-error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label online_label <?php echo ($auction_type != 1 ? 'hide' : '');?>" for="a_date">Online Auction Date</label>
                            <label class="col-md-2 col-form-label offline_label <?php echo ($auction_type != 0 ? 'hide' : '');?>" for="a_date">Auction Date</label>
                            <div class="col-md-4">
                                <input type="text" name="a_date" placeholder="Enter Date" class="form-control online_label <?php echo ($auction_type != 1 ? 'hide' : '');?>" value="<?php echo isset($auction->a_date) ? $auction->a_date : ''; ?>" <?php echo ($auction_type != 1 ? 'disabled="disabled"' : '');?>>
                                <input type="text" name="a_date" placeholder="Select Date" class="form-control offline_label datepicker auctiondate <?php echo ($auction_type != 0 ? 'hide' : '');?>" value="<?php echo isset($auction->a_date) ? date('m/d/Y',strtotime($auction->a_date)) : ''; ?>" data-plugin-options='{"autoclose": true}' <?php echo ($auction_type != 0 ? 'disabled="disabled"' : '');?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label online_label <?php echo ($auction_type != 1 ? 'hide' : '');?>" for="title">Auction End Date & End Time</label>
                            <label class="col-md-2 col-form-label offline_label <?php echo ($auction_type != 0 ? 'hide' : '');?>" for="title">Auction Time</label>
                            <div class="col-md-3">
                                <input type="text" name="a_time" class="form-control online_label <?php echo ($auction_type != 1 ? 'hide' : '');?>" placeholder="Enter Auction Time" value="<?php echo isset($auction->a_time) ? $auction->a_time : ''; ?>" <?php echo ($auction_type != 1 ? 'disabled="disabled"' : '');?>>
                                <input type="text" name="a_time" autocomplete="adsdad" class="form-control offline_label auction_timepicker <?php echo ($auction_type != 0 ? 'hide' : '');?>" placeholder="Select Auction Time" value="<?php echo isset($auction->a_time) ? date('h:i A', strtotime($auction->a_time)) : ''; ?>" <?php echo ($auction_type != 0 ? 'disabled="disabled"' : '');?>>
                            </div>
                        </div>
                        <div class="form-group row offline_label <?php echo ($auction_type != 0 ? 'hide' : '');?>">
                            <label class="col-md-2 col-form-label" for="title">Preview  Time</label>
                            <div class="col-md-3">
                                <input class="form-control from_timepicker check_from_time" autocomplete="adsdad" name="from_time" id="from_timepicker" placeholder="From" type="text" value="<?php echo isset($auction->from_time) ? date('h:i A', strtotime($auction->from_time)) : ''; ?>">
                            </div>
                            <div class="col-md-3">
                                <input class="form-control to_timepicker " autocomplete="adsdad" name="to_time" placeholder="To" type="text" value="<?php echo isset($auction->to_time) ? date('h:i A', strtotime($auction->to_time)) : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label online_label <?php echo ($auction_type != 1 ? 'hide' : '');?>" for="address">Winning Bidders Pickup of Goods Address</label>
                            <label class="col-md-2 col-form-label offline_label <?php echo ($auction_type != 0 ? 'hide' : '');?>" for="address">Address</label>
                            <div class="col-md-10">
                                <div class="ckeditor fl-wrap">
                                    <textarea class="form-control " name="address" id="address"  rows="5"><?php echo isset($auction->address) ? $auction->address : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="video_link">Video Link</label>
                            <div class="col-md-10">
                                <input class="form-control" name="video_link" placeholder="Enter Youtube Link" type="text" value="<?php echo isset($auction->video_link) ? $auction->video_link : ''; ?>">
                            </div>
                        </div>
                         <?php if (isset($auction->video_link) && $auction->video_link != '') { ?>
                            <?php
                                $video_string   = '<iframe width="200" height="200" src="'.$auction->video_link.'" frameborder="0" allowfullscreen></iframe>';
                                if(preg_match('/watch/',$auction->video_link)) {
                                    $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"https://www.youtube.com/embed/$2\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen></iframe>",$auction->video_link);
                                } else if(preg_match('/youtu.be/',$auction->video_link)) {
                                    $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"https://www.youtube.com/embed/$2\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen></iframe>",$auction->video_link);
                                }
                            ?>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label"></label>
                                <div class="col-md-10">
                                    <?php echo $video_string; ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="pdf">Equipment  Pdf</label>
                            <div class="col-md-10">
                                <input class="form-control equipmentpdf" name="equipment_pdf"  type="file" accept=".pdf">
                            </div>
                        </div>
                        <?php if(isset($auction) && $auction->equipment_pdf > 0) { ?>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label"></label>
                                <div class="col-md-10">
                                    <a target="_blink" href="<?php echo base_url().'attachment/image/85/85/'.md5($auction->equipment_pdf); ?>" ><i class="fa fa-file-pdf-o" aria-hidden="true"  style="font-size:60px;color:red"></i></a>
                                </div>
                            </div>
                            <?php 
                        }
                        ?>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="addition_comment">ADDITIONAL COMMENTS</label>
                            <div class="col-md-10">
                                <div class="ckeditor fl-wrap">
                                    <textarea class="form-control ckeditor" name="addition_comment" id="addition_comment"  rows="5"><?php echo isset($auction->additional_comment) ? $auction->additional_comment : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($auction->is_active) && ($auction->is_active == 1)) || !isset($auction)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                    <input type="hidden" class="auction_id" name="auction_id" value="<?php echo isset($auction->id) && ($auction->id > 0) ? encreptIt($auction->id) : ''; ?>">
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/auction/' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

