<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-8">
                <h6 class="page-title-heading mr-0 mr-r-5">Manage Enterprise Product</h6>
            </div>
            <div class="col-sm-4 pull-right">
                <a href="<?php echo admin_url().'enterprise-product/setup'; ?>" class="btn btn-primary pull-right">Add New</a>
            </div>
        </div>
    </div>
</div>
<div class="widget-list product_list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row product_search_block">
                        <div class="col-sm-4">
                            <select class="form-control search-enterprise-list" id="l13">
                                <option value="">Search By Client</option>
                                <option value="0">All</option>
                                <?php
                                if(!empty($enterprises)) {
                                    foreach($enterprises  as  $enterprise) { ?>
                                        <option  value ="<?php echo $enterprise->id; ?>"><?php echo $enterprise->name;?></option>
                                    <?php 
                                    }
                                }?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control top_search product_search" name="" type="text" placeholder="Search By Product Name">
                        </div>
                        <div class="col-sm-4 ">
                            <input class="btn btn-info pull-right product-reset-btn" name="" type="button" value ="Reset">
                        </div>
                    </div>
                    <hr>
                    <div class="product-block">
                    <!-- Products List -->
                        <div class="ajax_loader absolute">
                            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
