<?php if (isset($product_image) && !empty($product_image)) { ?>
        <label class="col-md-2 col-form-label" for="Images">Images</label>
        <div class="col-md-10">
            <div class="row">
                <?php
                    foreach ($product_image as $list) {
                        ?>
                        <div class="col-md-2">
                            <img width="100" src="<?php echo base_url() . 'attachment/image/0/300/' . md5($list->image); ?>">
                            <button rel="<?php echo md5($list->id); ?>" data-itemid="<?php echo md5($list->product_id); ?>" type="button" class="badge bg-important blog-image delete-product-image" aria-label="Close">
                                <span class="badge bg-important delete-image" data-rel="<?php echo md5($list->id); ?>">
                                    <i class="fa fa-times icon-delete"></i>
                                </span>
                            </button>
                        </div>
                        <?php
                    }
                ?>
            </div>
        </div>
<?php } ?>