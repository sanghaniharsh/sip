
<div class="row page-title clearfix">
    <div class="page-title-left">
        <div class="row">
            <div class="col-sm-8">
                <h6 class="page-title-heading mr-0 mr-r-5">Add Enterprise Product</h6>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>
<div class="alert alert-icon alert-success border-success fade show success-msg" role="alert" style="display:none;"></div>

<div class="tabs product_add_page">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active tabs-block" href="#general-tab" data-toggle="tab" aria-expanded="true" role="tab"  aria-controls="general-tab" aria-selected="true">General Information</a>
        </li>
        <?php
            $tab = '';
            if (isset($product) && !empty($product)) {
                $tab = 'data-toggle="tab"';
            }
        ?>

        <li class="nav-item">
            <a class="nav-link tabs-block products-tab" href="<?php echo isset($product) && !empty($product) ? '#products-tab' : 'javascript:void(0)"'; ?>" <?php echo $tab; ?> aria-expanded="false" role="tab">Product Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabs-block images-tab" href="<?php echo isset($product) && !empty($product) ? '#images-tab' : 'javascript:void(0)'; ?>" <?php echo $tab; ?> aria-expanded="false" role="tab">Images</a>
        </li>
    </ul>
    <!-- /.nav-tabs -->
    <div class="tab-content">
        <div class="tab-pane active" id="general-tab" aria-expanded="true"  role="tabpanel">
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="">
                            <div class="widget-body clearfix">
                                <?php
                                $url    = base_url() . 'admin/enterprise-product/commit';

                                if (isset($product->id) && ($product->id > 0)) {
                                    $url = base_url() . 'admin/enterprise-product/commit/' . encreptIt($product->id);
                                }
                                ?>
                                <form class="cmxform form-horizontal tasi-form enterpriseproductForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                                    <input type="hidden" name="enterprise_product_id"  class="enterprise_product_id" value="<?php echo isset($product->id) ? encreptIt($product->id) : ''; ?>">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label">Enterprise</label>
                                        <div class="col-md-10">
                                            <select name="enterprise[]" class="form-control enterprise_product" multiple="multiple">
                                                <!--<option value="">-- Select Enterprise --</option>-->
                                                <?php if (isset($enterprises) && !empty($enterprises)) { ?>
                                                <?php foreach ($enterprises as $enterprise) { ?>
                                                    <option value="<?php echo $enterprise->id; ?>" <?php echo (in_array($enterprise->id, $enterprise_ids)) ? 'selected="selected"' : ''; ?> ><?php echo isset($enterprise->name) ? $enterprise->name : ''; ?></option>
                                                    <!-- <option value="1" selected="selected">1</option> -->
                                                    <!-- <option value="1">2</option> -->
                                                    <?php } ?>
                                                <?php } ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pname">Product Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control enterprise-product" name="name" placeholder="Enter Product Name" value="<?php echo isset($product->name) ? $product->name : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="slug"> URL</label>
                                        <div class="col-md-10">
                                            <?php
                                            $slug = "";
                                            if (isset($product->slug) && ($product->slug != '')) {
                                                $slug = $product->slug;
                                            }
                                            ?>
                                            <input class="form-control " id="slug" name="slug" readonly="" type="hidden" value="<?php echo isset($product->slug) ? $product->slug : '' ?>"/>
                                            <input class="form-control " id="slugText" readonly="" type="text" value="<?php echo $slug; ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="pprice">Model Number</label>
                                        <div class="col-md-3">
                                            <input class="form-control" name="model_number" placeholder="Enter Product Model Number" type="text" value="<?php echo isset($product->model_number) ? $product->model_number : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row ep-price" <?php echo (isset($product->awarded_price) && ($product->awarded_price == '1') ? "style='display:none'" : '') ; ?>>
                                        <label class="col-md-2 col-form-label" for="pprice">Price</label>
                                        <div class="col-md-10">
                                            <input class="form-control eprice" name="price" placeholder="Enter Product Price" type="text" value="<?php echo isset($product->price) ? $product->price : ''; ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">Status</label>
                                        <div class="checkbox checkbox-primary col-md-3" style="padding-top: 0.5625rem">
                                            <label>
                                                <?php
                                                $active_checked = "";
                                                if ((isset($product->status) && ($product->status == '1')) || !isset($product)) {
                                                    $active_checked = "checked='checked'";
                                                }
                                                ?>
                                                <input type="checkbox" name="status" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="">Best Pricing Will Be Awarded</label>
                                        <div class="checkbox checkbox-primary col-md-3" style="padding-top: 0.5625rem">
                                            <label>
                                                <?php
                                                $awarded_checked = "";
                                                if ((isset($product->awarded_price) && ($product->awarded_price == '1'))) {
                                                    $awarded_checked = "checked='checked'";
                                                }
                                                ?>
                                                <input type="checkbox" class="awarded_price" name="awarded_price" <?php echo $awarded_checked; ?> /><span class="label-text"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12 ml-md-auto btn-list text-center">
                                                <button class="btn btn-success ripple enterpriseProduct" type="submit">Submit</button>
                                                <a href="<?php echo admin_url() . 'enterprise-product' ?>" class="btn btn-outline-default ripple">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (isset($product) && !empty($product)) { ?>
            <div class="tab-pane" id="products-tab" aria-expanded="false"  role="tabpanel">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="widget-body clearfix">
                                    <?php
                                    if (isset($product->id) && $product->id > 0) {
                                        $url = base_url() . 'admin/enterprise-product/commit-specification/' . encreptIt($product->id);
                                    }
                                    ?>
                                    <form class="cmxform form-horizontal tasi-form productDetailForm" method="POST" action="<?php echo $url; ?>" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="description">Description</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor description" name="description" id="description" onPaste="return false"><?php echo isset($product->description) ? $product->description : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="specification">Specification</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor" name="specification" id="specification"><?php echo isset($product->specification) ? $product->specification : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="warranty">Warranty</label>
                                            <div class="col-md-10 editor-error">
                                                <div class="ckeditor fl-wrap">
                                                    <textarea class="form-control ckeditor" name="warranty" id="warranty"><?php echo isset($product->warranty) ? $product->warranty : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="images-tab" aria-expanded="false"  role="tabpanel">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="">
                                <div class="widget-body clearfix">
                                    <form class="cmxform form-horizontal tasi-form productImageForm" method="POST" enctype="multipart/form-data">
                                        <div class="form-group row product_image_block">
                                            <?php if (isset($product_images) && !empty($product_images)) { ?>
                                                <label class="col-md-2 col-form-label" for="avatar">Images</label>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <?php
                                                        foreach ($product_images as $list) {
                                                            ?>
                                                            <div class="col-md-2">
                                                                <img width="100" src="<?php echo base_url() . 'attachment/image/0/300/' . md5($list->image); ?>">
                                                                <button rel="<?php echo md5($list->id); ?>" data-itemid="<?php echo md5($list->product_id); ?>" type="button" class="badge bg-important blog-image delete-product-image" aria-label="Close">
                                                                    <span class="badge bg-important delete-image" data-rel="<?php echo md5($list->id); ?>">
                                                                        <i class="fa fa-times icon-delete"></i>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label" for="avatar">Product Images</label>
                                            <div class="col-md-10">
                                                <div class="edit-profile-photo fl-wrap">
                                                    <div class="change-photo-btn photoUpload" style="margin-top: 5px; margin-bottom: 0px;">
                                                        <div class="file-loading">
                                                            <input type="file" class="input-44 product_images" accept="image/*" name="product_images[]" multiple="true">
                                                        </div>
                                                    </div>
                                                    <div class="img-error"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                                    <input class="product_id" type="hidden" name="product_id" value="<?php echo isset($product->id) && $product->id > 0 ? encreptIt($product->id) : ''; ?>"/>
                                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <!-- /.tab-content -->
</div>