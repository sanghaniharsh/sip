<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Admin user</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <?php
                        $url = base_url().'admin/enterprise-user/commit/'.$id;

                        if (isset($user->id) && ($user->id > 0))
                        {
                            $url = base_url().'admin/enterprise-user/commit/'.$id.'/'.encreptIt($user->id);
                        }
                    ?>
                    <form id="userForm" method="POST" action="<?php echo $url; ?>" enterprise_id="<?php echo $id; ?>">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="firstname">First name</label>
                            <div class="col-md-9">
                                <input class="form-control" name="firstname" placeholder="enter your firstname" type="text" value="<?php echo isset($user->firstname) ? $user->firstname : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="lastname">Last name</label>
                            <div class="col-md-9">
                                <input class="form-control" name="lastname" placeholder="enter your lastname" type="text" value="<?php echo isset($user->lastname) ? $user->lastname : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input class="form-control" name="email" placeholder="enter username" type="text" value="<?php echo isset($user->email) ? $user->email : ''; ?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="lastname">Phone Number</label>
                            <div class="col-md-9">
                                <input class="form-control" name="phone" placeholder="enter your phone number" type="text" value="<?php echo isset($user->phone) ? $user->phone : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input class="form-control" id="password" name="password" placeholder="Password" type="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="cpassword">Confirm Password</label>
                            <div class="col-md-9">
                                <input class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" type="password">
                            </div>
                        </div>
                        <input type="hidden" name="userId" id="userId" value="<?php echo isset($user->id) ? encreptIt($user->id) : ""; ?>">
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="checkbox checkbox-primary">
                                <label>
                                    <?php 
                                        $active_checked = "";
                                        if ((isset($user->is_active) && ($user->is_active == 1)) || !isset($user)) {
                                            $active_checked = "checked='checked'";
                                        }
                                    ?>
                                    <input type="checkbox" id="active" name="is_active" <?php echo $active_checked; ?> /><span class="label-text">Active</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="lastname">Sales Tax </label>
                            <div class="col-md-9">
                                <input class="form-control" name="sales_tax" placeholder="enter sales tax" type="text" value="<?php echo isset($user->sales_tax) ? $user->sales_tax : ''; ?>">
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 ml-md-auto btn-list text-center">
                                    <button class="btn btn-success ripple btn-lg" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'admin/user'; ?>" class="btn btn-outline-default ripple">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>