<div class="row page-title clearfix">

    <div class="page-title-left">

        <div class="row">

            <div class="col-sm-8">

                <h6 class="page-title-heading mr-0 mr-r-5">Order Invoice</h6>

            </div>

            <div class="col-sm-4 pull-right text-right">

                <a href="#" data-toggle="modal" data-target="#order-status-modal" class="btn btn-info" style="margin: 0;"> Update order status</a>

            </div>

        </div>

    </div>

</div>

<div class="alert alert-icon alert-success border-success fade show error-message-success" role="alert" style="display:none;"></div>

<div class="widget-list">

    <div class="row">

        <div class="col-md-12 widget-holder">

            <div class="widget-bg">

                <div class="widget-body clearfix">

                    <div class="ecommerce-invoice">

                        <div class="d-flex">

                            <div class="col-md-6">

                                <h3>Invoice</h3>

                                
                                 
                            </div>

                            <div class="col-md-3"></div>

                            <div class="col-md-3 text-right">

                                <?php if(isset($userDetail->order_status) && $userDetail->order_status != '') { ?>

                                        <?php if($userDetail->order_status == '0') { ?>

                                                <span class="btn btn-xs btn-outline-default flex-1 mt-2 justify-content-center fs-18 order_status_div">In Progress</span>

                                        <?php } ?> 

                                        <?php if($userDetail->order_status == '1') { ?>

                                                <span class="btn btn-xs btn-outline-info flex-1 mt-2 justify-content-center fs-18 order_status_div">Shipped</span>

                                        <?php } ?>

                                        <?php if($userDetail->order_status == '2') { ?>

                                                <span class="btn btn-xs btn-outline-success flex-1 mt-2 justify-content-center fs-18 order_status_div">Delivered</span>

                                        <?php } ?>

                                <?php } ?>

                                    <small class="mt-2 d-block fs-16">Date : <?php echo isset($userDetail->created_date) ? date('m/d/Y',strtotime($userDetail->created_date)) : ''; ?></small>

                            </div>

                        </div>

                        <!-- /.row -->

                        <hr>

                        <div class="d-flex">

                            <div class="col-md-6 text-muted">

                                <h6 class="mr-t-0">Invoice To</h6><?php echo (isset($userDetail->firstname) || isset($userDetail->lastname)) ? $userDetail->firstname . ' ' . $userDetail->lastname : ''; ?>

                                <br>

                                    <?php echo isset($userDetail->street_address) ? $userDetail->street_address : ''; ?>

                                <br>

                                    <?php echo isset($userDetail->city) ? $userDetail->city : '';?> ,

                                    <?php echo isset($userDetail->state) ? $userDetail->state : ''; ?>  , 

                                    <?php echo isset($userDetail->zipcode) ? $userDetail->zipcode : ''; ?>

                                <br/>

                                Email : <?php echo isset($userDetail->email) ? $userDetail->email : '';?>

                                <br/>

                                Tel#  : <?php echo isset($userDetail->phone) ? $userDetail->phone : '';?>

                            </div>

                            <div class="col-md-6 text-right">

                                <h6 class="mr-t-0">Details:</h6><strong>Date:</strong>  <span class="text-muted"><?php echo isset($userDetail->created_date) ? date("F d  , Y", strtotime($userDetail->created_date)) : ''; ?></span>

                                <br>

                                    <strong>ID:</strong>  

                                    <span class="text-muted"><?php echo isset($userDetail->id) ? display_order($userDetail->id) : ''; ?></span>

                                <br>

                                    <strong>Amount Due:</strong>  

                                    <span class="text-muted">

                                        $<?php echo isset($userDetail->amount) ? display_amount($userDetail->amount) : 0; ?>

                                    </span>

                            </div>

                        </div>

                        <!-- /.row -->

                        <hr class="border-0">

                        <table class="table table-bordered table-striped">

                            <thead>

                                <tr class="bg-primary-dark text-white">

                                    <th>Product</th>

                                    <th class="text-center">Unit Cost</th>

                                    <th class="text-center">Quantity</th>

                                    <th class="text-center">Shipping</th>

                                    <th class="text-center">Total</th>

                                </tr>

                            </thead>

                            <tbody>

                                 <?php  $subTotal = 0; ?>

                                 <?php  $shippingTotal = 0; ?>



                                <?php if (isset($order) && !empty($order)) { ?>

                                    <?php foreach ($order as $row) { ?>
                                        

                                        <?php $subTotal = $subTotal + ($row->quantity * $row->price ); ?>

                                        <?php $shippingTotal = $shippingTotal + $row->shipping_amount; ?>

                                        <tr>

                                            <td>

                                                <?php echo isset($row->name) ? $row->name : ''; ?> <br/>

                                                <?php echo isset($row->modal_number) ? "MODEL #:".$row->modal_number : ''; ?> 

                                            </td>

                                            <td class="text-center">

                                                $<?php echo isset($row->price) ? number_format($row->price , 2) : ''; ?>

                                            </td>

                                            <td class="text-center">

                                                <?php echo isset($row->quantity) ? $row->quantity : ''; ?>

                                            </td>

                                            <td class="text-center">

                                                <?php echo (isset($row->shipping_amount) && $row->shipping_amount > 0 ) ? $row->shipping_amount : 'Free'; ?>

                                            </td>

                                            <td class="text-center">

                                                $<?php echo (isset($row->quantity) && isset($row->price)) ?  number_format(($row->quantity * $row->price) + $row->shipping_amount , 2) : ''; ?>

                                            </td>

                                        </tr>

                                    <?php } ?>

                                <?php } else { ?>

                                <?php } ?>

                            </tbody>

                        </table>

                        <div class="row">

                            <div class="col-md-8">

<!--                                <p>Thanks for your business</p>

                                <ul class="text-muted small">

                                    <li>Aeserunt tenetur cum nihil repudiandae perferendis fuga vitae corporis!</li>

                                    <li>Laborum, necessitatibus recusandae ullam at iusto dolore.</li>

                                    <li>Voluptatum aperiam voluptates quasi!</li>

                                    <li>Assumenda, iusto, consequuntur corporis atque culpa saepe magnam recusandae</li>

                                    <li>Possimus odio ipsam magni sint reiciendis unde amet</li>

                                </ul>-->

                            </div>

                            <div class="col-md-4 invoice-sum text-right">

                                <ul class="list-unstyled">

                                    <li>Sub - Total amount: $<?php echo isset($subTotal) ? display_amount($subTotal) : 0; ?></li>

                                    <li>Tax: $<?php echo (isset($row->tax)) ?  number_format($row->tax, 2) : ''; ?></li>

                                    <li>Discount: $<?php echo isset($userDetail->discount) ? display_amount($userDetail->discount) : 0; ?> </li>

                                    <li>Shipping Amount   : <?php echo ($shippingTotal > 0) ? "$" . $shippingTotal : 'Free'; ?></li>

                                    <li><strong>Grand Total: $<?php echo isset($userDetail->amount) ? display_amount($userDetail->amount) : 0; ?> </strong>

                                    </li>

                                </ul>

                            </div>

                        </div>

                        <!-- /.row -->

                    </div>

                    <!-- /.ecommerce-invoice -->

                </div>

                <!-- /.widget-body -->

            </div>

            <!-- /.widget-bg -->

        </div>

        <!-- /.widget-holder -->

    </div>

    <!-- /.row -->

</div>



<div id="order-status-modal" class="modal fade order-status-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h5 class="modal-title" id="myLargeModalLabel">Updates Order Status</h5>

            </div>

            <form class="order_status_frm" action="<?php echo admin_url().'order/update_order_status'; ?>" name="order-status-form" method="post">

                <div class="modal-body ">

                        <div class="row">

                            <div class="alert alert-icon alert-danger border-danger fade show error-msg" role="alert" style="display:none;"></div>

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label>Select Status</label>

                                    <select class="form-control order_status" name="order_status">

                                        <option value="">Select Status</option>

                                        <option value="0" <?php echo (isset($userDetail->order_status) && $userDetail->order_status == '0') ? 'selected' : '';?> >In Progress</option>

                                        <option value="1" <?php echo (isset($userDetail->order_status) && $userDetail->order_status == '1') ? 'selected' : '';?>>Shipped</option>

                                        <option value="2" <?php echo (isset($userDetail->order_status) && $userDetail->order_status == '2') ? 'selected' : '';?>>Delivered</option>

                                    </select>

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label>Enter Captcha</label>

                                    <input class="form-control order_captcha_txt" autocomplete="vbvbvbv" name="order_captcha" id="captcha" type="text" >

                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">

                                    <img id="captcha_code" src="<?php echo base_url().'secureimage/securimage'; ?>" style="width: 170px">

                                    <span class="reload_captcha">

                                        <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">

                                    </span>

                                </div>

                            </div>

                        </div>

                </div>

                <div class="modal-footer">

                    <div class="row">

                        <div class="col-md-12 text-center">

                            <input type="hidden" name="order_id" value="<?php echo isset($userDetail->id) ? encreptIt($userDetail->id) :''; ?>">

                            <button class="btn btn-primary" type="submit">Update Status</button>

                        </div>

                    </div>

                </div>

            </form>

        </div>

    </div>

</div>