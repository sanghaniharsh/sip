<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">Manage Orders</h6>
    </div>
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                        <table class="table table-striped table-responsive dataTable" id="orderDataTable">
                            <thead>
                                <tr>
                                    <th class="sorting">Order Id</th>
                                    <th class="sorting">User</th>
                                    <th class="sorting">Email</th>
                                    <th class="sorting">Amount</th>
                                    <th class="sorting">Order Status</th>
                                    <th class="sorting">Created Date</th>
                                    <th class="sorting"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
