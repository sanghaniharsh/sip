<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/owl.theme.css">

<?php // p($blog); ?>
<div class="blog_post">
    <div class="container"> 
        <div class="row"> 
            <div class="col-xs-12 col-sm-9">
                <div class="page-title">
                    <h1><?php echo isset($blog->title) && $blog->title != '' ? $blog->title : ''; ?></h1>
                </div>
                <div class="entry-detail">
                    <?php if (isset($blog_images) && ! empty($blog_images)) { ?>
                        <div class="entry-photo">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="slider-items-products">
                                        <div id="blog-slider" class="product-flexslider hidden-buttons">
                                            <div class="slider-items slider-width-col1 owl-carousel owl-theme">
                                                <?php foreach ($blog_images as $image) { ?>
                                                    <div class="item">
                                                        <img src="<?php echo base_url() . 'attachment/image/0/850/' . md5($image); ?>" alt="Blog">
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="entry-meta-data">
                        <span class="cat">
                            <i class="fa fa-folder"></i>&nbsp; 
                            <a href="<?php echo isset($blog->category_slug) && $blog->category_slug != '' ? base_url() . 'blog/category/' . $blog->category_slug : ''; ?>">
                                <?php echo isset($blog->category_name) && $blog->category_name != '' ? $blog->category_name : ''; ?>
                            </a>
                        </span>
                        <span class="date">
                            <i class="fa fa-calendar">&nbsp;</i>&nbsp; <?php echo isset($blog->created_date) && $blog->created_date != '' ? date('M d, Y', strtotime($blog->created_date)) : ''; ?>
                        </span>
                    </div>
                    <div class="content-text clearfix">
                        <p><?php echo isset($blog->description) && $blog->description != '' ? html_entity_decode($blog->description) : ''; ?></p>
                    </div>

                </div>
            </div>
            <!-- ./ Center colunm --> 
            <!-- Left colunm -->

            <aside class="right sidebar col-xs-12 col-sm-3"> 
                <!-- Blog category -->
                <div class="block blog-module">
                    <p class="title_block">Blog Categories</p>
                    <div class="block_content"> 
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php if (!empty($blog_category)) { ?>
                                        <?php foreach ($blog_category as $row) { ?>
                                            <li>
                                                <a href="<?php echo isset($row->slug) && $row->slug != '' ? base_url() . 'blog/category/' . $row->slug : 'javascript:void(0);'; ?>">
                                                    <i class="fa fa-angle-right"></i>&nbsp; 
                                                    <?php echo isset($row->name) && $row->name != '' ? $row->name : ''; ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered --> 
                    </div>
                </div>
            </aside>
        </div>
    </div>
    <hr>
</div>