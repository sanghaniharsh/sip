<?php 
if(isset($postlist) && !empty($postlist)) {

    foreach($postlist as $row) { ?>
        <li>
            <div class="post-thumb"> <a href="<?php echo isset($row->slug) && $row->slug != '' ? base_url().'blog/'.$row->slug : ''; ?>"><img src="<?php echo base_url().'attachment/image/70/50/'.md5($row->b_image); ?>" alt="Blog"></a> </div>
            <div class="post-info">
                <h5 class="entry_title"><a href="<?php echo isset($row->slug) && $row->slug != '' ? base_url().'blog/'.$row->slug : ''; ?>"> <?php echo isset($row->title) && $row->title != '' ? $row->title : ''; ?></a></h5>
                <div class="post-meta"> <span class="date"><i class="fa fa-calendar"></i> <?php echo isset($row->created_date) && $row->created_date != '' ? date('Y-m-d',strtotime($row->created_date)) : ''; ?></span> </div>
            </div>
        </li>
        <?php 
    }
} ?>