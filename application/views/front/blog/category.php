<div class="blog_post">
    <div class="container"> 
        <div class="row"> 
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="page-title">
                    <h2>Blog post</h2>
                </div>
                <ul class="blog-posts">
                    <?php if (!empty($blog_list)) { ?>
                        <?php foreach($blog_list as $row) { ?>
                            <li class="post-item">
                                <article class="entry">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-4">
                                            <div class="entry-thumb"> 
                                                <a href="<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">
                                                    <?php
                                                        $img_data[0]
                                                                = '';
                                                        if (isset($row->blog_images) && $row->blog_images != '') {
                                                            $img_data = explode(',', $row->blog_images);
                                                        }
                                                    ?>
                                                    <img src="<?php echo base_url().'attachment/image/0/300/'.md5($img_data[0]); ?>" alt="Blog">
                                                </a> 
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-xs-8">
                                            <h3 class="entry-title">
                                                <a href="<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">
                                                    <?php echo isset($row->title) && $row->title != '' ? $row->title : ''; ?>
                                                </a>
                                            </h3>
                                            <div class="entry-meta-data">
                                                <span class="cat">
                                                    <i class="fa fa-folder"></i>&nbsp; 
                                                    <a href="<?php echo isset($row->category_slug) && $row->category_slug != '' ? $row->category_slug : '#'; ?>">
                                                        <?php echo isset($row->category_name) && $row->category_name != '' ? ucfirst($row->category_name) : ''; ?>
                                                    </a>
                                                </span>
                                                <span class="date">
                                                    <i class="fa fa-calendar"></i>&nbsp; <?php echo isset($row->created_date) && $row->created_date != '' ? $row->created_date : ''; ?>
                                                </span> 
                                            </div>
                                            <div class="entry-excerpt">
                                                <?php echo isset($row->description) && $row->description != '' ? description($row->description, 110) : ''; ?>
                                            </div>
                                            <div class="entry-more">
                                                <a href="<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>" class="button">Read more&nbsp; 
                                                    <i class="fa fa-angle-double-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <li class="post-item">
                            <article class="entry">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <center> No Blogs Found </center>
                                    </div>
                                </div>
                            </article>
                        </li>
                    <?php } ?>
                </ul>
                <div class="sortPagiBar">
                    <div class="pagination-area" style="visibility: visible;">
                        <?php echo isset($links) ? $links : ''; ?>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm --> 
            <!-- Left colunm -->

            <aside class="right sidebar col-xs-12 col-sm-3"> 
                <!-- Blog category -->
                <div class="block blog-module">
                    <p class="title_block">Blog Categories</p>
                    <div class="block_content"> 
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php if (!empty($blog_category)) { ?>
                                        <?php foreach ($blog_category as $row) { ?>
                                            <li>
                                                <a href="<?php echo isset($row->slug) && $row->slug != '' ? base_url().'blog/category/'.$row->slug : 'javascript:void(0);'; ?>">
                                                    <i class="fa fa-angle-right"></i>&nbsp; 
                                                    <?php echo isset($row->name) && $row->name != '' ? $row->name : ''; ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./blog category  -->
            </aside>
        </div>
    </div>
</div>