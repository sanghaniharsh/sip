<?php if (isset($parent['children']) && ! empty($parent['children'])) { ?>
    <li class="cat-item current-cat cat-parent"><a href= "#"><?php echo $parent['name']; ?></a>
        <ul class="children">
            <li class="cat-item cat-parent"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a>
                <ul class="children">
                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; sasa</a></li>
                    <li class="cat-item cat-parent"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Hasasandbags</a>
                        <ul style="display: none;" class="children">
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a></li>
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Sling bag</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a> </li>
            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a> </li>
        </ul>
    </li>
<?php } else { ?>
    <li class="cat-item"><a href="#"><?php echo $parent['name']; ?></a></li>
<?php } ?>