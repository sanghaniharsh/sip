<?php p($category); ?>
<div class="container" style="padding-top: 50px;">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div class="category-sidebar">
                <div class="sidebar-title">
                    <h3>Categories</h3>
                </div>
                <ul class="product-categories">
                    <?php
                        if (!empty($category)) {
                            for($i = 0; $i < count($category); $i++) {
                                for($j = 0; $j < count($category[$i]); $j++) {
                                    if (!empty($category[$i]['parent']['children'])) {
//                                        p($category[$i]['parent']['children']);
                                    }
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-main col-sm-9">
            <div class="page-title">
                <h2>Search result title goes here</h2>
            </div>
            <div class="toolbar">
                <div class="sorter">
                    <div class="short-by">
                        <label>Sort By:</label>
                        <select>
                            <option selected="selected">Position</option>
                            <option>Name</option>
                            <option>Price</option>
                            <option>Latest</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="product-block"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    pageURL = base_url+'search/products/';
    setTimeout(function() {
        loadProducts();
    }, 1000);
</script>