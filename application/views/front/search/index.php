<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="">
                        <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a>
                        <span>»</span>
                    </li>
                    <?php 
                        $total  = count($parents);
                        $i      = 1;
                        foreach($parents as $row) {
                    ?>
                        <li class="">
                            <?php if ($total != $i) { ?>
                                <a title="Go to <?php echo $row->name; ?>" href="<?php echo base_url().'category/'.$row->slug; ?>">
                                    <?php echo $row->name; ?>
                                </a>
                                <span>»</span>
                            <?php } else { ?>
                                <strong><?php echo $row->name; ?></strong>
                            <?php } ?>
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-sm-3 col-xs-12 search_sidebar_sticky">
            <div class="category-sidebar">
                <div class="sidebar-title">
                    <h3>Categories</h3>
                </div>
                <ul class="product-categories">
                    <?php foreach($category as $c_row) { ?>
                        <?php if (isset($c_row['parent']['name'])) { ?>
                            <li class="cat-item">
                                <a href= "<?php echo base_url().'search/'.$c_row['parent']['slug']; ?>"><?php echo $c_row['parent']['name']; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="col-main col-sm-9">
<!--            <div class="page-title">
                <h2>Search result title goes here</h2>
            </div>-->
            <div class="toolbar">
                <div class="sorter">
                    <div class="short-by">
                        <label>Sort By:</label>
                        <select>
                            <option selected="selected">Position</option>
                            <option>Name</option>
                            <option>Price</option>
                            <option>Latest</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="product-block">
                <div class="ajax_loader absolute">
                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var pageURL;
    $(document).ready(function(){
        var query_string = "<?php echo (isset($_GET['q'])) ? $_GET['q'] : ''; ?>"
        pageURL = base_url+'search/getProducts/'+'<?php echo $slug; ?>';
        console.log(query_string);
        if (query_string != '') {
            pageURL = base_url+'search/getProducts/'+'<?php echo $slug; ?>?q='+query_string;
        }
    });
</script>