<?php if (isset($products) && !empty($products)) { ?>
    <div class="product-grid-area">
        <ul class="products-grid">
            <?php foreach ($products as $list) { ?>
            <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                    <div class="item-inner">
                        <div class="product-thumbnail">
                            <div class="pr-img-area">
                                <figure>
                                    <?php
                                        $img_data[0] = '';
                                        if (isset($list->product_images) && $list->product_images != '') {
                                            $img_data = explode(',', $list->product_images);
                                        }
                                    ?>
                                    <a href="<?php echo base_url();?>product/<?php echo isset($list->slug) && $list->slug != '' ? $list->slug : ''; ?>">
                                        <img class="first-img" src="<?php echo base_url().'attachment/image/0/350/'.md5($img_data[0]); ?>" alt="">
                                    </a>
                                </figure>
                                <a href="<?php echo base_url();?>product/<?php echo isset($list->slug) && $list->slug != '' ? $list->slug : ''; ?>" class="add-to-cart-mt">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span> View details</span>
                                </a>
                            </div>
                        </div>
                        <div class="item-info">
                            <div class="item-title"> 
                                <a href="<?php echo base_url();?>product/<?php echo isset($list->slug) && $list->slug != '' ? $list->slug : ''; ?>">
                                    <?php echo isset($list->name) && $list->name != '' ? $list->name : ''; ?>
                                </a>
                            </div>
                            <div class="item-price">
                                <?php if (isset($list->call_for_price) && ($list->call_for_price == '1')) { ?>
                                <h6 class="call_for_price">Call for discounted price</h6>
                                <?php } else { ?>
                                    <p class="special-price">
                                        <span class="price">
                                            $<?php echo isset($list->price) ? display_amount($list->price) : 0; ?>
                                        </span>
                                    </p>
                                    <?php if (isset($list->list_price) && $list->list_price > 0) { ?>
                                        <p class="old-price">
                                            <span class="price">
                                                $<?php echo isset($list->list_price) ? display_amount($list->list_price) : 0; ?>
                                            </span>
                                        </p>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
<?php } else { ?>
    <div class="row no-product-found">
        <div class="col-sm-12">
            <i class="fa fa-shopping-cart"></i>
            <h3>No products found</h3>
            <p>Please try another category</p>
        </div>
    </div>
<?php } ?>
<div class="pagination-area wow fadeInUp">
    <?php echo isset($links) ? $links : ''; ?>
</div>