<div class="container">
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="">
                        <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a>
                        <span>»</span>
                    </li>
                    <?php if (isset($search_category) && ! empty($search_category)) { ?>
                        <li class="">
                            <a title="Search for DIPPER EQUIPMENT" href="<?php echo base_url().'dipper-equipment'; ?>">USED RESTAURANT EQUIPMENT</a>
                            <span>»</span>
                        </li>
                        <li class="">
                            <?php foreach($search_category as $key => $u_category) { ?>
                                <?php if ($key != 0) { ?>
                                    <span>/</span>
                                <?php } ?>
                                <a title="Search for <?php echo $u_category->name ?>" href="<?php echo base_url().'used-equipment?search_category='.$u_category->id; ?>"><strong><?php echo $u_category->name ?></strong></a>
                            <?php } ?>
                        </li>
                    <?php } else { ?>
                        <li class="">
                            <strong>Big Dipper Grease Trap Parts</strong>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-sm-3 col-xs-12 search_sidebar_sticky">
            <div class="category-sidebar old_product_sidebar">
                <div class="sidebar-title">
                    <h3>Refine Search</h3>
                </div>
                <form  name="oldproduct_search" id="dipper_product_search" method="GET" action="">
                    <div class="old_product_filter_box">
                        <div class="old_product_input">
                            <?php $search = $this->input->get('search');  
                                  $search_category = $this->input->get('search_category');
                                  if($search_category != "") {
                                      $search_category = explode(",", $search_category);
                                  }
                                  ?>
                            <input type="text" class="form-control input-lg"  name="search" placeholder="Search for Big Dipper Parts Grease Trap Parts" value="<?php if(isset($search) && !empty($search)){echo $search; } ?>" >
                        </div>
                        <div class="old_product_checkboxes">
                         <?php  if (!empty($categoryData)) { ?>
                            <?php foreach ($categoryData as $categoryNamelist) { ?>
                            <label class="custom_check"><?php echo isset($categoryNamelist['name']) && $categoryNamelist['name'] != '' ? $categoryNamelist['name'] : ''; ?>
                                <input type="checkbox" value="<?php echo $categoryNamelist['id'];?>" class="select_categoty"  <?php echo (!empty($search_category) && in_array($categoryNamelist['id'], $search_category)) ? 'checked' : '';?>>
                                <span class="checkmark"></span>
                            </label>
                         <?php } } ?>
                            <input type="hidden" name="search_category" id="search_category" value="">
                        </div>
                        <div class="filter_button">
                            <button type="submit" class="but_medium5"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-main col-sm-9">
            <div class="page-title">
                <h2>Big Dipper Grease Trap Parts </h2>
            </div>
            <ul class="products-grid gras-parts-list">
                <?php if(isset($product_list) && !empty($product_list)){ ?>
                <?php foreach($product_list as $productdata){ ?>
                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp">
                    <div class="product-item">
                        <div class="item-inner">
                            <div class="product-thumbnail">
                                <div class="pr-img-area">
                                    <figure>
                                        <?php 
                                            $images[0] =  '';
                                            if(isset($productdata->product_images) && $productdata->product_images != "") {
                                                $images = explode(',',$productdata->product_images);
                                            }
                                        ?>
                                        <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>">
                                            <img src="<?php echo base_url().'attachment/image/213/0/'.md5($images[0]); ?>" class="img-responsive">
                                        </a>
                                    </figure>
                                    <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>" class="add-to-cart-mt">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span> View details</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-info">
                                <div class="item-title"> 
                                    <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>"><?php echo $productdata->name; ?></a>
                                </div>
                                <div class="entry-meta-data">
                                    <span class="cat">
                                        <a href="#"><?php $result = $this->dipper_category_model->getCategoryNames($productdata->category_id);   echo isset($result->name) ? $result->name : ''; ?></a>
                                    </span>
                                </div>
                                <div class="item-price">
                                    <p class="special-price">
                                        <span class="price">
                                            <?php if ($productdata->call_for_price == 1) {  ?>
                                            <span class="call_for">Call for Discounted Price</span>
                                            <?php } else { ?>
                                                $<?php echo isset($productdata->price) ? display_amount($productdata->price) : 0; ?>
                                            <?php } ?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?php } } else { ?>
                    <li class="post-item">
                        <article class="entry">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h4 style="margin-top: 55px;"> No Product Found </h4>
                                </div>
                            </div>
                        </article>
                    </li>
                <?php } ?>
            </ul>
            <div class="row">
                <div class="col-md-12 text-center pagination-area" id="pagination">
                    <div class="btn-group float-right">
                        <?php echo isset($links) ? $links : ''; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
