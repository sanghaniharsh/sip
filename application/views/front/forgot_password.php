<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <div class="login-panel panel panel-default" style="background: #fff;">
                <div class="panel-heading">
                    <h3 class="panel-title">Forgot Password</h3></div>
                <div class="panel-body">
                    <form role="form" name="forgot_frm" id="forgot_frm" method="POST" style="margin-top: 30px;">
                        <fieldset>
                            <div class="form-group">
                                <label>Enter your email here</label>
                                <input class="form-control" placeholder="E-mail" name="email" type="text" autocomplete="off"> > 
                            </div>
                            <hr>
                            <button class="btn btn-login" name="forgot_submit" id="forgot_submit" type="submit">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        
        
    </div>
</div>