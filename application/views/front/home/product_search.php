<div class="layered">
    <div class="layered-content">
        <ul class="blog-list-sidebar popular-post-list location_search"> 
            <?php foreach($product as $row) { ?>
            <?php 
            $images[0] =  '';
            if(isset($row->product_images) && $row->product_images != "") {
                $images = explode(',',$row->product_images);
            }
            ?>
                <li  class="listing-li top_product_search_dropdown">
                    <a href="<?php echo base_url(); ?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">
                        <div class="post-thumb">
                            <img src="<?php echo base_url() . 'attachment/image/75/50/' . md5($images[0]); ?>" alt="Blog"> 
                        </div>
                        <div class="post-info">
                            <h5 class="entry_title">
                                <?php echo $row->name; ?>
                            </h5>
                            <div class="post-meta"><span class=""><?php echo isset($row->category_name) && $row->category_name != '' ? $row->category_name : ''; ?></span> </div>
                            <div class="post-meta">Price :    <span class="">$ <?php echo isset($row->price) && $row->price != '' ? number_format($row->price, 2) : ''; ?></span> </div>
                        </div>
                    </a>
                </li>
             <?php } ?>
        </ul>    
    </div>
</div>