<ul class="location_search">
    <?php foreach($location as $row) { ?>
        <?php 
            $data_name  = $row->city;
            $data_rel   = $row->city;
            $slug       = ''; 
            if($row->state != '') {
                $data_name  = $data_name .', ' . $row->state;
                $data_rel   = $data_rel .', ' . $row->state;
            }

            if (isset($row->citySearch) && $row->citySearch == true) {
                $slug       = $row->city;
            } else if($row->zipcode != '') {
                $data_name = $data_name . ' ' . $row->zipcode;
                $slug       = $row->zipcode;
            }
        ?>
        <li class="listing-li" data-name="<?php echo $data_name; ?>" data-rel="<?php echo $data_rel; ?>"  data-slug="<?php echo $slug; ?>">
            <span class="list_item"><?php echo $data_name; ?></span>
        </li>
    <?php } ?>
</ul>