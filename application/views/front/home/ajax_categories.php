<div class="collection-content">
    <div class="row">
        <?php 
            if (isset($category_list) && !empty($category_list)) {
                foreach ($category_list as $row) {
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="wow bounceInUp" data-wow-delay="0.2s"> 
                            <div class="single-collection">
                                <?php
                                    $url = isset($row->slug) ? base_url().'search/'.$row->slug : 'javascript:void(0);';
                                    if (isset($row->child) && $row->child > 0) {
                                        $url = isset($row->slug) ? base_url().'category/'.$row->slug : 'javascript:void(0);';
                                    }
                                ?>
                                <a href="<?php echo $url; ?>">
                                    <img src="<?php echo base_url().'attachment/image/350/350/'.md5($row->image); ?>" alt="" class="img-thumbnail">
                                </a>
                                <div class="collections-link">
                                    <a href="<?php echo $url; ?>">
                                        <?php echo isset($row->name) ? $row->name : ''; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            } else {
               echo  "Sorry No record found";
            }
        ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12 text-center" id="pagination">
        <div class="btn-group float-right">
            <?php 
            echo isset($pagination_link) ? $pagination_link : '';
            ?>
        </div>
    </div>
</div>