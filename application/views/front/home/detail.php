<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="home"> <a title="Go to Home Page" href="#">Home</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 1</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 2</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 3</a><span>»</span></li>
                    <li class="category13"><strong>Cooking Performance Group</strong></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="product-name">
                <h1>Cooking Performance Group S60-G24-N Natural Gas 6 Burner</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-main">
            <div class="product-view-area">
                <div class="product-big-image col-xs-12 col-sm-8 col-lg-8 col-md-8">
                    <div class="large-image"> 
                        <a href="<?php echo front_asset_url() ?>images/pro1.jpg" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 
                            <img class="zoom-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt="products"> 
                        </a> 
                    </div>
                    <div class="flexslider flexslider-thumb">
                        <ul class="previews-list slides">
                            <li>
                                <a href='<?php echo front_asset_url() ?>images/pro1.jpg' class='cloud-zoom-gallery'>
                                    <img src="<?php echo front_asset_url() ?>images/pro1.jpg" alt = "Thumbnail 2"/>
                                </a>
                            </li>
                            <li>
                                <a href='<?php echo front_asset_url() ?>images/pro1.jpg' class='cloud-zoom-gallery'>
                                    <img src="<?php echo front_asset_url() ?>images/pro1.jpg" alt = "Thumbnail 2"/>
                                </a>
                            </li>
                            <li>
                                <a href='<?php echo front_asset_url() ?>images/pro1.jpg' class='cloud-zoom-gallery'>
                                    <img src="<?php echo front_asset_url() ?>images/pro1.jpg" alt = "Thumbnail 2"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 col-md-4">
                    <div class="product-details-area">
                        <div class="price-box">
                            <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $329.99 </span> </p>
                            <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $359.99 </span> </p>
                        </div>
                        <div class="short-description">
                            <h2 class="text-center">We accept all types of payments.</h2>
                            <div class="payment">
                                <ul>
                                    <li><a href="#"><img title="Visa" alt="Visa" src="<?php echo front_asset_url() ?>images/visa.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Paypal" alt="Paypal" src="<?php echo front_asset_url() ?>images/paypal.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Discover" alt="Discover" src="<?php echo front_asset_url() ?>images/discover.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Master Card" alt="Master Card" src="<?php echo front_asset_url() ?>images/master-card.png" class="grayscale"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-variation add_to_cart_custom_block">
                            <form action="#" method="post">
                                <div class="cart-plus-minus">
                                    <label for="qty">Quantity:</label>
                                    <div class="numbers-row">
                                        <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                                        <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                                        <div onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                                    </div>
                                </div>
                                <button class="button pro-add-to-cart" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                            </form>
                        </div>
                        <div class="row support-client sidebar_free_shipping">
                            <div class="col-md-12 col-sm-10">
                                <div class="box-container free-shipping">
                                    <div class="box-inner">
                                        <h2>Free Shipping in this item</h2>
                                        <p>Should be arrive on monday</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-overview-tab wow fadeInUp">

            <div class="col-xs-12">
                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"> 
                        <a href="#description" data-toggle="tab"> Description </a> 
                    </li>
                    <li>
                        <a href="#specification" data-toggle="tab">Specifications</a>
                    </li>
                    <li> 
                        <a href="#warranty" data-toggle="tab">Warranty</a> 
                    </li>
                </ul>
                <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="description">
                        <p class="text-justify">Proin lectus ipsum, gravida et mattis vulputate, 
                            tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in 
                            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend 
                            laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla
                            luctus malesuada tincidunt. Nunc facilisis sagittis ullamcorper. Proin 
                            lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et 
                            lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et 
                            ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus 
                            adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada 
                            tincidunt. Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, 
                            gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. 
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                            cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl 
                            ut dolor dignissim semper. Nulla luctus malesuada tincidunt.</p>
                    </div>
                    <div class="tab-pane fade" id="specification">
                        <p class="text-justify"> Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
                    </div>
                    <div class="tab-pane fade" id="warranty">
                        <p class="text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="related-product-area" style="padding-bottom: 50px;">
            <div class="page-header-wrapper">
                <div class="page-header text-center wow fadeInUp">
                    <h2 style="margin-bottom: 35px;">Related <span class="text-main">Products</span></h2>
                    <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="product-item">
                            <div class="item-inner">
                                <div class="product-thumbnail">
                                    <div class="icon-new-label new-right">New</div>
                                    <div class="pr-img-area"><figure> <img class="first-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt=""></figure>
                                        <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> View details</span> </button>
                                    </div>
                                </div>
                                <div class="item-info">
                                    <div class="item-title"> 
                                        <a title="Ipsums Dolors Untra" href="detail.php">Ipsums Dolors Untra </a> 
                                    </div>
                                    <div class="item-price">
                                        <p class="special-price">$456.00</p>
                                        <p class="old-price">$567.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-3">
                    <div class="product-item">
                            <div class="item-inner">
                                <div class="product-thumbnail">
                                    <div class="icon-new-label new-right">New</div>
                                    <div class="pr-img-area"><figure> <img class="first-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt=""></figure>
                                        <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> View details</span> </button>
                                    </div>
                                </div>
                                <div class="item-info">
                                    <div class="item-title"> 
                                        <a title="Ipsums Dolors Untra" href="detail.php">Ipsums Dolors Untra </a> 
                                    </div>
                                    <div class="item-price">
                                        <p class="special-price">$456.00</p>
                                        <p class="old-price">$567.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-3">
                    <div class="product-item">
                            <div class="item-inner">
                                <div class="product-thumbnail">
                                    <div class="icon-new-label new-right">New</div>
                                    <div class="pr-img-area"><figure> <img class="first-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt=""></figure>
                                        <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> View details</span> </button>
                                    </div>
                                </div>
                                <div class="item-info">
                                    <div class="item-title"> 
                                        <a title="Ipsums Dolors Untra" href="detail.php">Ipsums Dolors Untra </a> 
                                    </div>
                                    <div class="item-price">
                                        <p class="special-price">$456.00</p>
                                        <p class="old-price">$567.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-3">
                    <div class="product-item">
                        <div class="item-inner">
                            <div class="product-thumbnail">
                                <div class="icon-new-label new-right">New</div>
                                <div class="pr-img-area"><figure> <img class="first-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt=""></figure>
                                    <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> View details</span> </button>
                                </div>
                            </div>
                            <div class="item-info">
                                <div class="item-title"> 
                                    <a title="Ipsums Dolors Untra" href="detail.php">Ipsums Dolors Untra </a> 
                                </div>
                                <div class="item-price">
                                    <p class="special-price">$456.00</p>
                                    <p class="old-price">$567.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>