<div class="container">
    <div class="row">
        <div class="col-md-12 text-center no-page-found learn_features">
            <img src="<?php echo base_url(); ?>assets/front-side/images/error-img.png">
            <h4>Ohh.....You Requested the page that is no longer There.</h4>
            <hr>
            <a href="<?php echo base_url() ?>" class="btn_blue btn">Back to Home</a>
        </div>
    </div>
</div>