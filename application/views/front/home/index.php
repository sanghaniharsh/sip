<div class="main-slideshow">
    <img src="<?php echo front_asset_url() ?>images/homepage.jpg"  alt="" style="width: 100%;"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> 
    <div class="slider_text_block">
        <h1>We are a full service restaurant supply<br> and commercial kitchen company</h1>
        <a href="<?php echo base_url(); ?>category" class="but_large2 slider_btn"> View Our Equipment</a>
    </div>
</div>
<!-- .fullwidth -->

<!-- COLLECTION-AREA-START -->
<section class="collestion-area">
    <div class="container">
        <div class="page-header text-center wow fadeInUp">
            <h2 class="blue">Top Categories</h2>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
        </div>
        <div class="category_block">
            <div class="ajax_loader absolute">
                <div class="lds-roller">
                    <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>   
                </div>
            </div>
        </div>
        <p class="lead text-gray text-center"> If you don't find something you're looking for, please call us.</p>
        <p class="lead text-gray text-center"> We sell products for over 200 manufacturers and are positive we can find what you're looking for, at a great price!</p>
    </div>
</section>
<section class="testimonials">
    <div class="page-header-wrapper">
        <div class="container">
            <div class="page-header text-center wow fadeInUp">
                <h2 class="blue">est. 1998</h2>
            </div>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            <p class="lead" style="color: #fff;text-align: center;"> 
                Restaurant Equipment Paradise has been in business for more than 20 years.
            </p>
            <p class="lead" style="color: #fff;text-align: center;">We have helped thousands of clients with their Restaurant and Commercial Kitchen  needs.</p>
        </div>
    </div>
</section>
<!-- home contact -->
<section id="contact" class="gray">
    <div class="container"> 

        <!-- Begin page header-->
        <div class="page-header-wrapper">
            <div class="container">
                <div class="page-header text-center wow fadeInUp">
                    <h2 class="blue">Contact Us</h2>
                    <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                    <p class="lead text-gray" style="margin: 0;"> Hours of operation:</p>
                    <p class="lead text-gray" style="margin: 0;"> Monday - Friday: 8am to 5pm (we are open)</p>
                    <p class="lead text-gray"> Saturday:  closed until further notice</p>
                </div>
            </div>
        </div>
        <!-- End page header-->

        <div class="row">
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-home fa-2x"></i>
                <h3>Our Address</h3>
                <span class="font-l">465 Park Avenue <br> East Hartford, CT 06108</span> 
            </div>
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-comment fa-2x"></i>
                <h3>Our mail</h3>
                <span class="font-l">Info (at) SaveInParadise.com</span> </div>
            <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-phone fa-2x"></i>
                <h3>Call Us</h3>
                <span class="font-l">860.282.8733</span> </div>
        </div>
    </div>
</section>
<!--<div class="modal fade food_truck_modal" id="FoodTruckModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--  <div class="modal-dialog" role="document">-->
<!--    <div class="modal-content">-->
<!--      <div class="modal-header">-->
<!--        <h5>Loaded Food Truck For Sale</h5>-->
<!--        <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--          <span aria-hidden="true">&times;</span>-->
<!--        </button>-->
<!--      </div>-->
<!--      <div class="modal-body">-->
<!--          <img src="<?php echo front_asset_url() ?>images/FoodTruck.jpg"  alt="" style="width: 100%;">-->
<!--          <a href="https://saveinparadise.com/product/truffles-food-truck--loaded">Click Here to Learn More</a>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->