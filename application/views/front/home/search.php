<div class="container" style="padding-top: 50px;">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div class="category-sidebar">
                <div class="sidebar-title">
                    <h3>Categories</h3>
                </div>
                <ul class="product-categories">
                    <li class="cat-item current-cat cat-parent"><a href= "#">Main Menu</a>
                        <ul class="children">
                            <li class="cat-item cat-parent"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a>
                                <ul class="children">
                                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; sasa</a></li>
                                    <li class="cat-item cat-parent"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Hasasandbags</a>
                                        <ul style="display: none;" class="children">
                                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a></li>
                                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Sling bag</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a> </li>
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Menu</a> </li>
                        </ul>
                    </li>
                    <li class="cat-item cat-parent"><a href="#">Main menu</a>
                        <ul class="children">
                            <li class="cat-item cat-parent"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Dresses</a>
                                <ul class="children">
                                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Casual</a></li>
                                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Designer</a></li>
                                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Evening</a></li>
                                    <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Hoodies</a></li>
                                </ul>
                            </li>
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Jackets</a> </li>
                            <li class="cat-item"><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Shoes</a> </li>
                        </ul>
                    </li>
                    <li class="cat-item"><a href="#">Main menu</a></li>
                    <li class="cat-item"><a href="#">Main menu</a></li>
                    <li class="cat-item"><a href="#">Main menu</a></li>
                </ul>
            </div>
        </div>
        <div class="col-main col-sm-9">
            <div class="page-title">
                <h2>Search result title goes here</h2>
            </div>
            <div class="toolbar">
                <div class="sorter">
                    <div class="short-by">
                        <label>Sort By:</label>
                        <select>
                            <option selected="selected">Position</option>
                            <option>Name</option>
                            <option>Price</option>
                            <option>Size</option>
                        </select>
                    </div>
                    <div class="short-by page">
                        <label>Show:</label>
                        <select>
                            <option selected="selected">8</option>
                            <option>12</option>
                            <option>16</option>
                            <option>30</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="product-grid-area">
                <ul class="products-grid">
                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 wow fadeInUp">
                        <div class="product-item">
                            <div class="item-inner">
                                <div class="product-thumbnail">
                                    <div class="icon-new-label new-right">New</div>
                                    <div class="pr-img-area"><figure> <img class="first-img" src="<?php echo front_asset_url() ?>images/pro1.jpg" alt=""></figure>
                                        <button type="button" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> View details</span> </button>
                                    </div>
                                </div>
                                <div class="item-info">
                                    <div class="item-title"> 
                                        <a title="Ipsums Dolors Untra" href="detail.php">Ipsums Dolors Untra </a> 
                                    </div>
                                    <div class="item-price">
                                        <p class="special-price">$456.00</p>
                                        <p class="old-price">$567.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="pagination-area wow fadeInUp">
                <ul>
                    <li><a class="active" href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>