
<html xmlns="http://www.w3.org/1999/xhtml">

    <body>
        <table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
            <tr>
                <td bgcolor="" style="">
                    <table width="100%" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" style="padding:20px 0px;background-color: #fff;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 50%;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="" style="padding:20px; ">
                                <table width="100%" cellspacing="0" cellpadding="0" style="">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello <?php echo isset($username) ? $username : '' ?>,</td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">You are receiving this email contact detail.</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Name : <?php echo $name; ?></td>                      
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Email : <?php echo $email; ?></td>                      
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Phone : <?php echo $phone; ?></td>                      
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Message : <?php echo $message; ?></td>                      
                                    </tr>

                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 40%;" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>