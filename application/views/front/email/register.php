<table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
    <tr>
        <td bgcolor="" style="">
            <table width="100%" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" bgcolor="" background="" style="padding:20px 0px;  background-color: #fff;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/img/logo.png" alt="" border="0" style="margin:0 0 0 10px;width: 200px;" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td valign="top" bgcolor="" style="padding:20px; ">
                        <table width="100%" cellspacing="0" cellpadding="0" style="">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello <?php echo isset($username)? $username : '' ; ?>,</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Congratulations! You have successfully registered to be a Applicant on StaffT. You are Just one step away.</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Please click the button below to confirm your email. We are looking forward to having you as a member! </td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo $activationLink; ?>" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif;border:1px solid #000;border-radius:8px;padding:10px 5px;color:#000;text-decoration:none;display:block;width:200px;margin:0 auto;text-align:center">Confirm your Email</a>
                                </td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you,</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">StaffT</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Website: <a href="<?php echo base_url(); ?>" target="_blank"></a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td height="" align="center"  style="padding:20px; font-size:16px; font-weight:normal; color:#000; font-family:Arial, Helvetica, sans-serif;  background-color: #fff;">
                        <table width="100%" cellspacing="0" cellpadding="0" style="text-align: center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; padding-left:10px; color:#000;">
                            <tr>
                                <td><br>
                                    <br>
                                   
                                </td>
                            </tr>                                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/img/logo.png" alt="" border="0" style="margin:0 0 0 10px;width: 125px;" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>