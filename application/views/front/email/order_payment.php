
<html xmlns="http://www.w3.org/1999/xhtml">

    <body>
        <table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
            <tr>
                <td bgcolor="" style="">
                    <table width="100%" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" style="padding:20px 0px;background-color: #fff;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 50%;" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="" style="padding:20px; ">
                                <table width="100%" cellspacing="0" cellpadding="0" style="">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello <?php echo isset($user_details->firstname) ? ucfirst($user_details->firstname) : '' ?>,</td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you for the order, please see order details below</td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" style="font-family: Arial,sans-serif;font-size: 14px;line-height: 22px;">
                                                            <h4 style="margin: 0;padding-bottom: 10px;font-size: 18px;">To</h4>
                                                            <p style="margin: 0;"><?php echo isset($user_details->username) ? ucfirst($user_details->username) : '' ?></p>
                                                            <p style="margin: 0;"><?php echo isset($user_details->street_address) ? $user_details->street_address : '' ?></p>
                                                            <p style="margin: 0;"><?php echo isset($user_details->state) ? $user_details->state : '' ?>, <?php echo isset($user_details->city) ? $user_details->city : '' ?>, <?php echo isset($user_details->zipcode) ? $user_details->zipcode : '' ?></p>
                                                        </td>
                                                        <td valign="top" style="font-family: Arial,sans-serif;font-size: 14px;line-height: 22px;text-align: right;">
                                                            <h4 style="margin: 0;padding-bottom: 10px;font-size: 18px;">Details</h4>
                                                            <p style="margin: 0;"><strong>Date :</strong> <?php echo (isset($order_details['created_date']) && ($order_details['created_date'] != '') ) ? date("m/d/Y", strtotime($order_details['created_date'])) : '' ?></p>
                                                            <p style="margin: 0;"><strong>Order ID :</strong> <?php echo isset($order_id) ? $order_id : '' ?></p>
                                                            <p style="margin: 0;"><strong>Amount Paid :</strong> $<?php echo isset($order_details['amount']) ? number_format($order_details['amount'], 2) : '0.00' ?></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Product Name</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Unit Price</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Quantity</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Shipping</th>
                                                        <th scope="col" style="padding:5px;text-align: left; font-family: Arial,sans-serif;background-color: #e9e9e9; font-size: 14px; line-height:20px;line-height:30px;border: 1px solid #ddd;">Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $sub_total = 0; ?>
                                                    <?php $shipping_total = 0; ?>
                                                    <?php if(isset($product_details) && !empty($product_details) ) {
                                                        foreach ($product_details as $items) { ?>
                                                            <tr>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">
                                                                    <?php echo $items['name']; ?><br/> MODEL #:<?php echo isset($items['modal_number']) ? $items['modal_number'] : ''; ?>
                                                                </td>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">$<?php echo number_format($items['price'], 2); ?></td>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;"><?php echo $items['quantity']; ?></td>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;"><?php echo ($items['shipping_amount'] > 0) ? "$" . $items['shipping_amount'] : 'Free'; ?></td>
                                                                <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;border: 1px solid #ddd;">$<?php echo number_format(($items['quantity'] * $items['price']) + $items['shipping_amount'], 2); ?></td>
                                                            </tr>
                                                        <?php
                                                            $sub_total      = $sub_total + ($items['quantity'] * $items['price']) + $items['shipping_amount'];
                                                            $shipping_total = $shipping_total + $items['shipping_amount'];
                                                        }
                                                    } ?>
                                                    <tr>
                                                        <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;border: 1px solid #ddd;">Sub Total</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;line-height:20px;border: 1px solid #ddd;">$<?php echo number_format($sub_total, 2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;border: 1px solid #ddd;">Tax</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;line-height:20px;border: 1px solid #ddd;">$<?php echo isset($order_details['tax']) ? number_format($order_details['tax'], 2) : '0.00' ?></td>
                                                    </tr>
                                                    <?php if(isset($order_details['discount']) && ($order_details['discount'] > 0) ) { ?>
                                                        <tr>
                                                            <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;background-color: #dbffe4; line-height:20px;border: 1px solid #ddd;">Discount</td>
                                                            <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 16px;background-color: #dbffe4; line-height:20px;border: 1px solid #ddd;">$<?php echo number_format($order_details['discount'], 2); ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #7cc9e1; line-height:20px;border: 1px solid #ddd;">Shipping Amount</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #7cc9e1; line-height:20px;border: 1px solid #ddd;"><?php echo ($shipping_total > 0) ? "$" . number_format($shipping_total, 2) : 'Free'; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" colspan="4" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #fffed3; line-height:20px;border: 1px solid #ddd;">Total Amount Paid</td>
                                                        <td valign="top" style="padding: 15px 5px; font-family: Arial,sans-serif; font-size: 18px; background: #fffed3; line-height:20px;border: 1px solid #ddd;">$<?php echo isset($order_details['amount']) ? number_format($order_details['amount'], 2) : '0.00'; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you for business - it is greatly appreciated,</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Restaurant Equipment Paradise, Inc.</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Customer Service</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">465 Park Avenue,</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hartford, CT 06108</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Tel# 860-282-8733</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Email: Info@SaveInParadise.com</td>
                                    </tr>
                                    <tr>
                                        <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Website: <a href="<?php echo base_url(); ?>" target="_blank">www.SaveInParadise.com</a></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" bgcolor="#CACACA"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                                <a href="<?php echo base_url(); ?>" target="_blank">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 40%;" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>