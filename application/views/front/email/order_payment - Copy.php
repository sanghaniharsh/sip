<table width="100%" align="center" cellpadding="0" cellspacing="0" style="max-width: 560px; margin: auto; border: 1px solid #CACACA;">
    <tr>
        <td bgcolor="" style="">
            <table width="100%" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" bgcolor="" background="" style="padding:20px 0px;  background-color: #fff;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 200px;" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td valign="top" bgcolor="" style="padding:20px; ">
                        <table width="100%" cellspacing="0" cellpadding="0" style="">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Hello <?php echo isset($username)? $username : '' ; ?>,</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you for the order, please see order details below</td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Transaction Id  : <b><?php echo isset($transaction_id)? $transaction_id : '' ; ?></b></td>
                            </tr>
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Thank you,</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">SIP</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; padding-left:10px;">Website: <a href="<?php echo base_url(); ?>" target="_blank">www.sip.com</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10" bgcolor="#CACACA"></td>
                </tr>
                <tr>
                    <td height="" align="center"  style="padding:20px; font-size:16px; font-weight:normal; color:#000; font-family:Arial, Helvetica, sans-serif;  background-color: #fff;">
                        <table width="100%" cellspacing="0" cellpadding="0" style="text-align: center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; padding-left:10px; color:#000;">
                            <tr>
                                <td>SIP</td>
                            </tr>                                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td align="center" bgcolor="" background="" style="padding:10px 0px;">
                        <a href="<?php echo base_url(); ?>" target="_blank">
                            <img src="<?php echo base_url(); ?>assets/front-side/images/logo-main.png" alt="" border="0" style="margin:0 0 0 10px;width: 125px;" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>