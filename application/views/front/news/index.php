
<div class="blog_post news_page">
    <div class="container">
        <div class="row"> 
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <div class="page-title">
                    <h2>News/Media Section</h2>
                </div>
                <ul class="blog-posts">
                    <?php if (!empty($news_list)) { ?>
                        <?php foreach($news_list as $row) { ?>
                            <li class="post-item">
                                <article class="entry">
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-4">
                                            <div class="entry-thumb"> 
                                                <a href="<?php echo isset($row->url) && $row->url != '' ? $row->url : 'javascript:void(0);'; ?>" target="_blank">
                                                    <img src="<?php echo base_url().'attachment/image/200/300/'.md5($row->image); ?>" alt="Blog">
                                                </a> 
                                            </div>
                                        </div>
                                        <div class="col-sm-7 col-xs-8">
                                            <h3 class="entry-title">
                                                <a href="<?php echo isset($row->url) && $row->url != '' ? $row->url : 'javascript:void(0);'; ?>" target="_blank">
                                                    <?php echo isset($row->title) && $row->title != '' ? $row->title : ''; ?>
                                                </a>
                                            </h3>
                                            <div class="entry-excerpt">
                                                <?php echo isset($row->description) && $row->description != '' ? $row->description : ''; ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php if (isset($row->url) && $row->url != '') { ?>
                                                <div class="entry-more text-right" style="margin: 0;">
                                                    <a href="<?php echo isset($row->url) && $row->url != '' ? $row->url : 'javascript:void(0);'; ?>" target="_blank" class="button">
                                                        View Article&nbsp; <i class="fa fa-angle-double-right"></i>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </article>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <div class="sortPagiBar">
                    <div class="pagination-area" style="visibility: visible;">
                        <?php echo isset($links) ? $links : ''; ?>
                    </div>
                </div>
            </div>
            <!-- ./left colunm --> 
        </div>
        <!-- ./row--> 
    </div>
</div>