<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92390288-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-92390288-1');
</script>

    <title><?php echo isset($meta_title) ? $meta_title : 'Restaurant Equipment Paradise |  Used Restaurant Equipment | Commercial Kitchen Equipment | Restaurant Equipment'; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Restaurant Equipment, Smallwares, Cooking, Commerical Kitchen, Stoves, Refrigerators, Cutting Boards, China, Dishware, Used Restaurant Equipment"/>
    
    <meta name="description" content="<?php echo isset($meta_description) ? $meta_description : 'Restaurant Equipment Paradise offers the largest selection of restaurant equipment and restaurant supplies. We are the top restaurant supply company in the United States.'; ?>"/>
        
    <link rel="shortcut icon" type="image/png" href="<?php echo front_asset_url(); ?>images/favicon.png"/>

    <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/font-awesome.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/simple-line-icons.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/style.css" media="all">
    <link rel="stylesheet" media="screen" href="<?php echo front_asset_url() ?>css/shortcodes.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/main.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/toastr.min.css" media="all">

    <?php echo (isset($css_files)) ? $css_files : ''; ?>

    <link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/custom.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/responsive.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/fileup.css" media="all">

    
    <script type="text/javascript" src="<?php echo front_asset_url() ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo front_asset_url() ?>js/fileup.js"></script>

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        var is_login = '<?php echo ($is_login == 1) ? true : false; ?>';
        var current_metod = '<?php echo $currentClass; ?>';
        var current_class = '<?php echo $currentMethod; ?>';
    </script>
    <?php if (isset($meta_image) && $meta_image != '' && isset($product)) { ?>
        <meta property="og:title" content="<?php echo isset($product->name) && $product->name != '' ?  str_replace('"', '', $product->name) : ''; ?>" /> 
        <meta property="og:description" content="<?php echo isset($meta_description) ? $meta_description : 'Restaurant Equipment Paradise offers the largest selection of restaurant equipment and restaurant supplies. We are the top restaurant supply company in the United States.'; ?>"/>
        <meta property="og:image" content="<?php echo base_url().'attachment/image/0/394/'.md5($meta_image); ?>" />
    <?php } ?>
</head>
    <body>

        <?php echo isset($main_content) ? $main_content : ''; ?>
        
        
        <script type="text/javascript" src="<?php echo front_asset_url() ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo front_asset_url() ?>js/owl.carousel.min.js"></script>
        <!-- jquery.mobile-menu js --> 
        <script type="text/javascript" src="<?php echo front_asset_url() ?>js/mobile-menu.js"></script>
        <script type="text/javascript" src="<?php echo front_asset_url() ?>js/waypoints.js"></script>
        <script type="text/javascript" src="<?php echo front_asset_url() ?>js/main.js"></script>
        <script src="<?php echo front_asset_url() ?>js/toastr.min.js"></script>
        <script src="<?php echo front_asset_url() ?>js/jquery.validate.min.js"></script>
        <!--<script src="<?php echo front_asset_url() ?>js/tabs.min.js"></script>-->
        <script src="<?php echo front_asset_url() ?>js/custom.js"></script>
        

        <script type="text/javascript">
            var errorToaster = "<?php echo $this->session->flashdata('error'); ?>";
            var successToaster = "<?php echo $this->session->flashdata('success'); ?>";

            if (successToaster != '') {
                showMessage('success', successToaster);
            }
            if (errorToaster != '') {
                showMessage('danger', errorToaster);
            }
        </script>

        <!-- Dynamic Load JS files starts -->
            <?php echo isset($js_files) ? $js_files : '';?>
        <!-- Dynamic Load JS files ends -->

    </body>
</html>