<!--<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/tabs.css" media="all">-->
<div class="container">
    <div class="account_page">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h2>User Account</h2>
                </div>
            </div>
        </div>
        <div class="row accounts_tabs">
            <div class="col-md-12">
                <ul class="tabs side" id="tabs">
                    <li><a href="#account_information" target="_self"><i class="fa fa-user"></i> Account Details</a></li>
                    <li><a href="#my_orders" class="my_order_tab" target="_self"><i class="fa fa-shopping-cart"></i> My Orders</a></li>  
                </ul>
                <div class="tabs-content">
                        <div id="account_information" class="tabs-panel">
                            <form name="user_profile_form" id="user_profile_form" method="POST" >
                            <h1><strong>Your Basic Informations</strong></h1>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="firstname" value="<?php echo isset($loginUser['firstname']) ? $loginUser['firstname'] : '' ?>" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="lastname" value="<?php echo isset($loginUser['lastname']) ? $loginUser['lastname'] : '' ?>" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" class="form-control" name="email" value="<?php echo isset($loginUser['email']) ? $loginUser['email'] : '' ?>" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" name="phone" value="<?php echo isset($loginUser['phone']) ? $loginUser['phone'] : '' ?>" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" id="password"  value="" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control valid" name="cpassword" value="" aria-required="true" aria-invalid="false">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h1><strong>Your Shipping Address</strong></h1>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Street Address</label>
                                        <input type="text" name="shipping_address" class="form-control" autocomplete="ha" value="<?php echo isset($loginUser['shipping_street_address']) ? $loginUser['shipping_street_address'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Zip Code</label>
                                    <input type="text" class="form-control shipping-search-zipcode" name="shipping_zipcode" autocomplete="aad" value="<?php echo isset($loginUser['shipping_zipcode']) ? $loginUser['shipping_zipcode'] : '' ?>">
                                    <div class="shipping_location_result"></div>
                                </div>
                                <div class="col-md-8">
                                    <label>City & State</label>
                                    <input type="text" class="form-control shipping_city_state" name="shipping_city" readonly="" value="<?php echo (isset($loginUser['shipping_city']) && isset($loginUser['shipping_state']) ) ? $loginUser['shipping_city'].', '.$loginUser['shipping_state'] : '' ?>">
                                </div>
                            </div>
                            <hr>
                            <h1><strong>Your Billing Address</strong></h1>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Street Address</label>
                                        <input type="text" name="address" class="form-control" autocomplete="ha" value="<?php echo isset($loginUser['street_address']) ? $loginUser['street_address'] : '' ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Zip Code</label>
                                    <input type="text" class="form-control search-zipcode" name="zipcode" autocomplete="aad" value="<?php echo isset($loginUser['zipcode']) ? $loginUser['zipcode'] : '' ?>">
                                    <div class="location_result"></div>
                                </div>
                                <div class="col-md-8">
                                    <label>City & State</label>
                                    <input type="text" class="form-control city_state" name="city" readonly="" value="<?php echo isset($loginUser['city']) ? $loginUser['city'].', '.$loginUser['state'] : '' ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input type="hidden" name="user_id" value="<?php echo isset($loginUser['id']) ? encreptIt($loginUser['id']) : '' ?>">
                                    <button type="submit" class="button btn-save-account"><i class="fa fa-file"></i>&nbsp; <span>Updates Details</span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="my_orders" class="tabs-panel">
                        <h1><strong>Your Order History</strong></h1>
                        <div class="table-responsive">
                            <table id="user_order_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Created Date</th>
                                        <th>Total Item</th>
                                        <th>Total Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

