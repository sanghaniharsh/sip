<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<!-- Generated for Managed SSL -->
<urlset xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://www.sitemaps.org/schemas/sitemap/0.9 https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
    <url><loc><?php echo base_url(); ?></loc></url>
    <url><loc><?php echo base_url(); ?>small-wares</loc></url>
    <url><loc><?php echo base_url(); ?>grease-traps</loc></url>
    <url><loc><?php echo base_url(); ?>used-equipment</loc></url>
    <url><loc><?php echo base_url(); ?>restaurant-design</loc></url>
    <url><loc><?php echo base_url(); ?>our-work</loc></url>
    <url><loc><?php echo base_url(); ?>auction</loc></url>
    <url><loc><?php echo base_url(); ?>contact-us</loc></url>
    <url><loc><?php echo base_url(); ?>page/user-agreement</loc></url>
    <url><loc><?php echo base_url(); ?>page/privacy-policy</loc></url>
    <url><loc><?php echo base_url(); ?>about-us</loc></url>
    <url><loc><?php echo base_url(); ?>blog</loc></url>
    <url><loc><?php echo base_url(); ?>news</loc></url>
    <url><loc><?php echo base_url(); ?>brand</loc></url>
    <url><loc><?php echo base_url(); ?>search</loc></url>
    <url><loc><?php echo base_url(); ?>campaign</loc></url>
    <?php foreach($final_url as $url) {  ?>
        <url><loc><?php echo $url; ?></loc></url>
    <?php } ?>
</urlset>