<p style="text-align: center;">SOFTWARE LICENSE & SERVICE AGREEMENT</p>
<p align="justify"> THIS AGREEMENT is entered into as of <?php echo isset($effective_date) ? '<strong>'.$effective_date.'</strong>' : '_____________' . date('Y'); ?> ("Effective Date") by and between Restaurant Workers Association Corp, who are the owners and operators of SIP, having a principal address at 41 Union Square West, Suite 325, New York, NY 10003 ("SIP" or "Company")  and <?php echo isset($business_name) ? '<strong>'.$business_name.'</strong>' : '_______________________'; ?> ("CLIENT"), (Hereinafter SIP and Client are jointly referred to as the “Parties”).</p>

<p style="text-align: center;"><b> <u>ACKNOWLEDGMENT</u> </b></p>

<p align="justify">BY USING THE LICENSED PROGRAMS, SOFTWARE AND OR SERVICES, CLIENT ACKNOWLEDGES THAT IT HAS READ AND UNDERSTANDS THE FOREGOING AND THAT CLIENT AGREES TO BE BOUND BY ALL ITS TERMS AND CONDITIONS.  </p>
<p align="justify">IN WITNESS WHEREOF, the parties have caused this Agreement to be executed as of the Effective Date: <?php echo isset($effective_date) ? '<strong>'.$effective_date.'</strong>' : '_______________'; ?></p>
<p align="justify"> CLIENT : <?php echo isset($business_name) ? '<strong>'.$business_name.'</strong>' : '________________________________________'; ?> </p>
<p align="justify"> NAME : <?php echo isset($client_name) ? '<strong>'.$client_name.'</strong>' : '________________________________________'; ?> (Signature) </p>
<p align="justify"> Amount paid: <?php echo isset($amount) ? '<strong>'.$amount.'</strong>' : '________________________________________'; ?> </p>
<p align="justify"> For SIP </p>
<p align="justify"> Signature: John Cangir</p>
<p align="left"><b> CEO </b></p><br><br>

<p align="justify">Date : <?php echo date('m-d-Y'); ?> </p>
<p align="justify">Time : <?php echo date('H:i:s'); ?> </p>
<p align="justify">IP Address : <?php echo $this->input->ip_address(); ?> </p>