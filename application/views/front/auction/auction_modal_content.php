<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Video</h4>
    </div>
    <div class="modal-body">
        <?php if (isset($video_link) && $video_link != '') { 

            $video_string   = '<iframe width="100%" height="350" src="'.$video_link.'" frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';

            if (preg_match('/watch/',$video_link)) {
                $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"https://www.youtube.com/embed/$2\" width=\"100%\" height=\"350\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",$video_link);
            } else if(preg_match('/youtu.be/',$video_link)) {
                $video_string = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"https://www.youtube.com/embed/$2\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen></iframe>",$video_link);
            }
            echo $video_string;
        }?>
    </div>
</div>