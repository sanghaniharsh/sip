
<div class="main-container col1-layout" style="margin-bottom: 80px;">
    <div class="container">
        <div class="action_extra_info">
            <h3>Ten Point Advertising Campaign - Get Your Restaurant Contents Sold FAST! <a href="<?php echo base_url(); ?>campaign">Click here for details</a></h3>
        </div>
        <div class="action_newsletter_block">
            <h3>Want to be notified when new auctions come up?</h3>
            <a href="https://app.getresponse.com/site2/restaurant_auctions?u=5Hjt&webforms_id=7sft" target="_blank" class="but_large2 slider_btn"> Notify Me &nbsp;<i class="fa fa-envelope" aria-hidden="true"></i></a>
        </div>
        <?php  if (isset($auction_list) && !empty($auction_list)) { ?>
            <?php foreach($auction_list as $row) { ?>

                <div class="aution_block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-name">
                                <h1><?php echo isset($row->title) ? $row->title : ''; ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-main">
                            <div class="product-view-area">
                                <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                                    <?php 
                                    $images[0] =  '';
                                    if(isset($row->auction_images) && $row->auction_images != "") {
                                        $images = explode(',',$row->auction_images);
                                    }
                                    ?>
                                    <div class="large-image"> 
                                        <a href="<?php echo base_url().'attachment/image/0/498/'.md5($images[0]); ?>" class="cloud-zoom" id="zoom<?php echo md5($images[0]);?>"> 
                                            <img class="zoom-img" src="<?php echo base_url().'attachment/image/0/498/'.md5($images[0]); ?>" alt="products"> 
                                        </a> 
                                    </div>
                                    <div class="flexslider flexslider-thumb">
                                        <ul class="previews-list slides">
                                        <?php if(isset($images) && !empty($images)) { ?>
                                            <?php foreach($images as $image) { ?>
                                                    <li>
                                                        <a href="" class="cloud-zoom-gallery" rel="useZoom: 'zoom<?php echo md5($images[0]);?>', smallImage: '<?php echo base_url().'attachment/image/0/498/'.md5($image);?>' ">
                                                            <img src="<?php echo base_url().'attachment/image/67/0/'.md5($image);?>" alt = "Thumbnail 2"/>
                                                        </a>
                                                    </li>
                                            <?php } ?>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                    <?php if($row->auction_type == 1) { ?>
                                    <div class="if_auction"> 
                                            <img src="<?php echo front_asset_url().'images/online-only-auction.png'; ?>" alt="products"> 
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
                                    <div class="product-details-area">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-compare auction_table_mobile">
                                                <tbody>
                                                    <tr>
                                                        <td class="compare-label"><?php echo ($row->auction_type == 1 ? 'Online Auction Date' : 'Auction Date');?></td>
                                                        <?php if($row->auction_type == 1) { ?>
                                                            <td class="auction_date"><?php echo isset($row->a_date) ? $row->a_date : ''; ?></td>
                                                        <?php } else { ?>
                                                            <td class="auction_date"><?php echo isset($row->a_date) ? date('l  F  d , Y',strtotime($row->a_date)) : ''; ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td class="compare-label"><?php echo ($row->auction_type == 1 ? 'Winning Bidders Pickup of Goods Address' : 'Auction Address');?></td>
                                                        <td><?php echo isset($row->address) ? $row->address : ''; ?></td>
                                                    </tr>
                                                    
                                                    <?php if($row->auction_type == 0) { ?>
                                                        <tr>
                                                            <td class="compare-label">Preview Time</td>
                                                            <td>  <?php echo isset($row->from_time) ? date('h:i A', strtotime($row->from_time)) : ''; ?> - <?php echo isset($row->to_time) ? date('h:i A', strtotime($row->to_time)) : ''; ?> </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td class="compare-label"><?php echo ($row->auction_type == 1 ? 'Auction End Date & End Time' : 'Auction time');?></td>
                                                        
                                                        <?php if($row->auction_type == 1) { ?>
                                                            <td><?php echo isset($row->a_time)  ? $row->a_time : ''; ?></td>
                                                        <?php } else { ?>
                                                            <td><?php echo isset($row->a_time)  ? date('h:i A', strtotime($row->a_time)) : ''; ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr class="additional_comments">
                                                        <td class="compare-label">Additional Comments</td>
                                                        <td><p><?php echo isset($row->additional_comment) ? $row->additional_comment : ''; ?></p></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive auction_view_imgs">
                                            <table class="table table-bordered table-compare">
                                                <tbody>
                                                    <tr>
                                                        <?php if (isset($row->video_link ) && $row->video_link != '') { ?>
                                                                <td>
                                                                    <a class="auction_video_btn" data-id="<?php echo encreptIt($row->id); ?>">
                                                                        <img src="<?php echo front_asset_url() ?>images/video_thumb.gif" alt = "Thumbnail 2"/>
                                                                    </a>
                                                                </td>
                                                        <?php } ?>
                                                        <?php if (isset($row->equipment_pdf ) && $row->equipment_pdf > 0) { ?>
                                                                <td>
                                                                    <a target="_blink" href="<?php echo base_url().'attachment/image/85/85/'.md5($row->equipment_pdf); ?>">
                                                                        <img src="<?php echo front_asset_url() ?>images/equipment_oval_button.jpg" alt = "Thumbnail 2"/>
                                                                    </a>
                                                                </td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <h3 class="auction_call_text">Call 860-282-8733 for more information.</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?> 
         <?php  echo "Record not found"; ?>
        <?php } ?>
        <div class="sortPagiBar">
            <div class="pagination-area" style="visibility: visible;">
                <?php echo isset($links) ? $links : ''; ?>
            </div>
        </div>
        <div class="action_extra_info">
            <h3>Ten Point Advertising Campaign - Get Your Restaurant Contents Sold FAST! <a href="<?php echo base_url(); ?>campaign">Click here for details</a></h3>
        </div>
    </div>
</div>
