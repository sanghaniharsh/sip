<div class="container">
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="">
                        <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a>
                        <span>»</span>
                    </li>
                    <?php if (isset($search_category) && ! empty($search_category)) { ?>
                        <li class="">
                            <a title="Search for USED EQUIPMENT" href="<?php echo base_url().'used-equipment'; ?>">USED RESTAURANT EQUIPMENT</a>
                            <span>»</span>
                        </li>
                        <li class="">
                            <?php foreach($search_category as $key => $u_category) { ?>
                                <?php if ($key != 0) { ?>
                                    <span>/</span>
                                <?php } ?>
                                <a title="Search for <?php echo $u_category->name ?>" href="<?php echo base_url().'used-equipment?search_category='.$u_category->id; ?>"><strong><?php echo $u_category->name ?></strong></a>
                            <?php } ?>
                        </li>
                    <?php } else { ?>
                        <li class="">
                            <strong>USED RESTAURANT EQUIPMENT</strong>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-sm-3 col-xs-12 search_sidebar_sticky">
            <div class="category-sidebar old_product_sidebar">
                <div class="sidebar-title">
                    <h3>Refine Search</h3>
                </div>
                <form  name="oldproduct_search" id="oldproduct_search" method="GET" action="">
                    <div class="old_product_filter_box">
                        <div class="old_product_input">
                            <?php $search = $this->input->get('search');  
                                  $search_category = $this->input->get('search_category');
                                  if($search_category != "") {
                                      $search_category = explode(",", $search_category);
                                  }
                                  ?>
                            <input type="text" class="form-control input-lg"  name="search" placeholder="Search for Used Equipment" value="<?php if(isset($search) && !empty($search)){echo $search; } ?>" >
                        </div>
                        <div class="old_product_checkboxes">
                         <?php  if (!empty($categoryData)) { ?>
                            <?php foreach ($categoryData as $categoryNamelist) { ?>
                            <label class="custom_check"><?php echo isset($categoryNamelist['name']) && $categoryNamelist['name'] != '' ? $categoryNamelist['name'] : ''; ?>
                                <input type="checkbox" value="<?php echo $categoryNamelist['id'];?>" class="select_categoty"  <?php echo (!empty($search_category) && in_array($categoryNamelist['id'], $search_category)) ? 'checked' : '';?>>
                                <span class="checkmark"></span>
                            </label>
                         <?php } } ?>
                            <input type="hidden" name="search_category" id="search_category" value="">
                        </div>
                        <div class="filter_button">
                            <button type="submit" class="but_medium5"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-main col-sm-9">
            <div class="page-title">
                <h2>Used Restaurant Equipment</h2>
            </div>
            <ul class="blog-posts used_equipments_block">
        <?php if(isset($product_list) && !empty($product_list)){ ?>
        <?php foreach($product_list as $productdata){ ?>
                <li class="post-item">
                    <article class="entry">
                        <div class="row">
                            <div class="col-sm-5 col-xs-4">
                                <div class="entry-thumb"> 
                                    <?php 
                                        $images[0] =  '';
                                        if(isset($productdata->product_images) && $productdata->product_images != "") {
                                            $images = explode(',',$productdata->product_images);
                                        }
                                    ?>
                                    <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>">
                                        <img src="<?php echo base_url().'attachment/image/213/0/'.md5($images[0]); ?>" class="img-responsive">
                                    </a> 
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-8">
                                <h3 class="entry-title">
                                    <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>"><?php echo $productdata->name; ?></a>
                                </h3>
                                <div class="entry-meta-data">
                                    <span class="cat">
                                        <i class="fa fa-cutlery"></i>&nbsp; 
                                        <a href="#"><?php $result = $this->used_category_model->getCategoryNames($productdata->category_id);   echo isset($result->name) ? $result->name : ''; ?></a>
                                    </span>
                                </div>
                                <div class="price" style="font-size: 23px;margin-bottom: 10px;">
                                    <?php if ($productdata->call_for_price == 1) {  ?>
                                        Call for Discounted Price
                                    <?php } else { ?>
                                        $<?php echo isset($productdata->price) ? display_amount($productdata->price) : 0; ?>
                                    <?php } ?>
                                </div>
<!--                                <div class="entry-excerpt">
                                    <?php if(isset($productdata->description) && !empty($productdata->description)){echo $string = substr(substr($productdata->description,0,250), 0, strrpos(substr($productdata->description,0,250), ' ')).'........';}?>                                
                                </div>-->
                                <div class="entry-more">
                                    <a href="<?php echo base_url();?>product/<?php echo isset($productdata->slug) && $productdata->slug != '' ? $productdata->slug : ''; ?>" class="button">View Details&nbsp; 
                                        <i class="fa fa-angle-double-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>
                </li>
                <?php } } else { ?>
                        <li class="post-item">
                            <article class="entry">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <center> No Product Found </center>
                                    </div>
                                </div>
                            </article>
                        </li>
                    <?php } ?>
            </ul>
        </div>
    </div>
<div class="row">
    <div class="col-md-12 text-center" id="pagination">
        <div class="btn-group float-right">
            <?php echo isset($links) ? $links : ''; ?>
        </div>
    </div>
</div>
</div>
