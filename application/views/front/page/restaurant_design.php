
<div class="container">
    <div class="row">
        <div class="about-page">
            <div class="col-xs-12 col-sm-8">
                <h1>Restaurant Design  / Commercial Kitchen</h1>
                <p>
                    Whether you’re opening a new restaurant or renovating an existing one, a vital part of the process is to properly plan/design the layout of the commercial kitchen and dining area/bar area. This typically requires you to hire an architect, or hire a Project Manager and a Design specialist, with experience in designing commercial kitchens/restaurants.
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="sidebar-add-slider">
                    <img src="<?php echo front_asset_url() ?>images/restaurant_design.jpg" class="imgframe2" alt="slide1" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row restarant_images_work">
        <div class="col-md-3 col-xs-6">
            <div class="pdf_view">
                <img src="<?php echo front_asset_url() ?>images/pdf1.png" class="imgframe4" style="width: 100%;">
                <div class="pdf_overlay">
                    <a href="<?php echo front_asset_url() ?>images/SaladoWEB.pdf" target="_blank" class="pdf_btn">View PDF</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6">
            <div class="pdf_view">
                <img src="<?php echo front_asset_url() ?>images/pdf2.png" class="imgframe4" style="width: 100%;">
                <div class="pdf_overlay">
                    <a href="<?php echo front_asset_url() ?>images/FoodTruckWEB.pdf" target="_blank" class="pdf_btn">View PDF</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6">
            <div class="pdf_view">
                <img src="<?php echo front_asset_url() ?>images/pdf3.png" class="imgframe4" style="width: 100%;">
                <div class="pdf_overlay">
                    <a href="<?php echo front_asset_url() ?>images/WoodNTap-EnfieldWEB.pdf" target="_blank" class="pdf_btn">View PDF</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6">
            <div class="pdf_view">
                <img src="<?php echo front_asset_url() ?>images/pdf4.png" class="imgframe4" style="width: 100%;">
                <div class="pdf_overlay">
                    <a href="<?php echo front_asset_url() ?>images/3DPERSPECTIVESWEB.pdf" target="_blank" class="pdf_btn">View PDF</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row about-page">
        <div class="col-md-6 right-border">
            <p>
                At Restaurant Equipment Paradise, we employ a seasoned team of professionals with extensive backgrounds in overseeing the design and construction of commercial kitchens and entire restaurants or other food services businesses.
            </p>
            <p><strong>Our Project Managers</strong> have meticulously overseen a wide variety of new and renovation projects, both large and small, for numerous restaurants and food service-related businesses.</p>
        </div>
        <div class="col-md-6" style="padding-left: 50px;">
            <p>
                <strong>Our Design Team</strong> will listen to your business concept, answer any questions you have, obtain measurements and basic layout of the building space, work with you to determine the equipment you desire and the placement of each item and bring it all to life in 2-D and 3-D blue-prints. In no time at all, we can quickly draft your restaurant floor plans, kitchen layout, as well as bar and seating design.
            </p>
        </div>
    </div>
    <hr>
    <div class="row featured-box">
        <div class="col-md-12">
            <h2 class="blue main-title" style="margin-bottom: 70px;">Here are just some of the things our Design Team can do for you</h2>
        </div>
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    1
                </div>
                <div class="title text-uppercase">
                    <h4>Initial consultation</h4>
                </div>
                <div class="desc">
                    Develop needs of client, design of space, equipment, based off of the menu and style of cooking.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    2
                </div>
                <div class="title text-uppercase">
                    <h4>SCHEMATIC DESIGN</h4>
                </div>
                <div class="desc">
                    Preliminary rough study drawing that illustrates the basic concept of design.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    3
                </div>
                <div class="title text-uppercase">
                    <h4>SITE INSPECTION</h4>
                </div>
                <div class="desc">
                    To ensure accurate measurements through out the project and identify any specific needs of the space.
                </div>
            </div>
        </div>
    </div>
    <div class="row featured-box">
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    4
                </div>
                <div class="title text-uppercase">
                    <h4>DESIGN DEVELOPMENT</h4>
                </div>
                <div class="desc">
                    Provide detailed specifications on all equipment. Finalize layout in conjunction with architects, interior designers, and engineers.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    5
                </div>
                <div class="title text-uppercase">
                    <h4>PROJECT COORDINATION</h4>
                </div>
                <div class="desc">
                    Working with fabricators, engineers, architects, designers and other specialists to ensure proper completion.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="featured-item feature-border-box">
                <div class="icon">
                    6
                </div>
                <div class="title text-uppercase">
                    <h4>PERMITTING</h4>
                </div>
                <div class="desc">
                    Meeting with health and building officials in order to obtain project permits.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="text-center" style="line-height: 32px;">We would love to help develop your plan as it relates to your commercial kitchen/restaurant layout/design and equipment needs. The experts at Restaurant Equipment Paradise will also make sure you get the best prices available on any restaurant equipment you purchase from our company.</h4>
            <hr>
            <h4 class="text-center" style="line-height: 32px;">For more information on customizing your project, price quotes, and package pricing, please call us at 888-372-8733 or<br> email us through this website.</h4>
        </div>
    </div>
</div>
