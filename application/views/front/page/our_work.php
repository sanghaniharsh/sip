<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/owl.theme.css">
<div class="our-work-page">    <div class="container">        <div class="row">           
 <div class="col-xs-12 col-sm-12">                <h2 class="main-title blue">A Few of Our Completed Projects</h2> 
 </div>        </div>        <div class="row">            <div class="col-sm-6">        
 <div class="work-slider-box">                    <div class="slider-items-products">         
 <div id="our-work-slider" class="product-flexslider hidden-buttons">                
 <div class="slider-items slider-width-col1 owl-carousel owl-theme">                
 <div class="item"> <img alt="" src="<?php echo front_asset_url() ?>images/slide1.jpg"></div>         
 <div class="item"> <img alt="" src="<?php echo front_asset_url() ?>images/slide2.jpg"></div>        
 <div class="item"> <img alt="" src="<?php echo front_asset_url() ?>images/slide3.jpg"></div>   
 <div class="item"> <img alt="" src="<?php echo front_asset_url() ?>images/slide4.jpg"></div>  
 </div>                        </div>                    </div>                    <div class="slider-footer">      
 <h4>Avon Old Farms School</h4>                        <p>Avon, Connecticut</p>                 
 </div>                </div>            </div>            <div class="col-sm-6">          
 <div class="work-slider-box">                    <div class="slider-items-products">      
 <div id="our-work-slider" class="product-flexslider hidden-buttons">                        
 <div class="slider-items slider-width-col1 owl-carousel owl-theme">  
  <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide1a.jpg"></div> 
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide2a.jpg"></div>         
  <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide3a.jpg"></div>     
 </div>                        </div>                    </div>                    <div class="slider-footer">     
 <h4>River: A Waterfront Restaurant & Bar</h4>                        <p>Wethersfield, Connecticut</p>      
 </div>                </div>            </div>            <div class="col-sm-6">             
 <div class="work-slider-box">                    <div class="slider-items-products">        
 <div id="our-work-slider" class="product-flexslider hidden-buttons">                       
 <div class="slider-items slider-width-col1 owl-carousel owl-theme">              
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide1b.jpg"></div>     
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide2b.jpg"></div>     
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide3b.jpg"></div>   
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide4b.jpg"></div>
 </div>                        </div>                    </div>                    <div class="slider-footer">    
 <h4>Southern Auto Auction</h4>                    
 <p>East Windsor, Connecticut</p>    
 </div>                </div>            </div>            <div class="col-sm-6">          
 <div class="work-slider-box">                    <div class="slider-items-products">         
 <div id="our-work-slider" class="product-flexslider hidden-buttons">                  
 <div class="slider-items slider-width-col1 owl-carousel owl-theme">                   
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide1c.jpg"></div>   
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide2c.jpg"></div>     
 <div class="item"><img alt="" src="<?php echo front_asset_url() ?>images/slide3c.jpg"></div>   
 </div>                        </div>                    </div>                    <div class="slider-footer">      
 <h4>Tapatio Springs Hill Country Club</h4>                    
 <p>Tapatio Springs, Texas</p>     
 </div>                </div>            </div>        </div>    </div></div>
 