<div class="questions_page">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row delivery_header question_thank">
                    <div class="col-md-12">
                        <i class="fa fa-thumbs-up"></i>
                        <h2>Thank you for providing your information</h2>
                        <p>Please contact your account executive with any questions. Tel# 860-282-8733</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>