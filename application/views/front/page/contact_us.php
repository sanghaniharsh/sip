<div class="main container">
    <div class="row">
        <section class="col-main col-sm-12">
            <div id="contact1" class="page-content page-contact">
                <div class="row">
                    <div class="col-xs-12 col-sm-6" id="contact_form_map">
                        <h3 class="page-subheading">Contact Us</h3>
<!--                        <p>Lorem ipsum dolor sit amet onsectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor dapibus eget. Mauris tincidunt aliquam lectus sed vestibulum. Vestibulum bibendum suscipit mattis.</p>
                        <br>
                        <ul>
                            <li>Point 1</li>
                            <li>Point 2</li>
                            <li>Point 3</li>
                        </ul>
                        --><br>
                        <p class="lead text-gray" style="margin: 0;"> Hours of operation:</p>
                        <p class="lead text-gray" style="margin: 0;"> Monday - Friday: 8am to 5pm (we are open)</p>
                        <p class="lead text-gray"> Saturday: closed until further notice</p>
                        <br>
                        <ul class="store_info" style="margin: 0">
                            <li><i class="fa fa-home"></i>465 Park Avenue, East Hartford, CT 06108</li>
                            <li><i class="fa fa-phone"></i><span>860.282.8733</span></li>
                            <li><i class="fa fa-envelope"></i>Email: <span>Info (at) SaveInParadise.com</span></li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <form name="contactForm" id="contactForm" method="POST">
                        <h3 class="page-subheading">Contact Form</h3>
                        <br>
                        <div class="contact-form-box">
                            <div class="form-selector">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control input-lg" id="name">
                            </div>
                            <div class="form-selector">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control input-lg" id="email">
                            </div>
                            <div class="form-selector">
                                <label>Phone</label>
                                <input type="text" name="phone" class="form-control input-lg" id="phone">
                            </div>
                            <div class="form-selector">
                                <label>Message</label>
                                <textarea name="message" class="form-control input-lg" rows="4" id="message"></textarea>
                            </div>
                             <div class="form-selector">
                                 <div class="row">
                                     <div class="col-md-6">
                                         <label>Enter Captcha</label>
                                         <input class="form-control input-lg register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text" >
                                     </div>
                                     <div class="col-md-6">
                                         <img id="captcha_code" src="<?php echo base_url() . 'secureimage/securimage'; ?>" style="width: 170px">
                                        <span class="reload_captcha">
                                            <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                                        </span>
                                     </div>
                                 </div>
                            </div>
                            <hr>
                            <div class="form-selector text-right">
                                <button class="submit button"><i class="fa fa-send"></i>&nbsp; <span>Send Message</span></button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>