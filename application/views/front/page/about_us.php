
<div class="container">
    <div class="row">
        <div class="about-page">
            <div class="col-xs-12 col-sm-6">
                <h1>Our Company</h1>
                <p>Restaurant Equipment Paradise has been in business since 1998. The principals have owned
                    restaurants &amp; catering businesses for many years and understand the restaurant industry
                    thoroughly. The company operates out of a 35,000 square foot facility that stocks new
                    commercial kitchen equipment, used restaurant equipment, smallwares, grease traps, and other
                    restaurant supplies. The devoted staff at Restaurant Equipment Paradise has a vast amount of
                    industry knowledge and experience. Restaurant Equipment Paradise prides itself on providing
                    the best customer service as possible.
                </p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="sidebar-add-slider">
                    <img src="<?php echo front_asset_url() ?>images/about-us.png" alt="slide1" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="about-page">
            <div class="col-md-6">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/WgEtyxQ7bas" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h1>Our Founder</h1>
                <p>Ken Swerdlick - President &amp; CEO</p>
                <p>
                    Ken is the driving force behind Restaurant Equipment Paradise. He is a Founding Member of the
                    Cooking Up Better Lives Foundation and he is the on the Dealer Committee Head of Equipment
                    for Excell Marketing Buying Group. Ken Swerdlick uses his first-hand knowledge of operating
                    multiple restaurants as a chef/owner, as well as building contracting, to consult every one of his
                    clients from conception to completion. He designs commercial kitchens that are efficient as well
                    as economical.
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 about-page" style="margin: 0">
            <p>
                Ken’s 25 plus years’ experience in all aspects of buying &amp; selling commercial restaurant
                equipment has helped Restaurant Equipment Paradise to grow and become a business that you
                can truly rely on for all aspects of your restaurants and commercial kitchen projects. Ken is
                proud of the company he has built that has customers returning year-after-year and a loyal staff.
                He is also an extremely proud father of his hockey loving/playing son and his talented singer-
                songwriting daughter, both who are now in college. When not working, Ken proudly roots for
                the Cal Bears of UC Berkeley (his son&#39;s college) and the MTSU Blue Raiders (his daughter&#39;s
                college) as well as the New England Patriots.
            </p>
        </div>
    </div>
</div>
