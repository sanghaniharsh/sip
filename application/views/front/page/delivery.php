<div class="questions_page">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row delivery_header">
                    <div class="col-md-12">
                        <h2>Delivery Questionnaire</h2>
                        <p>Please take a moment to provide as much information possible. This will be helpful to our
                            delivery team. Our goal is to provide the best service and to have a safe working
                            environment working for our staff & yours  -  while making your delivery.</p>
                    </div>
                </div>
                <form action="<?php echo base_url().'delivery-pdf'; ?>" method="post" id="delivery_form" enctype="multipart/form-data">
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Business Information:</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Business Name:</label>
                                    <input type="text" class="form-control" name="business_name" placeholder="Business Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Street:</label>
                                    <input type="text" class="form-control" name="street" placeholder="Street">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City:</label>
                                    <input type="text" class="form-control" name="city" placeholder="City">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State:</label>
                                    <input type="text" class="form-control" name="state" placeholder="State">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zip:</label>
                                    <input type="text" class="form-control" name="zip" placeholder="Zip">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telephone:</label>
                                    <input type="text" class="form-control" name="telephone" placeholder="Telephone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contact:</label>
                                    <input type="text" class="form-control" name="contact" placeholder="Contact">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Premises Type:</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="radio radio-inline" for="radio1">
                                        <input type="radio" data-toggle="radio" name="premises_type" value="Business" id="radio1">
                                        Business 
                                    </label>
                                    <label class="radio radio-inline" for="radio2">
                                        <input type="radio" data-toggle="radio" name="premises_type" value="Residence" id="radio2">
                                        Residence 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Other:</label>
                                    <input type="text" class="form-control" name="other_premises_type" placeholder="If other please describe">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Parking</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Is there parking close to the entrance of the premises that we will be delivering your equipment?</p>
                                <div class="form-group">
                                    <label class="radio radio-inline" for="radio3">
                                        <input type="radio" data-toggle="radio" name="parking" value="Yes" id="radio3">
                                        Yes 
                                    </label>
                                    <label class="radio radio-inline" for="radio4">
                                        <input type="radio" data-toggle="radio" name="parking"  value="No" id="radio4">
                                        No 
                                    </label>
                                </div>
                                <p>Please provide parking details (ex: street parking, back alley, parking lot, etc):</p>
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" name="parking_detail"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Entry Access:</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Are there stairs (steps) to the primary entrance door?</p>
                                <div class="form-group">
                                    <label class="radio radio-inline" for="radio5">
                                        <input type="radio" data-toggle="radio" name="steps" value="Yes" id="radio5">
                                        Yes 
                                    </label>
                                    <label class="radio radio-inline" for="radio6">
                                        <input type="radio" data-toggle="radio" name="steps" value="No" id="radio6">
                                        No 
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_box_inline">
                                    <p class="inline-p">If yes, approximately how many steps?</p>
                                    <input type="text" name="steps_count" class="form-control" style="width: 15%">
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p">If yes, what type of steps?</p>
                                    <input type="text" name="steps_type" class="form-control" placeholder="(example: wood, cement, stone, other, etc.)">
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p">If yes, what is the width of the steps along with any pathway?</p>
                                    <input type="text" name="steps_inches" class="form-control" style="width: 15%;">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p>If yes, are the stairs strong & safe enough to support the weight of the equipment that is being delivered?</p>
                                    <div class="form-group">
                                        <label class="radio radio-inline" for="radio7">
                                            <input type="radio" data-toggle="radio" name="stairs_strong" value="Yes" id="radio7">
                                            Yes 
                                        </label>
                                        <label class="radio radio-inline" for="radio8">
                                            <input type="radio" data-toggle="radio" name="stairs_strong" value="No" id="radio8">
                                            No 
                                        </label>
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p>If the answer is no, please provide details or concerns:</p>
                                    <div class="form-group">
                                        <textarea class="form-control" name="stairs_details" rows="2"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Entry Way: <small>where the delivery item(s) will enter the premises</small></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_box_inline">
                                    <p class="inline-p">How wide is the door entrance?</p>
                                    <input type="text" name="door_width" class="form-control" style="width: 15%">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p">What is the height of the door entrance?</p>
                                    <input type="text" name="door_height" class="form-control" style="width: 15%">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p>Type of door?</p>
                                    <div class="form-group">
                                        <label class="radio radio-inline" for="radio9">
                                            <input type="radio" data-toggle="radio" name="door_type" value="Wood" id="radio9">
                                            Wood 
                                        </label>
                                        <label class="radio radio-inline" for="radio10">
                                            <input type="radio" data-toggle="radio" name="door_type" value="Metal" id="radio10">
                                            Metal 
                                        </label>
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p> If other, please describe</p>
                                    <div class="form-group">
                                        <textarea class="form-control" name="other_door_type" rows="2"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Inside the Premises:</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_box_inline">
                                    <p class="inline-p">What is the ceiling height?</p>
                                    <input type="text" name="ceiling_height" class="form-control" style="width: 15%">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p class="">Are there are light fixtures, sprinkler heads or other items that lower the ceiling height at a specific point? If yes, provide details</p>
                                    <input type="text" name="ceiling_details" class="form-control">
                                </div>
                                <div class="input_box_inline">
                                    <p>What type of floors are inside the premise?</p>
                                    <div class="form-group">
                                        <label class="radio radio-inline" for="radio11">
                                            <input type="radio" data-toggle="radio" name="floor_type" value="Wood" id="radio11">
                                            Wood 
                                        </label>
                                        <label class="radio radio-inline" for="radio12">
                                            <input type="radio" data-toggle="radio" name="floor_type" value="Tile" id="radio12">
                                            Tile 
                                        </label>
                                        <label class="radio radio-inline" for="radio13">
                                            <input type="radio" data-toggle="radio" name="floor_type" value="Carpet" id="radio13">
                                            Carpet 
                                        </label>
                                        <label class="radio radio-inline" for="radio14">
                                            <input type="radio" data-toggle="radio" name="floor_type" value="Other" id="radio14">
                                            Other 
                                        </label>
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p> If tile, please specific the type:</p>
                                    <div class="form-group">
                                        <textarea class="form-control" name="other_floor_type" rows="2"></textarea>
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p> Are items being delivered on the entry level floor or a different floor?</p>
                                    <div class="form-group">
                                        <input type="text" name="delivery_floor" class="form-control">
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p> If different floor please provide details:</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="different_floor" placeholder="example: basement, 2nd floor, etc">
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p> If the items are on a different floor is there an elevator, stairs, or ramp?</p>
                                    <div class="form-group">
                                        <input type="text" name="different_detail" class="form-control">
                                    </div>
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p" style="line-height: 50px;"> 
                                        If there is an elevator, please provide the dimensions of the elevator door entrance. Please include the width and height here:
                                        <input type="text" name="elevator_width" class="form-control" style="width: 10%;"> (width) by <input type="text" class="form-control" name="elevator_height" style="width: 10%;">
                                        (height). What are the interior dimensions of the elevator? <input type="text" class="form-control" name="interior_dimensions_width" style="width: 10%;"> (width)
                                        <input type="text" name="interior_dimensions_depth" class="form-control" style="width: 10%;"> (depth) <input type="text" class="form-control" name="interior_dimensions_height" style="width: 10%;">
                                        (height)
                                    </p>
                                </div>
                                <div class="input_box_inline">
                                    <p> 
                                        If there are stairs what type of stairs are there?
                                    </p>
                                    <input type="text" class="form-control" name="stairs_type" placeholder="example: wood, cement, tile, other, etc" style="margin-left: 0px;">
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p"> 
                                        What is the width of the stairs?
                                    </p>
                                    <input type="text" class="form-control" name="stairs_width" style="width: 15%;">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p class="inline-p"> 
                                        What is the height of the ceiling from the stairs to the ceiling?
                                    </p>
                                    <input type="text" class="form-control" name="stairs_ceiling_height" style="width: 15%;">
                                    <p class="inline-p">(inches)</p>
                                </div>
                                <div class="input_box_inline">
                                    <p> 
                                        Are there any platforms or turns on the stairs? If yes, please describe:
                                    </p>
                                    <div class="form-group">
                                        <textarea class="form-control" name="stairs_description" rows="2"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Area/Room of Items Being Delivered:</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_box_inline">
                                    <p>Are the items being delivered confined to a small area?</p>
                                    <input type="text" class="form-control" name="confirm_deliverd" placeholder="example: closet or tiny room">
                                </div>
                                <div class="input_box_inline">
                                    <p>If yes, please provide dimensions of the room including ceiling height:</p>
                                    <input type="text" name="room_dimensions" class="form-control">
                                </div>
                                <div class="input_box_inline">
                                    <p>Does anything need to be moved or is anything obstructing the delivery point that would prevent us from making a successful delivery?</p>
                                    <input type="text" name="obstructing_delivery" class="form-control">
                                </div>
                                <div class="input_box_inline">
                                    <p>If yes, please provide details to allow us to assess:</p>
                                    <input type="text" name="assess_details" class="form-control">
                                </div>
                                <div class="input_box_inline">
                                    <p>Does the equipment need to be delivered over a counter or bar area?</p>
                                    <input type="text" name="equipment_delivered" class="form-control">
                                </div>
                                <div class="input_box_inline">
                                    <p>If Yes, please provide how tall/wide of an obstruction and the dimensions of the work area behind:</p>
                                    <input type="text" name="dimensions_work_area" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Misc. Notes</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input_box_inline">
                                    <p>Are there any other items that we should be aware of that can help this delivery?</p>
                                    <textarea class="form-control" name="misc_note" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-border">-->
                    <!--    <div class="row">-->
                    <!--        <div class="col-md-12">-->
                    <!--            <p style="margin: 0;">Upload Images</p>-->
                    <!--            <small>You can upload maximum 4 images</small>-->
                    <!--            <p>Please provide pictures if there are stairs, sharp angle or tight wall turn or something that could make the delivery more difficult. This will help our team assess the delivery configuration.</p>-->
                    <!--            <hr>-->
                    <!--            <input id="infile" name="infile[]" type="file" multiple="true" ></input>-->
                    <!--            <label class="custom_image_validation error hide" for="captcha">You can upload maximum 4 images & max size 5MB each</label>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-12">
                                <p style="margin: 0;">Upload Images</p>
                                <small>You can upload maximum 4 images</small>
                                <p>Please provide pictures if there are stairs, sharp angle or tight wall turn or something that could make the delivery more difficult. This will help our team assess the delivery configuration.</p>
                                <hr>
                                <button type="button" class="fileup-btn btn btn-success">
                                Select file
                                <input type="file" id="upload-demo" multiple accept="image/*">
                                </button>

                                <div id="upload-demo-queue" class="queue"></div>
                                <label class="custom_image_validation2 hide" style="color:red;" for="captcha">You can upload maximum 4 images & max size 10MB each</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-border">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Captcha</label>
                                    <input class="form-control register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text" >
                                </div>
                                <label class="custom_image_validation error" for="captcha"></label>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <img id="captcha_code" src="<?php echo base_url() . 'secureimage/securimage'; ?>" style="width: 170px">
                                    <span class="reload_captcha">
                                        <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                                    </span>
                                </div>
                            </div>
                            <div class="custom_images_content"></div>
                            <div class="col-md-4 text-right">
                                <button type="button" class="btn btn-lg btn-success submit_btn" style="margin-top: 15px;">Submit</button>
                            </div>
                        </div>
                    </div>
                    <div class="ajax_loader hide">
                        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$.fileup({
    url: 'http://192.168.0.104/new_sip/delivery-pdf',
    inputID: 'upload-demo',
    queueID: 'upload-demo-queue',
    onSuccess: function(response, file_number, file) {
        //console.log(file);
        // do something
    },
    onError: function(event, file, file_number) {
        // do something
    }
});
</script>