<div class="container">
    <div class="cms_page job-feature">
        <div class="row">
            <div class="page-title">
                <h2><?php echo isset($page->title) ? $page->title : ''; ?></h2>
            </div>
           <div class="content"><?php echo isset($page->content) ? $page->content : ''; ?></div>
        </div>  
    </div>
</div>