
<div class="container">
    <div class="row">
        <div class="about-page">
            <div class="col-xs-12 col-sm-6">
                <div class="sidebar-add-slider text-center">
                    <img src="<?php echo front_asset_url() ?>images/GreaseTraps.png" alt="slide1" style="width: 75%;">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <h1>GreaseTraps</h1>
                <p>Every restaurant and commercial kitchen needs to operate at its peak. One of the most important pieces of equipment that a commercial kitchen should have is a grease trap.<br>
                    <br>
                    No business wants to temporarily shut down or lose customers due to a grease over flow, non-compliant grease trap or draining issue.
                </p>
                <p>Restaurant Equipment Paradise is an authorized dealer of Thermaco Grease Traps.  
                    We proudly offer Big Dipper Grease Traps as well as parts.  Thermaco has been 
                    manufacturing grease interceptors, solids separators, and other pre-treatment 
                    accessories that make dealing with grease and food waste easier for restaurants and commercial kitchens.
                </p>
                <p>Our expert staff can answer any questions you may have whether it’s about new grease trap products, help with warning signs or even suggested frequency of service.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="https://saveinparadise.com/search/grease-traps-" class="btn btn-primary sip_button">Click Here for Pricing</a>
        </div>
    </div>
</div>
