<div>
    <h2 style="text-align: center;">Delivery Questionnaire</h2>
</div>
<div>
    <h4>Date : <?php echo date('m/d/Y'); ?></h4>
    <h4>IP : <?php echo $ip_address; ?></h4>
</div>
<div style="border: 1px solid #000;padding: 0px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Business Information</h4>
    <p>Business Name - <b><?php echo isset($delivery_data['business_name'])? $delivery_data['business_name'] : "";?></b></p>
    <p>Street - <b><?php echo isset($delivery_data['street'])? $delivery_data['street'] : "";?></b></p>
    <p>City - <b><?php echo isset($delivery_data['city'])? $delivery_data['city'] : "";?></b></p>
    <p>State - <b><?php echo isset($delivery_data['state'])? $delivery_data['state'] : "";?></b></p>
    <p>Zip - <b><?php echo isset($delivery_data['zip'])? $delivery_data['zip'] : "";?></b></p>
    <p>Telephone - <b><?php echo isset($delivery_data['telephone'])? $delivery_data['telephone'] : "";?></b></p>
    <p>Contact - <b><?php echo isset($delivery_data['contact'])? $delivery_data['contact'] : "";?></b></p>
</div>
<div style="border: 1px solid #000;padding: 0px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Premises Type:</h4>
    <?php if(isset($delivery_data['other_premises_type']) && $delivery_data['other_premises_type']) { ?>
        <p><?php echo $delivery_data['other_premises_type']; ?></p>
    <?php } elseif(isset($delivery_data['premises_type']) && $delivery_data['premises_type']) { ?>
         <p><?php echo $delivery_data['premises_type']; ?></p>
    <?php } ?>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Parking</h4>
    <p>Is there parking close to the entrance of the premises that we will be delivering your equipment?</p>
    <b><?php echo isset($delivery_data['parking'])? $delivery_data['parking'] : "";?></b>
    <br>
    <p>Please provide parking details (ex: street parking, back alley, parking lot, etc):</p>
    <b><?php echo isset($delivery_data['parking_detail'])? $delivery_data['parking_detail'] : "";?></b>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Entry Access</h4>
    <p>Are there stairs (steps) to the primary entrance door? - <b><?php echo isset($delivery_data['steps'])? $delivery_data['steps'] : "";?></b></p>
    <p>If yes, approximately how many steps? - <b><?php echo isset($delivery_data['steps_count'])? $delivery_data['steps_count'] : "";?></b></p>
    <p>If yes, what type of steps? - <b><?php echo isset($delivery_data['steps_type'])? $delivery_data['steps_type'] : "";?></b></p>
    <p>If yes, what is the width of the steps along with any pathway? - <b><?php echo isset($delivery_data['steps_inches'])? $delivery_data['steps_inches'] : "";?> (inches)</b></p>
    <p>If yes, are the stairs strong & safe enough to support the weight of the equipment that is being delivered? - <b><?php echo isset($delivery_data['stairs_strong'])? $delivery_data['stairs_strong'] : "";?></b></p>
    <p>If the answer is no, please provide details or concerns: <b><?php echo isset($delivery_data['stairs_details'])? $delivery_data['stairs_details'] : "";?></b></p>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Entry Way</h4>
    <p>How wide is the door entrance? - <b><?php echo isset($delivery_data['door_width'])? $delivery_data['door_width'] : "";?> (inches)</b></p>
    <p>What is the height of the door entrance? - <b><?php echo isset($delivery_data['door_height'])? $delivery_data['door_height'] : "";?> (inches)</b></p>
    <?php 
        $door_type = "";
        if(isset($delivery_data['other_door_type']) && $delivery_data['other_door_type'] != "") {
             $door_type = $delivery_data['other_door_type'];   
        } else if (isset($delivery_data['door_type']) && $delivery_data['door_type'] != "") {
            $door_type = $delivery_data['door_type'];
        }
    ?>
    <p>Type of door? - <b><?php echo $door_type;?></b></p>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Inside the Premises</h4>
    <p>What is the ceiling height? - <b><?php echo isset($delivery_data['ceiling_height'])? $delivery_data['ceiling_height'] : "";?> (inches)</b></p>
    <p>Are there are light fixtures, sprinkler heads or other items that lower the ceiling height at a specific point? If yes, provide details: <b><?php echo isset($delivery_data['ceiling_details'])? $delivery_data['ceiling_details'] : "";?></b></p>
    <p>What type of floors are inside the premise? - <b><?php echo isset($delivery_data['floor_type'])? $delivery_data['floor_type'] : "";?></b></p>
    <p>If tile, please specific the type: - <b><?php echo isset($delivery_data['other_floor_type'])? $delivery_data['other_floor_type'] : "";?></b></p>
    <p>Are items being delivered on the entry level floor or a different floor? - <b><?php echo isset($delivery_data['delivery_floor'])? $delivery_data['delivery_floor'] : "";?></b></p>
    <p>If different floor please provide details: <b><?php echo isset($delivery_data['different_floor'])? $delivery_data['different_floor'] : "";?></b></p>
    <p>If the items are on a different floor is there an elevator, stairs, or ramp? <b><?php echo isset($delivery_data['different_detail'])? $delivery_data['different_detail'] : "";?></b></p>
    <p>If there is an elevator, please provide the dimensions of the elevator door entrance. Please include the width and height here:
        <b><?php echo isset($delivery_data['elevator_width'])? $delivery_data['elevator_width'] : "";?> (width)</b> by <b><?php echo isset($delivery_data['elevator_height'])? $delivery_data['elevator_height'] : "";?> (height).</b>
        What are the interior dimensions of the elevator? <b><?php echo isset($delivery_data['interior_dimensions_width'])? $delivery_data['interior_dimensions_width'] : "";?> (width)</b> <b><?php echo isset($delivery_data['interior_dimensions_depth'])? $delivery_data['interior_dimensions_depth'] : "";?> (depth)</b> <b><?php echo isset($delivery_data['interior_dimensions_height'])? $delivery_data['interior_dimensions_height'] : "";?> (height).</b>
    </p>
    <p>If there are stairs what type of stairs are there? <b><?php echo isset($delivery_data['stairs_type'])? $delivery_data['stairs_type'] : "";?></b></p>
    <p>What is the width of the stairs? <b><?php echo isset($delivery_data['stairs_width'])? $delivery_data['stairs_width'] : "";?> (inches)</b></p>
    <p>What is the height of the ceiling from the stairs to the ceiling? <b><?php echo isset($delivery_data['stairs_ceiling_height'])? $delivery_data['stairs_ceiling_height'] : "";?> (inches)</b></p>
    <p>Are there any platforms or turns on the stairs? If yes, please describe: <b><?php echo isset($delivery_data['stairs_description'])? $delivery_data['stairs_description'] : "";?></b></p>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Area/Room of Items Being Delivered</h4>
    <p>Are the items being delivered confined to a small area? - <b><?php echo isset($delivery_data['confirm_deliverd'])? $delivery_data['confirm_deliverd'] : "";?></b></p>
    <p>If yes, please provide dimensions of the room including ceiling height: - <b><?php echo isset($delivery_data['room_dimensions'])? $delivery_data['room_dimensions'] : "";?></b></p>
    <p>Does anything need to be moved or is anything obstructing the delivery point that would prevent us from making a successful delivery? - <b><?php echo isset($delivery_data['obstructing_delivery'])? $delivery_data['obstructing_delivery'] : "";?></b></p>
    <p>If yes, please provide details to allow us to assess: - <b><?php echo isset($delivery_data['assess_details'])? $delivery_data['assess_details'] : "";?></b></p>
    <p>Does the equipment need to be delivered over a counter or bar area? - <b><?php echo isset($delivery_data['equipment_delivered'])? $delivery_data['equipment_delivered'] : "";?></b></p>
    <p>If Yes, please provide how tall/wide of an obstruction and the dimensions of the work area behind: - <b><?php echo isset($delivery_data['dimensions_work_area'])? $delivery_data['dimensions_work_area'] : "";?></b></p>
</div>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">Misc. Notes</h4>
    <p>Are there any other items that we should be aware of that can help this delivery?</p>
    <b><?php echo isset($delivery_data['misc_note'])? $delivery_data['misc_note'] : "";?></b>
</div>
<?php if($attachment_imgs) { ?>
<div style="border: 1px solid #000;padding: 10px 10px;margin-bottom: 20px;">
    <h4 style="font-size: 18px;">attachment</h4>
    <?php foreach($attachment_imgs as $attachment_img) { ?>
    <img width="400" style="margin-bottom: 10px;" src="<?php echo base_url() . 'attachment/image/0/300/' . md5($attachment_img); ?>">
    <?php } ?>
</div>
<?php } ?>