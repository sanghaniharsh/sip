<link rel="stylesheet" type="text/css" href="<?php echo front_asset_url() ?>css/tabs.css" media="all">
<div class="container">
    <div class="account_page">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h2>User Account</h2>
                </div>
            </div>
        </div>
        <div class="row accounts_tabs">
            <div class="col-md-12">
                <ul class="tabs side">
                    <li><a href="#demo-tab-1" target="_self"><i class="fa fa-user"></i> Account Details</a></li>
                    <li><a href="#demo-tab-2" target="_self"><i class="fa fa-shopping-cart"></i> My Orders</a></li>  
                </ul>
                <div class="tabs-content">
                    <div id="demo-tab-1" class="tabs-panel">
                        <h1><strong>Your Basic Informations</strong></h1>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control valid" name="email" aria-required="true" aria-invalid="false">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control valid" name="email" aria-required="true" aria-invalid="false">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="text" class="form-control valid" name="email" aria-required="true" aria-invalid="false">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control valid" name="email" aria-required="true" aria-invalid="false">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="button btn-save-account"><i class="fa fa-file"></i>&nbsp; <span>Updates Details</span></button>
                            </div>
                        </div>
                    </div>
                    <div id="demo-tab-2" class="tabs-panel">
                        <h1><strong>Your Order History</strong></h1>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Description</th>
                                    <th>Amount Paid</th>
                                    <th>Date</th>
                                    <th>Order ID</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td><a href="#" class="btn btn-success">Detail</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>