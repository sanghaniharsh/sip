
<div class="container">
    <div class="about-page">
        <div class="row">
            <div class="">
                <div class="col-xs-12 col-sm-4">
                    <div class="sidebar-add-slider">
                        <img src="<?php echo front_asset_url() ?>images/about-us.png" class="imgframe2" alt="slide1" style="width: 100%;">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <h1>Let Paradise Handle Your Auction Onsite!</h1>
                    <p>
                        If you're selling or closing your restaurant, <strong>Restaurant Equipment Paradise</strong> offers 
                        full auction services with our nationally recognized and award winning auctioneer, 
                        Dan Stanavage. We have provided live auctions, online auctions & liquidations 
                        for a myriad New England businesses specializing in restaurants and all types of stores.
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <p>We offer a comprehensive <strong>ten point advertising campaign</strong> complete with customized 
                    mailing lists, email marketing, SEO enhancements, text announcements, 
                    personalized calls to specific areas, phone blast saturation, flyer distribution
                    on the streets and in-house, pictures of your equipment on our auction section on 
                    our website, auctionzip submittals, and a multitude of ads on craigslist & online. 
                </p>
                <p>
                    Our staff will assess, appraise, and direct you in your best auction or buyout options. Restaurant Equipment Paradise will hold a “Paradise Auction” for you like we have successfully completed for many business owners making your needs our priority. 
                </p>
                <p>
                    Whether you have a 500 sq. foot or 10,000 sq. foot restaurant or store, our team can help. We have managed & clerked auctions across New England & online for over 15 years. 
                </p>
                <hr>
                <h2 class="main-title" style="color: #c2c2c2;">Our goal is to acquire the money which you deserve. We work hard to get you as much as the market will bear for your equipment, ample bidders to create the excitement, and a fast payout.</h2>
                <h3 class="main-title">Call Jennifer at 860-282-8733 or email <a href="#" class="blue">purchasing@saveinparadise.com</a></h3>
            </div>
        </div>
    </div>
</div>