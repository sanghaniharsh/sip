
<div class="modal fade" id="auction_video_modal" role="dialog">
    <div class="modal-dialog">
        
    </div>
</div>
<!--<div id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">				
                <h4 class="modal-title">We just launched our new and improved website.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>New products will be added on a daily basis.</p>
                <p>Please contact us at <span class="text-blue">860-282-8733</span> if you are looking for something specific.</p>
                <p>We ship nationwide.</p>
            </div>
        </div>
    </div>
</div>-->
<div id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModallLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <form name="loginForm" id="loginForm" method="POST" action="<?php echo base_url(). 'auth/check_login' ?>">
                <div class="modal-header">				
                    <h4 class="modal-title">Login</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">				
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="<?php echo (isset($cookie_user) && ($cookie_user != '')) ? $cookie_user : '';  ?>" >
                    </div>
                    <div class="form-group">
                        <div class="clearfix">
                            <label>Password</label>
                            <a href="javascript:void();" class="pull-right text-muted forgot_password "><small>Forgot password?</small></a>
                        </div>
                        <input type="password" class="form-control" name="password" value="<?php echo (isset($cookie_password) && ($cookie_password != '')) ? $cookie_password : '';  ?>" >
                    </div>
                </div>
                <div class="modal-footer">
                    <label class=" pull-left"><input type="checkbox" <?php echo $remember_checked; ?> name="remember_me"> Remember me</label>
                    <button type="submit" name="login_btn" id="login_btn" class="btn btn-primary pull-right">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModallLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-login" style="width: 560px;">
        <div class="modal-content">
            <form name="registerForm" id="registerForm" method="POST" action="">
                <div class="modal-header">
                    <h4 class="modal-title">Register</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="firstname" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lastname" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="text" class="form-control" name="email" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" name="phone" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" id="password" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="cpassword" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Enter Captcha</label>
                                <input class="form-control register_captcha_txt" name="register_captcha" autocomplete="off" id="captcha" type="text" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <img id="captcha_code" src="<?php echo base_url() . 'secureimage/securimage'; ?>" style="width: 170px">
                                <span class="reload_captcha">
                                    <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="" name="terms">
                                    I agree to the <a href="<?php echo base_url() ?>page/user-agreement" target="_blank" class="color_blue">User Agreement</a> & <a href="<?php echo base_url().'page/privacy-policy'; ?>" class="color_blue" target="_blank">Privacy Policy</a>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="register_btn" id="register_btn" class="btn btn-primary pull-right">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModallLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <form name="forgot_password_form" id="forgot_password_form" method="POST" >
                <div class="modal-header">
                    <h4 class="modal-title">Forgot Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="text" class="form-control" name="email" autocomplete="12132" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="forgot_btn" id="forgot_btn" class="btn btn-primary pull-right">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-lg-4 text-center">
                <div class="footer-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo front_asset_url() ?>images/logo-footer.png" style="width: 100%;">
                    </a>
                </div>
                <p>Restaurant Equipment Paradise has been in business since 1998.  <p>The company operates out of a 35,000 square foot facility that stocks new commercial kitchen equipment, used restaurant equipment, smallwares, grease traps, and other restaurant supplies.</p>
                <div class="payment">
                    <ul>
                        <li><img alt="Visa" src="<?php echo front_asset_url() ?>images/visa.png" class=""></li>
                        <li><img alt="Paypal" src="<?php echo front_asset_url() ?>images/paypal.png" class=""></li>
                        <li><img alt="Discover" src="<?php echo front_asset_url() ?>images/discover.png" class=""></li>
                        <li><img alt="Master Card" src="<?php echo front_asset_url() ?>images/master-card.png" class=""></li>
                        <li><img alt="Master Card" src="<?php echo front_asset_url() ?>images/AmericanExpresspoker.jpg" class="" style="width: 52px;"></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-xs-6 col-lg-2">
                <div class="footer-links">
                    <h3 class="links-title">Quick Links</h3>
                    <div class="tabBlock" id="TabBlock-1">
                        <ul class="list-links list-unstyled">
                            <li><a href="<?php echo base_url().'about-us'; ?>">About Us</a></li>
                            <li><a href="<?php echo base_url().'blog'; ?>">Blog</a></li>
                            <li><a href="<?php echo base_url().'news'; ?>">News</a></li>
                            <li><a href="<?php echo base_url().'contact-us'; ?>">Contact Us</a></li>
                            <li><a href="<?php echo base_url().'brand'; ?>">Brands</a></li>
                            <li><a href="<?php echo base_url().'enterprise'; ?>">Enterprise Portal Login</a></li>
                            <li><a href="<?php echo base_url().'page/user-agreement'; ?>">User Agreement</a></li>
                            <li><a href="<?php echo base_url().'page/privacy-policy'; ?>">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 social col-md-6 col-xs-6 col-lg-3 text-center">
                <img src="<?php echo front_asset_url() ?>images/20-year-celebration.png" class="mobile_achieve_img" style="margin-top: 45px;width: 60%">
            </div>
            <div class="col-sm-6 social col-md-6 col-xs-12 col-lg-3">
                <ul class="inline-mode custom_social">
                    <li class="social-network fb"><a title="Connect us on Facebook" target="_blank" href="https://www.facebook.com/eRestaurantStore/"><i class="fa fa-facebook"></i></a></li>
                    
                    <li class="social-network tw"><a title="Connect us on Twitter" target="_blank" href="https://twitter.com/saveinparadise"><i class="fa fa-twitter"></i></a></li>
                   
                 
                </ul>
                <div class="country_block">
                    <hr>
                    <p>We ship & deliver throughout <br>the United States</p>
                    <img src="<?php echo front_asset_url() ?>images/US-Map.png" class="">
                </div>
            </div>
        </div>
    </div>
</footer>