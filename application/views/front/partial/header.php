<div class="top-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-6">
                <h4><i class="fa fa-truck"></i> Free Shipping on Many Items</h4>
            </div>
            <div class="col-md-3 text-right header_contact_btn">
                <a href="<?php echo base_url(); ?>contact-us">Contact Us</a>
            </div>
            <div class="col-md-2 text-right">
                <h4><a href="tel:123-456-7890" style="color: #fff;"><i class="fa fa-phone"></i> 860-282-8733</a></h4>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="container">
        <div class="header-container">
            <div class="row">
                <div class="col-sm-5 col-xs-12"> 
                    <!-- Header Logo -->
                    <div class="logo">
                        <a title="e-commerce" href="<?php echo base_url(); ?>" class="logo-color">
                            <img src="<?php echo front_asset_url() ?>images/logo-main.png">
                        </a> 
                    </div>
                    <!-- End Header Logo --> 
                </div>
                <!--support client-->
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="support-client">
                        <div class="row">
                            <div class="col-sm-12 hidden-sm header_search">
                                <div class="form-selector">
                                    <input type="text" class="form-control input-lg search_product" placeholder="Search for New Products">
                                </div>
                                <div class="product_search_result"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- top cart -->
                <div class="col-xs-12 col-sm-3 col-md-2 header_login_btns">
                    <?php if(isset($loginUser) && !empty($loginUser) ) { ?>
                    <div class="language-currency-wrapper">
                        <div class="inner-cl">
                            <div class="block block-currency">
                                <div class="item-cur"> <span>Hi, <?php echo ucfirst($loginUser['firstname']); ?> </span> <i class="fa fa-angle-down"></i></div>
                                <ul>
                                    <li> <a href="<?php echo base_url() ?>myaccount">My Account</a> </li>
                                    <li> <a href="<?php echo base_url() ?>auth/logout">Logout</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                        <button class="button do_login_class">Login</button>
                        <button class="button register_class">Signup</button>
                    <?php } ?>
                </div>
                <div class="col-lg-2 col-xs-12" style="padding-right: 0;">
                    <div class="mm-toggle-wrap">
                        <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
                    </div>
                    <div class="top-cart-contain">
                        <div class="mini-cart">
                            <div class="basket dropdown-toggle"> 
                                <a href="<?php echo base_url() ?>order/cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="cart-title">Shopping Cart</span>
                                </a>
                                <span class="cart_total_item">
                                    <?php if (isset($cart_count) && ($cart_count > 0) ) { ?>
                                        <span class="cart_badge cart_total_item"> <?php echo $cart_count; ?> </span>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="mobile-menu">
            <ul>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>category">
                            <div class="title title_font"><span class="title-text">NEW EQUIPMENT</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>small-wares">
                            <div class="title title_font"><span class="title-text">SMALLWARES</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>grease-traps">
                            <div class="title title_font"><span class="title-text">GREASE TRAPS</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>used-equipment">
                            <div class="title title_font"><span class="title-text">USED EQUIPMENT</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>big-dipper-parts">
                            <div class="title title_font"><span class="title-text">BIG DIPPER PARTS</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>restaurant-design">
                            <div class="title title_font"><span class="title-text">RESTAURANT DESIGN</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>our-work">
                            <div class="title title_font"><span class="title-text">OUR WORK</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>auction">
                            <div class="title title_font"><span class="title-text">AUCTIONS</span></div>
                        </a>
                    </div>             
                </li>
                <li class="mt-root demo_custom_link_cms">
                    <div class="mt-root-item">
                        <a href="<?php echo base_url(); ?>contact-us">
                            <div class="title title_font"><span class="title-text">CONTACT</span></div>
                        </a>
                    </div>             
                </li>
            </ul>
            <div class="mm-toggle-wrap visible-xs mobile_close_menu">
                <div class="mm-toggle"> <i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
            </div>
        </div>
    </div>
    <nav>
        <div class="container-fluid">
            <div class="row">
                <div class="mtmegamenu">
                    <ul>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>category">
                                    <div class="title title_font"><span class="title-text">NEW EQUIPMENT</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>small-wares">
                                    <div class="title title_font"><span class="title-text">SMALLWARES</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>grease-traps">
                                    <div class="title title_font"><span class="title-text">GREASE TRAPS</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>big-dipper-parts">
                                    <div class="title title_font"><span class="title-text">BIG DIPPER PARTS</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>used-equipment">
                                    <div class="title title_font"><span class="title-text">USED EQUIPMENT</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>restaurant-design">
                                    <div class="title title_font"><span class="title-text">RESTAURANT DESIGN</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>our-work">
                                    <div class="title title_font"><span class="title-text">OUR WORK</span></div>
                                </a>
                            </div>             
                        </li>
                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>auction">
                                    <div class="title title_font"><span class="title-text">AUCTIONS</span></div>
                                </a>
                            </div>             
                        </li>
<!--                        <li class="mt-root demo_custom_link_cms">
                            <div class="mt-root-item">
                                <a href="<?php echo base_url(); ?>contact-us">
                                    <div class="title title_font"><span class="title-text">CONTACT</span></div>
                                </a>
                            </div>             
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
