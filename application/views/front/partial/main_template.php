<!--header start-->
    <?php echo isset($header) ? $header : ''; ?>
<!--header end-->

<!--Content start-->
    <div class="full_content">
        <?php echo isset($content) ? $content : ''; ?>
    </div>
<!--Content end-->

<!--footer start-->
    <?php echo isset($footer) ? $footer : ''; ?>
<!--footer end-->
