<?php if(isset($cart_data) && !empty($cart_data) ) { ?>
        <h5 class="added_in_text"><i class="fa fa-check"></i> Added to your cart</h5>
        <button type="button" data-id="<?php echo isset($cart_data->id) && $cart_data->id != '' ? encreptIt($cart_data->id) : ''; ?>" class="btn btn-default delete_cart_item"  title="Remove Cart" type="button">
            <span><i class="fa fa-remove"></i> Remove from Cart</span>
        </button>
        <a href="<?php echo base_url() ?>order/cart" class="btn btn-default">
            <i class="fa fa-shopping-cart"></i> View Cart
        </a>

<?php } else {  ?>

        <form action="" method="post" class="add-cart-frm">
            <div class="cart-plus-minus" style="display:<?php echo isset($product->type) && ($product->type == 1) ? 'none;': '' ?>">
                <label for="qty">Quantity:</label>
                <div class="numbers-row">
                    <div onClick="var result = document.getElementById('qty'); var qty = result.value; if (!isNaN(qty) & amp; & amp; qty & gt; 0) result.value--; return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                    <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="quantity">
                    <div onClick="var result = document.getElementById('qty');
                        var qty = result.value;
                        if (!isNaN(qty))
                            result.value++;
                        return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                </div>
            </div>
            <input type="hidden" name="product_id" value="<?php echo isset($product_id) ? encreptIt($product_id) : ''; ?>">
            <button type="submit" class="button pro-add-to-cart add-to-cart-btn" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
        </form>
<?php } ?>
