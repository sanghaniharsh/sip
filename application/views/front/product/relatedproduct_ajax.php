<?php if (isset($product) && !empty($product)) { ?>
        <div class="page-header-wrapper">
            <div class="page-header text-center wow fadeInUp">
                <h2 style="margin-bottom: 35px;">Related <span class="text-main">Products</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            </div>
        </div>
        <div class="row " >

        <?php foreach ($product as $row) { ?>
                <div class="col-md-3 col-xs-6">
                    <div class="product-item">
                        <div class="item-inner">
                            <div class="product-thumbnail">
                                <?php 
                                $images[0] =  '';
                                if (isset($row->product_images) && $row->product_images != "") {
                                    $images = explode(',',$row->product_images);
                                }
                                ?>
                                <div class="pr-img-area">
                                    <figure> 
                                        <a href="<?php echo base_url();?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>">
                                            <img class="first-img" src="<?php echo base_url().'attachment/image/0/176/'.md5($images[0]); ?>" alt="">
                                        </a>
                                    </figure>
                                    <a href="<?php echo base_url();?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>" class="add-to-cart-mt"> 
                                        <i class="fa fa-shopping-cart"></i><span> View details</span> 
                                    </a>
                                </div>
                            </div>
                            <div class="item-info">
                                <div class="item-title"> 
                                    <a title="<?php echo (isset($row->name) && $row->name != '') ? $row->name : ''; ?>" href="<?php echo base_url();?>product/<?php echo isset($row->slug) && $row->slug != '' ? $row->slug : ''; ?>"><?php echo (isset($row->name) && $row->name != '') ? $row->name : ''; ?> </a> 
                                </div>
                                <div class="item-price">
                                    <?php if (isset($row->call_for_price) && ($row->call_for_price == '1')) { ?>
                                    <h6 class="call_for_price">Call for discounted price</h6>
                                    <?php } else { ?>
                                        <p class="special-price">
                                            <!--<span class="price">$<?php echo (isset($row->price) && $row->price != '') ? $row->price : ''; ?></span>-->
                                            <span class="price">$<?php echo isset($row->price) ? display_amount($row->price) : 0; ?></span>
                                        </p>
                                        <?php if (isset($row->list_price) && $row->list_price > 0) { ?>
                                            <p class="old-price">
                                                <span class="price">$<?php echo isset($row->list_price) ? display_amount($row->list_price) : 0; ?></span>
                                                <!--<span class="price">$<?php echo (isset($row->list_price) && $row->list_price != '') ? $row->list_price : ''; ?></span>-->
                                            </p>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php } ?>       
    </div>
<?php } ?>