<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="home"> <a title="Go to Home Page" href="#">Home</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 1</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 2</a><span>»</span></li>
                    <li class=""> <a title="Go to Home Page" href="#">Category 3</a><span>»</span></li>
                    <li class="category13"><strong>Cooking Performance Group</strong></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="product-name">
                <h1><?php echo (isset($product->name) && $product->name != '') ? $product->name : ''; ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-main">
            <div class="product-view-area">
                <div class="product-big-image col-xs-12 col-sm-8 col-lg-8 col-md-8">
                    <?php 
                    $images[0] =  '';
                    if(isset($product->product_images) && $product->product_images != "") {
                        $images = explode(',',$product->product_images);
                    }
                    ?>
                    <div class="large-image"> 
                        <a href="<?php echo base_url().'attachment/image/0/394/'.md5($images[0]); ?>" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> 
                            <img class="zoom-img" src="<?php echo base_url().'attachment/image/550/394/'.md5($images[0]); ?>" alt="products"> 
                        </a> 
                    </div>
                    <div class="flexslider flexslider-thumb">
                        <ul class="previews-list slides">
                            <?php if(isset($images) && !empty($images)) { ?>
                                <?php foreach($images as $image) { ?>
                                        <li>
                                            <a href='<?php echo base_url().'attachment/image/1000/776/'.md5($image);?>' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '<?php echo base_url().'attachment/image/550/394/'.md5($image);?>' ">
                                                <img src="<?php echo base_url().'attachment/image/89/89/'.md5($image);?>" alt = "Thumbnail 2"/>
                                            </a>
                                        </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 col-md-4">
                    <div class="product-details-area">
                        <div class="price-box">
                            <p class="special-price"> <span class="price-label">Special Price</span> <span class="price">$<?php echo (isset($product->price) && $product->price != '') ? $product->price : ''; ?></span> </p>
                            <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price">$<?php echo (isset($product->list_price) && $product->list_price != '') ? $product->list_price : ''; ?> </span> </p>
                        </div>
                        <div class="short-description">
                            <h2 class="text-center">We accept all types of payments.</h2>
                            <div class="payment">
                                <ul>
                                    <li><a href="#"><img title="Visa" alt="Visa" src="<?php echo front_asset_url() ?>images/visa.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Paypal" alt="Paypal" src="<?php echo front_asset_url() ?>images/paypal.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Discover" alt="Discover" src="<?php echo front_asset_url() ?>images/discover.png" class="grayscale"></a></li>
                                    <li><a href="#"><img title="Master Card" alt="Master Card" src="<?php echo front_asset_url() ?>images/master-card.png" class="grayscale"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-variation add_to_cart_custom_block  add_to_cart_block" data-id="<?php echo isset($product->id) ? encreptIt($product->id) :'';?>">
                            <?php if(isset($cart_data) && !empty($cart_data) ) { ?>

                                    <button type="button" data-id="<?php echo isset($cart_data->id) && $cart_data->id != '' ? encreptIt($cart_data->id) : ''; ?>" class="btn btn-danger delete_cart_item"  title="Remove Cart" type="button"><span><i class="fa fa-remove"></i> Remove Cart</span></button>

                            <?php } else {  ?>

                                    <form action="" method="post" class="add-cart-frm">
                                        <div class="cart-plus-minus">
                                            <label for="qty">Quantity:</label>
                                            <div class="numbers-row">
                                                <div onClick="var result = document.getElementById('qty'); var qty = result.value; if (!isNaN(qty) & amp; & amp; qty & gt; 0) result.value--; return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                                                <input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty" name="quantity">
                                                <div onClick="var result = document.getElementById('qty');
                                                    var qty = result.value;
                                                    if (!isNaN(qty))
                                                        result.value++;
                                                    return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="product_id" value="<?php echo isset($product->id) && $product->id != '' ? encreptIt($product->id) : ''; ?>">
                                        <button type="submit" class="button pro-add-to-cart add-to-cart-btn" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                    </form>
                            <?php } ?>
                        </div>
                        <?php if(isset($product->is_free_shipping) && $product->is_free_shipping != '' && $product->is_free_shipping == '1') { ?>
                            <div class="row support-client sidebar_free_shipping">
                                <div class="col-md-12 col-sm-10">
                                    <div class="box-container free-shipping">
                                        <div class="box-inner">
                                            <h2>Free Shipping in this item</h2>
                                            <p>Should be arrive on monday</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-overview-tab wow fadeInUp">

            <div class="col-xs-12">
                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"> 
                        <a href="#description" data-toggle="tab"> Description </a> 
                    </li>
                    <li>
                        <a href="#specification" data-toggle="tab">Specifications</a>
                    </li>
                    <li> 
                        <a href="#warranty" data-toggle="tab">Warranty</a> 
                    </li>
                </ul>
                <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="description">
                        <p class="text-justify"><?php echo (isset($product->description) && $product->description != '') ? $product->description : ''; ?></p>
                    </div>
                    <div class="tab-pane fade" id="specification">
                        <p class="text-justify"><?php echo (isset($product->specification) && $product->specification != '') ? $product->specification : ''; ?></p>
                    </div>
                    <div class="tab-pane fade" id="warranty">
                        <p class="text-justify"><?php echo (isset($product->warranty) && $product->warranty != '') ? $product->warranty : ''; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="related-product-area" style="padding-bottom: 50px;">
            <div class="page-header-wrapper">
                <div class="page-header text-center wow fadeInUp">
                    <h2 style="margin-bottom: 35px;">Related <span class="text-main">Products</span></h2>
                    <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                </div>
            </div>
            <div class="row relatedproduct" data-id="<?php echo (isset($product->category_id) && $product->category_id != '') ? encreptIt($product->category_id) : ''; ?>">
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    setTimeout(function() {
        related_Product();
    }, 1000);
</script>