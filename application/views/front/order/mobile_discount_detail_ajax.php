<tr>
    <td>
        <div class="row hide promotionalbox">
            <div class="col-xs-8">
                <input type="text" name="promotional_code" class="form-control input-lg discount_code_txt" placeholder="Discount Code" value="<?php echo (isset($promotional_code) && $promotional_code != '') ? $promotional_code :''; ?>">
            </div>
            <div class="col-xs-4">
                <button class="button promotional_code_btn"><span>Apply Code</span></button>
            </div>
        </div>
        <div class="row applied_box promotional_appliedbox">
            <div class="col-xs-10">
                <h3><?php echo (isset($promotional_code) && $promotional_code != '') ? $promotional_code :'';  ?><span><i class="fa fa-check-circle"></i></span></h3>
            </div>
            <div class="col-xs-2">
                <button class="button cancel_code remove_promotional_code"><span>X</span></button>
            </div>
        </div>
    </td>
</tr>
<tr>
    <th >Sub Total</td>
    <td class="text-right sub_total_text">$<?php echo (isset($sub_total) && $sub_total != '') ? $sub_total :'';  ?> </td>
</tr>
<tr class="">
    <th >Tax</td>
    <td class="text-right total_tax">$<?php echo (isset($tax) && $tax != '') ? $tax :'';  ?> </td>
</tr>
<tr class="discountAmount">
    <th>Discount Amount</td>
    <td class="text-right">$<?php echo (isset($total_discount) && $total_discount != '') ? $total_discount :'';  ?></td>
</tr>
<tr class="">
    <th>Shipping Amount</td>
    <td class="text-right"><?php echo ($shipping_price > 0) ? "$" . number_format($shipping_price, 2) : "Free" ; ?> </td>
</tr>
<tr>
    <th><strong>Total</strong></td>
    <td class="text-right">$<strong class="total_amount"><?php echo (isset($total_amount) && $total_amount != '') ? $total_amount :'';  ?> </strong></td>
</tr>