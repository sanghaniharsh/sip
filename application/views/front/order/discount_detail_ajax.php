<tr>
    <td colspan="2" rowspan="5" class="calculate_discount">
        <div class="row hide promotionalbox">
            <div class="col-lg-7 col-xs-7">
                <input type="text" name="promotional_code" class="form-control input-lg discount_code_txt" placeholder="Discount Code" value="<?php echo (isset($promotional_code) && $promotional_code != '') ? $promotional_code :''; ?>">
            </div>
            <div class="col-lg-3 col-xs-5">
                <button class="button promotional_code_btn"><span>Apply Code</span></button>
            </div>
        </div>
        <div class="row applied_box promotional_appliedbox">
            <div class="col-lg-7 col-xs-7">
                <h3><?php echo (isset($promotional_code) && $promotional_code != '') ? $promotional_code :'';  ?><span><i class="fa fa-check-circle"></i></span></h3>
            </div>
            <div class="col-lg-3 col-xs-5">
                <button class="button cancel_code remove_promotional_code"><span>X</span></button>
            </div>
        </div>
    </td>
    <td colspan="3">Sub Total</td>
    <td colspan="2" class="text-right sub_total_text in_final_total" data-amount="<?php echo ($sub_total > 0) ? $sub_total : 0; ?>">$<?php echo (isset($sub_total) && $sub_total != '') ? $sub_total :'';  ?> </td>
</tr>
<tr class="">
<td colspan="3">Tax</td>
<td colspan="2" class="text-right total_tax in_final_total" data-amount="<?php echo $tax; ?>">$<?php echo (isset($tax) && $tax != '') ? $tax :'';  ?> </td>
</tr>
<tr class="discountAmount">
    <td colspan="3"><strong>Discount Amount</strong></td>
    <td colspan="2" class="text-right in_final_total" data-amount="<?php echo (isset($total_discount) && $total_discount != '') ? '-'.$total_discount : 0.00;  ?>"><strong class="" >$<?php echo (isset($total_discount) && $total_discount != '') ? $total_discount :'';  ?> </strong></td>
</tr>
<tr>
    <td colspan="3" class="shipping_calculator">
        <span class="calc_title">Shipping Amount</span>
        <span class="calc_input <?php echo isset($ups_shipping_class) ? $ups_shipping_class : ''; ?>">
            <input type="text" name="shipping_zipcoe" class="form-control" placeholder="Enter Your ZIP">
            <a href="javascript:void(0)" class="calculate_shipping_btn">Calculate</a>
        </span>
    </td>
    <!--<td colspan="3" class="text-right"><?php echo ($shipping_price > 0) ? "$" . number_format($shipping_price, 2) : "Free" ; ?> </td>-->
    <td colspan="2" class="text-right">$<span class="total_shipping_amount in_final_total" data-amount="<?php echo ($shipping_price > 0) ? number_format($shipping_price, 2) : "0.00" ; ?>"><?php echo ($shipping_price > 0) ? number_format($shipping_price, 2) : "0.00" ; ?></span></td>
</tr>
<tr>
    <td colspan="3"><strong>Total</strong></td>
    <td colspan="2" class="text-right">$<strong class="total_amount"><?php echo (isset($total_amount) && $total_amount != '') ? $total_amount :'';  ?> </strong></td>
</tr>