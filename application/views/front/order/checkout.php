<div class="main container checkout-page">
    <div class="col-main">
        <div class="row">
            <form name="checkout" class="checkoutForm" id="checkoutForm" action="<?php echo base_url().'order/commit'; ?>" method="POST">
                <div class="col-main col-sm-8">
                    <div class="page-title">
                        <h2>Checkout</h2>
                    </div>
                    <hr>
                    <div class="row spam_info">
                        <div class="col-md-2 col-xs-3 text-center">
                            <img src="<?php echo front_asset_url() ?>images/spam_icon.png">
                        </div>
                        <div class="col-md-10 col-xs-9">
                            <h4>We do not sell, trade or loan your information.<br> All data is kept confidential.</h4>
                        </div>
                    </div>
                    <hr>
                    <?php if(isset($this->loginUser) && !empty($this->loginUser) ) { ?>
                        <h2 class="user_loggedin_title">
                            <span>
                                <strong>Welcome <?php echo ucfirst($loginUser['firstname']); ?>,</strong>
                            </span> 
                        </h2>
                        <h4 class="checkout-sep">General Information</h4>
                        <div class="box-border logged_in_checkout">
                            <p style="margin: 0;">
                                <strong>Name :</strong> 
                                <?php echo ( isset($user_details->firstname) && isset($user_details->lastname) ) ? ucfirst($user_details->firstname) ." ". ucfirst($user_details->lastname) : '' ?>
                            </p>
                            <p style="margin: 0;">
                                <strong>Email :</strong> 
                                <?php echo ( isset($user_details->email)) ? $user_details->email : '' ?>
                            </p>
                            <p style="margin: 0;">
                                <strong>Phone :</strong> 
                                <?php echo ( isset($user_details->phone)) ? $user_details->phone : '' ?>
                            </p>
                            <a href="<?php echo base_url() ?>myaccount" class="but_small5"><i class="fa fa-cog"></i> Edit Details</a>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="checkout-sep">General Information</h4>
                            </div>
                            <div class="col-md-6 text-right checkout_login_btn">
                                <button class="button do_login_class"> Have an existing account? Login</button>
                            </div>
                        </div>
                        <div class="box-border">
                            <ul>
                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="first_name" class="required">First Name <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" class="input form-control" name="first_name">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="last_name" class="required">Last Name <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" name="last_name" class="input form-control">
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="" class="required">Email Address <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" class="input form-control" name="email">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="" class="required">Phone Number <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" name="phone" class="input form-control">
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h4 class="checkout-sep">Create Password</h4>
                        <div class="box-border">
                            <ul>
                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="first_name" class="required">Password <span class="field_required">*</span></label>
                                        <input type="password" autocomplete="adsdad" class="input form-control" name="password" id="password" >
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="last_name" class="required">Confirm Password <span class="field_required">*</span></label>
                                        <input type="password" autocomplete="adsdad" name="cpassword" class="input form-control">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>

                    <?php if (isset($user_details) && ($user_details->street_address != '' || $user_details->zipcode != 0)) { ?>
                        <h4 class="checkout-sep">Billing Informations</h4>
                        <div class="box-border logged_in_checkout">
                            <p><strong>Your Billing Address</strong></p>
                            <p style="margin: 0;"><?php echo isset($user_details->street_address) ? $user_details->street_address : '' ?></p>
                            <p style="margin: 0;"><?php echo isset($user_details->state) ? $user_details->state : '' ?>, <?php echo isset($user_details->city) ? $user_details->city : '' ?>, <?php echo isset($user_details->zipcode) ? $user_details->zipcode : '' ?></p>
                            <a href="<?php echo base_url() ?>myaccount" class="but_small5"><i class="fa fa-cog"></i> Edit Details</a>
                        </div>
                    <?php } else { ?>
                        <hr>
                        <h4 class="checkout-sep">Billing Informations</h4>
                        <div class="form-group"> 
                            <input type="checkbox" name="same_address" id="same_address">
                            <label for="same_address">Same as Shipping Address</label>
                        </div>
                        
                        <div class="box-border">
                            <ul>
                                <li class="row">
                                    <div class="col-sm-12">
                                        <label for="" class="required">Street Address <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" class="input form-control" name="address">
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="col-sm-4">
                                        <label for="" class="required">Zip Code <span class="field_required">*</span></label>
                                        <input type="text" autocomplete="adsdad" class="input form-control search-zipcode" name="zipcode">
                                        <div class="location_result"></div>
                                    </div>
                                    <div class="col-sm-8">
                                        <label for="" class="required">City & State</label>
                                        <input type="text" name="city" class="input form-control city_state" readonly="">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                        
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="checkout-sep">Shipping Informations</h4>
                        </div>
                        <div class="col-sm-7 text-right">
                            <?php if(isset($display_shipping_details) && $display_shipping_details == 1) { ?>
                                <i class="fa fa-truck fa-2x" > </i> <span class="display_popup"> Contact us for the best shipping price.</span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-border">
                        <ul>
                            <li class="row">
                                <div class="col-sm-12">
                                    <label for="" class="required">Shipping Street Address <span class="field_required">*</span></label>
                                    <input type="text" autocomplete="adsdad" class="input form-control" name="shipping_address">
                                </div>
                            </li>
                            <li class="row">
                                <?php if(isset($weights) && $weights > 140){ ?>
                                    <div class="col-sm-4">
                                        <label for="" class="required">Address Type <span class="field_required">*</span></label>
                                        <select class="form-control" name="address_type">
                                            <option value="0" <?php echo (isset($address_type) && ($address_type == 0)) ? 'selected' : ''; ?>>Commercial</option>
                                            <option value="1" <?php echo (isset($address_type) && ($address_type == 1)) ? 'selected' : ''; ?>>Residential</option>
                                        </select>
                                    </div>
                                <?php } ?>
                                <div class="col-sm-3">
                                    <label for="" class="required">Shipping Zip Code <span class="field_required">*</span></label>
                                    <input type="text" autocomplete="adsdad" class="input form-control shipping-search-zipcode" name="shipping_zipcode" value="<?php echo isset($shipping_zipcode) ? $shipping_zipcode : ''; ?>">
                                    <div class="shipping_location_result"></div>
                                </div>
                                <div class="col-sm-5">
                                    <label for="" class="required">City & State</label>
                                    <input type="text" name="shipping_city" class="input form-control shipping_city_state" readonly="" value="<?php echo isset($shipping_city_state) ? $shipping_city_state : ''; ?>">
                                </div>
                            </li>
                        </ul>
                    </div>  

                    <div class="box-border">
                        <ul>
                            <li class="row">
                                <div class="col-sm-6">
                                    <label for="" class="required">Enter Captcha <span class="field_required">*</span></label>
                                    <input class="form-control order_captcha_txt" name="checkout_captcha" autocomplete="adsdad" id="captcha" type="text" >
                                </div>
                                <div class="col-sm-6" style="margin-top: 20px;">
                                    <img id="captcha_code" src="<?php echo base_url().'secureimage/securimage'; ?>" style="width: 170px">
                                    <span class="reload_captcha">
                                        <img src="<?php echo base_url(); ?>assets/front-side/images/refresh.png" height="20" width="20" style="margin-left: 10px;">
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="box-border">
                        <ul>
                            <li class="row" style="margin: 0;">
                                <div class="col-sm-12 text-center">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="" name="terms">
                                        I agree to the <a href="<?php echo base_url() ?>page/user-agreement" target="_blank" class="color_blue">User/Purchase Agreement</a> & <a href="<?php echo base_url() ?>page/privacy-policy" class="color_blue" target="_blank">Privacy Policy</a>
                                    </label>
                                    <div class="checkbox_error"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <aside class="right sidebar col-sm-4 col-xs-12">
                    <div class="sidebar-checkout">
                        <div class="sidebar-bar-title">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span>
                                    Order Summary
                                </span>
                            </h3>
                        </div>
                        <div class="block-content order_summary_content">
                            <?php echo isset($order_summary_view) ? $order_summary_view : '' ?>
                        </div>
                    </div>
                </aside>
            </form>
        </div>
    </div>
</div>
