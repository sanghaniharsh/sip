<div class="main container">
    <div class="col-main thank-you-page">
        <div class="row">
            <div class="main-heading">
                <h1>Payment Successful</h1>
                <hr>
                <h2>Hello <span><?php echo (isset($user_name)) ? trim(ucfirst($user_name)) : ''; ?></span></h2>
            </div>
        </div>
        <div class="row sales_info">
            <div class="col-md-12 text-center successfull_block">
                <i class="fa fa-check"></i>
                <h3>
                    We have received your order, Thank you
                </h3>
                <h4>
                    Check your email for order information.
                </h4>
                <hr>
                <a href="<?php echo base_url(); ?>" class="btn btn-default">Back to Home</a>
            </div>
        </div>
    </div>
</div>