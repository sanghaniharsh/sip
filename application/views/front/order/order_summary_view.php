<ul class="list-group mb-3">

    <li class="list-group-item d-flex justify-content-between checkout_product">

        <div>

            <h6>

                Total Item

            </h6>

            <small>

                <a href="<?php echo base_url() . 'order/cart'; ?>">View Cart</a>

            </small>

        </div>

        <span class="price">

            <?php echo isset($cart_items) && !empty($cart_items) ? count($cart_items) : 0; ?>

        </span>

    </li>

    <?php $total = 0; ?>

    <li class="list-group-item shipping_total d-flex justify-content-between">

        <h6>Sub Total</h6>

        <span class="price">

            <strong>

                <?php echo ($sub_total > 0) ? "$" . display_amount($sub_total) : "Free"; ?>

            </strong>

        </span>

    </li>

    <li class="list-group-item shipping_total d-flex justify-content-between">

        <h6>Tax</h6>

        <span class="price">

            <strong>

                <?php echo (isset($total_tax) && $total_tax > 0) ? "$" . display_amount($total_tax) : "$0.00"; ?>

            </strong>

        </span>

    </li>

    <?php if (isset($total_discount) && ($total_discount > 0)) { ?>

        <li class="list-group-item d-flex justify-content-between bg-light">

            <div class="text-success">

                <h6>Promo code</h6>

                <small>

                    <?php echo isset($promo_code) && $promo_code != '' ? $promo_code : ''; ?>

                    <input type="hidden" name="promocode" value="<?php echo isset($promo_code) && $promo_code != '' ? $promo_code : ''; ?>">

                </small>

            </div>

            <span class="text-success price">

                - $<?php echo '-$' . isset($total_discount) ? $total_discount : 0; ?>

            </span>

        </li>

    <?php } ?>

    <li class="list-group-item shipping_total d-flex justify-content-between">

        <h6>Shipping Total</h6>

        <span class="price">

            <strong>

                <?php echo ($shipping_total > 0) ? "$" . display_amount($shipping_total) : "Free"; ?>

            </strong>

        </span>

    </li>

    <li class="list-group-item checkout_total d-flex justify-content-between">

        <h6>Total (USD)</h6>

        <span class="price">

            <strong>

                $<?php echo isset($total_amount) ? display_amount($total_amount + $shipping_total) : 0; ?>

            </strong>

        </span>

    </li>

    <li class="list-group-item text-center" style="padding-top: 20px;">

        <button type="submit" class="btn btn-success payment_btn">

            Proceed to Payment

        </button>

        <button type="submit" class="btn btn-default payment_btn">

            <img src="<?php echo front_asset_url() ?>images/checkout_logo.png">

        </button>

    </li>

</ul>