<section class="full-detail-description full-detail gray-bg" style="padding-top: 80px;padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" name="reset_password_form" id="reset_password_form" method="POST">
                            <div class="row no-mrg">
                                <div class="col-md-12 col-sm-12">
                                    <label>Password</label>
                                    <input class="form-control reset-password" placeholder="Password" name="password" id="password" type="password" autocomplete="off">
                                </div>
                                <div class="col-md-12 col-sm-12" style="margin-top: 15px;">
                                    <label>Confirm Password</label>
                                    <input class="form-control" placeholder="Confirm Password" name="cpassword" type="password" autocomplete="off"> 
                                </div>
                            </div>
                            <hr>
                            <div class="row no-mrg">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-login text-center btn-success" name="reset_submit" id="reset_submit" type="submit">Update Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>