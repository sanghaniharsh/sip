<div class="container">
    <div class="row no-page-found">
        <div class="col-md-12 text-center no-page-found learn_features">
            <img src="<?php echo front_asset_url() ?>images/error-img.png">
            <h4>Ohh.....You Requested the page that is no longer There.</h4>
            <hr>
            <a href="#" class="btn btn-default">Back to Home</a>
        </div>
    </div>
</div>