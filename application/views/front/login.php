<div class="container">
    <div class="row mobile_no_margin">
        <div class="col-md-4 col-md-offset-4 mobile_no_padding">
            <div class="login-panel panel panel-default" style="background: #fff;">
                <div class="panel-heading">
                    <h3 class="panel-title">Login here</h3></div>
                <div class="panel-body">

                    <form role="form" name="login_frm" id="login_frm" method="POST" style="margin-top: 30px;">
                        <fieldset>
                            <div class="form-group" style="margin-bottom: 0;">
                                <label>Email Address</label>
                                <input class="form-control" name="l_email" type="text" value="<?php echo isset($user_data[0]) ? $user_data[0] : '' ?>" autocomplete="off"> > 
                                <?php echo form_error('l_email', '<span for="r_firstname" class="form_error">', '</span>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="form-control" name="l_password" type="password" value="<?php echo isset($user_data[1]) ? $user_data[1] : '' ?>" autocomplete="off">> 
                                <?php echo form_error('l_password', '<span for="l_password" class="form_error">', '</span>'); ?>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="l_remember" id="l_remember" type="checkbox">Remember Me
                                </label>
                                <label class="pull-right">
                                    <a href="<?php echo base_url().'forgot-password'; ?>">Forgot password?</a>
                                </label>
                            </div>
                            <hr>
                            <button class="btn btn-login" name="l_submit" id="l_submit" type="submit">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
<!--        <div class="col-md-5">
            <div class="login-panel panel panel-default" style="background: #fff;">
                <div class="panel-heading">
                    <h3 class="panel-title">You don't have an account?</h3>
                    <p style="margin: 0">Do you have roles looking to fill?</p>
                    <p style="margin: 0">Signup today!</p>
                </div>
                <div class="panel-body">
                    <form role="form" style="margin-top: 30px;">
                        <ul class="signup_list">
                            <li><i class="fa fa-check"></i> Free to signup</li>
                            <li><i class="fa fa-check"></i> You can post job</li>
                            <li><i class="fa fa-check"></i> Get leads for each job you posted</li>
                            <li><i class="fa fa-check"></i> Get notified for each application</li>
                            <li><i class="fa fa-check"></i> Add unlimited roles and positions</li>
                        </ul>
                        <a href="sales.php" class="btn btn-login">Signup now</a>
                    </form>
                </div>
            </div>
        </div>-->
<!--        <div class="col-md-3 pull-right">
            <div class="sidebar right-sidebar">
                <div class="side-widget">
                    <h2 class="side-widget-title">Advertisment</h2>
                    <div class="widget-text padd-0">
                        <div class="ad-banner"><img src="<?php echo base_url() ?>assets/front-side/img/ad-banner.jpg" class="img-responsive" alt=""></div>
                    </div>
                </div>
                <div class="side-widget">
                    <h2 class="side-widget-title">Advertisment</h2>
                    <div class="widget-text padd-0">
                        <div class="ad-banner"><img src="<?php echo base_url() ?>assets/front-side/img/ad-banner.jpg" class="img-responsive" alt=""></div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>