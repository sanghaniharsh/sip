<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">
                <ul>
                    <li class="">
                        <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a>
                        <span>»</span>
                    </li>
                    <li class="">
                        <a title="brand" href="<?php echo base_url(); ?>brand">Brand</a>
                        <span>»</span>
                    </li>
                    <li class="">
                        <strong><?php echo isset($slug) ? $slug :'';?></strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-sm-3 col-xs-12 search_sidebar_sticky">
            <div class="category-sidebar">
                <div class="sidebar-title">
                    <h3>Brands</h3>
                </div>
                <ul class="product-categories">
                    <?php if(isset($brand_list) && !empty($brand_list)) { ?>
                        <?php foreach($brand_list as $row) { ?>
                                <li class="cat-item">
                                    <a href= "<?php echo base_url().'brand/'.$row->slug; ?>"><?php echo isset($row->name) ? $row->name :''; ?></a>
                                </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="col-main col-sm-9">
            <div class="page-title">
                <h2>Search result title goes here</h2>
            </div>
            <div class="toolbar">
                <div class="sorter">
                    <div class="short-by">
                        <label>Sort By:</label>
                        <select>
                            <option selected="selected">Position</option>
                            <option>Name</option>
                            <option>Price</option>
                            <option>Latest</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="product-block brand_product_list">
                <div class="ajax_loader absolute">
                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    BrandUrl = base_url+'brand/get_brand_produts/<?php echo $slug ?>';
    setTimeout(function() {
        loadBrandProducts();
    }, 1000);
</script>