<div class="collestion-area">
    <div class="container">
        <div class="page-title" style="padding-top: 25px;">
            <h2>Brands we sell</h2>
        </div>
        <hr>
        <div class="row top_brands brand_list">
            <div class="ajax_loader absolute">
                <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    setTimeout(function() {
        loadBrand();
    }, 2000);
</script>