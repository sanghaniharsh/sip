<?php if (isset($brand_list) && !empty($brand_list)) { ?> 
    <?php foreach ($brand_list as $row) { ?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
            <div class="wow bounceInUp" data-wow-delay="0.2s"> 
                <div class="single-collection">
                    <a href="<?php echo base_url();?>brand/<?php echo isset($row->slug) ? $row->slug : ''; ?>">
                        <img src="<?php echo base_url().'attachment/image/145/0/'.md5($row->image); ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="row no-product-found">
        <div class="col-sm-12">
            <i class="fa fa-shopping-cart"></i>
            <h3>No brand found</h3>
        </div>
    </div>
<?php } ?>
<!--<div class="pagination-area wow fadeInUp" id="pagination">
    <?php echo isset($pagination_link) ? $pagination_link : ''; ?>
</div>-->